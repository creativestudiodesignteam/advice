<?php
include 'libs/config.php'; 

use Dao\DaoInstitucional;
$daoInstitucional = new DaoInstitucional();
use Dao\DaoCategoriaSub;
$daoCategoriaS = new DaoCategoriaSub();


foreach ($_GET as $campo => $valor) {
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}


$lang = $daoLinguagemSite->BuscarPorCod($cod);
$_SESSION['linguagem'] = $lang->getId();
$_SESSION['linguagem_prefix'] = $lang->getPrefix();


/* print_r($_SESSION['URLPG']);
exit; */ 


if(!isset($_SESSION['URLPG'])){ //na home ele desativa a sessao (null)
   header("Location:/". $_SESSION['linguagem_prefix'] . '/home');

}else{

   if(is_int($_SESSION['URLPG'])){ //se vier do menu - sempre numeros inteiros

      $m_page_ = $daoMenuSite->BuscarPorCODLing($_SESSION['URLPG'], $_SESSION['linguagem']);
      header("Location:/". $_SESSION['linguagem_prefix'] . '/'.$m_page_->getLink());

   }else{ //se vier do institucional e produtos

      $UrL = explode('-', $_SESSION['URLPG']);

      /* print_r($UrL);
      exit; */
      
      if($UrL[0]=='i'){ //institucional

         $Inst = $daoInstitucional->BuscarPorCODLing($UrL[1], $_SESSION['linguagem']);
         header("Location:/". $_SESSION['linguagem_prefix'] . '/'.$Inst->getLink());
      }

      if($UrL[0]=='p'){ //produtos
         
         $l_ink = $daoMenuSite->BuscarPorCODLing(2, $_SESSION['linguagem']);                 
         $prod_ = $daoCategoriaS->BuscarPorCODLing($UrL[1], $_SESSION['linguagem']);        
         header("Location:/". $_SESSION['linguagem_prefix'] . '/'. $l_ink->getLink().'/'.$UrL[1].'/'.URL_amigavel($prod_->getTitulo()) );
      }
   
   }

}
?>
