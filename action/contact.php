<?php 

require_once('../libs/Mail.php');

if($_POST){
    
    $email[] = array('name' => 'Anastacio Overseas', 'email' => 'overseas@anastaciooverseas.com');
    //$email[] = array('name' => 'Anastacio Overseas', 'email' => 'rodrigo@deepocean.com.br');

    $mail = new Mail( 'overseas@anastaciooverseas.com', 'Over$$Seas10', 'smtp.office365.com', 587, 'http://www.anastaciooverseas.com/new/assets/img/site-logo.png');
    $mail->fromname = 'Anastacio Overseas';
    $mail->recipient = $email;
    $mail->subject = addslashes(utf8_decode('Anastacio Overseas | Nova contato do site!'));
    $mail->body = utf8_decode('
                    <tr>
                    <td style="word-wrap:break-word; font-size:0px; padding:0px; border-collapse:collapse" align="center">
                    <div style="color: rgb(85, 85, 85); font-family: sans-serif, serif, EmojiFont; font-size: 16px; line-height: 1.5; text-align: center;">
                    <b>Dados do Contato:</b><br><br>
                    <b>Nome: '.$_POST['name'].'</b><br>
                    <b>Assunto: '.$_POST['subject'].'</b><br>
                    <b>E-mail: '.$_POST['email'].'</b><br>
                    <b>Telefone: '.$_POST['phone']. '</b><br>
                    <b>Empresa: ' . $_POST['company'] . '</b><br>
                    <b>Mensagem: '.$_POST['message'].'</b><br>
                    </div>
                    </td>
                    </tr>
                    <tr>
                    <td style="word-wrap:break-word; font-size:0px; padding:0px; border-collapse:collapse;" align="center">
                    <div style="color: rgb(85, 85, 85); font-family: sans-serif, serif, EmojiFont; font-size: 16px; line-height: 1.5; text-align: center; margin-top: 10px">
                    </div>
                    </td>
                    </tr>
            ');

    $mail->altbody = utf8_decode('Anastacio Overseas | Novo contato do site');

    $send = $mail->send();

    if($send){
        $return['response']['mensagem'] = 'Mensagem enviada com sucesso!';
        $return['response']['classe'] = 'alert-success';
        $return['response']['result'] = 'success';
        $return['response']['redirect'] = '/home';
    }else{
        $return['response']['mensagem'] = 'Não foi possivel enviar a mensagem!';
        $return['response']['classe'] = 'alert-danger';
        $return['response']['result'] = 'error';
        $return['response']['redirect'] = '';
    }

    print_r(json_encode($return));
    exit();
}