<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Meta Data -->
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<title>Home - Software, App, SaaS landing HTML Template</title>

	<?php include('includes/head.php');?>


</head>

<body id="home-version-1" class="home-color-two" data-style="default">

<a href="#main_content" data-type="section-switch" class="return-to-top">
		<i class="fa fa-chevron-up"></i>
	</a>

	<!--=========================-->
	<!--=        Loader         =-->
	<!--=========================-->
	<?php include('includes/loader.php');?>
	<!-- /.page-loader -->

	<div id="main_content">


		<!--=========================-->
		<!--=        Navbar         =-->
		<!--=========================-->
		<?php include('includes/header-interna.php');?>
		<!-- /.site-header -->

		<!--==========================-->
		<!--=         Banner         =-->
		<!--==========================-->
		<section class="page-banner">
			<div class="container">
				<div class="page-title-wrapper">
					<h1 class="page-title">Nossos clientes</h1>

					<ul class="bradcurmed">
						<li><a href="index.php" rel="noopener noreferrer">Home</a></li>
						<li>Clientes</li>
					</ul>
				</div>
				<!-- /.page-title-wrapper -->
			</div>
			<!-- /.container -->


			<svg class="circle" data-parallax='{"x" : -200}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
                <path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
            </svg>

			<ul class="animate-ball">
				<li class="ball"></li>
				<li class="ball"></li>
				<li class="ball"></li>
				<li class="ball"></li>
				<li class="ball"></li>
			</ul>
		</section>
		<!-- /.page-banner -->

	<div id="main_content">

        <!--===============================-->
		<!--=         Testimonial         =-->
		<!--===============================-->
		
        <!--===========================-->
		<!--=         Feature         =-->
		<!--===========================-->
		<section class="featured-two-service">
			<div class="container">
				<div class="section-title text-center">
					<h3 class="sub-title wow pixFadeUp">Conheça</h3>
					<h2 class="title wow pixFadeUp" data-wow-delay="0.3s">Nossos parceiros</h2>
				</div>
				<!-- /.section-title -->

				<div class="row">
					<div class="col-lg-2 col-md-6">
						<div class="pixsass-icon-box-wrapper style-two wow pixFadeUp" data-wow-delay="0.3s">
							<div class="iapp-icon-box-icon">
								<img src="assets/img/01.jpg" alt="">
							</div>
							<!-- <div class="pixsass-icon-box-content">
								<h3 class="pixsass-icon-box-title"><a href="#">App Development</a></h3>
								<p>
									The full monty do one nancy boy<br> say gutted mate cockup Why at <br> public school.!
								</p>
							</div> -->
						</div>
						<!-- /.pixsass-box style-two wow pixFadeUp -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->

					<div class="col-lg-2 col-md-6">
						<div class="pixsass-icon-box-wrapper style-two wow pixFadeUp" data-wow-delay="0.5s">
							<div class="iapp-icon-box-icon">
                                <img src="assets/img/02.jpg" alt="">
							</div>
							<!-- <div class="pixsass-icon-box-content">
								<h3 class="pixsass-icon-box-title"><a href="#">Customization</a></h3>

								<p>
									The full monty do one nancy boy<br> say gutted mate cockup Why at <br> public school.!
								</p>
							</div> -->
						</div>
						<!-- /.pixsass-box style-two wow pixFadeUp -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->

					<div class="col-lg-2 col-md-6">
						<div class="pixsass-icon-box-wrapper style-two wow pixFadeUp" data-wow-delay="0.7s">
							<div class="iapp-icon-box-icon">
                                <img src="assets/img/03.jpg" alt="">
							</div>
							<!-- <div class="pixsass-icon-box-content">
								<h3 class="pixsass-icon-box-title"><a href="#">Multiple Managers</a></h3>
								<p>
									The full monty do one nancy boy<br> say gutted mate cockup Why at <br> public school.!
								</p>
							</div> -->
						</div>
						<!-- /.pixsass-box style-two wow pixFadeUp -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->

					<div class="col-lg-2 col-md-6">
						<div class="pixsass-icon-box-wrapper style-two wow pixFadeUp" data-wow-delay="0.9s">
							<div class="iapp-icon-box-icon">
                                <img src="assets/img/04.jpg" alt="">
							</div>
							<!-- <div class="pixsass-icon-box-content">
								<h3 class="pixsass-icon-box-title"><a href="#">Cloud Data Saved</a></h3>
								<p>
									The full monty do one nancy boy<br> say gutted mate cockup Why at <br> public school.!
								</p>
							</div> -->
						</div>
						<!-- /.pixsass-box style-two wow pixFadeUp -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->
					<div class="col-lg-2 col-md-6">
						<div class="pixsass-icon-box-wrapper style-two wow pixFadeUp" data-wow-delay="0.3s">
							<div class="iapp-icon-box-icon">
								<img src="assets/img/01.jpg" alt="">
							</div>
							<!-- <div class="pixsass-icon-box-content">
								<h3 class="pixsass-icon-box-title"><a href="#">App Development</a></h3>
								<p>
									The full monty do one nancy boy<br> say gutted mate cockup Why at <br> public school.!
								</p>
							</div> -->
						</div>
						<!-- /.pixsass-box style-two wow pixFadeUp -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->

					<div class="col-lg-2 col-md-6">
						<div class="pixsass-icon-box-wrapper style-two wow pixFadeUp" data-wow-delay="0.5s">
							<div class="iapp-icon-box-icon">
                                <img src="assets/img/02.jpg" alt="">
							</div>
							<!-- <div class="pixsass-icon-box-content">
								<h3 class="pixsass-icon-box-title"><a href="#">Customization</a></h3>

								<p>
									The full monty do one nancy boy<br> say gutted mate cockup Why at <br> public school.!
								</p>
							</div> -->
						</div>
						<!-- /.pixsass-box style-two wow pixFadeUp -->
					</div>
				

				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		</section>
		<!-- /.featured -->

		<?php include('includes/depoimentos.php');?>

		<?php include('includes/banner-demonstracao.php');?>
		<?php include('includes/footer.php')?>
	</div>
	<!-- /#site -->

    <div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Título do modal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary">Salvar mudanças</button>
            </div>
            </div>
        </div>
    </div>

	<!-- Dependency Scripts -->
    <?php include('includes/scripts.php')?>
    <script>
        var swiper = new Swiper('.swiper-container', {
            loop: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    </script>

</body>

</html>