<div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="<?php echo URL_SITE ?>/admin"><?php echo $_SESSION['NOME_USUARIO'] ?></a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#." class="profile-mini">
                            <img src="<?php echo URL_SITE ?>/admin/img/logo.png"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <a target="_blank" href="<?php echo URL_SITE ?>">
                            <img src="<?php echo URL_SITE ?>/admin/img/logo.png"/>
                        </a>
                            </div>
                            <div class="profile-data">
                                <!--<div class="profile-data-name"><?php echo $_SESSION['NOME_USUARIO'] ?></div>-->
                                <div class="profile-data-title"><br><!--categoria do usuário--></div>
                            </div>
                            <div class="profile-controls">
                                <a href="<?php echo URL_SITE ?>/admin/view/usuario/usuario-form.php?cod=<?php echo $_SESSION['COD_USUARIO'] ?>" class="profile-control-left"><span class="fa fa-info"></span></a>
                                <a href="<?php echo URL_SITE ?>/admin/view/contato/contato-list.php" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                            </div>
                        </div>                                                                        
                    </li>
                    <li class="xn-title">Menu</li>
                    <li class="<?php if($url[$total-1] == 'admin' || $url[$total-1] == ''){echo 'active';} ?>">
                        <a href="<?php echo URL_SITE ?>/admin"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                        
                    </li> 
                    <?php
                    //NIVEL 1
                      foreach ($retorno as $rs_menu){
                          $rs_pagina = $daoUsuarioPaginas->BuscarPorCOD($rs_menu->getPagina());
                          //echo $rs_pagina->getFuncao();?>
                    <li class="xn-openable <?php if($url[$total-2] == $rs_pagina->getFuncao()){echo 'active';} ?>">
                    
                        <a href="#"><span class="<?php echo $rs_pagina->getIcone();?>"></span> <span class="xn-text"><?php echo $rs_pagina->getPagina();?></span></a>
                            <ul>
                              <?php 
                               //verifica se for pagina de produtos, faz outra consulta, buscando categorias
                                  $r_sub_pag = $daoUsuarioPaginas->BuscarTodos($rs_menu->getPagina());
                                  foreach ($r_sub_pag as $rs_sub_pag){
                                  $menuFuncao = $rs_sub_pag->getFuncao();
                               ?>
                              <li class="<?php if($atual == $rs_sub_pag->getFuncao()){echo 'active';} ?>"><?php if($menuFuncao!=''){ ?><a href="<?php echo URL_SITE ?>/admin/<?php echo $menuFuncao; ?>"><span class="<?php echo $rs_sub_pag->getIcone();?>"></span> <?php echo $rs_sub_pag->getPagina();?></a><?php }else{ echo '<span class="'.$rs_sub_pag->getIcone().'"></span>'. $rs_sub_pag->getPagina(); }?></li>

                               <?php }//fim while?>
                            </ul>
                    </li>
                              <?php } //fim while
                   ?>
                    
                </ul>
                <!-- END X-NAVIGATION -->
            </div>