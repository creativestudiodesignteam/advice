<!-- START PLUGINS -->
        <script type="text/javascript" src="<?php echo URL_SITE ?>/admin/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo URL_SITE ?>/admin/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo URL_SITE ?>/admin/js/plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->

        <!-- START HOME PAGE PLUGINS-->        
        <script type='text/javascript' src='<?php echo URL_SITE ?>/admin/js/plugins/icheck/icheck.min.js'></script>        
        <script type="text/javascript" src="<?php echo URL_SITE ?>/admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="<?php echo URL_SITE ?>/admin/js/plugins/scrolltotop/scrolltopcontrol.js"></script>
        
        <script type="text/javascript" src="<?php echo URL_SITE ?>/admin/js/plugins/morris/raphael-min.js"></script>
        <script type="text/javascript" src="<?php echo URL_SITE ?>/admin/js/plugins/morris/morris.min.js"></script>       
        <script type="text/javascript" src="<?php echo URL_SITE ?>/admin/js/plugins/rickshaw/d3.v3.js"></script>
        <script type="text/javascript" src="<?php echo URL_SITE ?>/admin/js/plugins/rickshaw/rickshaw.min.js"></script>
        <script type='text/javascript' src='<?php echo URL_SITE ?>/admin/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'></script>
        <script type='text/javascript' src='<?php echo URL_SITE ?>/admin/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script>                
        <script type='text/javascript' src='<?php echo URL_SITE ?>/admin/js/plugins/bootstrap/bootstrap-datepicker.js'></script>                
        <script type="text/javascript" src="<?php echo URL_SITE ?>/admin/js/plugins/owl/owl.carousel.min.js"></script>                 
        
        <script type="text/javascript" src="<?php echo URL_SITE ?>/admin/js/plugins/moment.min.js"></script>
        <script type="text/javascript" src="<?php echo URL_SITE ?>/admin/js/plugins/daterangepicker/daterangepicker.js"></script>
        <!-- END HOME PAGE PLUGINS--> 
        
        <!-- LOGIN PAGE PLUGINS -->
              
        <script type='text/javascript' src='<?php echo URL_SITE ?>/admin/js/plugins/bootstrap/bootstrap-select.js'></script>        

        <script type='text/javascript' src='<?php echo URL_SITE ?>/admin/js/plugins/validationengine/languages/jquery.validationEngine-en.js'></script>
        <script type='text/javascript' src='<?php echo URL_SITE ?>/admin/js/plugins/validationengine/jquery.validationEngine.js'></script>        

        <script type='text/javascript' src='<?php echo URL_SITE ?>/admin/js/plugins/jquery-validation/jquery.validate.js'></script>                

        <script type='text/javascript' src='<?php echo URL_SITE ?>/admin/js/plugins/maskedinput/jquery.maskedinput.min.js'></script>
        <!-- END LOGIN PAGE PLUGINS -->   
        
        <!-- THIS PAGE PLUGINS -->
        <script type='text/javascript' src='<?php echo URL_SITE ?>/admin/js/plugins/icheck/icheck.min.js'></script>
        <script type="text/javascript" src="<?php echo URL_SITE ?>/admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        
        <script type='text/javascript' src='<?php echo URL_SITE ?>/admin/js/plugins/bootstrap/bootstrap-datepicker.js'></script>        
        <script type='text/javascript' src='<?php echo URL_SITE ?>/admin/js/plugins/bootstrap/bootstrap-select.js'></script>        

        <script type='text/javascript' src='<?php echo URL_SITE ?>/admin/js/plugins/validationengine/languages/jquery.validationEngine-en.js'></script>
        <script type='text/javascript' src='<?php echo URL_SITE ?>/admin/js/plugins/validationengine/jquery.validationEngine.js'></script>        

        <script type='text/javascript' src='<?php echo URL_SITE ?>/admin/js/plugins/jquery-validation/jquery.validate.js'></script>                

        <script type='text/javascript' src='<?php echo URL_SITE ?>/admin/js/plugins/maskedinput/jquery.maskedinput.min.js'></script>
        <!-- END THIS PAGE PLUGINS -->  

        <!-- START TEMPLATE -->
        
        
        <script type="text/javascript" src="<?php echo URL_SITE ?>/admin/js/plugins.js"></script>        
        <script type="text/javascript" src="<?php echo URL_SITE ?>/admin/js/actions.js"></script>
        
        <script type="text/javascript" src="<?php echo URL_SITE ?>/admin/js/demo_dashboard.js"></script>
        <!-- END TEMPLATE -->
        
        
        <script>
        function crud(formulario){

            var postData = $(formulario).serializeArray();
            var formURL = $(formulario).attr("action");
            
            $.ajax({
                    type: "POST",
                    url: formURL,
                    data: postData,
                    dataType: 'json',
                    success: function(data){

                       var msg = data['mensagem'];
                    var classe = data['classe'];

                    $("#simple-msg").html('<div style="padding: 10px; margin-top: 20px; text-align: justify" class="col-sm-12 '+classe+'"><b>'+msg+'</b></div>');
                    $("#simple-msg").slideDown();
                    setTimeout(function(){$("#simple-msg").slideUp();},4000);

                    if(data['result'] === 'error'){
                        return false;
                    }else if(data['result'] === 'success'){
                        location.reload();	
                    }



                    }
            });
	    }
        
        function remover_imagem(id, url, img) {
		var r = confirm("Tem certeza que deseja excluir esta imagem?");
		if (!r === true) {
   			 return false;
		} else {
            
   		 
                    var formdata = 'id_adv='+id+'&img='+img+'&acao=excluir_imagem';
                    $.ajax({
                            type: "POST",
                            url:   '<?php echo URL_SITE ?>/admin/controle/equipe/_equipe.php',
                            data: formdata,
                            dataType: 'json',
                            success: function(data){                                
                             var msg = data['mensagem'];
                            if(data['result'] === 'error'){
                                    alert(msg);
                                    return false;
                                }else if(data['result'] === 'success'){
                                    alert(msg);
                                    //location.reload();	
                                }


                            }
                    });
		}
        }
        function remover_imagem_noticia(id, url, img) {
		var r = confirm("Tem certeza que deseja excluir esta imagem?");
		if (!r === true) {
   			 return false;
		} else {
            
   		 
                    var formdata = 'id='+id+'&img='+img+'&acao=excluir_imagem';
                    $.ajax({
                            type: "POST",
                            url:   '<?php echo URL_SITE ?>/admin/controle/noticias/_noticias.php',
                            data: formdata,
                            dataType: 'json',
                            success: function(data){                                
                             var msg = data['mensagem'];
                            if(data['result'] === 'error'){
                                    alert(msg);
                                    return false;
                                }else if(data['result'] === 'success'){
                                    alert(msg);
                                    //location.reload();	
                                }


                            }
                    });
		}
        }        
        
        function remover(id, url) {
		var r = confirm("Tem certeza que deseja excluir este Registro?");
		if (!r === true) {
   			 return false;
		} else {
   		 
		var formdata = 'cod='+id+'&acao=excluir';
	$.ajax({
		type: "POST",
		url: url,
		data: formdata,
                dataType: 'json',
		success: function(data){
		
		 var msg = data['mensagem'];
		if(data['result'] === 'error'){
                        alert(msg);
                        return false;
                    }else if(data['result'] === 'success'){
                        alert(msg);
                        location.reload();	
                    }
			 
					
		}
	});
			}
}

function mascaraMutuario(o,f){
    v_obj=o
    v_fun=f
    setTimeout('execmascara()',1)
}
 
function execmascara(){
    v_obj.value=v_fun(v_obj.value)
}


	
function fone(v){

    //Remove tudo o que não é dígito
    v=v.replace(/\D/g,"")
 
    if (v.length <= 14) { //telefone
 
        //Coloca um ponto entre o terceiro e o quarto dígitos
        v=v.replace(/(\d{0})(\d)/,"($1$2")
		
		v=v.replace(/(\d{1})(\d)/,"$1$2) ")
 
        //Coloca um ponto entre o terceiro e o quarto dígitos
        //de novo (para o segundo bloco de números)
       
 
        //Coloca um hífen entre o terceiro e o quarto dígitos
        v=v.replace(/(\d{4})(\d)/,"$1-$2")
 
    } else { //celular
 
        //Coloca um ponto entre o terceiro e o quarto dígitos
        v=v.replace(/(\d{0})(\d)/,"($1$2")
		
		v=v.replace(/(\d{1})(\d)/,"$1$2) ")
 
        //Coloca um ponto entre o terceiro e o quarto dígitos
        //de novo (para o segundo bloco de números)
       
 
        //Coloca um hífen entre o terceiro e o quarto dígitos
        v=v.replace(/(\d{5})(\d{1,2})$/,"$1-$2")
 
    }
 
    return v
 }

function Vcep(v){
v=v.replace(/\D/g,"")
 
    
 
        //Coloca um ponto entre o segundo e o terceiro dígito
        //v=v.replace(/(\d{2})(\d)/,"$1.$2")
 
        //Coloca um ponto entre o terceiro e o quarto dígitos
        //de novo (para o segundo bloco de números)
        v=v.replace(/(\d{5})(\d)/,"$1-$2")
 
        //Coloca um hífen entre o terceiro e o quarto dígitos
       
 
    return v
}

function carregaModal(url){
	$("#conteudoModal").load(url);
}

function mostra_icone(icone, id){
            document.getElementById('basic_addon'+id).innerHTML = '<i class="'+icone+'" aria-hidden="true"></i>';
        }
        </script>