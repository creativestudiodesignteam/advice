<?php
include("../../include_master.php");

// Flag que indica se há erro ou não
$erro = null;
// Quando enviado o formulário
if (isset($_FILES['imagem1']))
{
    // Configurações
    $extensoes = array(".pdf");
    $caminho = URL_IMG.'/pdf/';
    // Recuperando informações do arquivo
    $img1_nome = $_FILES['imagem1']['name'];
    $temp = $_FILES['imagem1']['tmp_name'];
    // Verifica se a extensão é permitida
    if (!in_array(strtolower(strrchr($img1_nome, ".")), $extensoes)) {
		$erro = 'Extensão inválida';
	}
    // Se não houver erro
    if (!$erro) {
        // Movendo arquivo para servidor
        if (!move_uploaded_file($temp, $caminho . $img1_nome)){
            $erro = 'Não foi possível anexar o arquivo';
	}
    }
}

?>
<link href="../css/bootstrap/bootstrap.min.css" rel="stylesheet">    
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>    
    <script type="text/javascript">
    $(function($) {
        // Definindo página pai
        var pai = window.parent.document;
        

        <?php if (isset($erro)): // Se houver algum erro ?>
        
            // Exibimos o erro
            alert('<?php echo $erro ?>');
            
        <?php elseif (isset($img1_nome)): // Se não houver erro e o arquivo foi enviado 
		$div = explode(".",$img1_nome);
		?>
        
            // Adicionamos um item na lista ul
            $('#anexo_imagem1', pai).append('<li lang="<?php echo $img1_nome ?>"><div style="margin-top:10px; clear: both" id="IMG<?php echo $div[0] ?>" class="col-sm-8"><a style="width:100px; margin-right:10px" target="_blank" href="<?php echo URL_CAMINHO_IMG."/pdf/".$img1_nome ?>">Arquivo</a><i class="fa fa-trash" style="cursor: pointer" onclick="removePDF(\'<?php echo $img1_nome ?>\', \'<?php echo $div[0] ?>\')"></i><br>Link do PDF: <br><div ><div class="input-group col-lg-6"><input class="form-control" type="text" class="sim-edit-box-content-field-input title" name="link_img" id="link_img" value="<?php echo URL_CAMINHO_IMG."/pdf/".$img1_nome ?>" placeholder="Link do PDF"  onclick="this.select()"></div></div><br></div></li>');
			
		<?php elseif (isset($thumb_nome)): // Se não houver erro e o arquivo foi enviado ?>
        
            // Adicionamos um item na lista ul
            $('#anexo_thumb', pai).append('<li lang="<?php echo $thumb_nome ?>">Miniatura:<?php echo $thumb_nome ?> <img src="img/remove.png" alt="Remover" class="remover" onclick="removeAnexoThumb_noticias(this)" \/> </li>');	
            
        <?php endif ?>
       
        // Quando enviado o arquivo
    	$("#imagem1").change(function() {	    
            // Se o arquivo foi selecionado
            if (this.value != "")
            {    
                // Exibimos o loder
                $("#status").show();
                // Enviamos o formulário
                $("#upload_img1").submit();
            }
        });
				
    });
</script>
<style>
body{
    background: transparent;
}
.btn {
    display: inline-block;
padding: 5px 5px;
height: 30px;
width: 150px;
margin-right: auto;
margin-left: auto;
text-align: center;
color: #fff;
font-weight: 600;
font-size: 14px;
-webkit-border-radius: 3px;
-moz-border-radius: 3px;
border-radius: 3px;
-webkit-transition: background 0.5s;
-moz-transition: background 0.5s;
-o-transition: background 0.5s;
transition: background 0.5s;
cursor: pointer;
margin-bottom: 25px;
border: 1px solid #e4e4e5;
}
/*Also */
.fileinput-button {
    position: relative;
    overflow: hidden;
}
/*Also*/
 .fileinput-button input {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    opacity: 0;
    -ms-filter:'alpha(opacity=0)';
    font-size: 200px;
    direction: ltr;
    cursor: pointer;
}
</style>
<form id="upload_img1" action="<?php $PHP_SELF ?>" method="post" enctype="multipart/form-data">
<span class="btn btn-success fileinput-button">
    <span style="color: #fff">Subir PDF</span>
    <input type="file" name="imagem1" id="imagem1" />
</span>
</form>
