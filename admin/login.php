<?php

$login=true;

require_once ("include_master.php");

use Dao\DaoUsuario;

$daoUsuario = new DaoUsuario();

if(empty($_POST['userField'])){
	//session_destroy();
	$erro='';

}else{


$e_mail = $_POST['userField'];
$senha = $_POST['passwField'];

$rs_login = $daoUsuario->logar($e_mail, $senha);


if ($rs_login->getId() <> ""){
	
	

	$_SESSION['NOME_USUARIO']  = $rs_login->getNome();
	$_SESSION['EMAIL_USUARIO'] = $rs_login->getEmail();
	$_SESSION['COD_USUARIO']   = $rs_login->getId();
	$_SESSION['GRUPO_USUARIO'] = $rs_login->getGrupo();
	$_SESSION['SENHA_USUARIO'] = $rs_login->getSenha();
	
	 header('Location: index.php');
	?>
    
    <?php
	} 
	else {
	 header('Location: '.URL_SITE.'/admin/login.php?m=2');
	}
}
?>
<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>        
        <!-- META SECTION -->
        <title><?php echo TITULO_SITE ?> - Admin</title>           
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo URL_SITE ?>/admin/favicon.png" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->                                    
    </head>
    <body>
        
        <div class="login-container">
        
            <div class="login-box animated fadeInDown">
                <div class="login-logo" style="height: 100px"></div>
                <div class="login-body">
                    <div class="login-title">Bem Vindo ao <strong><?php echo TITULO_SITE ?> Admin</strong></div>
                    <form action="login.php" class="form-horizontal" id="form-login" method="post">
                    <div class="form-group">
                        <div class="col-md-12">
                            <input name="userField" required="" type="email" class="form-control" placeholder="E-mail"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input name="passwField" required="" type="password" class="form-control" placeholder="Senha"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <a href="<?php echo URL_SITE ?>/admin/recupera-senha.php" class="btn btn-link btn-block">Esqueci minha senha</a>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-info btn-block">Login</button>
                        </div>
                    </div>
                    </form>
                </div>
                <?php if(isset($_GET['m']) && $_GET['m'] == 1){ ?>
                <div class="alert-success" style="clear: both; padding: 10px">
                    <p>Senha enviada com sucesso</p>
                </div>
                <?php }else if(isset($_GET['m']) && $_GET['m'] == 0){?>
                <div class="alert-danger" style="clear: both; padding: 10px">
                    <p><?php echo ERROR_ADDRESS ?></p>
                </div>
                <?php }else if(isset($_GET['m']) && $_GET['m'] == 2){ ?>
                <div class="alert-danger" style="clear: both; padding: 10px">
                    <p><?php echo ERROR_LOGIN ?></p>
                </div>
                <?php }?>
                <div class="login-footer">
                    <div class="pull-left">
                        &copy; <?php echo date('Y') ?> <a href="http://www.deepocean.com.br">Deep Ocean</a>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- START SCRIPTS -->
            <?php include('includes/js.php') ?>
        <!-- END SCRIPTS -->     
        
        <script type="text/javascript">
            var jvalidate = $("#form-login").validate({
                ignore: [],
                rules: {  
                        passwField: {
                                required: true,
                                minlength: 5,
                                maxlength: 10
                        }
                        userField: {
                                required: true,
                                email: true
                        }
                    }                                        
                });                                    

        </script>
    </body>
</html>






