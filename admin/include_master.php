<?php
require $_SERVER['DOCUMENT_ROOT']."/libs/config.php";



use Dao\DaoUsuarioGrupoPaginas;
use Dao\DaoUsuarioPaginas;

if(!isset($login)){
    
    include("dados/logado.php");
    $daoUsuarioGrupoPaginas = new DaoUsuarioGrupoPaginas();
    $daoUsuarioPaginas = new DaoUsuarioPaginas();
    $retorno = $daoUsuarioGrupoPaginas->BuscarTodosGrupo($_SESSION['GRUPO_USUARIO']);
    
    $url = explode('/',  $_SERVER['REQUEST_URI']);
    $total = count($url);
    $atual = $url[$total-3].'/'.$url[$total-2].'/'.$url[$total-1];
    
    $_SESSION['linguagem'] = 1;
    $_SESSION['linguagem_prefix'] = 'pt';
}


?>