<?php
include("include_master.php");

$daoContatoIndex = $daoContato;

$rcont = $daoContatoIndex->BuscarTodosPaginacao(0, 5)

?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Área Administrativa <?php echo TITULO_SITE ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo URL_SITE ?>/admin/favicon.png" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE --> 
        <?php include('includes/css.php') ?>
        <!-- EOF CSS INCLUDE -->                                    
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <?php include('includes/sidebar.php') ?>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <?php include('includes/nav-vertical.php') ?>
                <!-- END X-NAVIGATION VERTICAL -->                     

                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>                    
                    <li class="active">Dashboard</li>
                </ul>
                <!-- END BREADCRUMB -->                       
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    
                    <!-- START WIDGETS -->                    
                    <div class="row">
                        <div class="col-md-6">
                            
                            <!-- START WIDGET MESSAGES -->
                            <div class="widget widget-default widget-item-icon" onclick="location.href='<?php echo URL_SITE ?>/admin/view/contato/contato-list.php';">
                                <div class="widget-item-left">
                                    <span class="fa fa-envelope"></span>
                                </div>                             
                                <div class="widget-data">
                                    <div class="widget-int num-count"><?php echo TOTAL_CONTATOS ?></div>
                                    <div class="widget-title">Novos contatos</div>
                                    <div class="widget-subtitle">Enviados pelo site</div>
                                </div>      
                                <div class="widget-controls">                                
                                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                                </div>
                            </div>                            
                            <!-- END WIDGET MESSAGES -->
                            
                        </div>
                        <div class="col-md-6">
                            
                            <!-- START WIDGET CLOCK -->
                            <div class="widget widget-info widget-padding-sm">
                                <div class="widget-big-int plugin-clock">00:00</div>                            
                                <div class="widget-subtitle plugin-date">Loading...</div>
                                <div class="widget-controls">                                
                                    <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="left" title="Remove Widget"><span class="fa fa-times"></span></a>
                                </div>                            
                            </div>                        
                            <!-- END WIDGET CLOCK -->
                            
                        </div>
                    </div>
                    <!-- END WIDGETS --> 
                    
                    <div class="row" style="margin-top: 50px">
                        <div class="col-md-12">
                           <div class="page-title">                    
                                <h2><span class="fa fa-envelope-o"></span> Cinco últimos novos Contatos</h2>
                           </div> 
                           <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Data de Contato</th>
                                                <th>Nome</th>
                                                <th>Assunto</th>
                                                <th>Ação</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($rcont as $r1){?>
                                            <tr>
                                                <td><?php echo date('d/m/Y H:i:s', strtotime($r1->getDataCadastro())) ?></td>
                                                <td><?php echo $r1->getNome() ?></td>
                                                <td><?php echo $r1->getAssunto() ?></td>
                                                <td><a onclick="carregaModal('<?php echo URL_SITE ?>/admin/view/contato/contato-form.php?cod=<?php echo $r1->getId() ?>')"  data-toggle="modal" data-target="#abreModal" href="#" class="btn btn-info"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                           </table> 
                            
                        </div>
                        
                    </div>
                    
                    <!-- CONTENT NEWS -->
                    
                    <!-- END CONTENT NEWS-->
                  
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <?php include('includes/msg-deslogar.php') ?>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <?php include('includes/audio.php') ?>
        <!-- END PRELOADS -->                  
        
    <!-- START SCRIPTS -->
        <?php include('includes/js.php') ?>
       
    <!-- END SCRIPTS -->         
    </body>
</html>






