<?php
include("../../include_master.php");
use Dao\Classes\Contato;
use Dao\DaoContato;

#Pega variaveis vinda do formulÃ¡rio via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

$contato = new Contato();
$title = 'Visualizar';

$daoContato = new DaoContato();



if(isset($cod) && !empty($cod)){
    $contato = $daoContato->BuscarPorCOD($cod);
    $v = $contato->getView() + 1;
    $daoContato->Visualizar($contato->getId(), $v);
}
?>
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Contato</h4>
            </div>
            <div class="modal-body">
              <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START VALIDATIONENGINE PLUGIN -->
                            <div class="block">
                                <h4><?php echo $title ?></h4>
                                <div id="simple-msg" style="text-align: justify; margin-bottom: 50px"></div>                                
                                <form role="form" class="form-horizontal" name="form_crud" id="form_crud" action="<?php echo URL_SITE ?>/admin/controle/usuario/_contato-usuario.php">                            
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Data de Contato:</label>
                                        <div class="col-md-9">
                                            <?php echo date('d/m/Y H:i:s', strtotime($contato->getDataCadastro())) ?>
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Contato:</label>
                                        <div class="col-md-9">
                                            <?php echo $contato->getNome() ?>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Tipo:</label>
                                        <div class="col-md-9">
                                            <?php echo $contato->getTipo() ?>
                                        </div>
                                    </div>
                                    <?php if($contato->getEmpresa() <> ''){ ?>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Empresa:</label>
                                            <div class="col-md-9">
                                                <?php echo $contato->getEmpresa() ?>
                                            </div>
                                        </div> 
                                    <?php }?> 
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">E-mail:</label>
                                        <div class="col-md-9">
                                            <?php echo $contato->getEmail() ?>
                                            
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Telefone:</label>
                                        <div class="col-md-9">
                                            <?php echo $contato->getTelefone() ?>
                                            
                                        </div>
                                    </div>  
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Mensagem:</label>
                                        <div class="col-md-9">
                                            <?php echo $contato->getMensagem() ?>
                                            
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                    
                                    </div>
                                </form>
                            </div>                                               
                            <!-- END VALIDATIONENGINE PLUGIN -->

                        </div>
                    </div>

                        
                </div>
                <!-- END PAGE CONTENT WRAPPER -->  
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>           
                
                              
            





