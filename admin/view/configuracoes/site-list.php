<?php
include("../../include_master.php");
use Dao\DaoSite;

$daoSite = new DaoSite();

$result = $daoSite->BuscarTodos($_SESSION['linguagem']);

?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Área Administrativa <?php echo TITULO_SITE ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo URL_SITE ?>/admin/favicon.png" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE --> 
        <?php include(RAIZ.'/admin/includes/css.php') ?>
        <!-- EOF CSS INCLUDE -->                                    
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <?php include(RAIZ.'/admin/includes/sidebar.php') ?>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <?php include(RAIZ.'/admin/includes/nav-vertical.php') ?>
                <!-- END X-NAVIGATION VERTICAL -->                     

                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo URL_SITE ?>/admin/">Home</a></li>
                    <li class="active"><a href="#.">Site</a></li>
                </ul>
                <!-- END BREADCRUMB -->     
                
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-globe"></span> Site</h2>
                </div>
                <!-- END PAGE TITLE --> 
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-heading">                                
                                    <h3 class="panel-title">Lista</h3>
                                    
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <!--<li><a href="<?php echo URL_SITE ?>/admin/view/configuracoes/site-form.php" class=""><i class="fa fa-plus" aria-hidden="true"></i></a></li>-->
                                    </ul>                                
                                </div>
                                <div class="panel-body">
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Site</th>
                                                <th>Ação</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($result as $r){?>
                                            <tr>
                                                <td><?php echo $r->getId() ?></td>
                                                <td><?php echo $r->getTituloSite() ?></td>
                                                <td><a href="<?php echo URL_SITE ?>/admin/view/configuracoes/site-form.php?cod=<?php echo $r->getId() ?>" class="btn btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                    <span style="margin: 0 5px"></span>
                                                    <?php if($r->getId() <> 1){ ?>
                                                        <a class="btn btn-danger" onclick="remover(<?php echo $r->getId() ?>, '<?php echo URL_SITE ?>/admin/controle/configuracoes/_site.php')" href=""><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                                    <?php }?>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->

                        </div>
                    </div>                                
                    
                </div>
                <!-- PAGE CONTENT WRAPPER -->                                 
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END MESSAGE BOX-->
        
        <!-- MESSAGE BOX-->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <?php include(RAIZ.'/admin/includes/audio.php') ?>
        <!-- END PRELOADS -->                  
        
    <!-- START SCRIPTS -->
        <?php include(RAIZ.'/admin/includes/js.php') ?>
        <script type="text/javascript" src="<?php echo URL_SITE ?>/admin/js/plugins/datatables/jquery.dataTables.min.js"></script>   
        
        
    <!-- END SCRIPTS -->    
    

    </body>
</html>






