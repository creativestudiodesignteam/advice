<?php
include("../../include_master.php");
use Dao\Classes\Configuracao;
use Dao\DaoConfiguracao;

#Pega variaveis vinda do formulÃ¡rio via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

$config = new Configuracao();
$title = 'Cadastrar';

$daoConfiguracao = new DaoConfiguracao();

if(isset($cod) && !empty($cod)){
    $config = $daoConfiguracao->BuscarPorCOD($cod);
    $title = 'Alterar';
    
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Área Administrativa <?php echo TITULO_SITE ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo URL_SITE ?>/admin/favicon.png" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <?php include(RAIZ.'/admin/includes/css.php') ?>
        <!-- EOF CSS INCLUDE -->                  
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <?php include(RAIZ.'/admin/includes/sidebar.php') ?>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <?php include(RAIZ.'/admin/includes/nav-vertical.php') ?>
                <!-- END X-NAVIGATION VERTICAL -->                    
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo URL_SITE ?>/admin/">Home</a></li>
                    <li class="active">Configurações do Site</li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-cogs"></span> Configurações</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START VALIDATIONENGINE PLUGIN -->
                            <div class="block">
                                <h4><?php echo $title ?></h4>
                                <div id="simple-msg" style="text-align: justify; margin-bottom: 50px"></div>
                                <form role="form" class="form-horizontal" name="form_crud" id="form_crud" action="<?php echo URL_SITE ?>/admin/controle/configuracoes/_configuracao.php">                            
                                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Analytics:</label>
                                        <div class="col-md-9">
                                            <textarea name="analytics" class="form-control"><?php echo $config->getAnalytics() ?></textarea>
                                            
                                        </div>
                                    </div>                            
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">E-mail:</label>
                                        <div class="col-md-9">
                                            <input type="email" value="<?php echo $config->getEmail() ?>" name="email" class="form-control" required=""/>
                                            
                                        </div>                        
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Telefone:</label>
                                        <div class="col-md-9">
                                            <input type="text" value="<?php echo $config->getTelefone() ?>" name="telefone" class="form-control" required=""/>
                                            
                                        </div>                        
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">E-mail de Envio:</label>
                                        <div class="col-md-9">
                                            <input type="email" value="<?php echo $config->getEmailEnvio() ?>" name="email_envio" class="form-control" required=""/>
                                            
                                        </div>                        
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">SMTP:</label>
                                        <div class="col-md-9">
                                            <input type="text" value="<?php echo $config->getSmtp() ?>" name="smtp" class="form-control" required=""/>
                                            
                                        </div>                        
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Senha:</label>
                                        <div class="col-md-9">
                                            <input type="text" value="<?php echo $config->getSenhaEnvio() ?>" name="senha_envio" class="form-control" required=""/>
                                            
                                        </div>                        
                                    </div>
                                    
                                    <div class="btn-group pull-right">
                                        <input type="hidden" name="acao" value="<?php echo strtolower($title) ?>">
                                        <input type="hidden" name="cod" value="<?php echo $config->getId() ?>">
                                        <button class="btn btn-primary" type="button" onclick="crud('#form_crud')"><?php echo $title ?></button>
                                    </div>   
                                    <div class="form-group">
                                    <div id="simple-msg" style="text-align: justify"></div>
                                    </div>
                                </form>
                            </div>                                               
                            <!-- END VALIDATIONENGINE PLUGIN -->
                            
                        </div>
                    </div>

                        
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
        
       

        <!-- START PRELOADS -->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END PRELOADS --> 
        
        <!-- MESSAGE BOX-->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END MESSAGE BOX-->
        
        <!-- START SCRIPTS -->
        <?php include(RAIZ.'/admin/includes/js.php') ?>
       
        
    <!-- END SCRIPTS -->          
        
    </body>
</html>






