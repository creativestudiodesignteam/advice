<?php
include("../../include_master.php");
use Dao\Classes\Endereco;
use Dao\DaoEndereco;
use Dao\DaoEstado;

#Pega variaveis vinda do formulÃ¡rio via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

$config = new Endereco();

$title = 'Cadastrar';

$daoEndereco = new DaoEndereco();
$daoEstado = new DaoEstado();

$estados = $daoEstado->BuscarTodos();

if(isset($cod) && !empty($cod)){
    $config = $daoEndereco->BuscarPorCOD($cod);
    $title = 'Alterar';
    
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Área Administrativa <?php echo TITULO_SITE ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo URL_SITE ?>/admin/favicon.png" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <?php include(RAIZ.'/admin/includes/css.php') ?>
        <!-- EOF CSS INCLUDE -->                  
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <?php include(RAIZ.'/admin/includes/sidebar.php') ?>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <?php include(RAIZ.'/admin/includes/nav-vertical.php') ?>
                <!-- END X-NAVIGATION VERTICAL -->                    
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo URL_SITE ?>/admin/">Home</a></li>
                    <li><a href="<?php echo URL_SITE ?>/admin/view/configuracoes/endereco-list.php">Endereço</a></li>
                    <li class="active"><?php echo $title ?></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-map-marker"></span> Endereço</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START VALIDATIONENGINE PLUGIN -->
                            <div class="block">
                                <h4><?php echo $title ?></h4>
                                <div id="simple-msg" style="text-align: justify; margin-bottom: 50px"></div>                                
                                <form enctype="multipart/form-data" role="form" class="form-horizontal" method="post" name="form_crud" id="form_crud" action="<?php echo URL_SITE ?>/admin/controle/configuracoes/_endereco.php">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Unidade:</label>
                                        <div class="col-md-9">
                                            <input value="<?php echo $config->getUnidade() ?>" type="text" name="unidade" class="form-control" required=""/>
                                        </div>
                                    </div>                            
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Endereço:</label>
                                        <div class="col-md-9">
                                            <input value="<?php echo $config->getEndereco() ?>" type="text" name="endereco" class="form-control" required=""/>
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Nº:</label>
                                        <div class="col-md-9">
                                            <input type="text" value="<?php echo $config->getNumero() ?>" name="numero" class="form-control" required=""/>
                                            
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">CEP:</label>
                                        <div class="col-md-9">
                                            <input type="text" value="<?php echo $config->getCep() ?>" name="cep" class="form-control"  required=""/>
                                            
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Estado:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="estado" id="formGender">
                                                <option value="">Escolha um Estado</option>
                                                <?php foreach ($estados as $estado){ ?>
                                                <option <?php if($config->getEstado() == $estado->getSigla()){echo 'selected';} ?> value="<?php echo $estado->getSigla() ?>"><?php echo $estado->getDescricao() ?></option>
                                                <?php }?>
                                            </select>  
                                        </div>                        
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Cidade:</label>
                                        <div class="col-md-9">
                                            <input type="text" value="<?php echo $config->getCidade() ?>" name="cidade" class="form-control" required=""/>
                                            
                                        </div>                        
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Bairro:</label>
                                        <div class="col-md-9">
                                            <input type="text" value="<?php echo $config->getBairro() ?>" name="bairro" class="form-control" />
                                            
                                        </div>                        
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Complemento:</label>
                                        <div class="col-md-9">
                                            <input type="text" value="<?php echo $config->getComplemento() ?>" name="complemento" class="form-control" />
                                            
                                        </div>                        
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Telefone:</label>
                                        <div class="col-md-9">
                                            <input type="text" onKeyPress="mascaraMutuario(this,fone);" value="<?php echo $config->getTelefone() ?>" maxlength="14" placeholder="Somente Numeros" name="telefone" class="form-control" required=""/>
                                            
                                        </div>                        
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">E-mail:</label>
                                        <div class="col-md-9">
                                            <input type="text" value="<?php echo $config->getEmail() ?>" name="email" class="form-control"/>
                                            
                                        </div>                        
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Latitude:</label>
                                        <div class="col-md-9">
                                            <input type="text" value="<?php echo $config->getLatitude() ?>" name="latitude" class="form-control" required=""/>
                                            
                                        </div>                        
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Longitude:</label>
                                        <div class="col-md-9">
                                            <input type="text" value="<?php echo $config->getLongitude() ?>" name="longitude" class="form-control" required=""/>
                                            
                                        </div>                        
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Imagem:</label>
                                        <div class="col-md-9">
                                            <input type="file" name="img_noticia" accept="image/*">
                                            
                                        </div>
                                    </div>
                                    <?php if($config->getImagem() <> ''){ ?>
                                    <input type="hidden" name="old_img" value="<?php echo $config->getImagem() ?>">
                                    <div class="form-group">
                                         <label class="col-md-3 control-label">Alterar Imagem:</label>
                                         <label class="radio-inline"><input type="radio" name="alt_img" id="alt_img" value="s" >Sim</label>
                                         <label class="radio-inline"><input type="radio" name="alt_img" id="alt_img" value="n" checked>Não</label> 
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Imagem Atual:</label>
                                        <div class="col-md-9">
                                            <img class="col-sm-6" src="<?php echo URL_CAMINHO_IMG.'/endereco/'.$config->getImagem() ?>">
                                            
                                        </div>
                                    </div>
                                    <?php }?>
                                    <div class="btn-group pull-right">
                                        <input type="hidden" name="acao" value="<?php echo strtolower($title) ?>">
                                        <input type="hidden" name="cod" value="<?php echo $config->getId() ?>">
                                        <button class="btn btn-primary" type="submit"><?php echo $title ?></button>
                                    </div>   
                                    <div class="form-group">
                                    
                                    </div>
                                </form>
                            </div>                                               
                            <!-- END VALIDATIONENGINE PLUGIN -->
                            
                        </div>
                    </div>

                        
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
        
       

        <!-- START PRELOADS -->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END PRELOADS --> 
        
        <!-- MESSAGE BOX-->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END MESSAGE BOX-->
        
        <!-- START SCRIPTS -->
        <?php include(RAIZ.'/admin/includes/js.php') ?>
       
        
    <!-- END SCRIPTS -->          
        
    </body>
</html>






