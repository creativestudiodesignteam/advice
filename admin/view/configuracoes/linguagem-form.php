<?php
include("../../include_master.php");
use Dao\Classes\Linguagem;
use Dao\DaoLinguagem;

#Pega variaveis vinda do formulÃ¡rio via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

$linguagem = new Linguagem();
$title = 'Cadastrar';

$daoLinguagem = new DaoLinguagem();
$imagem = '';

if(isset($_SESSION['retorno']['obj']) && !empty($_SESSION['retorno']['obj'])){
    $linguagem = $_SESSION['retorno']['obj'];
    unset($_SESSION['retorno']['obj']);
}

if(isset($cod) && !empty($cod)){
    $linguagem = $daoLinguagem->BuscarPorCOD($cod);
    $title = 'Alterar';
    $imagem = $linguagem->getBandeira();
   
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Área Administrativa <?php echo TITULO_SITE ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo URL_SITE ?>/admin/favicon.png" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <?php include(RAIZ.'/admin/includes/css.php') ?>
        <!-- EOF CSS INCLUDE -->                  
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <?php include(RAIZ.'/admin/includes/sidebar.php') ?>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <?php include(RAIZ.'/admin/includes/nav-vertical.php') ?>
                <!-- END X-NAVIGATION VERTICAL -->                    
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo URL_SITE ?>/admin/">Home</a></li>
                    <li><a href="<?php echo URL_SITE ?>/admin/view/configuracoes/linguagem-list.php">Linguagem</a></li>
                    <li class="active"><?php echo $title ?></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-bullseye"></span> Linguagem</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START VALIDATIONENGINE PLUGIN -->
                            <div class="block">
                                <h4><?php echo $title ?></h4>
                                <div id="simple-msg" style="text-align: justify; margin-bottom: 50px">
                                <?php if(isset($_SESSION['retorno'])){?>
                                  <div style="padding: 10px; margin-top: 20px; text-align: justify" class="col-sm-12 <?php echo $_SESSION['retorno']['classe'] ?>">
                                      <?php echo $_SESSION['retorno']['mensagem'];?>
                                  </div>
                                  <br>
                                  <br>  
                                    
                                    
                                <?php unset($_SESSION['retorno']); } ?>
                                </div>
                                <form enctype="multipart/form-data" method="post" role="form" class="form-horizontal" name="form_crud" id="form_crud" action="<?php echo URL_SITE ?>/admin/controle/configuracoes/_linguagem.php">                            
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Linguagem:</label>
                                        <div class="col-md-9">
                                            <input value="<?php echo $linguagem->getTitulo() ?>" type="text" name="linguagem" class="form-control" required=""/>
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                         <label class="col-md-3 control-label">Ativo:</label>
                                         <label class="radio-inline"><input type="radio" name="ativo" id="ativo" value="s" <?php if($linguagem->getAtivo() == 's'){ ?> checked<?php }?>>Sim</label>
                                         <label class="radio-inline"><input type="radio" name="ativo" id="ativo" value="n" <?php if($linguagem->getAtivo() == 'n'){ ?> checked<?php }?>>Não</label>  
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Bandeira:</label>
                                        <div class="col-md-9">
                                            <input type="file" name="img_bandeira" accept="image/*">
                                            
                                        </div>
                                    </div>
                                    <?php if($imagem <> ''){ ?>
                                    <input type="hidden" name="old_img" value="<?php echo $imagem ?>">
                                    <div class="form-group">
                                         <label class="col-md-3 control-label">Alterar Imagem:</label>
                                         <label class="radio-inline"><input type="radio" name="alt_img" id="alt_img" value="s" >Sim</label>
                                         <label class="radio-inline"><input type="radio" name="alt_img" id="alt_img" value="n" checked>Não</label> 
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Imagem Atual:</label>
                                        <div class="col-md-9">
                                            <img class="col-sm-1" src="<?php echo URL_CAMINHO_IMG.'/band/'.$imagem ?>">
                                            
                                        </div>
                                    </div>
                                    <?php }?>
                                    <div class="btn-group pull-right">
                                        <input type="hidden" name="acao" value="<?php echo strtolower($title) ?>">
                                        <input type="hidden" name="cod" value="<?php echo $linguagem->getId() ?>">
                                        <button class="btn btn-primary" type="submit"><?php echo $title ?></button>
                                    </div>   
                                    <div class="form-group">
                                    
                                    </div>
                                </form>
                            </div>                                               
                            <!-- END VALIDATIONENGINE PLUGIN -->

                        </div>
                    </div>

                        
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
        
       

        <!-- START PRELOADS -->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END PRELOADS --> 
        
        <!-- MESSAGE BOX-->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END MESSAGE BOX-->
        
        <!-- START SCRIPTS -->
        <?php include(RAIZ.'/admin/includes/js.php') ?>
       
        
    <!-- END SCRIPTS -->          
        
    </body>
</html>






