<?php
include("../../include_master.php");
use Dao\DaoRedesSociais;


$daoRedesSociais = new DaoRedesSociais();

$result = $daoRedesSociais->BuscarTodos();

?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Área Administrativa <?php echo TITULO_SITE ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo URL_SITE ?>/admin/favicon.png" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE --> 
        <?php include(RAIZ.'/admin/includes/css.php') ?>
        <!-- EOF CSS INCLUDE -->                                    
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <?php include(RAIZ.'/admin/includes/sidebar.php') ?>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <?php include(RAIZ.'/admin/includes/nav-vertical.php') ?>
                <!-- END X-NAVIGATION VERTICAL -->                     

                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>                    
                    <li class="active">Dashboard</li>
                </ul>
                <!-- END BREADCRUMB -->     
                
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-facebook-official"></span> Redes Sociais</h2>
                </div>
                <!-- END PAGE TITLE --> 
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-heading">                                
                                    <h3 class="panel-title">Lista</h3>
                                    
                                    <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="" onclick='adiciona_rs()'><i class="fa fa-plus" aria-hidden="true"></i></a></li>
                                        <li><a href="#" class="" onclick="crud('#form_crud')"><i class="fa fa-floppy-o" aria-hidden="true"></i></a></li>
                                    </ul>                                
                                </div>
                                <div class="panel-body">
                                    <form role="form" class="form-horizontal" name="form_crud" id="form_crud" action="<?php echo URL_SITE ?>/admin/controle/configuracoes/_rede-social.php">                            
                                    <table id="tabela" class="table datatable">
                                        <thead>
                                            <tr>
                                                <th><strong>Rede Social</strong></th>
                                                <th><strong>Link</strong></th>
                                                <th><strong>Icone</strong> -  <a target="_blank" href="http://fontawesome.io/icons/">Veja outros icones</a></th>
                                                <th><strong>Remover</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><input type="text" name="rede_social[]" id="rede_social0"  alt="rede_social[]" tabindex="1" class="form-control"/> </td>
                                                <td><input type="text" name="link[]" id="link0"  alt="link[]" tabindex="2" class="form-control"/> </td>
                                                <td>
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="basic_addon_0"></span>
                                                        <input type="text" value="" onchange="mostra_icone(this.value, '_0')" name="icone[]" id="icone0"  alt="icone[]" tabindex="3" class="form-control"/> 
                                                    </div>
                                                </td>
                                                <td><button class="btn btn-danger" onclick="RemoveTableRow(this)" type="button"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
                                            </tr>
                                            <?php $i=100; foreach ($result as $RS){?>
                                            <tr>
                                                <td><input type="text" value="<?php echo $RS->getTitulo() ?>" name="rede_social[]" id="rede_social[]"  alt="rede_social[]" tabindex="1" class="form-control"/> </td>
                                                <td><input type="text" value="<?php echo $RS->getLink() ?>" name="link[]" id="link[]"  alt="link[]" tabindex="2" class="form-control"/> </td>
                                                <td>
                                                    <div class="input-group">
                                                        <span class="input-group-addon" id="basic_addon_<?php echo $i; ?>" style="color: #fff"><i style="color: #fff" class="<?php echo $RS->getIcone() ?>" aria-hidden="true"></i></span>
                                                        <input type="text" value="<?php echo $RS->getIcone() ?>" onchange="mostra_icone(this.value, '_<?php echo $i; ?>')" name="icone[]" id="icone[]"  alt="icone[]" tabindex="3" class="form-control"/> 
                                                    </div>
                                                </td>
                                                <td><button class="btn btn-danger" onclick="RemoveTableRow(this)" type="button"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
                                            </tr>
                                            <?php $i++;}?>
                                            
                                        </tbody>
                                    </table>
                                        <input type="hidden" name="acao" value="alterar">
                                    </form>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->
                            <div class="form-group">
                                    <div id="simple-msg" style="text-align: justify"></div>
                                    </div>
                        </div>
                    </div>                                
                    
                </div>
                <!-- PAGE CONTENT WRAPPER -->                                 
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END MESSAGE BOX-->
        
        <!-- MESSAGE BOX-->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <?php include(RAIZ.'/admin/includes/audio.php') ?>
        <!-- END PRELOADS -->                  
        
    <!-- START SCRIPTS -->
        <?php include(RAIZ.'/admin/includes/js.php') ?>
        <script type="text/javascript" src="<?php echo URL_SITE ?>/admin/js/plugins/datatables/jquery.dataTables.min.js"></script>   
        <script>
        
       totals =0;
function adiciona_rs(){
    totals++;
        tbl = document.getElementById("tabela");
 
        var novaLinha = tbl.insertRow(+1);
        var novaCelula;
 
        if(totals%2==0) cl = "#F5E9EC";
        else cl = "#FBF6F7"; 
 
        novaCelula = novaLinha.insertCell(0);
        novaCelula.align = "left";
        novaCelula.innerHTML = '<input type="text" name="rede_social[]" id="rede_social'+totals+'"  alt="rede_social[]" tabindex="1" class="form-control"/>';
     
        novaCelula = novaLinha.insertCell(1);
        novaCelula.align = "left";
        novaCelula.innerHTML = '<input type="text" name="link[]" id="link'+totals+'"  alt="link[]" tabindex="1" class="form-control"/>';
        
        novaCelula = novaLinha.insertCell(2);
        novaCelula.align = "left";
        novaCelula.innerHTML = '<div class="input-group"><span class="input-group-addon" id="basic_addon'+totals+'"></span><input type="text" name="icone[]" id="icone'+totals+'"  alt="icone'+totals+'" onchange="mostra_icone(this.value, '+totals+')" tabindex="3" class="form-control"/></div>';
        
	novaCelula = novaLinha.insertCell(3);
        novaCelula.align = "left";
        novaCelula.innerHTML = "<button class=\"btn btn-danger\" onclick=\"RemoveTableRow(this)\" type=\"button\"><i class='fa fa-trash' aria-hidden='true'></i></button>";
    } 
    
    (function($) {

  RemoveTableRow = function(handler) {
    var tr = $(handler).closest('tr');

    tr.fadeOut(400, function(){ 
      tr.remove(); 
    }); 

    return false;
  };
})(jQuery);
        </script>
        
    <!-- END SCRIPTS -->    
    

    </body>
</html>






