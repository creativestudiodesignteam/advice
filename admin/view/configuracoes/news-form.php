<?php
include("../../include_master.php");
use Dao\Classes\Newsletter;
use Dao\DaoNewsletter;



#Pega variaveis vinda do formulÃ¡rio via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

$news = new Newsletter();
$title = 'Cadastrar';

$daoNewsletter = new DaoNewsletter();


if(isset($cod) && !empty($cod)){
    $news = $daoNewsletter->BuscarPorCOD($cod);
    $title = 'Alterar';
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Área Administrativa <?php echo TITULO_SITE ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo URL_SITE ?>/admin/favicon.png" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <?php include(RAIZ.'/admin/includes/css.php') ?>
        <!-- EOF CSS INCLUDE -->                  
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <?php include(RAIZ.'/admin/includes/sidebar.php') ?>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <?php include(RAIZ.'/admin/includes/nav-vertical.php') ?>
                <!-- END X-NAVIGATION VERTICAL -->                    
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo URL_SITE ?>/admin/">Home</a></li>
                    <li><a href="<?php echo URL_SITE ?>/admin/view/configuracoes/news-list.php">Newsletter</a></li>
                    <li class="active"><?php echo $title ?></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-envelope-o"></span> Newsletter</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START VALIDATIONENGINE PLUGIN -->
                            <div class="block">
                                <h4><?php echo $title ?></h4>
                                <div id="simple-msg" style="text-align: justify; margin-bottom: 50px"></div>
                                <form role="form" class="form-horizontal" name="form_crud" id="form_crud" action="<?php echo URL_SITE ?>/admin/controle/configuracoes/_news.php">                            
                                    <!-- <div class="form-group">
                                        <label class="col-md-3 control-label">Nome:</label>
                                        <div class="col-md-9">
                                            <input value="<?php echo $news->getNome(); ?>" type="text" name="nome" class="form-control" required=""/>
                                            
                                        </div>
                                    </div>  -->                           
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">E-mail:</label>
                                        <div class="col-md-9    ">
                                            <input type="email" value="<?php echo $news->getEmail() ?>" name="email_news" class="form-control" required=""/>
                                            
                                        </div>                        
                                    </div>
                                    <div class="btn-group pull-right">
                                        <input type="hidden" name="acao" value="<?php echo strtolower($title) ?>">
                                        <input type="hidden" name="cod" value="<?php echo $news->getId() ?>">
                                        <button class="btn btn-primary" type="button" onclick="crud('#form_crud')"><?php echo $title ?></button>
                                    </div>   
                                    <div class="form-group">
                                    
                                    </div>
                                </form>
                            </div>                                               
                            <!-- END VALIDATIONENGINE PLUGIN -->
                            
                        </div>
                    </div>

                        
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
        
       

        <!-- START PRELOADS -->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END PRELOADS --> 
        
        <!-- MESSAGE BOX-->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END MESSAGE BOX-->
        
        <!-- START SCRIPTS -->
        <?php include(RAIZ.'/admin/includes/js.php') ?>
       
        
    <!-- END SCRIPTS -->          
        
    </body>
</html>






