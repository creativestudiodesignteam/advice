<?php
include("../../include_master.php");
use Dao\Classes\Parceiro;
use Dao\DaoParceiro;

#Pega variaveis vinda do formulÃ¡rio via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}
//unset($_SESSION['retorno']['obj']);
//exit();
$parceiro = new Parceiro();
$title = 'Cadastrar';


$daoParceiro = new DaoParceiro();


if(isset($_SESSION['retorno']['obj']) && !empty($_SESSION['retorno']['obj'])){
    $parceiro = $_SESSION['retorno']['obj'];
    unset($_SESSION['retorno']['obj']);
}

if(isset($cod) && !empty($cod)){
    $parceiro = $daoParceiro->BuscarPorCOD($cod);
    $title = 'Alterar';
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Área Administrativa <?php echo TITULO_SITE ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon2.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <?php include(RAIZ.'/admin/includes/css.php') ?>
        <!-- EOF CSS INCLUDE -->                  
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <?php include(RAIZ.'/admin/includes/sidebar.php') ?>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <?php include(RAIZ.'/admin/includes/nav-vertical.php') ?>
                <!-- END X-NAVIGATION VERTICAL -->                    
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo URL_SITE ?>/admin/">Home</a></li>
                    <li><a href="<?php echo URL_SITE ?>/admin/view/parceiro/parceiro-list.php">Parceiros </a></li>
                    <li class="active"><?php echo $title ?></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-users"></span> Parceiros </h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START VALIDATIONENGINE PLUGIN -->
                            <div class="block">
                                <h4><?php echo $title ?></h4>
                                <div id="simple-msg" style="text-align: justify; margin-bottom: 50px">
                                <?php if(isset($_SESSION['retorno'])){?>
                                  <div style="padding: 10px; margin-top: 20px; text-align: justify" class="col-sm-12 <?php echo $_SESSION['retorno']['classe'] ?>">
                                      <?php echo $_SESSION['retorno']['mensagem'];?>
                                  </div>
                                  <br>
                                  <br>  
                                    
                                    
                                <?php unset($_SESSION['retorno']); } ?>
                                </div>
                                <form enctype="multipart/form-data" role="form" method="post" class="form-horizontal" name="form_crud" id="form_crud" action="<?php echo URL_SITE ?>/admin/controle/parceiro/_parceiro.php">                            
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Nome:</label>
                                        <div class="col-md-9">
                                            <input value="<?php echo $parceiro->getNome(); ?>" type="text" name="nome" class="form-control" required=""/>
                                        </div>
                                    </div>
                                    <!-- <div class="form-group">
                                        <label class="col-md-3 control-label">Tipo:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="tipo" id="formGender">
                                                <option value="">Escolha um Tipo</option>
                                                <option <?php if($parceiro->getTipo() == 'c'){echo 'selected';} ?> value="c">Cliente</option>
                                                <option <?php if($parceiro->getTipo() == 'p'){echo 'selected';} ?> value="p">Parceiro</option>
                                            </select>  
                                        </div>                        
                                    </div> -->
                                    <!-- 
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Site:</label>
                                        <div class="col-md-9">
                                            <input value="<?php echo $parceiro->getSite(); ?>" type="text" name="site" class="form-control" required=""/>
                                        </div>
                                    </div>   -->                          
                                    
                                    <div class="form-group">
                                         <label class="col-md-3 control-label">Ativo:</label>
                                         <label class="radio-inline"><input <?php if($parceiro->getAtivo() == ''){echo ' checked';} ?> type="radio" name="ativo" id="ativo" value="s" <?php if($parceiro->getAtivo() == 's'){ ?> checked<?php }?>>Sim</label>
                                         <label class="radio-inline"><input type="radio" name="ativo" id="ativo" value="n" <?php if($parceiro->getAtivo() == 'n'){ ?> checked<?php }?>>Não</label> 
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Imagem:</label>
                                        <div class="col-md-9">
                                            <input type="file" name="img_parceiro" accept="image/*">
                                            
                                        </div>
                                    </div>
                                    <?php if($parceiro->getImagem() <> ''){ ?>
                                    <input type="hidden" name="old_img" value="<?php echo $parceiro->getImagem() ?>">
                                    <div class="form-group">
                                         <label class="col-md-3 control-label">Alterar Imagem:</label>
                                         <label class="radio-inline"><input type="radio" name="alt_img" id="alt_img" value="s" >Sim</label>
                                         <label class="radio-inline"><input type="radio" name="alt_img" id="alt_img" value="n" checked>Não</label> 
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Imagem Atual:</label>
                                        <div class="col-md-9">
                                            <img src="<?php echo URL_CAMINHO_IMG.'/parceiro/'.$parceiro->getImagem() ?>">                                            
                                        </div>
                                    </div>
                                    <?php }?>
                                    <div class="btn-group pull-right">
                                        <input type="hidden" name="acao" value="<?php echo strtolower($title) ?>">
                                        <input type="hidden" name="cod" value="<?php echo $parceiro->getId() ?>">
                                        <button class="btn btn-primary" type="submit"><?php echo $title ?></button>
                                    </div>   
                                    <div class="form-group">
                                    
                                    </div>
                                </form>
                            </div>                                               
                            <!-- END VALIDATIONENGINE PLUGIN -->
                        </div>
                    </div>

                        
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
        
       

        <!-- START PRELOADS -->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END PRELOADS --> 
        
        <!-- MESSAGE BOX-->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END MESSAGE BOX-->
        
        <!-- START SCRIPTS -->
        <?php include(RAIZ.'/admin/includes/js.php') ?>
       <script type="text/javascript" src="<?php echo URL_SITE ?>/admin/js/plugins/summernote/summernote.js"></script>
        <script>
            var editor = CodeMirror.fromTextArea(document.getElementById("codeEditor"), {
                lineNumbers: true,
                matchBrackets: true,
                mode: "application/x-httpd-php",
                indentUnit: 4,
                indentWithTabs: true,
                enterMode: "keep",
                tabMode: "shift"                                                
            });
            editor.setSize('100%','420px');
        </script>   
    <!-- END SCRIPTS -->          
        
    </body>
</html>






