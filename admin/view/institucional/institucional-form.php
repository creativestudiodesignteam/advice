<?php
include("../../include_master.php");
use Dao\Classes\Institucional;
use Dao\DaoInstitucional;
use Dao\DaoLinguagem;
use Dao\DaoImage;
use Dao\DaoMenu;

#declarando vars
$imagem = '';
$link = '';
$ordem = '';
$ativo = '';
$destaque = '';
$icone = '';
$link = '';
$LinkLogo = '';
$menus ='';
$mn ='';
$menu_='';

#Pega variaveis vinda do formulÃ¡rio via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

$institucional = new Institucional();
$title = 'Cadastrar';

$_SESSION['TIPO'] = '';
$_SESSION['FK'] = '';

$daoInstitucional = new DaoInstitucional();
$daoLinguagem = new DaoLinguagem();
$daoImagem = new DaoImage();

$linguagens = $daoLinguagem->BuscarTodos('n');

    $aObj = [];
    foreach ($linguagens as $l){
            $aObj[$l->getId()] = new Institucional();
    }

if(isset($_SESSION['retorno']['obj']) && !empty($_SESSION['retorno']['obj'])){
    $institucional = $_SESSION['retorno']['obj'];
    unset($_SESSION['retorno']['obj']);
}


$daoMenu = new DaoMenu();
$menus = $daoMenu->BuscarTodos($_SESSION['linguagem'],'');



if(isset($cod) && !empty($cod)){
    $objs = $daoInstitucional->BuscarPorCOD($cod);

    foreach ($objs as $obj){
        $p = $obj->getLinguagem();
        if(isset($aObj[$p])){
            $aObj[$p]->setTitulo($obj->getTitulo());
            $aObj[$p]->setId($obj->getId());
            $aObj[$p]->setTexto($obj->getTexto());
            $aObj[$p]->setImagem($obj->getImagem());
            $aObj[$p]->setAtivo($obj->getAtivo());
            $aObj[$p]->setIcone($obj->getIcone());
            $aObj[$p]->setOrdem($obj->getOrdem());
            $aObj[$p]->setDestaque($obj->getDestaque());
            $aObj[$p]->setSub_titulo($obj->getSub_titulo());
            $aObj[$p]->setLinguagem($obj->getLinguagem());
            $aObj[$p]->setLink($obj->getLink());
            $aObj[$p]->setLinkLogo($obj->getLinkLogo());
        }
    }

    $title = 'Alterar';

    $imagem = $obj->getImagem();
    $ordem = $obj->getOrdem();
    $ativo = $obj->getAtivo();
    $destaque = $obj->getDestaque();
    $icone = $obj->getIcone();
    $link = $obj->getLink();
    $LinkLogo = $obj->getLinkLogo();

     
                                               
}else{$cod='';}
$mn = $daoMenu->BuscarMenuInstitucional($cod); 
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- META SECTION -->
        <title>Área Administrativa <?php echo TITULO_SITE ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <link rel="icon" href="<?php echo URL_SITE ?>/admin/favicon.png" type="image/x-icon" />
        <!-- END META SECTION -->

        <!-- CSS INCLUDE -->
        <?php include(RAIZ.'/admin/includes/css.php') ?>
        <!-- EOF CSS INCLUDE -->
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">

            <!-- START PAGE SIDEBAR -->
            <?php include(RAIZ.'/admin/includes/sidebar.php') ?>
            <!-- END PAGE SIDEBAR -->

            <!-- PAGE CONTENT -->
            <div class="page-content">

                <!-- START X-NAVIGATION VERTICAL -->
                <?php include(RAIZ.'/admin/includes/nav-vertical.php') ?>
                <!-- END X-NAVIGATION VERTICAL -->

                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo URL_SITE ?>/admin/">Home</a></li>
                    <li><a href="<?php echo URL_SITE ?>/admin/view/institucional/institucional-list.php">Institucional</a></li>
                    <li class="active"><?php echo $title ?></li>
                </ul>
                <!-- END BREADCRUMB -->

                <!-- PAGE TITLE -->
                <div class="page-title">
                    <h2><span class="fa fa-align-left"></span> Institucional</h2>
                </div>
                <!-- END PAGE TITLE -->

                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">

                    <div class="row">
                        <div class="col-md-12">

                            <!-- START VALIDATIONENGINE PLUGIN -->
                            <div class="block">
                                <h4><?php echo $title ?></h4>
                                <div id="simple-msg" style="text-align: justify; margin-bottom: 50px">
                                <?php if(isset($_SESSION['retorno'])){?>
                                  <div style="padding: 10px; margin-top: 20px; text-align: justify" class="col-sm-12 <?php echo $_SESSION['retorno']['classe'] ?>">
                                      <?php echo $_SESSION['retorno']['mensagem'];?>
                                  </div>
                                  <br>
                                  <br>


                                <?php unset($_SESSION['retorno']); } ?>
                                </div>
                                <form enctype="multipart/form-data" role="form" method="post" class="form-horizontal" name="form_crud" id="form_crud" action="<?php echo URL_SITE ?>/admin/controle/institucional/_institucional.php">
                                    <ul class="nav nav-tabs" style="margin-bottom: 40px">
                                      <?php foreach ($linguagens as $linguagem){ ?>
                                        <li class="nav-item <?php if($linguagem->getId() == 1){ ?>active<?php }?>"><a class="nav-link" data-toggle="tab" href="#l<?php echo $linguagem->getId() ?>" role="tab"><?php echo $linguagem->getTitulo() ?></a></li>
                                      <?php }?>
                                    </ul>
                                    <div class="tab-content card">

                                    <div class="form-group">
                                            <label class="col-md-3 control-label">Menu relacionado à página:</label>
                                            <div class="col-md-3">
                                                <select class="select" name="id_institucional" required=""id="id_institucional">
                                                    <option value="">Escolha o Menu</option>
                                                    <?php 
                                                    
                                                    foreach ($menus as $menu){
                                                        
                                                        $menu_pai='';
                                                        
                                                
                                                        if($menu->getSubmenu()<>0){
                                                            $menu_ = $daoMenu->BuscarPorCODLing($menu->getSubmenu(), $_SESSION['linguagem']);
                                                            $menu_pai= $menu_->getTitulo()." -> ";                                                    
                                                        }    
                                                        if($menu->getLocal()=='1'){
                                                            $menu_final = 'Rodapé -> '.$menu->getTitulo();
                                                        }else{
                                                            $menu_final =$menu_pai.$menu->getTitulo();
                                                        }
                                                        ?>
                                                    <option <?php if($mn->getId() == $menu->getId()){echo 'selected';} ?> value="<?php echo $menu->getId() ?>"><?php echo $menu_final; ?></option>
                                                    <?php }?>
                                                </select>                           
                                                
                                            </div>                        
                                        </div>
                                        <?php foreach ($linguagens as $linguagem){ ?>
                                        <div class="tab-pane fade <?php if($linguagem->getId() == 1){ ?>in active<?php }?>" id="l<?php echo $linguagem->getId() ?>" role="tabpanel">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Titulo:</label>
                                        <div class="col-md-9">
                                            <input value="<?php echo $aObj[$linguagem->getId()]->getTitulo(); ?>" type="text" name="titulo[]" class="form-control" required=""/>

                                        </div>
                                    </div>
                                    <!-- <div class="form-group">
                                        <label class="col-md-3 control-label">Sub Titulo:</label>
                                        <div class="col-md-9">
                                            <input value="<?php echo $aObj[$linguagem->getId()]->getSub_titulo(); ?>" type="text" name="sub_titulo[]" class="form-control"/>

                                        </div>
                                    </div> -->
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Texto:</label>
                                        <div class="col-md-9">
                                            <textarea name="texto[]" class="summernote"><?php echo $aObj[$linguagem->getId()]->getTexto() ?></textarea>

                                        </div>
                                    </div>
                                    <input type="hidden" name="linguagem[]" value="<?php echo $linguagem->getId() ?>">
                                            </div>
                                        <?php }?>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Ordem:</label>
                                        <div class="col-md-9">
                                            <input value="<?php echo $ordem ?>" type="text" name="ordem" class="form-control" />
                                        </div>
                                    </div>
                                    <!-- <div class="form-group">
                                        <label class="col-md-3 control-label">Link:</label>
                                        <div class="col-md-9">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1">http://www.nelmadvogados.com/</span>
                                                <input value="<?php echo $link ?>" placeholder="link-da-pagina" type="text" name="link" class="form-control" />
                                            </div>
                                        </div>
                                    </div> -->
                                    <!-- <div class="form-group">
                                        <label class="col-md-3 control-label">Link do logo:</label>
                                        <div class="col-md-9">
                                            <input value="<?php echo $LinkLogo ?>" placeholder="Link do logo" type="text" name="link_logo" class="form-control" />
                                        </div>
                                    </div> -->
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Ativo:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="ativo" id="formGender">
                                                <option <?php if($ativo == 's'){ ?> selected=""<?php }?> value="s">Sim</option>
                                                <option <?php if($ativo == 'n'){ ?> selected=""<?php }?> value="n">Não</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- <div class="form-group">
                                        <label class="col-md-3 control-label">Home:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="destaque" id="formGender">
                                                <option <?php if($destaque == 's'){ ?> selected=""<?php }?> value="s">Sim</option>
                                                <option <?php if($destaque == 'n'){ ?> selected=""<?php }?> value="n">Não</option>
                                            </select>
                                        </div>
                                    </div> -->
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Imagem:</label>
                                        <div class="col-md-9">
                                            <input type="file" name="img_banner" accept="image/*">

                                        </div>
                                    </div>
                                    <?php if($imagem <> ''){ ?>
                                    <input type="hidden" name="old_img" value="<?php echo $imagem ?>">
                                    <div class="form-group">
                                         <label class="col-md-3 control-label">Alterar Imagem:</label>
                                         <label class="radio-inline"><input type="radio" name="alt_img" id="alt_img" value="s" >Sim</label>
                                         <label class="radio-inline"><input type="radio" name="alt_img" id="alt_img" value="n" checked>Não</label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Imagem Atual:</label>
                                        <div class="col-md-9">
                                            <img class="col-sm-6" src="<?php echo URL_CAMINHO_IMG.'/placeholders/'.$imagem ?>">

                                        </div>
                                    </div>
                                    <?php }?>
                                    <!-- <div class="form-group">
                                        <label class="col-md-3 control-label">Icone:</label>
                                        <div class="col-md-9">
                                            <input type="file" name="img_icone" accept="image/*">

                                        </div>
                                    </div> -->
                                    <?php if($icone <> ''){ ?>
                                    <!-- <input type="hidden" name="old_icone" value="<?php echo $icone ?>">
                                    <div class="form-group">
                                         <label class="col-md-3 control-label">Alterar Icone:</label>
                                         <label class="radio-inline"><input type="radio" name="alt_icone" id="alt_icone" value="s" >Sim</label>
                                         <label class="radio-inline"><input type="radio" name="alt_icone" id="alt_icone" value="n" checked>Não</label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Icone Atual:</label>
                                        <div class="col-md-2">
                                            <img class="col-sm-6" src="<?php echo URL_CAMINHO_IMG.'/institucional/'.$icone ?>">

                                        </div>
                                    </div> -->
                                    <?php }?>
                                    <div class="btn-group pull-right">
                                        <input type="hidden" name="acao" value="<?php echo strtolower($title) ?>">
                                        <input type="hidden" name="cod" value="<?php echo $cod ?>">
                                        <button class="btn btn-primary" type="submit"><?php echo $title ?></button>
                                    </div>
                                    <div class="form-group">

                                    </div>
                                </form>
                            </div>
                            <!-- END VALIDATIONENGINE PLUGIN -->
                        </div>
                    </div>


                </div>
                <!-- END PAGE CONTENT WRAPPER -->
            </div>
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->



        <!-- START PRELOADS -->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END PRELOADS -->

        <!-- MESSAGE BOX-->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END MESSAGE BOX-->

        <!-- START SCRIPTS -->
        <?php include(RAIZ.'/admin/includes/js.php') ?>
       <script type="text/javascript" src="<?php echo URL_SITE ?>/admin/js/plugins/summernote/summernote.js"></script>
        <script>
            var editor = CodeMirror.fromTextArea(document.getElementById("codeEditor"), {
                lineNumbers: true,
                matchBrackets: true,
                mode: "application/x-httpd-php",
                indentUnit: 4,
                indentWithTabs: true,
                enterMode: "keep",
                tabMode: "shift"
            });
            editor.setSize('100%','420px');
        </script>
    <!-- END SCRIPTS -->

    </body>
</html>
