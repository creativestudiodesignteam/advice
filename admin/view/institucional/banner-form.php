<?php
include("../../include_master.php");
use Dao\Classes\Banner;
use Dao\DaoBanner;
use Dao\DaoLinguagem;

#Pega variaveis vinda do formulÃ¡rio via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

$banner = new Banner();
$title = 'Cadastrar';

$daoBanner = new DaoBanner();
$daoLinguagem = new DaoLinguagem();

$linguagens = $daoLinguagem->BuscarTodos('n');

    $aObj = [];
    foreach ($linguagens as $l){
            $aObj[$l->getId()] = new Banner();
    }

$daoBanner = new DaoBanner();

$imagem = '';
$link = '';
$ordem = '';
$ativo = '';


if(isset($_SESSION['retorno']['obj']) && !empty($_SESSION['retorno']['obj'])){
    $banner = $_SESSION['retorno']['obj'];
    unset($_SESSION['retorno']['obj']);
}

if(isset($cod) && !empty($cod)){
    $objs = $daoBanner->BuscarPorCOD($cod);
    
    foreach ($objs as $obj){
        $p = $obj->getLinguagem();
        if(isset($aObj[$p])){
        $aObj[$p]->setTexto($obj->getTexto());
        $aObj[$p]->setTexto2($obj->getTexto2());
        $aObj[$p]->setId($obj->getId());
        $aObj[$p]->setImagem($obj->getImagem());
        $aObj[$p]->setAtivo($obj->getAtivo());
        $aObj[$p]->setOrdem($obj->getOrdem());
        $aObj[$p]->setLink($obj->getLink());
        $aObj[$p]->setLinguagem($obj->getLinguagem());
        $aObj[$p]->setTitulo($obj->getTitulo());
        }
    }
    $imagem = $obj->getImagem();
      $link = $obj->getLink();
     $ordem = $obj->getOrdem();
     $ativo = $obj->getAtivo();
     $title = 'Alterar';
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Área Administrativa <?php echo TITULO_SITE ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo URL_SITE ?>/admin/favicon.png" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <?php include(RAIZ.'/admin/includes/css.php') ?>
        <!-- EOF CSS INCLUDE -->                  
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <?php include(RAIZ.'/admin/includes/sidebar.php') ?>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <?php include(RAIZ.'/admin/includes/nav-vertical.php') ?>
                <!-- END X-NAVIGATION VERTICAL -->                    
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo URL_SITE ?>/admin/">Home</a></li>
                    <li><a href="<?php echo URL_SITE ?>/admin/view/institucional/banner-list.php">Banner</a></li>
                    <li class="active"><?php echo $title ?></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-align-left"></span> Banner</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START VALIDATIONENGINE PLUGIN -->
                            <div class="block">
                                <h4><?php echo $title ?></h4>
                                <div id="simple-msg" style="text-align: justify; margin-bottom: 50px">
                                <?php if(isset($_SESSION['retorno'])){?>
                                  <div style="padding: 10px; margin-top: 20px; text-align: justify" class="col-sm-12 <?php echo $_SESSION['retorno']['classe'] ?>">
                                      <?php echo $_SESSION['retorno']['mensagem'];?>
                                  </div>
                                  <br>
                                  <br>  
                                    
                                    
                                <?php unset($_SESSION['retorno']); } ?>
                                </div>
                                <form enctype="multipart/form-data" role="form" method="post" class="form-horizontal" name="form_crud" id="form_crud" action="<?php echo URL_SITE ?>/admin/controle/institucional/_banner.php">                            
                                    
                                     <ul class="nav nav-tabs">
                                      <?php foreach ($linguagens as $linguagem){ ?>
                                        <li class="nav-item <?php if($linguagem->getId() == 1){ ?>active<?php }?>"><a class="nav-link" data-toggle="tab" href="#l<?php echo $linguagem->getId() ?>" role="tab"><?php echo $linguagem->getTitulo() ?></a></li>
                                      <?php }?>
                                    </ul>
                                    <div class="tab-content card">
                                        <?php foreach ($linguagens as $linguagem){ ?>
                                            <div class="tab-pane fade <?php if($linguagem->getId() == 1){ ?>in active<?php }?>" id="l<?php echo $linguagem->getId() ?>" role="tabpanel">
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Titulo:</label>
                                                    <div class="col-md-9">
                                                        <input value="<?php echo $aObj[$linguagem->getId()]->getTitulo(); ?>" type="text" name="titulo[]" class="form-control" required=""/>
                                                        
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Titulo 2:</label>
                                                    <div class="col-md-9">
                                                        <input value="<?php echo $aObj[$linguagem->getId()]->getTexto(); ?>" type="text" name="texto[]" class="form-control" required=""/>
                                                        
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-3 control-label">Titulo 3:</label>
                                                    <div class="col-md-9">
                                                        <input value="<?php echo $aObj[$linguagem->getId()]->getTexto2(); ?>" type="text" name="texto2[]" class="form-control" />
                                                        
                                                    </div>
                                                </div>
                                                <input type="hidden" name="linguagem[]" value="<?php echo $linguagem->getId() ?>">
                                            </div>
                                        <?php }?>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Link:</label>
                                        <div class="col-md-9">
                                            <input type="text" value="<?php echo $link?>" name="link" class="form-control"/>
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Ordem:</label>
                                        <div class="col-md-9">
                                            <input type="text" value="<?php echo $ordem?>" name="ordem" class="form-control" required=""/>
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Ativo:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="ativo" id="formGender">
                                                <option <?php if($ativo == 's'){ ?> selected=""<?php }?> value="s">Sim</option>
                                                <option <?php if($ativo == 'n'){ ?> selected=""<?php }?> value="n">Não</option>
                                            </select>  
                                        </div>                        
                                    </div> 
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Banner:</label>
                                        <div class="col-md-9">
                                            <input type="file" name="img_banner" accept="image/*">
                                            <p>1920px x 1000px</p>
                                        </div>
                                    </div>
                                    <?php if($imagem <> ''){ ?>
                                    <div class="form-group">
                                         <label class="col-md-3 control-label">Alterar Imagem:</label>
                                         <label class="radio-inline"><input type="radio" name="alt_img" id="alt_img" value="s" >Sim</label>
                                         <label class="radio-inline"><input type="radio" name="alt_img" id="alt_img" value="n" checked>Não</label> 
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Imagem Atual:</label>
                                        <div class="col-md-9">
                                            <img class="col-sm-6" src="<?php echo URL_CAMINHO_IMG.'/banner/'.$imagem ?>">
                                            
                                        </div>
                                    </div>
                                    <?php }?>
                                    <div class="btn-group pull-right">
                                        <input type="hidden" name="acao" value="<?php echo strtolower($title) ?>">
                                        <input type="hidden" name="img_old" value="<?php echo $imagem ?>">
                                        <input type="hidden" name="cod" value="<?php echo $cod ?>">
                                        <button class="btn btn-primary" type="submit"><?php echo $title ?></button>
                                    </div>   
                                    <div class="form-group">
                                    
                                    </div>
                                </form>
                            </div>                                               
                            <!-- END VALIDATIONENGINE PLUGIN -->
                        </div>
                    </div>

                        
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
        
       

        <!-- START PRELOADS -->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END PRELOADS --> 
        
        <!-- MESSAGE BOX-->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END MESSAGE BOX-->
        
        <!-- START SCRIPTS -->
        <?php include(RAIZ.'/admin/includes/js.php') ?>
       
    <!-- END SCRIPTS -->          
        
    </body>
</html>






