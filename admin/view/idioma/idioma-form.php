<?php
include("../../include_master.php");
use Dao\Classes\Idioma;
use Dao\DaoIdioma;
use Dao\DaoLinguagem;

#Pega variaveis vinda do formulÃ¡rio via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

$obj = new Idioma();
$title = 'Cadastrar';

$daoIdioma = new DaoIdioma();

$daoLinguagem = new DaoLinguagem();
$linguagens = $daoLinguagem->BuscarTodos('n');

    $aObj = [];
    foreach ($linguagens as $l){
            $aObj[$l->getId()] = new Idioma();
    }

 

if(isset($_SESSION['retorno']['obj']) && !empty($_SESSION['retorno']['obj'])){
    $obj = $_SESSION['retorno']['obj'];
    unset($_SESSION['retorno']['obj']);
}

if(isset($cod) && !empty($cod)){
    $objs = $daoIdioma->BuscarPorCOD($cod);
    
    foreach ($objs as $obj){
        $p = $obj->getLinguagem();
        if(isset($aObj[$p])){
        $aObj[$p]->setTitulo($obj->getTitulo());
        $aObj[$p]->setLinguagem($obj->getLinguagem());
        $aObj[$p]->setAtivo($obj->getAtivo());
        $aObj[$p]->setId($obj->getId());
        }
    }
    $title = 'Alterar';
   
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Área Administrativa <?php echo TITULO_SITE ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo URL_SITE ?>/admin/favicon.png" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <?php include(RAIZ.'/admin/includes/css.php') ?>
        <!-- EOF CSS INCLUDE -->                  
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <?php include(RAIZ.'/admin/includes/sidebar.php') ?>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <?php include(RAIZ.'/admin/includes/nav-vertical.php') ?>
                <!-- END X-NAVIGATION VERTICAL -->                    
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo URL_SITE ?>/admin/">Home</a></li>
                    <li><a href="<?php echo URL_SITE ?>/admin/view/idioma/idioma-list.php">Idiomas</a></li>
                    <li class="active"><?php echo $title ?></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-bullseye"></span> Idioma</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START VALIDATIONENGINE PLUGIN -->
                            <div class="block">
                                <h4><?php echo $title ?></h4>
                                <div id="simple-msg" style="text-align: justify; margin-bottom: 50px">
                                <?php if(isset($_SESSION['retorno'])){?>
                                  <div style="padding: 10px; margin-top: 20px; text-align: justify" class="col-sm-12 <?php echo $_SESSION['retorno']['classe'] ?>">
                                      <?php echo $_SESSION['retorno']['mensagem'];?>
                                  </div>
                                  <br>
                                  <br>  
                                    
                                    
                                <?php unset($_SESSION['retorno']); } ?>
                                </div>
                                <form enctype="multipart/form-data" method="post" role="form" class="form-horizontal" name="form_crud" id="form_crud" action="<?php echo URL_SITE ?>/admin/controle/idioma/_idioma.php">                            
                                    
                                    <ul class="nav nav-tabs">
                                      <?php foreach ($linguagens as $linguagem){ ?>
                                        <li class="nav-item <?php if($linguagem->getId() == 1){ ?>active<?php }?>"><a class="nav-link" data-toggle="tab" href="#l<?php echo $linguagem->getId() ?>" role="tab"><?php echo $linguagem->getTitulo() ?></a></li>
                                      <?php }?>
                                    </ul>
                                    <div class="tab-content card">
                                        <?php foreach ($linguagens as $linguagem){ ?>
                                        <div class="tab-pane fade <?php if($linguagem->getId() == 1){ ?>in active<?php }?>" id="l<?php echo $linguagem->getId() ?>" role="tabpanel">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Idioma:</label>
                                                <div class="col-md-9">
                                                    <input value="<?php echo $aObj[$linguagem->getId()]->getTitulo() ?>" type="text" name="titulo[]" class="form-control" required=""/>
                                                </div>
                                            </div>  
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Ativo:</label>
                                                <div class="col-md-3">
                                                    <select class="select" name="ativo[]" id="formGender">
                                                        <option <?php if($aObj[$linguagem->getId()]->getAtivo() == 's'){ ?> selected=""<?php }?> value="s">Sim</option>
                                                        <option <?php if($aObj[$linguagem->getId()]->getAtivo() == 'n'){ ?> selected=""<?php }?> value="n">Não</option>
                                                    </select>  
                                                </div>                        
                                            </div>
                                            <input type="hidden" name="linguagem[]" value="<?php echo $linguagem->getId() ?>">   
                                            
                                                </div>
                                        <?php }?>
                                    </div>
                                    
                                    
                                    <div class="btn-group pull-right">
                                        <input type="hidden" name="acao" value="<?php echo strtolower($title) ?>">
                                        <input type="hidden" name="cod" value="<?php echo $cod ?>">   
                                        <button class="btn btn-primary" type="submit"><?php echo $title ?></button>
                                    </div>   
                                    <div class="form-group">
                                    
                                    </div>
                                </form>
                            </div>                                               
                            <!-- END VALIDATIONENGINE PLUGIN -->

                        </div>
                    </div>

                        
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
        
       

        <!-- START PRELOADS -->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END PRELOADS --> 
        
        <!-- MESSAGE BOX-->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END MESSAGE BOX-->
        
        <!-- START SCRIPTS -->
        <?php include(RAIZ.'/admin/includes/js.php') ?>
       
        
    <!-- END SCRIPTS -->          
        
    </body>
</html>






