<?php
include("../../include_master.php");
use Dao\Classes\Produto;
use Dao\DaoProduto;
use Dao\DaoLinguagem;
use Dao\DaoCategoria;
use Dao\DaoSegmento;
use Dao\DaoAplicacao;
use Dao\DaoCategoriaProd;
use Dao\DaoDocumentoTipo;
use Dao\DaoDocumento;
use Dao\Classes\Documento;

#Pega variaveis vinda do formulÃ¡rio via GET
foreach ($_GET as $campo => $valor) {
    $$campo = antiInjection($valor);
    $$campo = str_replace("'", "", $valor);
}

$obj = new Produto();
$title = 'Cadastrar';

$daoProduto = new DaoProduto();

$daoLinguagem = new DaoLinguagem();
$linguagens = $daoLinguagem->BuscarTodos('n');

$aObj = [];
foreach ($linguagens as $l) {
    $aObj[$l->getId()] = new Produto();
}

$daoCategoria = new DaoCategoria();
$categorias = $daoCategoria->BuscarTodos(1, 'n');

$daoSegmento = new DaoSegmento();
$segmentosxcat = [];

$daoAplicacao = new DaoAplicacao();
$aplicacaoxcat = [];

$daoCategoriaProd = new DaoCategoriaProd();
$categoriaProdxcat = [];

$daoDocumentoTipo = new DaoDocumentoTipo();
$tiposDocumentos = $daoDocumentoTipo->BuscarTodos('n');

$daoDocumento = new DaoDocumento();
$documento = new Documento();

if (isset($_SESSION['retorno']['obj']) && !empty($_SESSION['retorno']['obj'])) {
    $obj = $_SESSION['retorno']['obj'];
    unset($_SESSION['retorno']['obj']);
}

if (isset($cod) && !empty($cod)) {
    $objs = $daoProduto->BuscarPorCOD($cod);
    $segmentosxcat = $daoSegmento->BuscarCategoriaXSegmento(1, $cod);
    $aplicacaoxcat = $daoAplicacao->BuscarCategoriaXAplicacao(1, $cod);
    $documento = $daoDocumento->BuscarPorProduto($cod);

    foreach ($objs as $obj) {
        $p = $obj->getLinguagem();
        if (isset($aObj[$p])) {
            $aObj[$p]->setTitulo($obj->getTitulo());
            $aObj[$p]->setLinguagem($obj->getLinguagem());
            $aObj[$p]->setAtivo($obj->getAtivo());
            $aObj[$p]->setId($obj->getId());
        }
    }
    $title = 'Alterar';
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- META SECTION -->
    <title>Área Administrativa <?php echo TITULO_SITE ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="favicon2.ico" type="image/x-icon" />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <?php include(RAIZ . '/admin/includes/css.php') ?>
    <!-- EOF CSS INCLUDE -->
</head>

<body>
    <!-- START PAGE CONTAINER -->
    <div class="page-container">

        <!-- START PAGE SIDEBAR -->
        <?php include(RAIZ . '/admin/includes/sidebar.php') ?>
        <!-- END PAGE SIDEBAR -->

        <!-- PAGE CONTENT -->
        <div class="page-content">

            <!-- START X-NAVIGATION VERTICAL -->
            <?php include(RAIZ . '/admin/includes/nav-vertical.php') ?>
            <!-- END X-NAVIGATION VERTICAL -->

            <!-- START BREADCRUMB -->
            <ul class="breadcrumb">
                <li><a href="<?php echo URL_SITE ?>/admin/">Home</a></li>
                <li><a href="<?php echo URL_SITE ?>/admin/view/produtos/produtos-list.php">Produtos</a></li>
                <li class="active"><?php echo $title ?></li>
            </ul>
            <!-- END BREADCRUMB -->

            <!-- PAGE TITLE -->
            <div class="page-title">
                <h2><span class="fa fa-bullseye"></span> Produto</h2>
            </div>
            <!-- END PAGE TITLE -->

            <!-- PAGE CONTENT WRAPPER -->
            <div class="page-content-wrap">

                <div class="row">
                    <div class="col-md-12">

                        <!-- START VALIDATIONENGINE PLUGIN -->
                        <div class="block">
                            <h4><?php echo $title ?></h4>
                            <div id="simple-msg" style="text-align: justify; margin-bottom: 50px">
                                <?php if (isset($_SESSION['retorno'])) { ?>
                                    <div style="padding: 10px; margin-top: 20px; text-align: justify" class="col-sm-12 <?php echo $_SESSION['retorno']['classe'] ?>">
                                        <?php echo $_SESSION['retorno']['mensagem']; ?>
                                    </div>
                                    <br>
                                    <br>


                                    <?php unset($_SESSION['retorno']);
                                } ?>
                            </div>
                            <form enctype="multipart/form-data" method="post" role="form" class="form-horizontal" name="form_crud" id="form_crud" action="<?php echo URL_SITE ?>/admin/controle/produtos/_produtos.php">

                                <ul class="nav nav-tabs">
                                    <?php foreach ($linguagens as $linguagem) { ?>
                                        <li class="nav-item <?php if ($linguagem->getId() == 1) { ?>active<?php } ?>"><a class="nav-link" data-toggle="tab" href="#l<?php echo $linguagem->getId() ?>" role="tab"><?php echo $linguagem->getTitulo() ?></a></li>
                                    <?php } ?>
                                </ul>
                                <div class="tab-content card">
                                    <?php foreach ($linguagens as $linguagem) { ?>
                                        <div class="tab-pane fade <?php if ($linguagem->getId() == 1) { ?>in active<?php } ?>" id="l<?php echo $linguagem->getId() ?>" role="tabpanel">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Produto:</label>
                                                <div class="col-md-9">
                                                    <input value="<?php echo $aObj[$linguagem->getId()]->getTitulo() ?>" type="text" name="produto[]" class="form-control" <?php if ($linguagem->getId() == 1) { ?>required=""<?php } ?> />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Farmacopéias:</label>
                                                <div class="col-md-9">
                                                    <input value="<?php echo $aObj[$linguagem->getId()]->getFarmacopeias() ?>" type="text" name="farmacopeias[]" class="form-control" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Ativo:</label>
                                                <div class="col-md-3">
                                                    <select class="select" name="ativo[]" id="formGender">
                                                        <option <?php if ($aObj[$linguagem->getId()]->getAtivo() == 's') { ?> selected="" <?php } ?> value="s">Sim</option>
                                                        <option <?php if ($aObj[$linguagem->getId()]->getAtivo() == 'n') { ?> selected="" <?php } ?> value="n">Não</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <input type="hidden" name="linguagem[]" value="<?php echo $linguagem->getId() ?>">

                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Tipo de documento:</label>
                                    <div class="col-md-3">
                                        <select class="select" name="tipo_doc" id="formGender">
                                            <option value="">Escolha um Tipo</option>
                                            <?php foreach ($tiposDocumentos as $c) { ?>
                                                <option <?php if ($documento->getTipo() == $c->getId()) {
                                                            echo 'selected';
                                                        } ?> value="<?php echo $c->getId() ?>"><?php echo $c->getTitulo() ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Anexe aqui o documento: :</label>
                                    <div class="col-md-9">
                                        <input type="file" name="upload" accept="image/*">

                                    </div>
                                </div>
                                <?php if ($documento->getDocumento() <> '') { ?>
                                    <input type="hidden" name="old_img" value="<?php echo $documento->getDocumento() ?>">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Alterar documento:</label>
                                        <label class="radio-inline"><input type="radio" name="alt_img" id="alt_img" value="s">Sim</label>
                                        <label class="radio-inline"><input type="radio" name="alt_img" id="alt_img" value="n" checked>Não</label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Documento Atual:</label>
                                        <div class="col-md-9">
                                            <a class="col-sm-6" href="<?php echo URL_SITE . '/area-do-cliente/' . $documento->getDir() . $documento->getDocumento() ?>">
                                                <?php echo $documento->getDocumento() ?>
                                            </a>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button id="add_linha" onclick='adiciona_segmentos()' class="btn btn-success" type="button"><i class="fa fa fa-plus" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                                <table class="table table-striped" id="tabela_1">
                                    <thead>
                                        <tr>
                                            <td><strong>Segmento</strong></td>
                                            <td><strong>Categoria</strong></td>
                                            <td><strong>Remover</strong></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $o = 0;
                                        foreach ($segmentosxcat as $s) {
                                            $segmentoCat = $daoSegmento->BuscarTodosCategoria(1, $s->getCategoria());
                                            ?>
                                            <tr id="linha_prog">
                                                <td>
                                                    <select name="categoria_segmento[]" class="categoria0 form-control" onchange="carregaSegmento(this.value,'<?php echo $o ?>-s')" id="formGender">
                                                        <option value="">Selecione uma categoria</option>
                                                        <?php foreach ($categorias as $c) { ?>
                                                            <option <?php if ($s->getCategoria() == $c->getId()) {
                                                                        echo 'selected';
                                                                    } ?> value="<?php echo $c->getId() ?>"><?php echo $c->getTitulo() ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div id='load_segmento<?php echo $o ?>-s'>
                                                        <select name="segmentos[]" class="form-control" id="formGender">
                                                            <?php foreach ($segmentoCat as $c) { ?>
                                                                <option <?php if ($s->getId() == $c->getId()) {
                                                                            echo 'selected';
                                                                        } ?> value="<?php echo $c->getId() ?>"><?php echo $c->getTitulo() ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td>
                                                    <button class="btn btn-danger" onclick="RemoveTableRow(this)" type="button"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                </td>
                                            </tr>
                                            <?php $o++;
                                        } ?>
                                        <tr id="linha_prog">
                                            <td>
                                                <select name="categoria_segmento[]" class="categoria0 form-control" onchange="carregaSegmento(this.value,0)" id="formGender">
                                                    <option value="">Selecione uma categoria</option>
                                                    <?php foreach ($categorias as $c) { ?>
                                                        <option value="<?php echo $c->getId() ?>"><?php echo $c->getTitulo() ?></option>
                                                    <?php } ?>
                                                </select>
                                            </td>
                                            <td>
                                                <div id='load_segmento0'>
                                                    <select name="segmentos[]" class="form-control" id="formGender">
                                                        <option value="">Selecione uma categoria</option>
                                                    </select>
                                                </div>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger" onclick="RemoveTableRow(this)" type="button"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button id="add_linha" onclick='adiciona_sub_apli()' class="btn btn-success" type="button"><i class="fa fa fa-plus" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                                <table class="table table-striped" id="sub_apli">
                                    <thead>
                                        <tr>
                                            <td><strong>Segmento</strong></td>
                                            <td><strong>Sub-Categoria</strong></td>
                                            <td><strong>Aplicação</strong></td>
                                            <td><strong>Remover</strong></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $o = 0;
                                        foreach ($aplicacaoxcat as $s) {
                                            $aplicacao = $daoAplicacao->BuscarTodosCategoria(1, $s->getCategoria());
                                            $subcategoria = $daoCategoriaProd->BuscarTodosCategoria(1, $s->getCategoria());
                                            ?>
                                            <tr id="linha_prog">
                                                <td>
                                                    <select name="categoria2[]" class="categoria0 form-control" onchange="carregaSubCatxAplicacao(this.value,'<?php echo $o ?>-s')" id="formGender">
                                                        <option value="">Selecione uma categoria</option>
                                                        <?php foreach ($categorias as $c) { ?>
                                                            <option <?php if ($s->getCategoria() == $c->getId()) {
                                                                        echo 'selected';
                                                                    } ?> value="<?php echo $c->getId() ?>"><?php echo $c->getTitulo() ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <div id='load_sub<?php echo $o ?>-s'>
                                                        <select name="subcategoria[]" class="form-control" id="formGender">
                                                            <option value="">Selecione uma <?php echo $s->getCategoria_prod() ?></option>
                                                            <?php foreach ($subcategoria as $c) { ?>
                                                                <option <?php if ($s->getCategoria_prod() == $c->getId()) {
                                                                            echo 'selected';
                                                                        } ?> value="<?php echo $c->getId() ?>"><?php echo $c->getTitulo() ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div id='load_aplicacao<?php echo $o ?>-s'>
                                                        <select name="aplicacao[]" class="form-control" id="formGender">
                                                            <option value="">Selecione uma categoria</option>
                                                            <?php foreach ($aplicacao as $c) { ?>
                                                                <option <?php if ($s->getId() == $c->getId()) {
                                                                            echo 'selected';
                                                                        } ?> value="<?php echo $c->getId() ?>"><?php echo $c->getTitulo() ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </td>
                                                <td>
                                                    <button class="btn btn-danger" onclick="RemoveTableRow(this)" type="button"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>

                                <div class="btn-group pull-right">
                                    <input type="hidden" name="acao" value="<?php echo strtolower($title) ?>">
                                    <input type="hidden" name="cod" value="<?php echo $cod ?>">
                                    <button class="btn btn-primary" type="submit"><?php echo $title ?></button>
                                </div>
                                <div class="form-group">

                                </div>
                            </form>
                        </div>
                        <!-- END VALIDATIONENGINE PLUGIN -->

                    </div>
                </div>


            </div>
            <!-- END PAGE CONTENT WRAPPER -->
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->



    <!-- MESSAGE BOX-->
    <?php include(RAIZ . '/admin/includes/msg-deslogar.php') ?>
    <!-- END MESSAGE BOX-->

    <!-- START SCRIPTS -->
    <?php include(RAIZ . '/admin/includes/js.php') ?>

    <script>
        totals = 0;

        function adiciona_segmentos() {
            totals++
            tbl = document.getElementById("tabela_1")

            var novaLinha = tbl.insertRow(-1);
            var novaCelula;

            if (totals % 2 == 0) cl = "#F5E9EC";
            else cl = "#FBF6F7";

            novaCelula = novaLinha.insertCell(0);
            novaCelula.align = "left";
            novaCelula.innerHTML = "<select onchange='carregaSegmento(this.value," + totals + ")' class=\"form-control categoria" + totals + "\" name=\"categoria[]\" id=\"categoria" + totals + "\"><option value=\"\">Selecione uma categoria</option><?php foreach ($categorias as $c) { ?> <option value=\"<?php echo $c->getId() ?>\"><?php echo $c->getTitulo() ?></option><?php } ?></select>";

            novaCelula = novaLinha.insertCell(1);
            novaCelula.align = "left";
            novaCelula.innerHTML = "<div id='load_segmento" + totals + "'>" +
                "<select class='segmento0 form-control' name='segmento[]' id='segmento" + totals + "\"'>" +
                "<option value=''></option>" +
                "</select>" +
                "</div>";

            novaCelula = novaLinha.insertCell(2);
            novaCelula.align = "left";
            novaCelula.innerHTML = "<button class=\"btn btn-danger\" onclick=\"RemoveTableRow(this)\" type=\"button\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></button>";
        }



        total = 0;

        function adiciona_sub_apli() {
            total++
            tbl = document.getElementById("sub_apli")

            var novaLinha = tbl.insertRow(-1);
            var novaCelula;

            if (total % 2 == 0) cl = "#F5E9EC";
            else cl = "#FBF6F7";

            novaCelula = novaLinha.insertCell(0);
            novaCelula.align = "left";
            novaCelula.innerHTML = "<select onchange='carregaSubCatxAplicacao(this.value," + total + ")' class=\"form-control categoria2" + total + "\" name=\"categoria2[]\" id=\"categoria2" + total + "\"><option value=\"\">Selecione uma categoria</option><?php foreach ($categorias as $c) { ?> <option value=\"<?php echo $c->getId() ?>\"><?php echo $c->getTitulo() ?></option><?php } ?></select>";

            novaCelula = novaLinha.insertCell(1);
            novaCelula.align = "left";
            novaCelula.innerHTML = "<div id='load_sub" + total + "'>" +
                "<select class='subcategoria0 form-control' name='subcategoria[]' id='subcategoria" + total + "\"'>" +
                "<option value=''></option>" +
                "</select>" +
                "</div>";

            novaCelula = novaLinha.insertCell(2);
            novaCelula.align = "left";
            novaCelula.innerHTML = "<div id='load_aplicacao" + total + "'>" +
                "<select class='segmento0 form-control' name='segmento[]' id='segmento" + total + "\"'>" +
                "<option value=''></option>" +
                "</select>" +
                "</div>";

            novaCelula = novaLinha.insertCell(3);
            novaCelula.align = "left";
            novaCelula.innerHTML = "<button class=\"btn btn-danger\" onclick=\"RemoveTableRow(this)\" type=\"button\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></button>";
        }

        function carregaSegmento(categoria, linha) {
            var url = "<?php echo URL_SITE ?>/admin/controle/produtos/_segmentos.php?acao=busca&categoria=" + categoria + "&linha=" + linha;
            $.get(url, function(dataReturn) {
                $('#load_segmento' + linha).html(dataReturn);
            });
        }

        function carregaSubCatxAplicacao(categoria, linha) {
            carregaAplicacao(categoria, linha);
            carregaSubCat(categoria, linha);
        }

        function carregaAplicacao(categoria, linha) {
            var url = "<?php echo URL_SITE ?>/admin/controle/produtos/_aplicacao.php?acao=busca&categoria=" + categoria + "&linha=" + linha;
            $.get(url, function(dataReturn) {
                $('#load_aplicacao' + linha).html(dataReturn);
            });
        }

        function carregaSubCat(categoria, linha) {
            var url = "<?php echo URL_SITE ?>/admin/controle/produtos/_subcategorias.php?acao=busca&categoria=" + categoria + "&linha=" + linha;
            $.get(url, function(dataReturn) {
                $('#load_sub' + linha).html(dataReturn);
            });
        }

        (function($) {

            RemoveTableRow = function(handler) {
                var tr = $(handler).closest('tr');

                tr.fadeOut(400, function() {
                    tr.remove();
                });

                return false;
            };
        })(jQuery);
    </script>
    <!-- END SCRIPTS -->

</body>

</html>