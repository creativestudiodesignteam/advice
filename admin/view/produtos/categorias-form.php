<?php
include("../../include_master.php");
use Dao\Classes\Categoria;
use Dao\DaoCategoria;
use Dao\DaoLinguagem;

#Pega variaveis vinda do formulÃ¡rio via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

$obj = new Categoria();
$title = 'Cadastrar';

$daoCategoria = new DaoCategoria();

$daoLinguagem = new DaoLinguagem();
$linguagens = $daoLinguagem->BuscarTodos('n');

    $aObj = [];
    foreach ($linguagens as $l){
            $aObj[$l->getId()] = new Categoria();
    }

 

if(isset($_SESSION['retorno']['obj']) && !empty($_SESSION['retorno']['obj'])){
    $obj = $_SESSION['retorno']['obj'];
    unset($_SESSION['retorno']['obj']);
}

$imagem = '';
$background = '';
$img_adicional_um = '';
$img_adicional_dois = '';


if(isset($cod) && !empty($cod)){
    $objs = $daoCategoria->BuscarPorCOD($cod);
    
    foreach ($objs as $obj){
        $p = $obj->getLinguagem();
        if(isset($aObj[$p])){
            $aObj[$p]->setTitulo($obj->getTitulo());
            $aObj[$p]->setLinguagem($obj->getLinguagem());
            $aObj[$p]->setAtivo($obj->getAtivo());
            $aObj[$p]->setEmail($obj->getEmail());
            $aObj[$p]->setTitulo_img_1($obj->getTitulo_img_1());
            $aObj[$p]->setTitulo_img_2($obj->getTitulo_img_2());
            $aObj[$p]->setTitulo_img_3($obj->getTitulo_img_3());
            $aObj[$p]->setTitulo_img_4($obj->getTitulo_img_4());
            $aObj[$p]->setId($obj->getId());
        }

        $imagem = $obj->getImagem();
        $background = $obj->getBackground();
        $img_adicional_um = $obj->getImg_adicional_um();
        $img_adicional_dois = $obj->getImg_adicional_dois();
        $img_adicional_tres = $obj->getImg_adicional_tres();
    }

    $title = 'Alterar';
   
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Área Administrativa <?php echo TITULO_SITE ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon2.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <?php include(RAIZ.'/admin/includes/css.php') ?>
        <!-- EOF CSS INCLUDE -->                  
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <?php include(RAIZ.'/admin/includes/sidebar.php') ?>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <?php include(RAIZ.'/admin/includes/nav-vertical.php') ?>
                <!-- END X-NAVIGATION VERTICAL -->                    
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo URL_SITE ?>/admin/">Home</a></li>
                    <li><a href="<?php echo URL_SITE ?>/admin/view/produtos/categorias-list.php">Categorias</a></li>
                    <li class="active"><?php echo $title ?></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-bullseye"></span> Categoria</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START VALIDATIONENGINE PLUGIN -->
                            <div class="block">
                                <h4><?php echo $title ?></h4>
                                <div id="simple-msg" style="text-align: justify; margin-bottom: 50px">
                                <?php if(isset($_SESSION['retorno'])){?>
                                  <div style="padding: 10px; margin-top: 20px; text-align: justify" class="col-sm-12 <?php echo $_SESSION['retorno']['classe'] ?>">
                                      <?php echo $_SESSION['retorno']['mensagem'];?>
                                  </div>
                                  <br>
                                  <br>  
                                    
                                    
                                <?php unset($_SESSION['retorno']); } ?>
                                </div>
                                <form enctype="multipart/form-data" method="post" role="form" class="form-horizontal" name="form_crud" id="form_crud" action="<?php echo URL_SITE ?>/admin/controle/produtos/_categorias.php">                            
                                    
                                    <ul class="nav nav-tabs">
                                      <?php foreach ($linguagens as $linguagem){ ?>
                                        <li class="nav-item <?php if($linguagem->getId() == 1){ ?>active<?php }?>"><a class="nav-link" data-toggle="tab" href="#l<?php echo $linguagem->getId() ?>" role="tab"><?php echo $linguagem->getTitulo() ?></a></li>
                                      <?php }?>
                                    </ul>
                                    <div class="tab-content card">
                                        <?php foreach ($linguagens as $linguagem){ ?>
                                        <div class="tab-pane fade <?php if($linguagem->getId() == 1){ ?>in active<?php }?>" id="l<?php echo $linguagem->getId() ?>" role="tabpanel">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Categoria:</label>
                                                <div class="col-md-9">
                                                    <input value="<?php echo $aObj[$linguagem->getId()]->getTitulo() ?>" type="text" name="categoria[]" class="form-control"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Titulo Imagem adicional 1:</label>
                                                <div class="col-md-9">
                                                    <input value="<?php echo $aObj[$linguagem->getId()]->getTitulo_img_1() ?>" type="text" name="titulo_img_1[]" class="form-control"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Titulo Imagem adicional 2:</label>
                                                <div class="col-md-9">
                                                    <input value="<?php echo $aObj[$linguagem->getId()]->getTitulo_img_2() ?>" type="text" name="titulo_img_2[]" class="form-control"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Titulo Imagem adicional 3:</label>
                                                <div class="col-md-9">
                                                    <input value="<?php echo $aObj[$linguagem->getId()]->getTitulo_img_3() ?>" type="text" name="titulo_img_3[]" class="form-control"/>
                                                </div>
                                            </div> 
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Titulo Imagem adicional 4:</label>
                                                <div class="col-md-9">
                                                    <input value="<?php echo $aObj[$linguagem->getId()]->getTitulo_img_4() ?>" type="text" name="titulo_img_4[]" class="form-control"/>
                                                </div>
                                            </div> 
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Ativo:</label>
                                                <div class="col-md-3">
                                                    <select class="select" name="ativo[]" id="formGender">
                                                        <option <?php if($aObj[$linguagem->getId()]->getAtivo() == 's'){ ?> selected=""<?php }?> value="s">Sim</option>
                                                        <option <?php if($aObj[$linguagem->getId()]->getAtivo() == 'n'){ ?> selected=""<?php }?> value="n">Não</option>
                                                    </select>  
                                                </div>                        
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">E-mail:</label>
                                                <div class="col-md-9">
                                                    <textarea name="email[]" class="form-control"><?php echo $aObj[$linguagem->getId()]->getEmail() ?></textarea>
                                                    
                                                </div>
                                            </div>
                                            <input type="hidden" name="linguagem[]" value="<?php echo $linguagem->getId() ?>">   
                                            
                                                </div>
                                        <?php }?>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Icone:</label>
                                        <div class="col-md-9">
                                            <input type="file" name="img_noticia" accept="image/*">
                                            
                                        </div>
                                    </div>
                                    <?php if($imagem <> ''){ ?>
                                    <input type="hidden" name="old_img" value="<?php echo $imagem ?>">
                                    <div class="form-group">
                                         <label class="col-md-3 control-label">Alterar Icone:</label>
                                         <label class="radio-inline"><input type="radio" name="alt_img" id="alt_img" value="s" >Sim</label>
                                         <label class="radio-inline"><input type="radio" name="alt_img" id="alt_img" value="n" checked>Não</label> 
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Icone Atual:</label>
                                        <div class="col-md-9">
                                            <img class="col-sm-1" src="<?php echo URL_CAMINHO_IMG.'/segment/'.$imagem ?>">
                                            
                                        </div>
                                    </div>
                                    <?php }?>
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="col-md-6 control-label">Imagem adicional 1:</label>
                                                <div class="col-md-6">
                                                    <input type="file" name="img_background" accept="image/*">

                                                </div>
                                            </div>
                                            <?php if($background <> ''){ ?>
                                                <input type="hidden" name="old_background" value="<?php echo $background ?>">
                                                <div class="form-group">
                                                    <label class="col-md-8 control-label">Alterar Imagem adicional 1:</label>
                                                    <label class="radio-inline"><input type="radio" name="alt_background" id="alt_background" value="s" >Sim</label>
                                                    <label class="radio-inline"><input type="radio" name="alt_background" id="alt_background" value="n" checked>Não</label>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-6 control-label">Imagem adicional 1 Atual:</label>
                                                    <div class="col-md-6">
                                                        <img class="col-sm-12" src="<?php echo URL_CAMINHO_IMG.'/segment/'.$background ?>">

                                                    </div>
                                                </div>
                                            <?php }?>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="col-md-6 control-label">Imagem adicional 2:</label>
                                                <div class="col-md-6">
                                                    <input type="file" name="img_adicional_um" accept="image/*">

                                                </div>
                                            </div>
                                            <?php if($img_adicional_um <> ''){ ?>
                                                <input type="hidden" name="old_img_adicional_um" value="<?php echo $img_adicional_um ?>">
                                                <div class="form-group">
                                                    <label class="col-md-8 control-label">Alterar Imagem adicional 2:</label>
                                                    <label class="radio-inline"><input type="radio" name="alt_img_adicional_um" id="alt_img_adicional_um" value="s" >Sim</label>
                                                    <label class="radio-inline"><input type="radio" name="alt_img_adicional_um" id="alt_img_adicional_um" value="n" checked>Não</label>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-6 control-label">Imagem adicional 2 Atual:</label>
                                                    <div class="col-md-6">
                                                        <img class="col-sm-12" src="<?php echo URL_CAMINHO_IMG.'/segment/'.$img_adicional_um ?>">

                                                    </div>
                                                </div>
                                            <?php }?>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="col-md-6 control-label">Imagem adicional 3:</label>
                                                <div class="col-md-6">
                                                    <input type="file" name="img_adicional_dois" accept="image/*">

                                                </div>
                                            </div>
                                            <?php if($img_adicional_dois <> ''){ ?>
                                                <input type="hidden" name="old_img_adicional_dois" value="<?php echo $img_adicional_dois ?>">
                                                <div class="form-group">
                                                    <label class="col-md-8 control-label">Alterar Imagem adicional 3:</label>
                                                    <label class="radio-inline"><input type="radio" name="alt_img_adicional_dois" id="alt_img_adicional_dois" value="s" >Sim</label>
                                                    <label class="radio-inline"><input type="radio" name="alt_img_adicional_dois" id="alt_img_adicional_dois" value="n" checked>Não</label>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-6 control-label">Imagem adicional 3 Atual:</label>
                                                    <div class="col-md-6">
                                                        <img class="col-sm-12" src="<?php echo URL_CAMINHO_IMG.'/segment/'.$img_adicional_dois ?>">

                                                    </div>
                                                </div>
                                            <?php }?>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="col-md-6 control-label">Imagem adicional 4:</label>
                                                <div class="col-md-6">
                                                    <input type="file" name="img_adicional_tres" accept="image/*">

                                                </div>
                                            </div>
                                            <?php if($img_adicional_tres <> ''){ ?>
                                                <input type="hidden" name="old_img_adicional_tres" value="<?php echo $img_adicional_tres ?>">
                                                <div class="form-group">
                                                    <label class="col-md-8 control-label">Alterar Imagem adicional 4:</label>
                                                    <label class="radio-inline"><input type="radio" name="alt_img_adicional_tres" id="alt_img_adicional_tres" value="s" >Sim</label>
                                                    <label class="radio-inline"><input type="radio" name="alt_img_adicional_tres" id="alt_img_adicional_tres" value="n" checked>Não</label>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-6 control-label">Imagem adicional 4 Atual:</label>
                                                    <div class="col-md-6">
                                                        <img class="col-sm-12" src="<?php echo URL_CAMINHO_IMG.'/segment/'.$img_adicional_tres ?>">

                                                    </div>
                                                </div>
                                            <?php }?>
                                        </div>
                                    </div>




                                    <div class="btn-group pull-right">
                                        <input type="hidden" name="acao" value="<?php echo strtolower($title) ?>">
                                        <input type="hidden" name="cod" value="<?php echo $cod ?>">   
                                        <button class="btn btn-primary" type="submit"><?php echo $title ?></button>
                                    </div>   
                                    <div class="form-group">
                                    
                                    </div>
                                </form>
                            </div>                                               
                            <!-- END VALIDATIONENGINE PLUGIN -->

                        </div>
                    </div>

                        
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
        
       

        <!-- START PRELOADS -->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END PRELOADS --> 
        
        <!-- MESSAGE BOX-->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END MESSAGE BOX-->
        
        <!-- START SCRIPTS -->
        <?php include(RAIZ.'/admin/includes/js.php') ?>
       
        
    <!-- END SCRIPTS -->          
        
    </body>
</html>






