<?php
include("../../include_master.php");

use Dao\Classes\UsuarioGrupo;
use Dao\DaoUsuarioGrupo;
use Dao\DaoUsuarioPaginas;

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

$grupo = new UsuarioGrupo();
$title = 'Cadastrar';

$daoUsuarioGrupo = new DaoUsuarioGrupo();

$daoUsuarioPaginas = new DaoUsuarioPaginas();

$paginas = $daoUsuarioPaginas->BuscarTodos(0);



if(isset($cod) && !empty($cod)){
    $grupo = $daoUsuarioGrupo->BuscarPorCOD($cod);
    $title = 'Alterar';
   
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Área Administrativa <?php echo TITULO_SITE ?></title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo URL_SITE ?>/admin/favicon.png" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <?php include(RAIZ.'/admin/includes/css.php') ?>
        <!-- EOF CSS INCLUDE -->                  
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <?php include(RAIZ.'/admin/includes/sidebar.php') ?>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <?php include(RAIZ.'/admin/includes/nav-vertical.php') ?>
                <!-- END X-NAVIGATION VERTICAL -->                    
                
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo URL_SITE ?>/admin/">Home</a></li>
                    <li><a href="<?php echo URL_SITE ?>/admin/view/usuario/grupo-usuario-list.php">Grupo de Usuários</a></li>
                    <li class="active"><?php echo $title ?></li>
                </ul>
                <!-- END BREADCRUMB -->
                
                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-users"></span> Grupo de Usuários</h2>
                </div>
                <!-- END PAGE TITLE -->                
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">

                            <!-- START VALIDATIONENGINE PLUGIN -->
                            <div class="block">
                                <h4><?php echo $title ?></h4>
                                <div id="simple-msg" style="text-align: justify; margin-bottom: 50px"></div>
                                <form role="form" class="form-horizontal" name="form_crud" id="form_crud" action="<?php echo URL_SITE ?>/admin/controle/usuario/_grupo-usuario.php">                            
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Grupo:</label>
                                        <div class="col-md-9">
                                            <input value="<?php echo $grupo->getGrupo() ?>" type="text" name="nome" class="form-control" required=""/>
                                            
                                        </div>
                                    </div>   
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Páginas:</label>
                                        <div class="col-md-3">
                                            <?php foreach ($paginas as $rsCat){?>
                                                <div class="col-sm-12">    
                                                    <input <?php echo $daoUsuarioGrupoPaginas->GrupoxPag($grupo->getId(),$rsCat->getId()) ?> class="marcar_sindicato" name="pagina[]" type="checkbox" value="<?php echo $rsCat->getId();?>"> <?php echo $rsCat->getPagina();?>
                                                </div>
                                            <?php } ?>                          
                                            
                                        </div>                        
                                    </div>
                                    <div class="form-group">
                                         <label class="col-md-3 control-label">Ativo:</label>
                                         <label class="radio-inline"><input type="radio" name="ativo" id="ativo" value="s" <?php if($grupo->getAtivo() == 's'){ ?> checked<?php }?>>Sim</label>
                                         <label class="radio-inline"><input type="radio" name="ativo" id="ativo" value="n" <?php if($grupo->getAtivo() == 'n'){ ?> checked<?php }?>>Não</label>  
                                    </div>
                                    <div class="btn-group pull-right">
                                        <input type="hidden" name="acao" value="<?php echo strtolower($title) ?>">
                                        <input type="hidden" name="cod" value="<?php echo $grupo->getId() ?>">
                                        <button class="btn btn-primary" type="button" onclick="crud('#form_crud')"><?php echo $title ?></button>
                                    </div>   
                                    <div class="form-group">
                                    
                                    </div>
                                </form>
                            </div>                                               
                            <!-- END VALIDATIONENGINE PLUGIN -->

                        </div>
                    </div>

                        
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->
        
       

        <!-- START PRELOADS -->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END PRELOADS --> 
        
        <!-- MESSAGE BOX-->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END MESSAGE BOX-->
        
        <!-- START SCRIPTS -->
        <?php include(RAIZ.'/admin/includes/js.php') ?>
       
        
    <!-- END SCRIPTS -->          
        
    </body>
</html>






