<?php
include("../../include_master.php");
use Dao\Classes\Noticia;
use Dao\DaoLinguagem;
use Dao\DaoNoticia;
use Dao\DaoCategoria;

#Pega variaveis vinda do formulÃ¡rio via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}
$_SESSION['TIPO'] = '';
$_SESSION['FK'] = '';

$noticia = new Noticia();
$title = 'Cadastrar';
$noticia->setCadastro(date('Y-m-d H:i:s'));

$daoLinguagem = new DaoLinguagem();
$daoNoticia = new DaoNoticia();

$daoCategoria = new DaoCategoria();
$categorias = $daoCategoria->BuscarTodos($_SESSION['linguagem'], 'n');

$linguagens = $daoLinguagem->BuscarTodos('n');

    $aObj = [];
    foreach ($linguagens as $l){
        $aObj[$l->getId()] = new Noticia();

    }


if(isset($_SESSION['retorno']['obj']) && !empty($_SESSION['retorno']['obj'])){
    $noticia = $_SESSION['retorno']['obj'];
    $grp = $daoCategoria->BuscarPorCOD($noticia->getCategoria());
    unset($_SESSION['retorno']['obj']);
}

$imagem = '';
$cat = '';
$adv = '';
$id = '';

if(isset($cod) && !empty($cod)){
    $objs = $daoNoticia->BuscarPorCOD($cod);

    foreach ($objs as $obj){
        $p = $obj->getLinguagem();
        if(isset($aObj[$p])){
            $aObj[$p]->setTitulo($obj->getTitulo());
            $aObj[$p]->setLinguagem($obj->getLinguagem());
            $aObj[$p]->setAtivo($obj->getAtivo());
            $aObj[$p]->setCategoria($obj->getCategoria());
            $aObj[$p]->setCadastro($obj->getCadastro());
            $aObj[$p]->setAdvogado($obj->getAdvogado());
            $aObj[$p]->setImagem($obj->getImagem());
            $aObj[$p]->setTexto($obj->getTexto());
            $aObj[$p]->setUltima_alteracao($obj->getUltima_alteracao());
            $aObj[$p]->setResumo($obj->getResumo());
            $aObj[$p]->setUsuario($obj->getUsuario());
            $aObj[$p]->setId($obj->getId());

            $cats = $daoCategoria->BuscarPorCOD($obj->getCategoria());
            $grp[$p] = new Categoria();
            foreach ($cats as $cat){
                $grp[$p]->setTitulo($cat->getTitulo());
                $grp[$p]->setLinguagem($cat->getLinguagem());
                $grp[$p]->setAtivo($cat->getAtivo());
                $grp[$p]->setId($cat->getId());
            }
        }

        $adv = $obj->getAdvogado();
        $imagem = $obj->getImagem();
        $cat = $obj->getCategoria();
        $id = $obj->getId();

    }

    $title = 'Alterar';
}


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- META SECTION -->
        <title>Área Administrativa <?php echo TITULO_SITE ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <link rel="icon" href="<?php echo URL_SITE ?>/admin/favicon.png" type="image/x-icon" />
        <!-- END META SECTION -->

        <!-- CSS INCLUDE -->
        <?php include(RAIZ.'/admin/includes/css.php') ?>
        <!-- EOF CSS INCLUDE -->
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        <div class="page-container">

            <!-- START PAGE SIDEBAR -->
            <?php include(RAIZ.'/admin/includes/sidebar.php') ?>
            <!-- END PAGE SIDEBAR -->

            <!-- PAGE CONTENT -->
            <div class="page-content">

                <!-- START X-NAVIGATION VERTICAL -->
                <?php include(RAIZ.'/admin/includes/nav-vertical.php') ?>
                <!-- END X-NAVIGATION VERTICAL -->

                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo URL_SITE ?>/admin/">Home</a></li>
                    <li><a href="<?php echo URL_SITE ?>/admin/view/noticias/noticias-list.php">Noticia</a></li>
                    <li class="active"><?php echo $title ?></li>
                </ul>
                <!-- END BREADCRUMB -->

                <!-- PAGE TITLE -->
                <div class="page-title">
                    <h2><span class="fa fa-newspaper-o"></span> Noticia</h2>
                </div>
                <!-- END PAGE TITLE -->

                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">

                    <div class="row">
                        <div class="col-md-12">
                            <!-- <div class="col-md-12" style="font-family: Arial; clear:both; height:20px">
                                <iframe style="font-family: Arial; height:100px" name="upload_img" id="upload_img" src="<?php echo URL_SITE ?>/admin/upload/upload.php" frameborder="0" scrolling="yes" width="90%"></iframe>
                                
                            </div>
                            <div class="col-md-12">
                                <ul style="font-family: Arial; list-style-type: none; padding: 0px; margin: 20px" id="anexo_imagem1"></ul>
                            </div> -->
                            
                            <!-- START VALIDATIONENGINE PLUGIN -->
                            <div class="block">
                                <h4><?php echo $title ?></h4>
                                <div id="simple-msg" style="text-align: justify; margin-bottom: 50px">
                                <?php if(isset($_SESSION['retorno'])){?>
                                  <div style="padding: 10px; margin-top: 20px; text-align: justify" class="col-sm-12 <?php echo $_SESSION['retorno']['classe'] ?>">
                                      <?php echo $_SESSION['retorno']['mensagem'];?>
                                  </div>
                                  <br>
                                  <br>


                                <?php unset($_SESSION['retorno']); } ?>
                                </div>
                                <form enctype="multipart/form-data" role="form" method="post" class="form-horizontal" name="form_crud" id="form_crud" action="<?php echo URL_SITE ?>/admin/controle/noticias/_noticias.php">
                                    <ul class="nav nav-tabs">
                                      <?php foreach ($linguagens as $linguagem){ ?>
                                        <li class="nav-item <?php if($linguagem->getId() == 1){ ?>active<?php }?>"><a class="nav-link" data-toggle="tab" href="#l<?php echo $linguagem->getId() ?>" role="tab"><?php echo $linguagem->getTitulo() ?></a></li>
                                      <?php }?>
                                    </ul>
                                    <div class="tab-content card">
                                <?php foreach ($linguagens as $linguagem){ ?>
                                    <div class="tab-pane fade <?php if($linguagem->getId() == 1){ ?>in active<?php }?>" id="l<?php echo $linguagem->getId() ?>" role="tabpanel">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Titulo:</label>
                                        <div class="col-md-9">
                                            <input value="<?php echo $aObj[$linguagem->getId()]->getTitulo(); ?>" type="text" name="titulo[]" class="form-control" <?php if($linguagem->getId() == 1){ ?>required=""<?php }?> />

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Data:</label>
                                        <div class="col-md-9">
                                            <input value="<?php echo date('d/m/Y', strtotime($aObj[$linguagem->getId()]->getCadastro())); ?>" type="text" name="cadastro[]" class="form-control" <?php if($linguagem->getId() == 1){ ?>required=""<?php }?>/>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Resumo:</label>
                                        <div class="col-md-9">
                                            <textarea name="resumo[]" class="form-control char-textarea" data-length=280><?php echo $aObj[$linguagem->getId()]->getResumo() ?></textarea>
                                            <?php                                            
                                            $diferenca = 280 - (int)strlen($aObj[$linguagem->getId()]->getResumo()) ; 
                                            if($diferenca<0){$diferenca=0;}                                           
                                            ?>
                                            <h4 style="font-size:11px">Faltam <span class="char-count" style="font-weight:bold;"><?php echo $diferenca?></span> caracteres </h4>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Texto:</label>
                                        <div class="col-md-9">
                                            <textarea name="texto[]" class="summernote"><?php echo $aObj[$linguagem->getId()]->getTexto() ?></textarea>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Ativo:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="ativo[]" id="formGender">
                                                <option <?php if($aObj[$linguagem->getId()]->getAtivo() == 's'){ ?> selected=""<?php }?> value="s">Sim</option>
                                                <option <?php if($aObj[$linguagem->getId()]->getAtivo() == 'n'){ ?> selected=""<?php }?> value="n">Não</option>
                                            </select>
                                        </div>
                                    </div>
                                    <input type="hidden" name="linguagem[]" value="<?php echo $linguagem->getId() ?>">
                                    </div>

                                    <?php }?>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Categoria:</label>
                                        <div class="col-md-3">
                                            <select class="select" name="categoria" id="formGender">
                                                <option value="">Escolha uma Categoria</option>
                                                <?php foreach ($categorias as $categoria){ ?>
                                                <option <?php if($cat == $categoria->getId()){echo 'selected';} ?> value="<?php echo $categoria->getId() ?>"><?php echo $categoria->getTitulo() ?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Advogados:</label>
                                        <div class="col-md-9" style="max-height: 200px; overflow-x:hidden">
                                            <?php foreach ($ads as $a){ ?>
                                                <div class="form-check form-check-inline col-md-6">
                                                    <input name="advogados[]" <?php if($daoNoticia->CheckedAdv($id, $a->getId())>0){echo 'checked';}; ?> class="form-check-input" type="checkbox" id="inlineCheckbox1" value="<?php echo $a->getId()?>">
                                                    <label class="form-check-label" for="inlineCheckbox<?php echo $a->getId()?>"><?php echo $a->getTitulo()?></label>
                                                </div>
                                            <?php }?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Assunto (Atuações):</label>
                                        <div class="col-md-9" style="max-height: 200px; overflow-x:hidden">
                                            <?php foreach ($atuacoes as $r){ ?>
                                                <div class="form-check form-check-inline col-md-6">
                                                    <input name="atuacao[]" <?php if($daoNoticia->CheckedAtuacao($id, $r->getId())>0){echo 'checked';}; ?> class="form-check-input" type="checkbox" id="inlineCheckbox1" value="<?php echo $r->getId()?>">
                                                    <label class="form-check-label" for="inlineCheckbox<?php echo $r->getId()?>"><?php echo $r->getTitulo()?></label>
                                                </div>
                                            <?php }?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Imagem:</label>
                                        <div class="col-md-9">
                                            <input type="file" name="img_noticia" accept="image/*">

                                        </div>
                                    </div>
                                    <?php if($imagem <> ''){ ?>
                                    <input type="hidden" name="old_img" value="<?php echo $imagem ?>">
                                    <div class="form-group">
                                         <label class="col-md-3 control-label">Alterar Imagem:</label>
                                         <label class="radio-inline"><input type="radio" name="alt_img" id="alt_img" value="s" >Sim</label>
                                         <label class="radio-inline"><input type="radio" name="alt_img" id="alt_img" value="n" checked>Não</label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Imagem Atual:</label>
                                        <div class="col-md-9">
                                            <img class="col-sm-6" src="<?php echo URL_CAMINHO_IMG.'/noticias/'.$imagem ?>">
                                            <a class="btn btn-danger" onclick="remover_imagem_noticia(<?php echo $cod ?>, '<?php echo URL_SITE ?>/admin/controle/noticias/_noticias.php','<?php echo $imagem ?>')" href=""><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                    <?php }?>
                                    <div class="btn-group pull-right">
                                        <input type="hidden" name="acao" value="<?php echo strtolower($title) ?>">
                                        <input type="hidden" name="cod" value="<?php echo $cod ?>">
                                        <button class="btn btn-primary" type="submit"><?php echo $title ?></button>
                                    </div>
                                    <div class="form-group">

                                    </div>
                                </form>

                            </div>
                            <!-- END VALIDATIONENGINE PLUGIN -->
                        </div>
                    </div>


                </div>
                <!-- END PAGE CONTENT WRAPPER -->
            </div>
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->



        <!-- START PRELOADS -->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END PRELOADS -->

        <!-- MESSAGE BOX-->
        <?php include(RAIZ.'/admin/includes/msg-deslogar.php') ?>
        <!-- END MESSAGE BOX-->

        <!-- START SCRIPTS -->
        <?php include(RAIZ.'/admin/includes/js.php') ?>
        <script type="text/javascript" src="<?php echo URL_SITE ?>/admin/js/plugins/summernote/summernote.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
        <script>


            function aba(aba){
                if(aba === 'l1'){
                    document.getElementById('l1').style.display = "block";
                    document.getElementById('l2').style.display = "none";
;
                }else if(aba === 'l2'){
                    document.getElementById('l2').style.display = "block";
                    document.getElementById('l1').style.display = "none";
                }
            }

            function removePDF(img,cod){
                $.ajax({
                    type: "GET",
                    url: "<?php echo URL_SITE ?>/admin/upload/remove.php?img="+img,
                    
                    success: function(data){
                
                    var resp = data.split("-");
                    if(resp[1]=='error'){
                        var msg = resp[0];
                        var classe = 'alert-error';
                        //alert(msg);	
                    }else{
                        var msg = "Imagem excluida com sucesso!";
                        var classe = 'alert-sucess';
                        alert(msg);	
                        var div = "#IMG"+cod;
                        $(div).fadeOut("fast",
                            function(){
                                $( "div" ).remove(div);
                            }
                        );
                        
                    }
                                            
                    }
                });
            }


$(".char-textarea").on("keyup",function(event){
  checkTextAreaMaxLength(this,event);
});

/*
Checks the MaxLength of the Textarea
-----------------------------------------------------
@prerequisite:	textBox = textarea dom element
				e = textarea event
                length = Max length of characters
*/
function checkTextAreaMaxLength(textBox, e) { 
    
    var maxLength = parseInt($(textBox).data("length"));
    
  
    if (!checkSpecialKeys(e)) { 
        if (textBox.value.length > maxLength - 1) textBox.value = textBox.value.substring(0, maxLength); 
   } 
  $(".char-count").html(maxLength - textBox.value.length);
    
    return true; 
} 
/*
Checks if the keyCode pressed is inside special chars
-------------------------------------------------------
@prerequisite:	e = e.keyCode object for the key pressed
*/
function checkSpecialKeys(e) { 
    if (e.keyCode != 8 && e.keyCode != 46 && e.keyCode != 37 && e.keyCode != 38 && e.keyCode != 39 && e.keyCode != 40) 
        return false; 
    else 
        return true; 
}            
        </script>
    <!-- END SCRIPTS -->

    </body>
</html>
