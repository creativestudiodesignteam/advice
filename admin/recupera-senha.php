<?php
require $_SERVER['DOCUMENT_ROOT']."/libs/config.php";

use Dao\DaoUsuario;
$daoUsuario = new DaoUsuario();

if(empty($_POST['userField'])){
	//session_destroy();
	$erro='';

}else{


$e_mail = $_POST['userField'];

$rs_login = $daoUsuario->recupera($e_mail);


if ($rs_login->getId() <> ""){
	
	
            $daoUsuario->envia_senha($rs_login, EMAIL_ENVIO);
	
            header('Location: '.URL_SITE.'/admin/login.php?m=1');
	?>
    
    <?php
	} 
	else {
	 header('Location: '.URL_SITE.'/admin/login.php?m=0');
	}
}
?>
<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>        
        <!-- META SECTION -->
        <title>Área Administrativa <?php echo TITULO_SITE ?></title>   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="<?php echo URL_SITE ?>/admin/favicon.png" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="css/theme-default.css"/>
        <!-- EOF CSS INCLUDE -->                                    
    </head>
    <body>
        
        <div class="login-container">
        
            <div class="login-box animated fadeInDown">
                <div class="login-logo"></div>
                <div class="login-body">
                    <div class="login-title"><strong>Recuperar</strong> senha</div>
                    <form action="../action/lembrar_senha.php" class="form-horizontal" id="form-login" method="post">
                    <div class="form-group">
                        <div class="col-md-12">
                            <input name="userField" required="" type="email" class="form-control" placeholder="E-mail"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <button class="btn btn-info btn-block">Enviar</button>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="login-footer">
                    <div class="pull-left">
                        &copy; <?php echo date('Y') ?> <a href="http://www.deepocean.com.br">Deep Ocean</a>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- START SCRIPTS -->
            <?php include('includes/js.php') ?>
        <!-- END SCRIPTS -->     
        
        <script type="text/javascript">
            var jvalidate = $("#form-login").validate({
                ignore: [],
                rules: {  
                        passwField: {
                                required: true,
                                minlength: 5,
                                maxlength: 10
                        }
                        userField: {
                                required: true,
                                email: true
                        }
                    }                                        
                });                                    

        </script>
    </body>
</html>






