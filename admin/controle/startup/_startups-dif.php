<?php 
//include("../../libs/conf.php"); 
include("../../../libs/config.php"); 
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}



####################################### USUARIO ######################################################################

//cadastrar
if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
    
        $daoStartup = new DaoStartup();
        $startupObj = new Startup();
         
        /* if(isset($_FILES['img_noticia'])){
            
            $ext = strtolower(substr($_FILES['img_noticia']['name'],-4)); //Pegando extensão do arquivo
            $new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
            $dir = URL_IMG.'/noticias/'; //Diretório para uploads

            if(!move_uploaded_file($_FILES['img_noticia']['tmp_name'], $dir.$new_name)){
                $new_name = '';
            } //Fazer upload do arquivo
            
        }else{
            $_SESSION['retorno']['mensagem'] = 'Preencha a imagem!';
            $_SESSION['retorno']['classe'] = 'alert-danger';
            $_SESSION['retorno']['result'] = 'error';
            //$_SESSION['retorno']['obj'] = $startupObj;

            header('Location: '.URL_SITE.'/admin/view/startup/diferenciais-form.php');
            exit();
        } */
        
        $table = $daoStartup->statusTable('tbl_startup_diferenciais');
        $id = $table['Auto_increment'];
        
        for($i=0;$i<count($linguagem);$i++){
            
                  
            $startupObj->setTitulo($titulo[$i]);            
            $startupObj->setTexto($texto[$i]);
            $startupObj->setAtivo($ativo[$i]);
            $startupObj->setOrdem($ordem[$i]);            
            $startupObj->setLinguagem($linguagem[$i]);
            $startupObj->setId($id);
            $cod = $daoStartup->Inserir($startupObj);
            
        } 
        		 		 
	$_SESSION['retorno']['mensagem'] = 'Dados cadastrados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        //$_SESSION['retorno']['obj'] = $startupObj;
        
        header('Location: '.URL_SITE.'/admin/view/startup/diferenciais-form.php?cod='.$cod.'');
        exit;	

}
//alterar
if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){

	 $daoStartup = new DaoStartup();
         $startupObj = new Startup();

        /* if((isset($_FILES['img_noticia']) && $alt_img == 's') || (isset($_FILES['img_noticia']) && !isset($alt_img))){
            
            $ext = strtolower(substr($_FILES['img_noticia']['name'],-4)); //Pegando extensão do arquivo
            $new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
            $dir = URL_IMG.'/noticias/'; //Diretório para uploads

            if(!move_uploaded_file($_FILES['img_noticia']['tmp_name'], $dir.$new_name)){
                $new_name = '';
            } //Fazer upload do arquivo
            
            
        }else if(empty($_FILES['img_noticia']['name']) && $alt_img == 's'){
            $_SESSION['retorno']['mensagem'] = 'Preencha a imagem!';
            $_SESSION['retorno']['classe'] = 'alert-danger';
            $_SESSION['retorno']['result'] = 'error';
            //$_SESSION['retorno']['obj'] = $startupObj;

            header('Location: '.URL_SITE.'/admin/view/startup/diferenciais-form.php?cod='.$cod.'');
            exit();
        } */
         
        for($i=0;$i<count($linguagem);$i++){
                       
            $startupObj->setTitulo($titulo[$i]);
            $startupObj->setTexto($texto[$i]);
            $startupObj->setAtivo($ativo[$i]);
            $startupObj->setOrdem($ordem[$i]);             
            $startupObj->setLinguagem($linguagem[$i]);
            $startupObj->setId($cod);
            
           /*  if(isset($new_name) && $new_name <> '' && !empty($_FILES['img_noticia']['name'])){
                $startupObj->setImagem($new_name);
            } */
            
            $obj = $daoStartup->BuscarPorCODLing($cod,$linguagem[$i]);
            
            if($obj->getId() <> ''){
                $daoStartup->Editar($startupObj);
            }else{
                $cod = $daoStartup->Inserir($startupObj);
            }
        } 
		
	$_SESSION['retorno']['mensagem'] = 'Dados alterados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';

        header('Location: '.URL_SITE.'/admin/view/startup/diferenciais-form.php?cod='.$cod.'');
        exit;
		

}
//deletar
if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){
	 
    $daoStartup = new DaoStartup();
    $daoStartup->Deletar($cod);
    
    $responta['mensagem'] = 'Startup Diferencial apagado com sucesso';
    $responta['classe'] = 'alert-success';
    $responta['result'] = 'success';
    echo json_encode($responta);
    exit;
	
}



?>