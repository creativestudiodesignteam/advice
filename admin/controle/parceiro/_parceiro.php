<?php 
//include("../../libs/conf.php"); 
include("../../../libs/config.php"); 
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}



####################################### USUARIO ######################################################################

//cadastrar
if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
    
        $daoParceiro = new DaoParceiro();
        $pojo = new Parceiro();
         
                         
        $pojo->setNome($nome);
        $pojo->setAtivo($ativo);
        $pojo->setTexto('');
        $pojo->setTipo($tipo);
        $pojo->setSite('');
         
        
        if(isset($_FILES['img_parceiro'])){
            
            $ext = strtolower(substr($_FILES['img_parceiro']['name'],-4)); //Pegando extensão do arquivo
            if($ext <> '.jpg' && $ext <> '.png' && $ext <> '.gif'){
                $_SESSION['retorno']['mensagem'] = 'Somente imagens';
                $_SESSION['retorno']['classe'] = 'alert-danger';
                $_SESSION['retorno']['result'] = 'error';
                header('Location: '.URL_SITE.'/admin/view/parceiro/parceiro-form.php');
                exit();
            }
            $new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
            $dir = URL_IMG.'/parceiro/'; //Diretório para uploads

            move_uploaded_file($_FILES['img_parceiro']['tmp_name'], $dir.$new_name); //Fazer upload do arquivo
            
        }else{
            $_SESSION['retorno']['mensagem'] = 'Preencha a imagem!';
            $_SESSION['retorno']['classe'] = 'alert-danger';
            $_SESSION['retorno']['result'] = 'error';
            //$_SESSION['retorno']['obj'] = $parceiroObj;

            header('Location: '.URL_SITE.'/admin/view/parceiro/parceiro-form.php');
            exit();
        }
        
        $pojo->setImagem($new_name);
         
        $cod = $daoParceiro->Inserir($pojo);

        
			 		 
	$_SESSION['retorno']['mensagem'] = 'Dados cadastrados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        //$_SESSION['retorno']['obj'] = $parceiroObj;
        
        header('Location: '.URL_SITE.'/admin/view/parceiro/parceiro-form.php?cod='.$cod.'');
        exit;	

}
//alterar
if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){

	$daoParceiro = new DaoParceiro();
        $pojo = new Parceiro();
         
                         
        $pojo->setNome($nome);
        $pojo->setAtivo($ativo);
        $pojo->setTexto('');
        $pojo->setSite('');
        $pojo->setTipo($tipo);
        $pojo->setImagem($old_img);
        $pojo->setId($cod);
         
         if((isset($_FILES['img_parceiro']) && $alt_img == 's') || (isset($_FILES['img_parceiro']) && !isset($alt_img))){
            
            $ext = strtolower(substr($_FILES['img_parceiro']['name'],-4)); //Pegando extensão do arquivo
            if($ext <> '.jpg' && $ext <> '.png' && $ext <> '.gif'){
                $_SESSION['retorno']['mensagem'] = 'Somente imagens';
                $_SESSION['retorno']['classe'] = 'alert-danger';
                $_SESSION['retorno']['result'] = 'error';
                header('Location: '.URL_SITE.'/admin/view/parceiro/parceiro-form.php?cod='.$cod.'');
                exit();
            }
            $new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
            $dir = URL_IMG.'/parceiro/'; //Diretório para uploads

            move_uploaded_file($_FILES['img_parceiro']['tmp_name'], $dir.$new_name); //Fazer upload do arquivo
            $pojo->setImagem($new_name);
            
        }else if((!isset($_FILES['img_parceiro']) || empty ($_FILES['img_parceiro'])) && $alt_img == 's'){
            $_SESSION['retorno']['mensagem'] = 'Preencha a imagem!';
            $_SESSION['retorno']['classe'] = 'alert-danger';
            $_SESSION['retorno']['result'] = 'error';
            //$_SESSION['retorno']['obj'] = $parceiroObj;

            header('Location: '.URL_SITE.'/admin/view/parceiro/parceiro-form.php?cod='.$cod.'');
            exit();
        }
         
         
         $daoParceiro->Editar($pojo);
         
        
		
	$_SESSION['retorno']['mensagem'] = 'Dados alterados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';

        header('Location: '.URL_SITE.'/admin/view/parceiro/parceiro-form.php?cod='.$cod.'');
        exit;
		

}
//deletar
if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){
	 
    $daoParceiro = new DaoParceiro();
    $daoParceiro->Deletar($cod);
    
    $resposta['mensagem'] = 'Parceiro deletada com sucesso';
    $resposta['classe'] = 'alert-success';
    $resposta['result'] = 'success';
    echo json_encode($resposta);
    exit;
	
}



?>