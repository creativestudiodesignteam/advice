<?php 
//include("../../libs/conf.php"); 
include("../../../libs/config.php"); 
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}



####################################### USUARIO ######################################################################

//cadastrar
if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
    
        $daoNoticia = new DaoNoticia();
        $noticiaObj = new Noticia();
         
        if(isset($_FILES['img_noticia'])){
            
            $ext = strtolower(substr($_FILES['img_noticia']['name'],-4)); //Pegando extensão do arquivo
            $new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
            $dir = URL_IMG.'/noticias/'; //Diretório para uploads

            if(!move_uploaded_file($_FILES['img_noticia']['tmp_name'], $dir.$new_name)){
                $new_name = '';
            } //Fazer upload do arquivo
            
        }else{
            $_SESSION['retorno']['mensagem'] = 'Preencha a imagem!';
            $_SESSION['retorno']['classe'] = 'alert-danger';
            $_SESSION['retorno']['result'] = 'error';
            //$_SESSION['retorno']['obj'] = $noticiaObj;

            header('Location: '.URL_SITE.'/admin/view/noticias/noticias-form.php');
            exit();
        }
        
        $table = $daoNoticia->statusTable('tbl_noticia');
        $id = $table['Auto_increment'];
        
        for($i=0;$i<count($linguagem);$i++){
            
            $d = implode("-",array_reverse(explode("/",$cadastro[$i])));
            
            $noticiaObj->setTitulo($titulo[$i]);
            $noticiaObj->setResumo($resumo[$i]);
            $noticiaObj->setTexto($texto[$i]);
            $noticiaObj->setAtivo($ativo[$i]);
            $noticiaObj->setUsuario($_SESSION['NOME_USUARIO']);
            $noticiaObj->setCadastro($d);
            $noticiaObj->setUltima_alteracao(date('Y-m-d H:i:s'));
            $noticiaObj->setImagem($new_name);
            $noticiaObj->setCategoria($categoria);
            $noticiaObj->setAdvogado($advogado);
            $noticiaObj->setLinguagem($linguagem[$i]);
            $noticiaObj->setId($id);
            $cod = $daoNoticia->Inserir($noticiaObj);
            
        } 

        if(isset($atuacao) && count($atuacao)>0){
            for($i=0; $i<count($atuacao); $i++){
                $daoNoticia->InserirAtuacao($id, $atuacao[$i]);
            }
        }

        if(isset($advogados) && count($advogados)>0){
            for($i=0; $i<count($advogados); $i++){
                $daoNoticia->InserirAdv($id, $advogados[$i]);
            }
        }
        		 		 
	$_SESSION['retorno']['mensagem'] = 'Dados cadastrados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        //$_SESSION['retorno']['obj'] = $noticiaObj;
        
        header('Location: '.URL_SITE.'/admin/view/noticias/noticias-form.php?cod='.$cod.'');
        exit;	

}
//alterar
if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){

	 $daoNoticia = new DaoNoticia();
         $noticiaObj = new Noticia();

        if((isset($_FILES['img_noticia']) && isset($alt_img) && $alt_img == 's') || (isset($_FILES['img_noticia']) && !isset($alt_img))){
            
            $ext = strtolower(substr($_FILES['img_noticia']['name'],-4)); //Pegando extensão do arquivo
            $new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
            $dir = URL_IMG.'/noticias/'; //Diretório para uploads

            if(!move_uploaded_file($_FILES['img_noticia']['tmp_name'], $dir.$new_name)){
                $new_name = '';
            } //Fazer upload do arquivo
            
            
        }else if(empty($_FILES['img_noticia']['name']) && $alt_img == 's'){
            $_SESSION['retorno']['mensagem'] = 'Preencha a imagem!';
            $_SESSION['retorno']['classe'] = 'alert-danger';
            $_SESSION['retorno']['result'] = 'error';
            //$_SESSION['retorno']['obj'] = $noticiaObj;

            header('Location: '.URL_SITE.'/admin/view/noticias/noticias-form.php?cod='.$cod.'');
            exit();
        }
        
         
        for($i=0;$i<count($linguagem);$i++){
            
            $d = implode("-",array_reverse(explode("/",$cadastro[$i])));
            
            $noticiaObj->setTitulo($titulo[$i]);
            $noticiaObj->setResumo($resumo[$i]);
            $noticiaObj->setTexto($texto[$i]);
            $noticiaObj->setAtivo($ativo[$i]);
            $noticiaObj->setUsuario($_SESSION['NOME_USUARIO']);
            $noticiaObj->setCadastro($d);
            $noticiaObj->setUltima_alteracao(date('Y-m-d H:i:s'));

            if(isset($old_img)){
                $noticiaObj->setImagem($old_img);
            }
            
            $noticiaObj->setCategoria($categoria);
            $noticiaObj->setAdvogado($advogado);
            $noticiaObj->setLinguagem($linguagem[$i]);
            $noticiaObj->setId($cod);
            
            if(isset($new_name) && $new_name <> '' && !empty($_FILES['img_noticia']['name'])){
                $noticiaObj->setImagem($new_name);
            }
                                 

            $obj = $daoNoticia->BuscarPorCODLing($cod,$linguagem[$i]);
            

            if($obj->getId() <> ''){
                $daoNoticia->Editar($noticiaObj);
            }else{
                $cod = $daoNoticia->Inserir($noticiaObj);
            }
            
        }
                
        
        $daoNoticia->DeletarAtuacao($cod);
        if(isset($atuacao) && count($atuacao)>0){
            

            for($i=0; $i<count($atuacao); $i++){
                $daoNoticia->InserirAtuacao($cod, $atuacao[$i]);
            }
        
        }

        $daoNoticia->DeletarAdv($cod);
        if(isset($advogados) && count($advogados)>0){
            for($i=0; $i<count($advogados); $i++){
                $daoNoticia->InserirAdv($cod, $advogados[$i]);
            }
        }
		
	$_SESSION['retorno']['mensagem'] = 'Dados alterados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';

        header('Location: '.URL_SITE.'/admin/view/noticias/noticias-form.php?cod='.$cod.'');
        exit;
		

}
//deletar
if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){
	 
    $daoNoticia = new DaoNoticia();
    $daoNoticia->Deletar($cod);
    $daoNoticia->DeletarAdv($cod);
    $daoNoticia->DeletarAtuacao($cod);
    
    $responta['mensagem'] = 'Noticia deletada com sucesso';
    $responta['classe'] = 'alert-success';
    $responta['result'] = 'success';
    echo json_encode($responta);
    exit;
	
}

//deletar
if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir_imagem")){
     
    //unlink(URL_CAMINHO_IMG.'/equipe/'.$img); 
   
   $daoNoticia = new DaoNoticia();
               
   $daoNoticia->DeletarFoto($id, $img);

   $responta['mensagem'] = 'Imagem apagada com sucesso';
   $responta['classe'] = 'alert-success';
   $responta['result'] = 'success';
    echo json_encode($responta); 
  /*  echo json_encode($id_adv); */
   exit;
   
}


?>