<?php 
//include("../../libs/conf.php"); 
include("../../../libs/config.php"); 
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}



####################################### USUARIO ######################################################################

//cadastrar
if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
    
         $daoCategoria = new DaoCategoria();
         $categoriaObj = new Categoria();
         
         $table = $daoCategoria->statusTable('tbl_categoria');
         $id = $table['Auto_increment'];
         
         for($i=0;$i<count($linguagem);$i++){                
            $categoriaObj->setTitulo($categoria[$i]);
            $categoriaObj->setAtivo($ativo[$i]);
            $categoriaObj->setLinguagem($linguagem[$i]);
            $categoriaObj->setId($id);
            
            $cod = $daoCategoria->Inserir($categoriaObj);
         }
         
			 		 
	$_SESSION['retorno']['mensagem'] = 'Dados cadastrados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        
        header('Location: '.URL_SITE.'/admin/view/noticias/categorias-form.php?cod='.$cod.'');
        exit;	

}
//alterar
if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){

	 $daoCategoria = new DaoCategoria();
         $categoriaObj = new Categoria();
         
         
         for($i=0;$i<count($linguagem);$i++){                
            $categoriaObj->setTitulo($categoria[$i]);
            $categoriaObj->setAtivo($ativo[$i]);
            $categoriaObj->setLinguagem($linguagem[$i]);
            $categoriaObj->setId($cod);
            
            $obj = $daoCategoria->BuscarPorCODLing($cod,$linguagem[$i]);
            
            if($obj->getId() <> ''){
                $daoCategoria->Editar($categoriaObj);
            }else{
                $cod = $daoCategoria->Inserir($categoriaObj);
            }
         }

         
         
         
        
		
	$_SESSION['retorno']['mensagem'] = 'Dados alterados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';

        header('Location: '.URL_SITE.'/admin/view/noticias/categorias-form.php?cod='.$cod.'');
        exit;
		

}
//deletar
if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){
	 
    $daoCategoria = new DaoCategoria();
    $daoCategoria->Deletar($cod);
    
    $responta['mensagem'] = 'Categoria deletada com sucesso';
    $responta['classe'] = 'alert-success';
    $responta['result'] = 'success';
    echo json_encode($responta);
    exit;
	
}



?>