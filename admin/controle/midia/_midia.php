<?php

include("../../../libs/config.php"); 
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach($_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}




if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir_imagem")){
    $daoImage = new DaoImage();
    $daoImage->Deletar($cod);
    
    $resposta['mensagem'] = 'Imagem deletada com sucesso';
    $resposta['classe'] = 'alert-success';
    $resposta['result'] = 'success';
    echo json_encode($resposta);
    exit;
}
