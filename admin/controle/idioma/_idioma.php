<?php 
//include("../../libs/conf.php"); 
include("../../../libs/config.php"); 
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}



####################################### USUARIO ######################################################################

//cadastrar
if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
    
         $daoIdioma = new DaoIdioma();
         $idiomaObj = new Idioma();
         
         $table = $daoIdioma->statusTable('tbl_idioma');
         $id = $table['Auto_increment'];
         
         for($i=0;$i<count($linguagem);$i++){                
            $idiomaObj->setTitulo($titulo[$i]);
            $idiomaObj->setAtivo($ativo[$i]);
            $idiomaObj->setLinguagem($linguagem[$i]);
            $idiomaObj->setId($id);
            
            $cod = $daoIdioma->Inserir($idiomaObj);
         }
         
			 		 
	$_SESSION['retorno']['mensagem'] = 'Dados cadastrados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        
        header('Location: '.URL_SITE.'/admin/view/idioma/idioma-form.php?cod='.$cod.'');
        exit;	

}
//alterar
if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){

	 $daoIdioma = new DaoIdioma();
         $idiomaObj = new Idioma();
         
         
         for($i=0;$i<count($linguagem);$i++){                
            $idiomaObj->setTitulo($titulo[$i]);
            $idiomaObj->setAtivo($ativo[$i]);
            $idiomaObj->setLinguagem($linguagem[$i]);
            $idiomaObj->setId($cod);
            
            $obj = $daoIdioma->BuscarPorCODLing($cod,$linguagem[$i]);
            
            if($obj->getId() <> ''){
                $daoIdioma->Editar($idiomaObj);
            }else{
                $cod = $daoIdioma->Inserir($idiomaObj);
            }
         }

         
         
         
        
		
	$_SESSION['retorno']['mensagem'] = 'Dados alterados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';

        header('Location: '.URL_SITE.'/admin/view/idioma/idioma-form.php?cod='.$cod.'');
        exit;
		

}
//deletar
if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){
	 
    $daoIdioma = new DaoIdioma();
    $daoIdioma->Deletar($cod);
    
    $responta['mensagem'] = 'Idioma deletada com sucesso';
    $responta['classe'] = 'alert-success';
    $responta['result'] = 'success';
    echo json_encode($responta);
    exit;
	
}



?>