<?php 
include("../../../libs/config.php"); 
include("../../dados/logado.php");

use Dao\DaoSite;
use Dao\Classes\Site;

/* echo TITULO_SITE;
exit; */

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}



####################################### USUARIO ######################################################################

//cadastrar
if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
    
         $daoSite2 = new daoSite();
         $siteObj = new Site();
         
         $table = $daoSite2->statusTable('tbl_site');
         $id = $table['Auto_increment'];
         
         for($i=0;$i<count($linguagem);$i++){                
            $siteObj->setTituloSite($titulo[$i]);
            $siteObj->setCopyright($copyright[$i]);
            $siteObj->setLinguagem($linguagem[$i]);
            $siteObj->setId($id);
            
            $cod = $daoSite2->Inserir($siteObj);
         }
         
			 		 
	$_SESSION['retorno']['mensagem'] = 'Dados cadastrados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        
        header('Location: '.URL_SITE.'/admin/view/configuracoes/site-form.php?cod='.$cod.'');
        exit;	

}
//alterar
if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){

	 $daoSite2 = new daoSite();
    $siteObj = new Site();
         
         
         for($i=0;$i<count($linguagem);$i++){                
            $siteObj->setTituloSite($titulo[$i]);
            $siteObj->setCopyright($copyright[$i]);
            $siteObj->setLinguagem($linguagem[$i]);
            $siteObj->setId($cod);
            
            $obj = $daoSite2->BuscarPorCODLing($cod,$linguagem[$i]);
            
            if($obj->getId() <> ''){
                $daoSite2->Editar($siteObj);
            }else{
                $cod = $daoSite2->Inserir($siteObj);
            }
         }
        
		
	$_SESSION['retorno']['mensagem'] = 'Dados alterados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';

        header('Location: '.URL_SITE.'/admin/view/configuracoes/site-form.php?cod='.$cod.'');
        exit;
		

}
//deletar
if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){
	 
    $daoSite2 = new daoSite();
    $daoSite2->Deletar($cod);
    
    $responta['mensagem'] = 'Site deletada com sucesso';
    $responta['classe'] = 'alert-success';
    $responta['result'] = 'success';
    echo json_encode($responta);
    exit;
	
}



?>