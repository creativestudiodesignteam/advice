<?php 
include("../../../libs/config.php"); 
include("../../dados/logado.php");

require_once '../../../vendor/autoload.php';

use Respect\Validation\Validator as v;


#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

###################################  CONFIGURAÇÕES ################################################################
if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){
        $config = new Configuracao();
        $daoConfig = new DaoConfiguracao();
        
        
        if(empty($telefone)){
            $responta['mensagem'] = 'Preencha o Telefone';
            $responta['classe'] = 'alert-danger';
            $responta['result'] = 'error';
            echo json_encode($responta);
            exit;
        }
        
        if(empty($email) || !v::email()->validate($email)){
            $responta['mensagem'] = 'E-mail inválido';
            $responta['classe'] = 'alert-danger';
            $responta['result'] = 'error';
            echo json_encode($responta);
            exit;
        }
        
        if(empty($email_envio) || !v::email()->validate($email_envio)){
            $responta['mensagem'] = 'E-mail de envio inválido';
            $responta['classe'] = 'alert-danger';
            $responta['result'] = 'error';
            echo json_encode($responta);
            exit;
        }
        
        if (empty($senha_envio)) { 
            $responta['mensagem'] = 'Preencha a senha de envio';
            $responta['classe'] = 'alert-danger';
            $responta['result'] = 'error';
            echo json_encode($responta);
            exit;
        }	

       if (empty($smtp)) { 
            $responta['mensagem'] = 'Preencha o SMTP';
            $responta['classe'] = 'alert-danger';
            $responta['result'] = 'error';
            echo json_encode($responta);
            exit;
       }	
	
       $config->setAnalytics($analytics);
       $config->setTelefone($telefone);
       $config->setEmail($email);
       $config->setEmailEnvio($email_envio);
       $config->setSenhaEnvio($senha_envio);
       $config->setSmtp($smtp);
       $config->setId($cod);
       //exit;
       $daoConfig->Editar($config);
       //echo $config->getTelefone();
       //exit();
        $responta['mensagem'] = 'Alteração realizada com sucesso';
        $responta['classe'] = 'alert-success';
        $responta['result'] = 'success';
        echo json_encode($responta);
        exit;
                                                            
	
}


?>