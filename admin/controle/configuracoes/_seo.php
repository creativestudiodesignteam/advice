<?php
include("../../../libs/config.php"); 
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}


################################### CEO - Buscadores ####################################################################
if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){
        $seo = new Seo();
        $daoSeo = new DaoSeo();
        
        $seo->setId($cod);
        $seo->setKeywords($palavras_chaves);
        $seo->setMetaDescricao($descricao);
        
        $daoSeo->Editar($seo);
	 
	$responta['mensagem'] = 'Alteração realizada com sucesso';
        $responta['classe'] = 'alert-success';
        $responta['result'] = 'success';
        echo json_encode($responta);
        exit;
}

 ?>