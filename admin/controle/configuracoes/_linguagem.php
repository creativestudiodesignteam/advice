<?php 
//include("../../libs/conf.php"); 
include("../../../libs/config.php"); 
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}



####################################### USUARIO ######################################################################

//cadastrar
if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
    
        $daoLinguagem = new DaoLinguagem();
        $linguagemObj = new Linguagem();
         
                         
        $linguagemObj->setTitulo($linguagem);
        $linguagemObj->setAtivo($ativo);
        
        if(isset($_FILES['img_bandeira'])){
            
            $ext = strtolower(substr($_FILES['img_bandeira']['name'],-4)); //Pegando extensão do arquivo
            $new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
            $dir = URL_IMG.'/band/'; //Diretório para uploads

            if(move_uploaded_file($_FILES['img_bandeira']['tmp_name'], $dir.$new_name)){ //Fazer upload do arquivo
                $linguagemObj->setBandeira($new_name);
            }
            
        }else{
            $_SESSION['retorno']['mensagem'] = 'Preencha a imagem!';
            $_SESSION['retorno']['classe'] = 'alert-danger';
            $_SESSION['retorno']['result'] = 'error';
            //$_SESSION['retorno']['obj'] = $noticiaObj;

            header('Location: '.URL_SITE.'/admin/view/configuracoes/linguagem-form.php');
            exit();
        }
        
        $cod = $daoLinguagem->Inserir($linguagemObj);
        
        
			 		 
	$_SESSION['retorno']['mensagem'] = 'Dados cadastrados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        
        header('Location: '.URL_SITE.'/admin/view/configuracoes/linguagem-form.php?cod='.$cod.'');
        exit;	

}
//alterar
if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){

	 $daoLinguagem = new DaoLinguagem();
         $linguagemObj = new Linguagem();
         
         
         $linguagemObj->setTitulo($linguagem);
         $linguagemObj->setAtivo($ativo);
         $linguagemObj->setId($cod);
         $linguagemObj->setBandeira($old_img);
         
         if(isset($_FILES['img_bandeira'])){
            
            $ext = strtolower(substr($_FILES['img_bandeira']['name'],-4)); //Pegando extensão do arquivo
            $new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
            $dir = URL_IMG.'/band/'; //Diretório para uploads

            if(move_uploaded_file($_FILES['img_bandeira']['tmp_name'], $dir.$new_name)){ //Fazer upload do arquivo
                $linguagemObj->setBandeira($new_name);
            }
            
        }else{
            $_SESSION['retorno']['mensagem'] = 'Preencha a imagem!';
            $_SESSION['retorno']['classe'] = 'alert-danger';
            $_SESSION['retorno']['result'] = 'error';
            //$_SESSION['retorno']['obj'] = $noticiaObj;

            header('Location: '.URL_SITE.'/admin/view/configuracoes/linguagem-form.php');
            exit();
        }
         
         $daoLinguagem->Editar($linguagemObj);
         
        
		
	$_SESSION['retorno']['mensagem'] = 'Dados alterados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';

        header('Location: '.URL_SITE.'/admin/view/configuracoes/linguagem-form.php?cod='.$cod.'');
        exit;
		

}
//deletar
if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){
	 
    $daoLinguagem = new DaoLinguagem();
    $daoLinguagem->Deletar($cod);
    
    $responta['mensagem'] = 'Linguagem deletada com sucesso';
    $responta['classe'] = 'alert-success';
    $responta['result'] = 'success';
    echo json_encode($responta);
    exit;
	
}



?>