<?php
include("../../../libs/config.php"); 
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

###################################  ENDEREÇO ################################################################
//cadastrar
if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
    
    $new_name = '';
    if(isset($_FILES['img_noticia'])){
        
        $ext = strtolower(substr($_FILES['img_noticia']['name'],-4)); //Pegando extensão do arquivo
        $new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
        $dir = URL_IMG.'/endereco/'; //Diretório para uploads

        if(!move_uploaded_file($_FILES['img_noticia']['tmp_name'], $dir.$new_name)){
            $new_name = '';
        } //Fazer upload do arquivo
        
    }
	
	$end = new Endereco();
        $daoEndereco = new DaoEndereco();
        
        $end->setUnidade($unidade);
        $end->setBairro($bairro);
        $end->setCep($cep);
        $end->setCidade($cidade);
        $end->setComplemento($complemento);
        $end->setEndereco($endereco);
        $end->setEstado($estado);
        $end->setNumero($numero);
        $end->setTelefone($telefone);
        $end->setLatitude($latitude);
        $end->setLongitude($longitude);
        $end->setImagem($new_name);
        $end->setEmail($email);
              
        $daoEndereco->Inserir($end);
        
        $_SESSION['retorno']['mensagem'] = 'Dados alterados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';

        header('Location: '.URL_SITE.'/admin/view/configuracoes/endereco-form.php?cod='.$cod.'');
        exit;
	
}
//alterar
if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){
    
    $new_name = '';
   
    if((isset($_FILES['img_noticia']) && isset($alt_img) && $alt_img == 's') || (isset($_FILES['img_noticia']) && !isset($alt_img))){
        
        $ext = strtolower(substr($_FILES['img_noticia']['name'],-4)); //Pegando extensão do arquivo
        $new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
        $dir = URL_IMG.'/endereco/'; //Diretório para uploads

        if(!move_uploaded_file($_FILES['img_noticia']['tmp_name'], $dir.$new_name)){
            $new_name = '';
        } //Fazer upload do arquivo
       
        
    }
        
	
	$end = new Endereco();
        $daoEndereco = new DaoEndereco();
        
        $end->setId($cod);
        $end->setUnidade($unidade);
        $end->setBairro($bairro);
        $end->setCep($cep);
        $end->setCidade($cidade);
        $end->setComplemento($complemento);
        $end->setEndereco($endereco);
        $end->setEstado($estado);
        $end->setNumero($numero);
        $end->setTelefone($telefone);
        $end->setLatitude($latitude);
        $end->setLongitude($longitude);
        $end->setEmail($email);
        if(isset($old_img)){
            $end->setImagem($old_img);
        }
        $end->setId($cod);
        
        if(isset($new_name) && $new_name <> '' && !empty($_FILES['img_noticia']['name'])){
            $end->setImagem($new_name);
        }
              
        $daoEndereco->Editar($end);
        
        $_SESSION['retorno']['mensagem'] = 'Dados alterados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';

        header('Location: '.URL_SITE.'/admin/view/configuracoes/endereco-form.php?cod='.$cod.'');
        exit;
	
}

?>