<?php
include("../../../libs/config.php"); 
include("../../dados/logado.php");

require_once '../../../dao/classes/Newsletter.php';
require_once '../../../dao/DaoNewsletter.php';

require_once '../../../vendor/autoload.php';

use Respect\Validation\Validator as v;

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

################################### NEWS ###########################################################################
//cadastrar
if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
        if(empty($email_news) || !v::email()->validate($email_news)){
            $responta['mensagem'] = 'E-mail Inválido';
            $responta['classe'] = 'alert-danger';
            $responta['result'] = 'error';
            echo json_encode($responta);
            exit;
        }
        
        /* if(empty($nome)){
            $responta['mensagem'] = 'Preencha o nome';
            $responta['classe'] = 'alert-danger';
            $responta['result'] = 'error';
            echo json_encode($responta);
            exit;
        } */
         
         $newsletter = new Newsletter();
         $daoNewsletter = new DaoNewsletter();
         
         $rs = $daoNewsletter->BuscarPorEmail($email_news, '');
         if($rs->getEmail()<>''){
                $responta['mensagem'] = 'E-mail já cadastrado';
                $responta['classe'] = 'alert-danger';
                $responta['result'] = 'error';
                echo json_encode($responta);
                exit;
         }
         
	 $date = date('Y-m-d H:i:s'); 
         $newsletter->setDataCadastro($date);
         $newsletter->setNome($nome);
         $newsletter->setEmail($email_news);
         
         $daoNewsletter->Inserir($newsletter);
         
            $responta['mensagem'] = 'E-mail cadastrado com sucesso';
            $responta['classe'] = 'alert-success';
            $responta['result'] = 'success';
            echo json_encode($responta);
            exit;
	
}
//alterar
if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){
	 
	if(empty($email_news) || !v::email()->validate($email_news)){
            $responta['mensagem'] = 'E-mail Inválido';
            $responta['classe'] = 'alert-danger';
            $responta['result'] = 'error';
            echo json_encode($responta);
            exit;
        }
        
        /* if(empty($nome)){
            $responta['mensagem'] = 'Preencha o nome';
            $responta['classe'] = 'alert-danger';
            $responta['result'] = 'error';
            echo json_encode($responta);
            exit;
        } */
         
         $newsletter = new Newsletter();
         $daoNewsletter = new DaoNewsletter();
         
         $rs = $daoNewsletter->BuscarPorEmail($email_news, $cod);
         if($rs->getEmail()<>''){
                $responta['mensagem'] = 'E-mail já cadastrado';
                $responta['classe'] = 'alert-danger';
                $responta['result'] = 'error';
                echo json_encode($responta);
                exit;
         }
         
         $newsletter->setNome($nome);
         $newsletter->setEmail($email_news);
         $newsletter->setId($cod);
         
         $daoNewsletter->Editar($newsletter);
		
            $responta['mensagem'] = 'E-mail alterado com sucesso';
            $responta['classe'] = 'alert-success';
            $responta['result'] = 'success';
            echo json_encode($responta);
            exit;
	
}
//deletar
if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){
        $daoNewsletter = new DaoNewsletter();
        $daoNewsletter->Deletar($cod);
		
	$responta['mensagem'] = 'E-mail excluido com sucesso';
        $responta['classe'] = 'alert-success';
        $responta['result'] = 'success';
        echo json_encode($responta);
        exit;
	
}


 ?>