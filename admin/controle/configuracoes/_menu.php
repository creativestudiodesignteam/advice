<?php 
//include("../../libs/conf.php"); 
include("../../../libs/config.php"); 
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}



####################################### USUARIO ######################################################################

//cadastrar
if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
    
         $daoMenu = new DaoMenu();
         $menuObj = new Menu();
         
         $table = $daoMenu->statusTable('tbl_menu');
         $id = $table['Auto_increment'];
         
         for($i=0;$i<count($linguagem);$i++){                
            $menuObj->setTitulo($menu[$i]);
            
            $menuObj->setAtivo($ativo[$i]);
            $menuObj->setLinguagem($linguagem[$i]);
            $menuObj->setId($id);
            $menuObj->setOrdem($ordem);
            $menuObj->setId_institucional($id_institucional);

            if($submenu=='r'){
               $menuObj->setLocal(1);
            }else{
               $menuObj->setSubMenu($submenu);
            }
            
            $menuObj->setLink(URL_amigavel($menu[$i]));

            $cod = $daoMenu->Inserir($menuObj);

            
         }
         
			 		 
	$_SESSION['retorno']['mensagem'] = 'Dados cadastrados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        
        header('Location: '.URL_SITE.'/admin/view/configuracoes/menu-form.php?cod='.$cod.'');
        exit;	

}
//alterar
if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){

	 $daoMenu = new DaoMenu();
         $menuObj = new Menu();
         
         
         for($i=0;$i<count($linguagem);$i++){                
            $menuObj->setTitulo($menu[$i]);
            $menuObj->setAtivo($ativo[$i]);
            $menuObj->setLinguagem($linguagem[$i]);
            $menuObj->setId($cod);
            $menuObj->setOrdem($ordem);
            $menuObj->setId_institucional($id_institucional);


            if($submenu=='r'){
               $menuObj->setLocal(1);
            }else{
               $menuObj->setSubMenu($submenu);
            }


            $menuObj->setLink(URL_amigavel($menu[$i]));
            
            $obj = $daoMenu->BuscarPorCODLing($cod,$linguagem[$i]);
            
            if($obj->getId() <> ''){
                $daoMenu->Editar($menuObj);
            }else{
                $cod = $daoMenu->Inserir($menuObj);
            }
         }

         
         
         
        
		
	$_SESSION['retorno']['mensagem'] = 'Dados alterados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';

        header('Location: '.URL_SITE.'/admin/view/configuracoes/menu-form.php?cod='.$cod.'');
        exit;
		

}
//deletar
if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){
	 
    $daoMenu = new DaoMenu();
    $daoMenu->Deletar($cod);
    
    $responta['mensagem'] = 'Menu deletada com sucesso';
    $responta['classe'] = 'alert-success';
    $responta['result'] = 'success';
    echo json_encode($responta);
    exit;
	
}



?>