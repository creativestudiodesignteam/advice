<?php

include("../../../libs/config.php"); 
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach($_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}




if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
         
        $daoAtuacao = new DaoAtuacao();
        $atuacao = new Atuacao();

        $new_name = ''; 
        if(isset($_FILES['img_atuacao'])){
            
            $ext = strtolower(substr($_FILES['img_atuacao']['name'],-4)); //Pegando extensão do arquivo
            $new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
            $dir = URL_IMG.'/atuacao/'; //Diretório para uploads

            if(!move_uploaded_file($_FILES['img_atuacao']['tmp_name'], $dir.$new_name)){ //Fazer upload do arquivo
                $new_name = ''; 
            }
            
        }

        
        $table = $daoAtuacao->statusTable('tbl_atuacao');
        $id = $table['Auto_increment'];
        
        for($i=0;$i<count($linguagem);$i++){
            $atuacao->setTitulo($nome[$i]);
            $atuacao->setResumo($resumo[$i]);
            $atuacao->setDescricao($texto[$i]);
            $atuacao->setOrdem($ordem);
            $atuacao->setIcone($icone);
            $atuacao->setAtivo($ativo);
            $atuacao->setImagem($new_name);
            $atuacao->setLinguagem($linguagem[$i]);
            $atuacao->setId($id);
            
            $cod = $daoAtuacao->Inserir($atuacao);
        }

        if(isset($ads) && count($ads)>0){
            for($i=0; $i<count($ads); $i++){
                $daoAtuacao->InserirAdvogado($ads[$i], $cod);
            }
        
        }

                 
        
        //exit();
        $_SESSION['retorno']['mensagem'] = 'Dados cadastrados com sucesso!';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        $_SESSION['retorno']['obj'] = $atuacao;

        header('Location: '.URL_SITE.'/admin/view/atuacao/atuacao-form.php?cod='.$cod);
        exit();
}

if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){
         
        $daoAtuacao = new DaoAtuacao();
        $atuacao = new Atuacao();

        $new_name = ''; 
        if((isset($_FILES['img_atuacao']) && (isset($alt_img) && $alt_img == 's')) || (isset($_FILES['img_atuacao']) && !isset($alt_img))){
            
            $ext = strtolower(substr($_FILES['img_atuacao']['name'],-4)); //Pegando extensão do arquivo
            $new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
            $dir = URL_IMG.'/atuacao/'; //Diretório para uploads

            if(!move_uploaded_file($_FILES['img_atuacao']['tmp_name'], $dir.$new_name)){ //Fazer upload do arquivo
                $new_name = ''; 
            }
            
        }
        
        for($i=0;$i<count($linguagem);$i++){
            
            $atuacao->setTitulo($nome[$i]);
            $atuacao->setResumo($resumo[$i]);
            $atuacao->setDescricao($texto[$i]);
            $atuacao->setOrdem($ordem);
            $atuacao->setIcone($icone);
            $atuacao->setAtivo($ativo);
            
            if(isset($old_img)){
                $atuacao->setImagem($old_img);
            }

            $atuacao->setLinguagem($linguagem[$i]);
            $atuacao->setId($cod);

            if(isset($new_name) && $new_name <> '' && !empty($_FILES['img_atuacao']['name'])){
                $atuacao->setImagem($new_name);
            }

            $obj = $daoAtuacao->BuscarPorCODLing($cod,$linguagem[$i]);
            
            if($obj->getId() <> ''){
                $daoAtuacao->Editar($atuacao);
            }else{
                $cod = $daoAtuacao->Inserir($atuacao);
            }
        }

        if(isset($ads) && count($ads)>0){
            $daoAtuacao->DeletarAdvogado($cod);

            for($i=0; $i<count($ads); $i++){
                $daoAtuacao->InserirAdvogado($ads[$i], $cod);
            }
        
        }
        
        $_SESSION['retorno']['mensagem'] = 'Dados alterados com sucesso!';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        //$_SESSION['retorno']['obj'] = $atuacao;

        header('Location: '.URL_SITE.'/admin/view/atuacao/atuacao-form.php?cod='.$cod);
        exit();
}

if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){
	 
        $daoAtuacao = new DaoAtuacao();
        $daoAtuacao->Deletar($cod);

        $responta['mensagem'] = 'Atuação deletada com sucesso';
        $responta['classe'] = 'alert-success';
        $responta['result'] = 'success';
        echo json_encode($responta);
        exit;

}
