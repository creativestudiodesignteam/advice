<?php 
include("../../../libs/config.php"); 

use Respect\Validation\Validator as v;
use Dao\Classes\Usuario;
use Dao\DaoUsuario;

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}



####################################### USUARIO ######################################################################

//cadastrar
if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
    
    $usuario = new Usuario();
    $daoUsuario = new DaoUsuario();

    $resposta = array();
        
    
    if(empty($nome)){
       
        $responta['mensagem'] = 'Preencha o Nome';
        $responta['classe'] = 'alert-danger';
        $responta['result'] = 'error';
        echo json_encode($responta);
        exit;
    }
    
    if(empty($grupo_usuario)){
        
        $responta['mensagem'] = 'Selecione um Grupo de Usuário';
        $responta['classe'] = 'alert-danger';
        $responta['result'] = 'error';
        echo json_encode($responta);
        exit;
    }

    
    if(empty($email) || !v::email()->validate($email)){
        
        $responta['mensagem'] = 'E-mail Inválido';
        $responta['classe'] = 'alert-danger';
        $responta['result'] = 'error';
        echo json_encode($responta);
        exit;
    }
    
    $rs = $daoUsuario->BuscarPorEmail($email, '');
    if($rs->getEmail()<>''){
        $responta['mensagem'] = 'E-mail já cadastrado para outro usuário';
        $responta['classe'] = 'alert-danger';
        $responta['result'] = 'error';
        echo json_encode($responta);
        exit;
    }
    
    if(!v::stringType()->length(6, null)->validate($senha)){
        $responta['mensagem'] = 'A senha deve ter no mínimo 6 caracteres';
        $responta['classe'] = 'alert-danger';
        $responta['result'] = 'error';
        echo json_encode($responta);
        exit;
    }
    
    if(empty($senha)){
        $responta['mensagem'] = 'Preencha a Senha';
        $responta['classe'] = 'alert-danger';
        $responta['result'] = 'error';
        echo json_encode($responta);
        exit;
    }
    
    if(empty($conf_senha)){
        $responta['mensagem'] = 'Preencha o Repetir Senha';
        $responta['classe'] = 'alert-danger';
        $responta['result'] = 'error';
        echo json_encode($responta);
        exit;
    }
    
    if($senha <> $conf_senha){
        $responta['mensagem'] = 'As senhas devem ser iguais';
        $responta['classe'] = 'alert-danger';
        $responta['result'] = 'error';
        echo json_encode($responta);
        exit;
    }
    
    if($ativo == ''){
        $responta['mensagem'] = 'Marque ativo ou não';
        $responta['classe'] = 'alert-danger';
        $responta['result'] = 'error';
        echo json_encode($responta);
        exit;
    }
    
    $usuario->setNome($nome);
    $usuario->setSenha($senha);
    $usuario->setEmail($email);
    $usuario->setGrupo($grupo_usuario);
    $usuario->setDataCadastro(date("Y-m-d H:i:s"));
    $usuario->setAtivo($ativo);    
    
    $daoUsuario->Inserir($usuario);
			 		 
   
    $responta['mensagem'] = 'Usuário cadastrado com sucesso';
    $responta['classe'] = 'alert-success';
    $responta['result'] = 'success';
    echo json_encode($responta);
    exit;

}
//alterar
if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){
    $usuario = new Usuario();
    $daoUsuario = new DaoUsuario();
    
    $resposta = array();
    
    
    
    if(empty($nome)){
       
        $responta['mensagem'] = 'Preencha o Nome';
        $responta['classe'] = 'alert-danger';
        $responta['result'] = 'error';
        echo json_encode($responta);
        exit;
    }
    
    if(empty($grupo_usuario)){
        
        $responta['mensagem'] = 'Selecione um Grupo de Usuário';
        $responta['classe'] = 'alert-danger';
        $responta['result'] = 'error';
        echo json_encode($responta);
        exit;
    }
    
    if(empty($email) || !v::email()->validate($email)){
        
        $responta['mensagem'] = 'E-mail Inválido';
        $responta['classe'] = 'alert-danger';
        $responta['result'] = 'error';
        echo json_encode($responta);
        exit;
    }
    
    $rs = $daoUsuario->BuscarPorEmail($email, $cod);
    if($rs->getEmail()<>''){
        $responta['mensagem'] = 'E-mail já cadastrado para outro usuário';
        $responta['classe'] = 'alert-danger';
        $responta['result'] = 'error';
        echo json_encode($responta);
        exit;
    }
    
    if(!v::stringType()->length(6, null)->validate($senha)){
        $responta['mensagem'] = 'A senha deve ter no mínimo 6 caracteres';
        $responta['classe'] = 'alert-danger';
        $responta['result'] = 'error';
        echo json_encode($responta);
        exit;
    }
    
    if(empty($senha)){
        $responta['mensagem'] = 'Preencha a Senha';
        $responta['classe'] = 'alert-danger';
        $responta['result'] = 'error';
        echo json_encode($responta);
        exit;
    }
    
    if(empty($conf_senha)){
        $responta['mensagem'] = 'Preencha o Repetir Senha';
        $responta['classe'] = 'alert-danger';
        $responta['result'] = 'error';
        echo json_encode($responta);
        exit;
    }
    
    if($senha <> $conf_senha){
        $responta['mensagem'] = 'As senhas devem ser iguais';
        $responta['classe'] = 'alert-danger';
        $responta['result'] = 'error';
        echo json_encode($responta);
        exit;
    }

    $usuario->setNome($nome);
    $usuario->setSenha($senha);
    $usuario->setEmail($email);
    $usuario->setGrupo($grupo_usuario);
    $usuario->setAtivo($ativo);
    $usuario->setId($cod);
    
    $daoUsuario->Editar($usuario);
    
    if($_SESSION['COD_USUARIO'] == $cod){
        $_SESSION['NOME_USUARIO']  = $usuario->getNome();
	$_SESSION['EMAIL_USUARIO'] = $usuario->getEmail();
	$_SESSION['COD_USUARIO']   = $usuario->getId();
	$_SESSION['GRUPO_USUARIO'] = $usuario->getGrupo();
	$_SESSION['SENHA_USUARIO'] = $usuario->getSenha();
    }
    
    $responta['mensagem'] = 'Usuário alterado com sucesso';
    $responta['classe'] = 'alert-success';
    $responta['result'] = 'success';
    echo json_encode($responta);
    exit;
		

}
//deletar
if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){
	 
	$daoUsuario = new DaoUsuario();
        $daoUsuario->Deletar($cod);
        
	$responta['mensagem'] = 'Usuário deletado com sucesso';
        $responta['classe'] = 'alert-success';
        $responta['result'] = 'success';
        echo json_encode($responta);
        exit;	
	
}



