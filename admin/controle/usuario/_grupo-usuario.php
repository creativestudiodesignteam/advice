<?php 
include("../../../libs/config.php"); 
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}



####################################### USUARIO ######################################################################

//cadastrar
if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
    
         $daoUsuarioGrupo = new DaoUsuarioGrupo();
         $usuarioGrupo = new UsuarioGrupo();
         
         $daoUsuarioGrupoPaginas = new DaoUsuarioGrupoPaginas();
         $usuarioGrupoPaginas = new UsuarioGrupoPaginas();
         
         $resposta = array();
         
         if(empty($nome)){

            $responta['mensagem'] = 'Preencha o Grupo';
            $responta['classe'] = 'alert-danger';
            $responta['result'] = 'error';
            echo json_encode($responta);
            exit;
         }
         
         if($ativo == ''){
            $responta['mensagem'] = 'Marque ativo ou não';
            $responta['classe'] = 'alert-danger';
            $responta['result'] = 'error';
            echo json_encode($responta);
            exit;
        }
                  
         $usuarioGrupo->setGrupo($nome);
         $usuarioGrupo->setAtivo($ativo);
         
         $insert = $daoUsuarioGrupo->Inserir($usuarioGrupo);

	 
         
         $cod = $insert;
         
         if(isset($pagina)){       
            for($i=0;$i<count($pagina); $i++){
                if(isset($pagina[$i]) && $pagina[$i]<> ""){
                    
                    $usuarioGrupoPaginas->setGrupo($cod);
                    $usuarioGrupoPaginas->setPagina($pagina[$i]);
                    
                    $daoUsuarioGrupoPaginas->Inserir($usuarioGrupoPaginas);

                }   
            }
         }else{
            $responta['mensagem'] = 'Selecione pelo menos uma página';
            $responta['classe'] = 'alert-danger';
            $responta['result'] = 'error';
            echo json_encode($responta);
            exit;
         }
			 		 
	$responta['mensagem'] = 'Grupo de Usuário cadastrado com sucesso';
        $responta['classe'] = 'alert-success';
        $responta['result'] = 'success';
        echo json_encode($responta);
        exit;

	}
//alterar
if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){

	 $daoUsuarioGrupo = new DaoUsuarioGrupo();
         $usuarioGrupo = new UsuarioGrupo();
         
         $daoUsuarioGrupoPaginas = new DaoUsuarioGrupoPaginas();
         $usuarioGrupoPaginas = new UsuarioGrupoPaginas();
         
         if(empty($nome)){

            $responta['mensagem'] = 'Preencha o Grupo';
            $responta['classe'] = 'alert-danger';
            $responta['result'] = 'error';
            echo json_encode($responta);
            exit;
         }
         
         if($ativo == ''){
            $responta['mensagem'] = 'Marque ativo ou não';
            $responta['classe'] = 'alert-danger';
            $responta['result'] = 'error';
            echo json_encode($responta);
            exit;
        }
         
         $usuarioGrupo->setGrupo($nome);
         $usuarioGrupo->setAtivo($ativo);
         $usuarioGrupo->setId($cod);
         
         $daoUsuarioGrupo->Editar($usuarioGrupo);
         
         $daoUsuarioGrupoPaginas->Deletar($cod);
         
         if(isset($pagina)){       
            for($i=0;$i<count($pagina); $i++){
                if(isset($pagina[$i]) && $pagina[$i]<> ""){
                    
                    $usuarioGrupoPaginas->setGrupo($cod);
                    $usuarioGrupoPaginas->setPagina($pagina[$i]);
                    
                    $daoUsuarioGrupoPaginas->Inserir($usuarioGrupoPaginas);

                }   
            }
        }else{
            $responta['mensagem'] = 'Selecione pelo menos uma página';
            $responta['classe'] = 'alert-danger';
            $responta['result'] = 'error';
            echo json_encode($responta);
            exit;
         }
		
	$responta['mensagem'] = 'Grupo de Usuário alterado com sucesso';
        $responta['classe'] = 'alert-success';
        $responta['result'] = 'success';
        echo json_encode($responta);
        exit;
		

}
//deletar
if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){
	 
    $daoUsuarioGrupo = new DaoUsuarioGrupo();
    $daoUsuarioGrupo->Deletar($cod);
    
    $responta['mensagem'] = 'Grupo de Usuário deletado com sucesso';
    $responta['classe'] = 'alert-success';
    $responta['result'] = 'success';
    echo json_encode($responta);
    exit;

}



?>