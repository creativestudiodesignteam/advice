<?php

include("../../../libs/config.php"); 
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach($_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}




if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
         
        $daoDepoimentos = new DaoDepoimentos();
        $depoimentos = new Depoimentos();
        
        $table = $daoDepoimentos->statusTable('tbl_depoimentos');
        $id = $table['Auto_increment'];
        
        for($i=0;$i<count($linguagem);$i++){
            $depoimentos->setTitulo($nome[$i]);
            $depoimentos->setTexto($texto[$i]);
            $depoimentos->setAtivo($ativo);
            $depoimentos->setData(date('Y-m-d H:i:s'));
            $depoimentos->setLinguagem($linguagem[$i]);
            $depoimentos->setId($id);
            
            $cod = $daoDepoimentos->Inserir($depoimentos);
        }

                 
        
        //exit();
        $_SESSION['retorno']['mensagem'] = 'Dados cadastrados com sucesso!';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        $_SESSION['retorno']['obj'] = $depoimentos;

        header('Location: '.URL_SITE.'/admin/view/depoimentos/depoimentos-form.php?cod='.$cod);
        exit();
}

if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){
         
        $daoDepoimentos = new DaoDepoimentos();
        $depoimentos = new Depoimentos();
        
        for($i=0;$i<count($linguagem);$i++){
            $depoimentos->setTitulo($nome[$i]);
            $depoimentos->setTexto($texto[$i]);
            $depoimentos->setAtivo($ativo);
            $depoimentos->setLinguagem($linguagem[$i]);
            $depoimentos->setId($cod);

            $obj = $daoDepoimentos->BuscarPorCODLing($cod,$linguagem[$i]);
           

            if($obj->getId() <> ''){
                $daoDepoimentos->Editar($depoimentos);
            }else{
                $cod = $daoDepoimentos->Inserir($depoimentos);
            }

        }

        $_SESSION['retorno']['mensagem'] = 'Dados alterados com sucesso!';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        //$_SESSION['retorno']['obj'] = $depoimentos;

        header('Location: '.URL_SITE.'/admin/view/depoimentos/depoimentos-form.php?cod='.$cod);
        exit();
}

if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){
	 
        $daoDepoimentos = new DaoDepoimentos();
        $daoDepoimentos->Deletar($cod);

        $responta['mensagem'] = 'Depoimento deletada com sucesso';
        $responta['classe'] = 'alert-success';
        $responta['result'] = 'success';
        echo json_encode($responta);
        exit;

}
