<?php 
//include("../../libs/conf.php"); 
include("../../../libs/config.php"); 
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}



####################################### USUARIO ######################################################################

//cadastrar
if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
    
         $daoDocumentoTipo = new DaoDocumentoTipo();
         $categoriaObj = new DocumentoTipo();
       
                  
            $categoriaObj->setTitulo($categoria);
            $categoriaObj->setAtivo($ativo);
            $categoriaObj->setSCodigo($codigo);
            
            $cod = $daoDocumentoTipo->Inserir($categoriaObj);
    
         
			 		 
	$_SESSION['retorno']['mensagem'] = 'Dados cadastrados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        
        header('Location: '.URL_SITE.'/admin/view/produtos/tipodocumentos-form.php?cod='.$cod.'');
        exit;	

}
//alterar
if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){

    $daoDocumentoTipo = new DaoDocumentoTipo();
    $categoriaObj = new DocumentoTipo();
        
    $categoriaObj->setTitulo($categoria);
    $categoriaObj->setAtivo($ativo);
    $categoriaObj->setSCodigo($codigo);
    $categoriaObj->setId($cod);
    
    $daoDocumentoTipo->Editar($categoriaObj);
    
    $_SESSION['retorno']['mensagem'] = 'Dados alterados com sucesso! ';
    $_SESSION['retorno']['classe'] = 'alert-success';
    $_SESSION['retorno']['result'] = 'success';

    header('Location: '.URL_SITE.'/admin/view/produtos/tipodocumentos-form.php?cod='.$cod.'');
    exit;
		

}
//deletar
if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){
	 
    $daoDocumentoTipo = new DaoDocumentoTipo();
    $daoDocumentoTipo->Deletar($cod);
    
    $responta['mensagem'] = 'DocumentoTipo deletada com sucesso';
    $responta['classe'] = 'alert-success';
    $responta['result'] = 'success';
    echo json_encode($responta);
    exit;
	
}



?>