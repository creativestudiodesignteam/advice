<?php 
//include("../../libs/conf.php"); 
include("../../../libs/config.php"); 
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}



####################################### USUARIO ######################################################################

//cadastrar
if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
    
         $daoCategoria = new DaoCategoria();
         $categoriaObj = new Categoria();

         /* if(isset($_FILES['img_noticia'])){
            
            $ext = strtolower(substr($_FILES['img_noticia']['name'],-4)); //Pegando extensão do arquivo
            $new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
            $dir = URL_IMG.'/segment/'; //Diretório para uploads

            if(!move_uploaded_file($_FILES['img_noticia']['tmp_name'], $dir.$new_name)){
                $new_name = '';
            } //Fazer upload do arquivo
            
        }else{
            $_SESSION['retorno']['mensagem'] = 'Preencha a imagem!';
            $_SESSION['retorno']['classe'] = 'alert-danger';
            $_SESSION['retorno']['result'] = 'error';
            //$_SESSION['retorno']['obj'] = $noticiaObj;

            header('Location: '.URL_SITE.'/admin/view/produtos/categorias-form.php');
            exit();
        } */
         
         $table = $daoCategoria->statusTable('tbl_categoria');
         $id = $table['Auto_increment'];


         for($i=0;$i<count($linguagem);$i++){                
            $categoriaObj->setTitulo($categoria[$i]);
            $categoriaObj->setAtivo($ativo[$i]);
            $categoriaObj->setLinguagem($linguagem[$i]);
            $categoriaObj->setId($id);

            if(isset($new_name) && $new_name <> '' && !empty($_FILES['img_noticia']['name'])){
                $categoriaObj->setImagem($new_name);
            }
            
            $cod = $daoCategoria->Inserir($categoriaObj);
         }         
         
			 		 
	$_SESSION['retorno']['mensagem'] = 'Dados cadastrados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        
        header('Location: '.URL_SITE.'/admin/view/produtos/categorias-form.php?cod='.$cod.'');
        exit;	

}
//alterar
if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){

	 $daoCategoria = new DaoCategoria();
         $categoriaObj = new Categoria();

        if((isset($_FILES['img_noticia']) && $alt_img == 's') || (isset($_FILES['img_noticia']) && !isset($alt_img))){
        
            $ext = strtolower(substr($_FILES['img_noticia']['name'],-4)); //Pegando extensão do arquivo
            $new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
            $dir = URL_IMG.'/segment/'; //Diretório para uploads

            if(!move_uploaded_file($_FILES['img_noticia']['tmp_name'], $dir.$new_name)){
                $new_name = '';
            } //Fazer upload do arquivo
        
        
        }else if(empty($_FILES['img_noticia']['name']) && $alt_img == 's'){
            $_SESSION['retorno']['mensagem'] = 'Preencha a imagem!';
            $_SESSION['retorno']['classe'] = 'alert-danger';
            $_SESSION['retorno']['result'] = 'error';
            //$_SESSION['retorno']['obj'] = $noticiaObj;

            header('Location: '.URL_SITE.'/admin/view/produtos/categorias-form.php?cod='.$cod.'');
            exit();
        } 
         
         
         for($i=0;$i<count($linguagem);$i++){                
            $categoriaObj->setTitulo($categoria[$i]);
            $categoriaObj->setAtivo($ativo[$i]);
            $categoriaObj->setLinguagem($linguagem[$i]);
            $categoriaObj->setId($cod);
            
            $obj = $daoCategoria->BuscarPorCODLing($cod,$linguagem[$i]);
            
            if($obj->getId() <> ''){
                $daoCategoria->Editar($categoriaObj);
            }else{
                $cod = $daoCategoria->Inserir($categoriaObj);
            }
         }

         
         
         
        
		
	$_SESSION['retorno']['mensagem'] = 'Dados alterados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';

        header('Location: '.URL_SITE.'/admin/view/produtos/categorias-form.php?cod='.$cod.'');
        exit;
		

}
//deletar
if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){
	 
    $daoCategoria = new DaoCategoria();
    $daoCategoria->Deletar($cod);
    
    $responta['mensagem'] = 'Categoria deletada com sucesso';
    $responta['classe'] = 'alert-success';
    $responta['result'] = 'success';
    echo json_encode($responta);
    exit;
	
}



?>