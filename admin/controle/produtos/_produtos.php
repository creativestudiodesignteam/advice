<?php 
//include("../../libs/conf.php"); 
include("../../../libs/config.php"); 
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}



####################################### USUARIO ######################################################################

//cadastrar
if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
    
        $daoProduto = new DaoProduto();
        $produtoObj = new Produto();
        $daoDocumentoTipo = new DaoDocumentoTipo();

        $tipo = $daoDocumentoTipo->BuscarPorCOD($tipo_doc);
        $new_name = '';
        if(isset($_FILES['upload']['name'])){
            
            $ext = strtolower(substr($_FILES['upload']['name'],-4)); //Pegando extensão do arquivo
            //$new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
            $new_name = $_FILES['upload']['name'];
            $dir = LINK_DOC.'/'.$tipo->getTitulo().'/'; //Diretório para uploads

            if(!move_uploaded_file($_FILES['upload']['tmp_name'], $dir.$new_name)){
                $new_name = '';
            } //Fazer upload do arquivo
            
        }
        
        $table = $daoProduto->statusTable('produtos');
        $id = $table['Auto_increment'];
         
        for($i=0;$i<count($linguagem);$i++){                
            $produtoObj->setTitulo($produto[$i]);
            $produtoObj->setAtivo($ativo[$i]);
            $produtoObj->setFarmacopeias($farmacopeias[$i]);
            $produtoObj->setLinguagem($linguagem[$i]);
            $produtoObj->setId($id);
            
            $cod = $daoProduto->Inserir($produtoObj);
        }

        if(!empty($segmento)){	 
            for ($i = 0; $i < count($segmento); $i++){ 
                if(isset($segmento[$i]) && !empty($segmento[$i])){
                    $daoProduto->InserirProdutoSegmento($cod, $segmento[$i]);
                }
            } 
        }
         
        if(!empty($subcategoria)){	 
            for ($i = 0; $i < count($subcategoria); $i++){ 
                if(isset($subcategoria[$i]) && !empty($subcategoria[$i])){
                    $daoProduto->InserirProdutoCategoria_prod($cod, $subcategoria[$i], $categoria_segmento[$i]);
    
                    if(isset($aplicacao[$i]) && !empty($aplicacao[$i])){  
                    $daoProduto->InserirProdutoAplicacao($cod, $aplicacao[$i], $subcategoria[$i]);
                    }
                }    
                    
            } 
        }

        $daoDocumento = new DaoDocumento();
        $documento = new Documento();

        if($new_name<>''){
            $documento->setCod_iqa($cod);
            $documento->setDir('produtos/docs/'.$tipo->getTitulo().'/');
            $documento->setTipo($tipo->getId());
            $documento->setDocumento($new_name);
            $documento->setAtivo('s');

            $daoDocumento->Inserir($documento);
        }                                 

	    $_SESSION['retorno']['mensagem'] = 'Dados cadastrados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        
        header('Location: '.URL_SITE.'/admin/view/produtos/produtos-form.php?cod='.$cod.'');
        exit;	

}
//alterar
if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){

    $daoProduto = new DaoProduto();
    $produtoObj = new Produto();
    $daoDocumentoTipo = new DaoDocumentoTipo();

    $tipo = $daoDocumentoTipo->BuscarPorCOD($tipo_doc);
    $new_name = '';
    if((isset($_FILES['upload']) && $alt_img == 's') || (isset($_FILES['upload']) && !isset($alt_img))){
            
        $ext = strtolower(substr($_FILES['upload']['name'],-4)); //Pegando extensão do arquivo
        //$new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
        $new_name = $_FILES['upload']['name'];
        $dir = LINK_DOC.'/'.$tipo->getTitulo().'/'; //Diretório para uploads

        if(!move_uploaded_file($_FILES['upload']['tmp_name'], $dir.$new_name)){
            $new_name = '';
        } //Fazer upload do arquivo
        
        
    }else if(empty($_FILES['upload']['name']) && $alt_img == 's'){
        $_SESSION['retorno']['mensagem'] = 'Preencha o PDF!';
        $_SESSION['retorno']['classe'] = 'alert-danger';
        $_SESSION['retorno']['result'] = 'error';
        //$_SESSION['retorno']['obj'] = $noticiaObj;

        header('Location: '.URL_SITE.'/admin/view/produtos/produtos-form.php?cod='.$cod.'');
        exit();
    }

    $daoDocumento = new DaoDocumento();
    $documento = new Documento();

    if($new_name<>''){
        $daoDocumento->Deletar($cod);

        $documento->setCod_iqa($cod);
        $documento->setDir('produtos/docs/'.$tipo->getTitulo().'/');
        $documento->setTipo($tipo->getId());
        $documento->setDocumento($new_name);
        $documento->setAtivo('s');

        $daoDocumento->Inserir($documento);
    }
        
    for($i=0;$i<count($linguagem);$i++){                
        $produtoObj->setTitulo($produto[$i]);
        $produtoObj->setFarmacopeias($farmacopeias[$i]);
        $produtoObj->setAtivo($ativo[$i]);
        $produtoObj->setLinguagem($linguagem[$i]);
        $produtoObj->setId($cod);
    
        $obj = $daoProduto->BuscarPorCODLing($cod,$linguagem[$i]);
        
        if($obj->getId() <> ''){
            $daoProduto->Editar($produtoObj);
        }else{
            $cod = $daoProduto->Inserir($produtoObj);
        }
    }

    if(!empty($segmentos)){	 
        $daoProduto->DeletarProdutoSegmento($cod);

        for ($i = 0; $i < count($segmentos); $i++){ 
            if(isset($segmentos[$i]) && !empty($segmentos[$i])){
                $daoProduto->InserirProdutoSegmento($cod, $segmentos[$i]);
            }
        } 
    }
    
    if(!empty($subcategoria)){	 
        $daoProduto->DeletarProdutoCategoria_prod($cod);
        $daoProduto->DeletarProdutoAplicacao($cod);

        for ($i = 0; $i < count($subcategoria); $i++){ 
            if(isset($subcategoria[$i]) && !empty($subcategoria[$i])){
                    $daoProduto->InserirProdutoCategoria_prod($cod, $subcategoria[$i], $categoria2[$i]);

                if(isset($aplicacao[$i]) && !empty($aplicacao[$i])){  
                    $daoProduto->InserirProdutoAplicacao($cod, $aplicacao[$i], $subcategoria[$i]);
                }
            }    
                
        } 
    }
   
         
         
        
		
	    $_SESSION['retorno']['mensagem'] = 'Dados alterados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';

        header('Location: '.URL_SITE.'/admin/view/produtos/produtos-form.php?cod='.$cod.'');
        exit;
		

}
//deletar
if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){
	 
    $daoProduto = new DaoProduto();
    $daoProduto->Deletar($cod);
    
    $responta['mensagem'] = 'Produto deletada com sucesso';
    $responta['classe'] = 'alert-success';
    $responta['result'] = 'success';
    echo json_encode($responta);
    exit;
	
}



?>