<?php 
//include("../../libs/conf.php"); 
include("../../../libs/config.php"); 
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}



####################################### USUARIO ######################################################################

//cadastrar
if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
    
        $daoAplicacaos = new DaoAplicacaos();
        $categoriaObj = new Aplicacaos();


        $table = $daoAplicacaos->statusTable('segmento');
        $id = $table['Auto_increment'];
        
        for($i=0;$i<count($linguagem);$i++){                
            $categoriaObj->setTitulo($categoria[$i]);
            $categoriaObj->setAtivo($ativo[$i]);
            $categoriaObj->setLinguagem($linguagem[$i]);
            $categoriaObj->setCategoria($categorias);
            $categoriaObj->setId($id);
            
            $cod = $daoAplicacaos->Inserir($categoriaObj);
        }
         
			 		 
	    $_SESSION['retorno']['mensagem'] = 'Dados cadastrados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        
        header('Location: '.URL_SITE.'/admin/view/produtos/aplicacao-form.php?cod='.$cod.'');
        exit;	

}
//alterar
if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){

	 $daoAplicacaos = new DaoAplicacao();
         $categoriaObj = new Aplicacao(); 
         
         
        for($i=0;$i<count($linguagem);$i++){                
            $categoriaObj->setTitulo($categoria[$i]);
            $categoriaObj->setAtivo($ativo[$i]);
            $categoriaObj->setLinguagem($linguagem[$i]);
            $categoriaObj->setCategoria($categorias);
            $categoriaObj->setId($cod);
            
            $obj = $daoAplicacaos->BuscarPorCODLing($cod,$linguagem[$i]);
            
            if($obj->getId() <> ''){
                $daoAplicacaos->Editar($categoriaObj);
            }else{
                $cod = $daoAplicacaos->Inserir($categoriaObj);
            }
        }

         
         
         
        
		
	$_SESSION['retorno']['mensagem'] = 'Dados alterados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';

        header('Location: '.URL_SITE.'/admin/view/produtos/aplicacao-form.php?cod='.$cod.'');
        exit;
		

}
//deletar
if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){
	 
    $daoAplicacaos = new DaoAplicacao();
    $daoAplicacaos->Deletar($cod);
    $daoAplicacaos->DeletarCatXCatProd($cod);

    $resposta['mensagem'] = 'Aplicacaos deletada com sucesso';
    $resposta['classe'] = 'alert-success';
    $resposta['result'] = 'success';
    echo json_encode($resposta);
    exit;
}
	
if(isset($_GET["acao"]) && ($_GET["acao"]=="busca")){
    
    $DaoAplicacao = new DaoAplicacao();
    $aplicacao = $DaoAplicacao->BuscarTodosCategoria(1, $categoria);

    
?>
    <select class="aplicacao<?php echo $linha ?> form-control" name="aplicacao[]" id="aplicacao<?php echo $linha ?>">
    <?php foreach($aplicacao as $s){
    echo "<option value=".$s->getId().">".$s->getTitulo()."</option>";
    }
?>
</select>
<?php }?>