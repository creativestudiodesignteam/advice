<?php 
//include("../../libs/conf.php"); 
include("../../../libs/config.php"); 
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}



####################################### USUARIO ######################################################################

//cadastrar
if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
    
        $daoCategoriaProd = new DaoCategoriaProd();
        $categoriaObj = new CategoriaProd();
        
        $table = $daoCategoriaProd->statusTable('categorias_prod');
        $id = $table['Auto_increment'];
        
        for($i=0;$i<count($linguagem);$i++){                
            $categoriaObj->setTitulo($categoria[$i]);
            $categoriaObj->setAtivo($ativo[$i]);
            $categoriaObj->setLinguagem($linguagem[$i]);
            $categoriaObj->setId($id);
            
            $cod = $daoCategoriaProd->Inserir($categoriaObj);
        }

        if(count($categorias)>0){
            for($i=0;$i<count($categorias);$i++){
                $daoCategoriaProd->InserirCatXCatProd($categorias[$i], $cod);
            }
        }else{
            $_SESSION['retorno']['mensagem'] = 'Selecione pelo menos uma categoria! ';
            $_SESSION['retorno']['classe'] = 'alert-error';
            $_SESSION['retorno']['result'] = 'error';
            
            header('Location: '.URL_SITE.'/admin/view/produtos/subcategorias-form.php?cod='.$cod.'');
            exit;
        }
         
			 		 
	    $_SESSION['retorno']['mensagem'] = 'Dados cadastrados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        
        header('Location: '.URL_SITE.'/admin/view/produtos/subcategorias-form.php?cod='.$cod.'');
        exit;	

}
//alterar
if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){

	 $daoCategoriaProd = new DaoCategoriaProd();
         $categoriaObj = new CategoriaProd();
         
         
        for($i=0;$i<count($linguagem);$i++){                
            $categoriaObj->setTitulo($categoria[$i]);
            $categoriaObj->setAtivo($ativo[$i]);
            $categoriaObj->setLinguagem($linguagem[$i]);
            $categoriaObj->setId($cod);
            
            $obj = $daoCategoriaProd->BuscarPorCODLing($cod,$linguagem[$i]);
            
            if($obj->getId() <> ''){
                $daoCategoriaProd->Editar($categoriaObj);
            }else{
                $cod = $daoCategoriaProd->Inserir($categoriaObj);
            }
        }

        if(count($categorias)>0){
            $daoCategoriaProd->DeletarCatXCatProd($cod);

            for($i=0;$i<count($categorias);$i++){
                $daoCategoriaProd->InserirCatXCatProd($categorias[$i], $cod);
            }
        }else{
            $_SESSION['retorno']['mensagem'] = 'Selecione pelo menos uma categoria! ';
            $_SESSION['retorno']['classe'] = 'alert-error';
            $_SESSION['retorno']['result'] = 'error';
            
            header('Location: '.URL_SITE.'/admin/view/produtos/subcategorias-form.php?cod='.$cod.'');
            exit;
        }

         
         
         
        
		
	$_SESSION['retorno']['mensagem'] = 'Dados alterados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';

        header('Location: '.URL_SITE.'/admin/view/produtos/subcategorias-form.php?cod='.$cod.'');
        exit;
		

}
//deletar
if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){
	 
    $daoCategoriaProd = new DaoCategoriaProd();
    $daoCategoriaProd->Deletar($cod);
    $daoCategoriaProd->DeletarCatXCatProd($cod);

    $resposta['mensagem'] = 'CategoriaProd deletada com sucesso';
    $resposta['classe'] = 'alert-success';
    $resposta['result'] = 'success';
    echo json_encode($resposta);
    exit;
	
}

if(isset($_GET["acao"]) && ($_GET["acao"]=="busca")){
    
    $DaoCategoriaProd = new DaoCategoriaProd();
    $subcategoria = $DaoCategoriaProd->BuscarTodosCategoria(1, $categoria);

    
?>
    <select class="subcategoria<?php echo $linha ?> form-control" name="subcategoria[]" id="subcategoria<?php echo $linha ?>">
    <?php foreach($subcategoria as $s){
    echo "<option value=".$s->getId().">".$s->getTitulo()."</option>";
    }
?>
</select>
<?php }?>