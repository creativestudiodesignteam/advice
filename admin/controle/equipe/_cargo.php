<?php 
//include("../../libs/conf.php"); 
include("../../../libs/config.php"); 
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}



####################################### USUARIO ######################################################################

//cadastrar
if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
    
         $daoCargo = new DaoCargo();
         $cargoObj = new Cargo();
         
         $table = $daoCargo->statusTable('tbl_cargo');
         $id = $table['Auto_increment'];
         
         for($i=0;$i<count($linguagem);$i++){                
            $cargoObj->setTitulo($titulo[$i]);
            $cargoObj->setAtivo($ativo);
            $cargoObj->setLinguagem($linguagem[$i]);
            $cargoObj->setId($id);
            
            $cod = $daoCargo->Inserir($cargoObj);
         }
         
			 		 
	$_SESSION['retorno']['mensagem'] = 'Dados cadastrados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        
        header('Location: '.URL_SITE.'/admin/view/equipe/cargo-form.php?cod='.$cod.'');
        exit;	

}
//alterar
if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){

	    $daoCargo = new DaoCargo();
        $cargoObj = new Cargo();
         
         
        for($i=0;$i<count($linguagem);$i++){                
            $cargoObj->setTitulo($titulo[$i]);
            $cargoObj->setAtivo($ativo);
            $cargoObj->setLinguagem($linguagem[$i]);
            $cargoObj->setId($cod);
            
            $obj = $daoCargo->BuscarPorCODLing($cod,$linguagem[$i]);
            
            if($obj->getId() <> ''){
                $daoCargo->Editar($cargoObj);
            }else{
                $cod = $daoCargo->Inserir($cargoObj);
            }
        }

         
         
         
        
		
	$_SESSION['retorno']['mensagem'] = 'Dados alterados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';

        header('Location: '.URL_SITE.'/admin/view/equipe/cargo-form.php?cod='.$cod.'');
        exit;
		

}
//deletar
if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){
	 
    $daoCargo = new DaoCargo();
    $daoCargo->Deletar($cod);
    
    $responta['mensagem'] = 'Cargo deletada com sucesso';
    $responta['classe'] = 'alert-success';
    $responta['result'] = 'success';
    echo json_encode($responta);
    exit;
	
}



?>