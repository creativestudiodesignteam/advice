<?php 
//include("../../libs/conf.php"); 
include("../../../libs/config.php"); 
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}



####################################### USUARIO ######################################################################

//cadastrar
if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
    
         $daoReconhecimento = new DaoReconhecimento();
         $reconhecimentoObj = new Reconhecimento();
         
         $table = $daoReconhecimento->statusTable('tbl_reconhecimentos');
         $id = $table['Auto_increment'];

        $new_name = ''; 
        if(isset($_FILES['img_noticia'])){
            
            $ext = strtolower(substr($_FILES['img_noticia']['name'],-4)); //Pegando extensão do arquivo
            $new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
            $dir = URL_IMG.'/placeholders/'; //Diretório para uploads

            if(!move_uploaded_file($_FILES['img_noticia']['tmp_name'], $dir.$new_name)){ //Fazer upload do arquivo
                $new_name = ''; 
            }
            
        }

        for($i=0;$i<count($linguagem);$i++){                
            $reconhecimentoObj->setTitulo($titulo[$i]);
            $reconhecimentoObj->setAtivo($ativo);
            $reconhecimentoObj->setOrdem($ordem);
            $reconhecimentoObj->setDestaque($destaque);
            $reconhecimentoObj->setLinguagem($linguagem[$i]);
            $reconhecimentoObj->setTexto($texto[$i]);
            $reconhecimentoObj->setId($id);
            $reconhecimentoObj->setUrl($url);

            if(isset($new_name) && $new_name <> '' && !empty($_FILES['img_noticia']['name'])){
                $reconhecimentoObj->setImagem($new_name);
            }
            
            $cod = $daoReconhecimento->Inserir($reconhecimentoObj);
        }

    $_SESSION['retorno']['mensagem'] = 'Dados cadastrados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        
        header('Location: '.URL_SITE.'/admin/view/institucional/reconhecimento-form.php?cod='.$cod.'');
        exit;	

}
//alterar
if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){

	    $daoReconhecimento = new DaoReconhecimento();
        $reconhecimentoObj = new Reconhecimento();
        
        $new_name = ''; 
        if((isset($_FILES['img_noticia']) && $alt_img == 's') || (isset($_FILES['img_noticia']) && !isset($alt_img))){
            
            $ext = strtolower(substr($_FILES['img_noticia']['name'],-4)); //Pegando extensão do arquivo
            $new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
            $dir = URL_IMG.'/placeholders/'; //Diretório para uploads

            if(!move_uploaded_file($_FILES['img_noticia']['tmp_name'], $dir.$new_name)){ //Fazer upload do arquivo
                $new_name = ''; 
            }
            
        }
         
        for($i=0;$i<count($linguagem);$i++){                
            $reconhecimentoObj->setTitulo($titulo[$i]);
            $reconhecimentoObj->setAtivo($ativo);
            $reconhecimentoObj->setOrdem($ordem);
            $reconhecimentoObj->setDestaque($destaque);
            $reconhecimentoObj->setLinguagem($linguagem[$i]);
            $reconhecimentoObj->setTexto($texto[$i]);
            $reconhecimentoObj->setId($cod);
            $reconhecimentoObj->setUrl($url);
            
            $obj = $daoReconhecimento->BuscarPorCODLing($cod,$linguagem[$i]);
            
            if(isset($old_img)){
                $reconhecimentoObj->setImagem($old_img);
            }

            if(isset($new_name) && $new_name <> '' && !empty($_FILES['img_noticia']['name'])){
                $reconhecimentoObj->setImagem($new_name);
            }
            
            if($obj->getId() <> ''){
                $daoReconhecimento->Editar($reconhecimentoObj);
            }else{
                $cod = $daoReconhecimento->Inserir($reconhecimentoObj);
            }
        }

         
         
         
        
		
	    $_SESSION['retorno']['mensagem'] = 'Dados alterados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';

        header('Location: '.URL_SITE.'/admin/view/institucional/reconhecimento-form.php?cod='.$cod.'');
        exit;
		

}
//deletar
if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){
	 
    $daoReconhecimento = new DaoReconhecimento();
    $daoReconhecimento->Deletar($cod);
    
    $responta['mensagem'] = 'Reconhecimento apagado com sucesso';
    $responta['classe'] = 'alert-success';
    $responta['result'] = 'success';
    echo json_encode($responta);
    exit;
	
}



?>