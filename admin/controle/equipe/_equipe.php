<?php 
//include("../../libs/conf.php"); 
include("../../../libs/config.php"); 
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}



####################################### USUARIO ######################################################################

//cadastrar
if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
    
        $DaoAdvogado = new DaoAdvogado();
        $pojo = new Advogado();
        
        $new_name = ''; 
        if(isset($_FILES['img_equipe'])){
            
            $ext = strtolower(substr($_FILES['img_equipe']['name'],-4)); //Pegando extensão do arquivo
            $new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
            $dir = URL_IMG.'/equipe/'; //Diretório para uploads

            if(!move_uploaded_file($_FILES['img_equipe']['tmp_name'], $dir.$new_name)){ //Fazer upload do arquivo
                $new_name = ''; 
            }
            
        }

        $new_vcard = ''; 
        if(isset($_FILES['vcard'])){
            
            $ext = strtolower(substr($_FILES['vcard']['name'],-4)); //Pegando extensão do arquivo
            $new_vcard = date("YmdHis") . $ext; //Definindo um novo nome para o arquivo
            $dir = RAIZ.'/vcard/'; //Diretório para uploads

            if(!move_uploaded_file($_FILES['vcard']['tmp_name'], $dir.$new_vcard)){ //Fazer upload do arquivo
                $new_vcard = ''; 
            }
            
        }

        $table = $DaoAdvogado->statusTable('tbl_advogados');
        $id = $table['Auto_increment'];

        for($i=0;$i<count($linguagem);$i++){    
            $pojo->setId($id);
            $pojo->setTitulo($titulo[$i]);
            $pojo->setLinguagem($linguagem[$i]);
            $pojo->setAtividades($atividades[$i]);
            $pojo->setAtivo($ativo[$i]);
            $pojo->setFormacao_academica($formacao[$i]);
            $pojo->setTelefone($telefone);
            $pojo->setEmail($email);
            $pojo->setVcard($new_vcard);
            $pojo->setCargo($posicao[$i]);
            $pojo->setGplus($gplus);
            $pojo->setLinkedin($linkedin);
            $pojo->setImagem($new_name);
            $pojo->setOrdem($ordem);

            $cod = $DaoAdvogado->Inserir($pojo);
        }

        /* exit(); */

        if(isset($idioma) && count($idioma)>0){

            for($i=0; $i<count($idioma); $i++){
                $DaoAdvogado->InserirIdioma($id, $idioma[$i]);
                //echo $idioma[$i];
            }

        }
       // exit();
        if(isset($atuacao) && count($atuacao)>0){
            for($i=0; $i<count($atuacao); $i++){
                $DaoAdvogado->InserirAtuacao($id, $atuacao[$i]);
            }
        }

        if(isset($reconhecimento) && count($reconhecimento)>0){
            for($i=0; $i<count($reconhecimento); $i++){
                $DaoAdvogado->InserirReconhecimento($id, $reconhecimento[$i]);
            }
        
        }
        
			 		 
	$_SESSION['retorno']['mensagem'] = 'Dados cadastrados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        //$_SESSION['retorno']['obj'] = $equipeObj;
        
        header('Location: '.URL_SITE.'/admin/view/equipe/equipe-form.php?cod='.$id.'');
        exit;	

}
//alterar
if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){

	    $DaoAdvogado = new DaoAdvogado();
        $pojo = new Advogado();
         
        $new_name = ''; 
        if((isset($_FILES['img_equipe']) && $alt_img == 's') || (isset($_FILES['img_equipe']) && !isset($alt_img))){
            
            $ext = strtolower(substr($_FILES['img_equipe']['name'],-4)); //Pegando extensão do arquivo
            $new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
            $dir = URL_IMG.'/equipe/'; //Diretório para uploads

            if(!move_uploaded_file($_FILES['img_equipe']['tmp_name'], $dir.$new_name)){ //Fazer upload do arquivo
                $new_name = ''; 
            }
            
        }

        $new_vcard = ''; 
        
        if((isset($_FILES['vcard']['name']) && !empty($_FILES['vcard']['name']) && isset($alt_vcard) && $alt_vcard == 's') || (isset($_FILES['vcard']['name']) && !isset($alt_vcard))){
            
            $ext = strtolower(substr($_FILES['vcard']['name'],-4)); //Pegando extensão do arquivo
            $new_vcard = date("YmdHis") . $ext; //Definindo um novo nome para o arquivo
            $dir = RAIZ.'/vcard/'; //Diretório para uploads

            if(!move_uploaded_file($_FILES['vcard']['tmp_name'], $dir.$new_vcard)){ //Fazer upload do arquivo
                $new_vcard = ''; 
            }
        }
        
            for($i=0;$i<count($linguagem);$i++){                
                $pojo->setTitulo($titulo[$i]);
                $pojo->setLinguagem($linguagem[$i]);
                $pojo->setAtividades($atividades[$i]);
                $pojo->setAtivo($ativo[$i]);
                $pojo->setFormacao_academica($formacao[$i]);
                $pojo->setTelefone($telefone);
                $pojo->setEmail($email);
                $pojo->setOrdem($ordem);

                if(isset($old_vcard)){
                    $pojo->setVcard($old_vcard);
                }

                if(isset($old_img)){
                    $pojo->setImagem($old_img);
                }

                $pojo->setCargo($posicao[$i]);
                $pojo->setGplus($gplus);
                $pojo->setLinkedin($linkedin);
                $pojo->setId($cod);

                if(isset($new_name) && $new_name <> '' && !empty($_FILES['img_equipe']['name'])){
                    $pojo->setImagem($new_name);
                }
                
                if(isset($new_vcard) && $new_vcard <> '' && !empty($_FILES['vcard']['name'])){
                    $pojo->setVcard($new_vcard);
                }
                
                $obj = $DaoAdvogado->BuscarPorCODLing($cod,$linguagem[$i]);
                
                if($obj->getId() <> ''){
                    $DaoAdvogado->Editar($pojo);
                }else{
                    $cod = $DaoAdvogado->Inserir($pojo);
                }
            }

            /* exit(); */
            

            if(isset($idioma) && count($idioma)>0){
                $DaoAdvogado->DeletarIdioma($cod);

                for($i=0; $i<count($idioma); $i++){
                    $DaoAdvogado->InserirIdioma($cod, $idioma[$i]);
                    //echo $idioma[$i];
                }

            }
           // exit();
            if(isset($atuacao) && count($atuacao)>0){
                $DaoAdvogado->DeletarAtuacao($cod);

                for($i=0; $i<count($atuacao); $i++){
                    $DaoAdvogado->InserirAtuacao($cod, $atuacao[$i]);
                }
            
            }

            if(isset($reconhecimento) && count($reconhecimento)>0){
                $DaoAdvogado->DeletarReconhecimento($cod);

                for($i=0; $i<count($reconhecimento); $i++){
                    $DaoAdvogado->InserirReconhecimento($cod, $reconhecimento[$i]);
                }
            
            }

            
            $DaoAdvogado->Editar($pojo);
            
		
	    $_SESSION['retorno']['mensagem'] = 'Dados alterados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';

        header('Location: '.URL_SITE.'/admin/view/equipe/equipe-form.php?cod='.$cod.'');
        exit;
		

}
//deletar
if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){
	 
    $DaoAdvogado = new DaoAdvogado();
    $DaoAdvogado->Deletar($cod);
    $DaoAdvogado->DeletarIdioma($cod);
    $DaoAdvogado->DeletarAtuacao($cod);
    
    $responta['mensagem'] = 'Advogado deletada com sucesso';
    $responta['classe'] = 'alert-success';
    $responta['result'] = 'success';
    echo json_encode($responta);
    exit;
	
}

//deletar
if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir_imagem")){
     
     //unlink(URL_CAMINHO_IMG.'/equipe/'.$img); 
    
    $DaoAdvogado = new DaoAdvogado();
                
    $DaoAdvogado->DeletarFoto($id_adv, $img);
 
    $responta['mensagem'] = 'Imagem apagada com sucesso';
    $responta['classe'] = 'alert-success';
    $responta['result'] = 'success';
     echo json_encode($responta); 
   /*  echo json_encode($id_adv); */
    exit;
	
}




?>