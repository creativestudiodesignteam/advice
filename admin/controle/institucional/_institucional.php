<?php

include("../../../libs/config.php");
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach($_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}


if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){

        $daoInstitucional = new DaoInstitucional();
        $institucional = new Institucional();

        $table = $daoInstitucional->statusTable('tbl_institucional');
         $id = $table['Auto_increment'];

        if(isset($_FILES['img_banner'])){

                $ext = strtolower(substr($_FILES['img_banner']['name'],-4)); //Pegando extensão do arquivo
                $new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
                $dir = URL_IMG.'/placeholders/'; //Diretório para uploads

                move_uploaded_file($_FILES['img_banner']['tmp_name'], $dir.$new_name); //Fazer upload do arquivo
        }else{
                $_SESSION['retorno']['mensagem'] = 'Preencha o Banner!';
                $_SESSION['retorno']['classe'] = 'alert-danger';
                $_SESSION['retorno']['result'] = 'error';
                $_SESSION['retorno']['obj'] = $institucional;

                header('Location: '.URL_SITE.'/admin/view/institucional/institucional-form.php');
                exit();
        }

        $new_icone = '';
        if(isset($_FILES['img_icone'])){

                $ext = strtolower(substr($_FILES['img_icone']['name'],-4)); //Pegando extensão do arquivo
                $new_icone = date("Y_m_d_H_i_s").'_icon' . $ext; //Definindo um novo nome para o arquivo
                $dir = URL_IMG.'/'; //Diretório para uploads

                move_uploaded_file($_FILES['img_icone']['tmp_name'], $dir.$new_icone); //Fazer upload do arquivo

        }

         for($i=0;$i<count($linguagem);$i++){

            $institucional->setTitulo($titulo[$i]);
            $institucional->setSub_titulo($sub_titulo[$i]);
            $institucional->setTexto($texto[$i]);
            $institucional->setAtivo($ativo);
            $institucional->setDestaque($destaque);
            $institucional->setImagem($new_name);
            $institucional->setIcone($new_icone);
            $institucional->setLink($link);
            $institucional->setLinkLogo($link_logo);
            $institucional->setOrdem($ordem);
            $institucional->setLinguagem($linguagem[$i]);
            $institucional->setId($id);

            $cod = $daoInstitucional->Inserir($institucional);

            
         }
        $daoMenu = new DaoMenu();
        $alterar_menu = $daoMenu-> EditarMenuInstucional($cod, $id_institucional);
        
        $_SESSION['retorno']['mensagem'] = 'Dados cadastrados com sucesso!';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        $_SESSION['retorno']['obj'] = $institucional;

        header('Location: '.URL_SITE.'/admin/view/institucional/institucional-form.php?cod='.$cod);
        exit();
}

if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){

        $daoInstitucional = new DaoInstitucional();
        $institucional = new Institucional();

        if((isset($_FILES['img_banner']) && $alt_img == 's') || (isset($_FILES['img_banner']) && !isset($alt_img))){

                $ext = strtolower(substr($_FILES['img_banner']['name'],-4)); //Pegando extensão do arquivo
                $new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
                $dir = URL_IMG.'/placeholders/'; //Diretório para uploads
                //$dir = '/Web/site/images/institucional/';
                
                if(!move_uploaded_file($_FILES['img_banner']['tmp_name'], $dir.$new_name)){
                    $new_name = '';
                } //Fazer upload do arquivo


        }else if(empty($_FILES['img_banner']['name']) && $alt_img == 's'){
                $_SESSION['retorno']['mensagem'] = 'Preencha a imagem!';
                $_SESSION['retorno']['classe'] = 'alert-danger';
                $_SESSION['retorno']['result'] = 'error';
                //$_SESSION['retorno']['obj'] = $noticiaObj;

                header('Location: '.URL_SITE.'/admin/view/institucional/institucional-form.php?cod='.$cod.'');
                exit();
        }

        if((isset($_FILES['img_icone']) && $alt_icone == 's') || (isset($_FILES['img_icone']) && !isset($alt_icone))){

                $ext = strtolower(substr($_FILES['img_icone']['name'],-4)); //Pegando extensão do arquivo
                $new_icone = date("Y_m_d_H_i_s").'_icon'.$ext; //Definindo um novo nome para o arquivo
                $dir = URL_IMG.'/'; //Diretório para uploads

                if(!move_uploaded_file($_FILES['img_icone']['tmp_name'], $dir.$new_icone)){
                    $new_icone = '';
                   /*  print_r($_FILES['img_icone']);  */
                } //Fazer upload do arquivo


        }

        for($i=0;$i<count($linguagem);$i++){

            $institucional->setTitulo($titulo[$i]);
            $institucional->setSub_titulo($sub_titulo[$i]);
            $institucional->setTexto($texto[$i]);
            $institucional->setAtivo($ativo);
            $institucional->setDestaque($destaque);
            $institucional->setImagem($old_img);
            if(isset($old_icone)){
                $institucional->setIcone($old_icone);
            }
            $institucional->setOrdem($ordem);
            $institucional->setLink($link);
            $institucional->setLinkLogo($link_logo);
            $institucional->setLinguagem($linguagem[$i]);
            $institucional->setId($cod);

            if(isset($new_name) && $new_name <> '' && !empty($_FILES['img_banner']['name'])){
                $institucional->setImagem($new_name);
            }

            if(isset($new_icone) && $new_icone <> '' && !empty($_FILES['img_icone']['name'])){
                $institucional->setIcone($new_icone);
            }

            $obj = $daoInstitucional->BuscarPorCODLing($cod,$linguagem[$i]);

            if($obj->getId() <> ''){
                $daoInstitucional->Editar($institucional);
            }else{
                $cod = $daoInstitucional->Inserir($institucional);
            }
         }

        $daoMenu = new DaoMenu();
        $alterar_menu = $daoMenu-> EditarMenuInstitucional($cod, $id_institucional);

        $_SESSION['retorno']['mensagem'] = 'Dados alterados com sucesso!';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        //$_SESSION['retorno']['obj'] = $institucional;

        header('Location: '.URL_SITE.'/admin/view/institucional/institucional-form.php?cod='.$cod.'');
        exit();
}

if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){

        $daoInstitucional = new DaoInstitucional();
        $daoInstitucional->Deletar($cod);

        $responta['mensagem'] = 'Institucional deletado com sucesso';
        $responta['classe'] = 'alert-success';
        $responta['result'] = 'success';
        echo json_encode($responta);
        exit;

}
