<?php 
//include("../../libs/conf.php"); 
include("../../../libs/config.php"); 
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach( $_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}



####################################### USUARIO ######################################################################

//cadastrar
if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
    
         $daoTraducao = new DaoTraducao();
         $traducaoObj = new Traducao();
         
         $table = $daoTraducao->statusTable('tbl_site_traducao');
         $id = $table['Auto_increment'];
         
         for($i=0;$i<count($linguagem);$i++){                
            $traducaoObj->setTexto1($texto1[$i]);
            $traducaoObj->setTexto2($texto2[$i]);
            $traducaoObj->setTexto3($texto3[$i]);
            $traducaoObj->setTexto4($texto4[$i]);
            $traducaoObj->setTexto5($texto5[$i]);
            $traducaoObj->setTexto6($texto6[$i]);
            $traducaoObj->setPagina($pagina[$i]);
            $traducaoObj->setLinguagem($linguagem[$i]);
            $traducaoObj->setId($id);
            
            $cod = $daoTraducao->Inserir($traducaoObj);
         }
         
			 		 
	$_SESSION['retorno']['mensagem'] = 'Dados cadastrados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        
        header('Location: '.URL_SITE.'/admin/view/institucional/traducao-form.php?cod='.$cod.'');
        exit;	

}
//alterar
if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){

	 $daoTraducao = new DaoTraducao();
         $traducaoObj = new Traducao();
         
         
         for($i=0;$i<count($linguagem);$i++){                
            $traducaoObj->setTexto1($texto1[$i]);
            $traducaoObj->setTexto2($texto2[$i]);
            $traducaoObj->setTexto3($texto3[$i]);
            $traducaoObj->setTexto4($texto4[$i]);
            $traducaoObj->setTexto5($texto5[$i]);
            $traducaoObj->setTexto6($texto6[$i]);
            $traducaoObj->setPagina($pagina[$i]);
            $traducaoObj->setLinguagem($linguagem[$i]);
            $traducaoObj->setId($cod);
            
            $obj = $daoTraducao->BuscarPorCODLing($cod,$linguagem[$i]);
            
            if($obj->getId() <> ''){
                $daoTraducao->Editar($traducaoObj);
            }else{
                $cod = $daoTraducao->Inserir($traducaoObj);
            }
         }
        
		
	$_SESSION['retorno']['mensagem'] = 'Dados alterados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';

        header('Location: '.URL_SITE.'/admin/view/institucional/traducao-form.php?cod='.$cod.'');
        exit;
		

}
//deletar
if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){
	 
    $daoTraducao = new DaoTraducao();
    $daoTraducao->Deletar($cod);
    
    $responta['mensagem'] = 'Traducao deletada com sucesso';
    $responta['classe'] = 'alert-success';
    $responta['result'] = 'success';
    echo json_encode($responta);
    exit;
	
}



?>