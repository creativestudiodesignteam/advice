<?php

include("../../../libs/config.php"); 
include("../../dados/logado.php");

#Pega variaveis vinda do formulário via POST
foreach( $_POST as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}

#Pega variaveis vinda do formulário via GET
foreach($_GET as $campo => $valor){
   $$campo = antiInjection($valor);
   $$campo = str_replace("'", "", $valor);
}




if(isset($_POST["acao"]) && ($_POST["acao"]=="cadastrar")){
         
        $daoBanner = new DaoBanner();
        $banner = new Banner();
        
        $table = $daoBanner->statusTable('tbl_banners');
         $id = $table['Auto_increment'];
         
        if(isset($_FILES['img_banner'])){
            
            $ext = strtolower(substr($_FILES['img_banner']['name'],-4)); //Pegando extensão do arquivo
            $new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
            $dir = URL_IMG.'/placeholders/'; //Diretório para uploads

            move_uploaded_file($_FILES['img_banner']['tmp_name'], $dir.$new_name); //Fazer upload do arquivo
            
        }else{
            $_SESSION['retorno']['mensagem'] = 'Preencha o Banner!';
            $_SESSION['retorno']['classe'] = 'alert-danger';
            $_SESSION['retorno']['result'] = 'error';
            $_SESSION['retorno']['obj'] = $banner;

            header('Location: '.URL_SITE.'/admin/view/institucional/banner-form.php');
            exit();
        }
        
        for($i=0;$i<count($linguagem);$i++){
            $banner->setLink($link);
            $banner->setImagem($new_name);
            $banner->setAtivo($ativo);
            $banner->setOrdem($ordem);       
            $banner->setLinguagem($linguagem[$i]);
            $banner->setTitulo($titulo[$i]);
            $banner->setTexto($texto[$i]);
            $banner->setTexto2($texto2[$i]);
            $banner->setId($id);
            
            $cod = $daoBanner->Inserir($banner);
        }
        
        $_SESSION['retorno']['mensagem'] = 'Dados cadastrados com sucesso!';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        //$_SESSION['retorno']['obj'] = $banner;

        header('Location: '.URL_SITE.'/admin/view/institucional/banner-form.php?cod='.$cod);
        exit();
}

if(isset($_POST["acao"]) && ($_POST["acao"]=="alterar")){
         
        $daoBanner = new DaoBanner();
        $banner = new Banner();

        if((isset($_FILES['img_banner']) && $alt_img == 's') || (isset($_FILES['img_banner']) && !isset($alt_img))){
            
            $ext = strtolower(substr($_FILES['img_banner']['name'],-4)); //Pegando extensão do arquivo
            $new_name = date("Y_m_d_H_i_s") . $ext; //Definindo um novo nome para o arquivo
            $dir = URL_IMG.'/placeholders/'; //Diretório para uploads

            move_uploaded_file($_FILES['img_banner']['tmp_name'], $dir.$new_name); //Fazer upload do arquivo
            
            $banner->setImagem($new_name);
            
        }else if((!isset($_FILES['img_banner']) || empty ($_FILES['img_banner'])) && $alt_img == 's'){
            $_SESSION['retorno']['mensagem'] = 'Preencha o Banner!';
            $_SESSION['retorno']['classe'] = 'alert-danger';
            $_SESSION['retorno']['result'] = 'error';
            $_SESSION['retorno']['obj'] = $banner;

            header('Location: '.URL_SITE.'/admin/view/institucional/banner-form.php?cod='.$cod);
            exit();
        }
        
        for($i=0;$i<count($linguagem);$i++){
            $banner->setLink($link);
            $banner->setImagem($img_old);
            $banner->setAtivo($ativo);
            $banner->setOrdem($ordem);       
            $banner->setLinguagem($linguagem[$i]);
            $banner->setTitulo($titulo[$i]);
            $banner->setTexto($texto[$i]);
            $banner->setTexto2($texto2[$i]);
            $banner->setId($cod);
            
            if(isset($new_name) && $new_name <> ''){
                $banner->setImagem($new_name);
            }
            
            $obj = $daoBanner->BuscarPorCODLing($cod,$linguagem[$i]);
            
            if($obj->getId() <> ''){
                $daoBanner->Editar($banner);
            }else{
                $cod = $daoBanner->Inserir($banner);
            }
        }
   
        //exit();
        $_SESSION['retorno']['mensagem'] = 'Dados alterados com sucesso! ';
        $_SESSION['retorno']['classe'] = 'alert-success';
        $_SESSION['retorno']['result'] = 'success';
        //$_SESSION['retorno']['obj'] = $banner;

        header('Location: '.URL_SITE.'/admin/view/institucional/banner-form.php?cod='.$cod.'');
        exit();
}

if(isset($_POST["acao"]) && ($_POST["acao"]=="excluir")){
	 
        $daoBanner = new DaoBanner();
        $daoBanner->Deletar($cod);

        $responta['mensagem'] = 'Banner deletado com sucesso';
        $responta['classe'] = 'alert-success';
        $responta['result'] = 'success';
        echo json_encode($responta);
        exit;

}
