<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Meta Data -->
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<title>Home - Software, App, SaaS landing HTML Template</title>

	<?php include('includes/head.php');?>


</head>

<body id="home-version-1" class="home-color-two" data-style="default">

<a href="#main_content" data-type="section-switch" class="return-to-top">
		<i class="fa fa-chevron-up"></i>
	</a>

	<!--=========================-->
	<!--=        Loader         =-->
	<!--=========================-->
	<?php include('includes/loader.php');?>
	<!-- /.page-loader -->

	<div id="main_content">


		<!--=========================-->
		<!--=        Navbar         =-->
		<!--=========================-->
		<?php include('includes/header-interna.php');?>
		<!-- /.site-header -->

		<!--==========================-->
		<!--=         Banner         =-->
		<!--==========================-->
		<section class="page-banner">
			<div class="container">
				<div class="page-title-wrapper">
					<h1 class="page-title">Serviços</h1>

					<ul class="bradcurmed">
						<li><a href="index.php" rel="noopener noreferrer">Home</a></li>
						<li>Serviços</li>
					</ul>
				</div>
				<!-- /.page-title-wrapper -->
			</div>
			<!-- /.container -->


			<svg class="circle" data-parallax='{"x" : -200}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
		<path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
	</svg>

			<ul class="animate-ball">
				<li class="ball"></li>
				<li class="ball"></li>
				<li class="ball"></li>
				<li class="ball"></li>
				<li class="ball"></li>
			</ul>
		</section>
		<!-- /.page-banner -->

	<div id="main_content">

		<!--==========================-->
		<!--=         Feature        =-->
		<!--==========================-->
		<section class="featured-five">
			<div class="container">
				<div class="section-title style-three text-center">
					<h3 class="sub-title" data-wow-delay="0.2s">Servicos</h3>
					<h2 class="title wow pixFadeUp" data-wow-delay="0.2s">
						A  autonomia que sua empresa precisa
					</h2>
				</div>
				<!-- /.section-title -->

				<div class="row">
					<div class="col-lg-4 col-md-6">
						<div class="pixsass-icon-box-wrapper style-two wow pixFadeRight" data-wow-delay="0.9s">
							<div class="iapp-icon-box-icon">
								<h1><i class="fas fa-laptop"></i></h1>
							</div>
							<div class="pixsass-icon-box-content">
								<h3 class="pixsass-icon-box-title"><a href="#">Acesso Remoto</a></h3>
								<p>
									Conectamos diretamente aos
computadores da sua empresa para verificar
on-line a situação do sistema e oferecer uma
solução rápida e precisa sobre a situação.
								</p>
							</div>
						</div>
						<!-- /.pixsass-box style-two -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->

					<div class="col-lg-4 col-md-6">
						<div class="pixsass-icon-box-wrapper style-two wow pixFadeRight" data-wow-delay="1.1s">
							<div class="iapp-icon-box-icon">
								 <h1><i class="far fa-life-ring" ></i></h1>
							</div>
							<div class="pixsass-icon-box-content">
								<h3 class="pixsass-icon-box-title"><a href="#">Suporte</a></h3>

								<p>
									A Advice System possui uma equipe de profissionais
qualificados para o pronto atendimento e
esclarecimento de dúvidas através da Central
Telefônica.
								</p>
							</div>
						</div>
						<!-- /.pixsass-box style-two -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->

					<div class="col-lg-4 col-md-6">
						<div class="pixsass-icon-box-wrapper style-two wow pixFadeRight" data-wow-delay="1.3s">
							<div class="iapp-icon-box-icon">
								 <h1><i class="far fa-comments"></i></h1>
							</div>
							<div class="pixsass-icon-box-content">
								<h3 class="pixsass-icon-box-title"><a href="#">Help Desk</a></h3>
								<p>
									Através dessa área, por meio da abertura de um
chamado, o cliente é rapidamente atendido por um
de nossos profissionais, que irá auxiliá-lo em
relação as suas dúvidas e dificuldades.
								</p>
							</div>
						</div>
						<!-- /.pixsass-box style-two -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->
				</div>
			</div>
			<!-- /.container -->
		</section>
		<!-- /.featured -->

		<!--==========================-->
		<!--=         Feature        =-->
		<!--==========================-->
		<section class="featured-six">
			<div class="container">
				<div class="section-title style-three text-center">
					<h2 class="title wow pixFadeUp" data-wow-delay="0.2s">
					Apoio total para obter o melhor aproveitamento <br>de todo o sistema Advice Pós.
					</h2>
				</div>
				<!-- /.section-title -->

				<div class="row align-items-center">
					<div class="col-lg-5 col-md-5">
						<div class="feature-image-wrapper wow pixFadeLeft">
							<img src="media/feature/tablet.png" alt="">
						</div>
						<!-- /.feature-image-wrapper -->
					</div>
					<!-- /.col-lg-5 -->

					<div class="col-lg-7 col-md-7">
						<div class="pixsass-icon-box-wrapper style-six wow pixFadeRight" data-wow-delay="0.4s">
							<div class="saaspik-icon-box-icon">
								<img src="media/feature/22.png" alt="icon">
							</div>
							<div class="pixsass-icon-box-content">
								<h3 class="pixsass-icon-box-title">
									<a href="#.">Os treinamentos são essenciais para garantir a economia de tempo e melhor uso dos recursos humanos, pois irão refletir na qualidade do trabalho e no retorno sobre o investimento realizado. Os treinamentos ocorrem durante a fase de implantação, de maneira presencial, na empresa ou na Advice System.</a>
								</h3>
							</div>
						</div>
						<!-- /.pixsass-box style-six -->

						<div class="pixsass-icon-box-wrapper style-six wow pixFadeRight" data-wow-delay="0.5s">
							<div class="saaspik-icon-box-icon">
								<img src="media/feature/23.png" alt="icon">
							</div>
							<div class="pixsass-icon-box-content">
								<h3 class="pixsass-icon-box-title">
									<a href="#.">Equipe de Consultores com amplo conhecimento no ADVICE POS e em atividades de Gestão, que auxiliam no tratamento de aspectos como: otimização de processos e implementação de rotinas específicas.</a>
								</h3>
							</div>
						</div>
						<!-- /.pixsass-box style-six -->

					</div>
					<!-- /.col-lg-7 -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->

			<div class="down-bg">
				<img src="media/background/down-bg.png" alt="down-bg">
			</div>
		</section>
		<!-- /.featured -->

		<!--==========================-->
		<!--=         App Interface        =-->
		<!--==========================-->
		<section class="interface">
			<div class="container">
				<div class="interface-toparea">
					<div class="row">
						<div class="col-lg-7 pix-order-one">
							<div class="editor-content pt-7">
								<div class="interface-title pixFadeUp">
									<h2>
									Maior qualidade e agilidade. <br>Mais vantagens para sua empresa.
									</h2>

									<p class="wow pixFadeUp" data-wow-delay="0.3s">
									A implantação de um Sistema de Gestão deve proporcionar à empresa uma ferramenta para aprimorar a
									qualidade das informações e garantir o maior controle e segurança dos dados. O processo de implantação
									é primordial, <br>pois é a base de sustentação para a utilização eficiente da ferramenta.
									<br>
									Pensando nisso, a Advice System tem como diferencial a metodologia de implantação:

									</p>

								</div>
								<!-- /.section-title style-two -->

								<!-- <ul class="list-items wow pixFadeUp" data-wow-delay="0.4s">
									<li>Implantação presencial.</li>
									<li>Cronograma detalhado</li>
									<li>Configurações</li>
									<li>Parametrizações</li>
									<li>Treinamento dos usuários </li>
									<li>Acompanhamento</li>
								</ul>

								<a href="solicoes.php" class="pix-btn btn-outline-two wow pixFadeUp" data-wow-delay="0.5s">Ver soluções</a> -->
								<div class="description wow pixFadeUp" data-wow-delay="1.1s">
									<ul class="list-items wow pixFadeUp" data-wow-delay="1.1s">
										<li>Implantação presencial.</li>
										<li>Cronograma detalhado</li>
										<li>Configurações</li>
										<li>Parametrizações</li>
										<li>Treinamento dos usuários </li>
										<li>Acompanhamento</li>
									</ul>

									<br><a href="solucoes.php" class="pix-btn wow pixFadeUp" data-wow-delay="0.5s" style="margin-right: 15px">Ver soluçõess</a>
								</div>

							</div>
							<!-- /.interface-content -->
						</div>
						<!-- /.col-lg-6 -->


						<div class="col-lg-5">
							<div class="interface-image-wrapper style-one">
								<div class="image-one wow pixFadeUp">
									<img src="media/feature/27.png" data-parallax='{"y" : 50}' alt="interface">
								</div>

								<div class="image-two wow pixFadeDown">
									<img src="media/feature/26.png" data-parallax='{"y" : -50}' alt="interface">
								</div>


								<svg xmlns="http://www.w3.org/2000/svg" class="svgbg-one" xmlns:xlink="http://www.w3.org/1999/xlink" width="700px" height="700px">
							<path fill-rule="evenodd" opacity="0.102" fill="rgb(251, 201, 82)" d="M350.000,-0.000 C543.300,-0.000 700.000,156.700 700.000,350.000 C700.000,543.299 543.300,700.000 350.000,700.000 C156.700,700.000 -0.000,543.299 -0.000,350.000 C-0.000,156.700 156.700,-0.000 350.000,-0.000 Z" />
						</svg>
							</div>
							<!-- /.interface-image-wrapper -->
						</div>
						<!-- /.col-lg-6 -->
					</div>
					<!-- /.interface-toparea -->

				</div>
				<!-- /.row -->

				<div class="row">
					<div class="col-lg-7">
						<div class="interface-image-wrapper style-two">
							<div class="image-one wow pixFadeRight">
								<img src="media/feature/28.png" data-parallax='{"x" : 30}' alt="interface">
							</div>

							<div class="image-two wow pixFadeLeft">
								<img src="media/feature/29.png" data-parallax='{"x" : -30}' alt="interface">
							</div>


							<svg xmlns="http://www.w3.org/2000/svg" class="svgbg-two" xmlns:xlink="http://www.w3.org/1999/xlink" width="700px" height="700px">
						<path fill-rule="evenodd" opacity="0.102" fill="rgb(82, 251, 237)" d="M350.000,-0.000 C543.300,-0.000 700.000,156.700 700.000,350.000 C700.000,543.299 543.300,700.000 350.000,700.000 C156.700,700.000 -0.000,543.299 -0.000,350.000 C-0.000,156.700 156.700,-0.000 350.000,-0.000 Z" />
					</svg>
						</div>
						<!-- /.interface-image-wrapper -->
					</div>
					<!-- /.col-lg-6 -->

					<div class="col-lg-5 pix-order-one">
						<div class="interface-content">

							<div class="interface-title pixFadeUp">
							<h3 class="sub-title -services-title" data-wow-delay="0.2s">ADVICE INFRAESTRUTURA E SEGURANÇA</h3>
								<h2>
								Sua empresa melhor estruturada e protegida.
								</h2>

								<!-- <p class="wow pixFadeUp md-brn" data-wow-delay="0.3s">
									Lost the plot cracking goal give us a bell is bog horse play<br> knackered lemon squeezy, cup of char cack bleeder matie<br> boy he lost his bottle.!
								</p> -->

							</div>
							<!-- /.section-title style-two -->

							<ul class="list-items list-with-icon wow pixFadeUp" data-wow-delay="0.4s">
								<li><i class="fas fa-project-diagram"></i> Criação e Execução de Projetos;</li>
								<li><i class="fas fa-bolt"></i> Rede Elétrica;</li>
								<li><i class="fas fa-network-wired"></i> Cabeamento Estruturado;</li>
								<li><i class="fas fa-cog"></i> Manutenção;</li>
								<li><i class="fas fa-cloud-upload-alt"></i> Backup e Segurança de dados;</li>
							</ul>

							<a href="contato.php" class="pix-btn btn-outline-two wow pixFadeUp" data-wow-delay="0.5s">Entre em contato</a>

						</div>
						<!-- /.interface-content -->
					</div>
					<!-- /.col-lg-6 -->
				</div>
				<!-- /.row -->

				<div class="border-wrap">
					<span class="ball" data-parallax='{"x" : -100}'></span>
					<!-- Generator: Adobe Illustrator 23.0.1, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
					<svg version="1.1" id="animate-border" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 908.5 976.2" style="enable-background:new 0 0 908.5 976.2;" xml:space="preserve">
				<style type="text/css">
					.st0 {
						fill: #FFFFFF;
					}

					.st1 {
						opacity: 5.000000e-02;
					}

					.st2 {
						fill: #1A4A88;
					}

					.st3 {
						opacity: 0.8;
						fill: #EA7225;
					}

					.st4 {
						fill: #D86928;
					}

					.st5 {
						fill: #EA7225;
					}

					.st6 {
						fill: #F89938;
					}

					.st7 {
						fill: #FDFEFE;
					}

					.st8 {
						opacity: 0.5;
						fill: #EA7225;
					}

					.st9 {
						opacity: 0.75;
						fill: #EA7225;
					}

					.st10 {
						fill: none;
						stroke: #1A4A88;
						stroke-width: 1.5;
						stroke-miterlimit: 10;
					}

					.st11 {
						opacity: 0.1;
					}

					.st12 {
						opacity: 0.85;
						fill: #EA7225;
					}

					.st13 {
						opacity: 0.81;
						fill: #EA7225;
					}

					.st14 {
						opacity: 0.4;
						fill: #EA7225;
					}

					.st15 {
						opacity: 0.7;
						fill: #EA7225;
					}

					.st16 {
						fill: none;
						stroke: #F0577E;
						stroke-width: 2;
						stroke-miterlimit: 10;
					}

					.st17 {
						fill: none;
						stroke: #F0577E;
						stroke-width: 2;
						stroke-miterlimit: 10;
						stroke-dasharray: 3.9904, 5.9856;
					}
				</style>
				<g>
					<g>
						<g>
							<path class="st16" d="M8.4,908.7c0,0,0.6,0.4,1.7,1.1" />
							<path class="st17 path" d="M15.2,912.9c36.7,22,207.6,117.4,147.2-44.2c-67.5-180.6-137.7-410.5,320-358
							c457.7,52.5,448.6-207.4,383-331C808.1,71.8,725.1,26.6,704.9,16.8" />
							<path class="st16" d="M702.2,15.5c-1.2-0.5-1.8-0.8-1.8-0.8" />
						</g>
					</g>
				</g>
			</svg>
				</div>


			</div>
			<!-- /.container -->
		</section>
		<!-- /.interface -->

		<?php include('includes/banner-demonstracao.php');?>

		<!--===================================-->
		<!--=         Genera Informes         =-->
		<!--===================================-->
		<section class="download">
			<div class="container">
				<div class="row">
					<div class="col-lg-5 pix-order-one">
						<div class="download-wrapper">
							<h2 class="title wow pixFadeUp">Tenha a gestão da sua empresa <br> na palma da sua mão.</h2>

							<p class="wow pixFadeUp" data-wow-delay="0.3s">
							Acesse o Advice Pós pelo seu celular. Sistema responsivo para facilitar 
o gerenciamento e monitoramento da sua empresa.

							</p>

							<div class="app-btn-wrapper">
								<a href="#." class="app-btn btn-active wow flipInX" data-wow-delay="0.5s">
							<i class="fi flaticon-google-play"></i>
							<span>Google play</span>
						</a>

								<a href="#." class="app-btn wow flipInX" data-wow-delay="0.5s">
							<i class="fi flaticon-apple-logo"></i>
							<span>App Store</span>
						</a>
							</div>
							<!-- /.app-btn-wrapper -->
						</div>
						<!-- /.download-wrapper -->
					</div>
					<!-- /.col-lg-5 -->

					<div class="col-lg-7">
						<div class="download-feature-image">
							<img src="media/download/1.png" alt="" class="image-one wow pixFadeRight">
							<img src="media/download/2.png" alt="" class="image-two wow pixFadeLeft">

							<svg xmlns="http://www.w3.org/2000/svg" class="circle-svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="500px" height="500px">
						<path fill-rule="evenodd" opacity="0.102" fill="rgb(251, 138, 82)" d="M250.000,-0.000 C388.071,-0.000 500.000,111.929 500.000,250.000 C500.000,388.071 388.071,500.000 250.000,500.000 C111.929,500.000 -0.000,388.071 -0.000,250.000 C-0.000,111.929 111.929,-0.000 250.000,-0.000 Z" />
					</svg>
						</div>
						<!-- /.download-feature-image -->
					</div>
					<!-- /.col-lg-7 -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		</section>
		<!-- /.download -->

		<!--=============================-->
		<!--=         Blog Post         =-->
		<!--=============================-->
		<section class="blog-grid-two">
			<div class="container">
				<div class="section-title color-two text-center">

					<h2 class="title wow pixFadeUp" data-wow-delay="0.3s">
					Conheça tudo o que o Advice PÓS<br>pode fazer por você.
					</h2>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<article class="blog-post-two color-two wow pixFadeLeft" data-wow-delay="0.4s">
							<div class="feature-image">
								<a href="solucoes.php">
									<img src="media/blog/23.jpg" alt="blog-thumb">
								</a>
							</div>
							<!-- /.feature-image -->
							<div class="blog-content">
								<!-- <ul class="post-meta">
									<li>By<a href="#"> Admin</a></li>
									<li><a href="#">Jun 24, 2019</a></li>
								</ul> -->

								<h3 class="entry-title">
									<a href="solucoes.php">SOLUÇÕES PARA INSTITUIÇÕES DE ENSINO</a>
								</h3>

								<!-- <p>
									Bobby car boot bubble and squeak Charles tinkety tonk old fruit vagabond are you taking the piss elizabeth the wireless.
								</p> -->

								<a href="solucoes.php" class="read-more">Ver nossa solução</a>

							</div>
							<!-- /.blog-content -->
						</article>
						<!-- /.blog-post-two -->
					</div>
					<!-- /.col-lg-4 -->

					<div class="col-lg-6">
						<article class="blog-post-two color-two wow pixFadeRight" data-wow-delay="0.4s">
							<div class="feature-image">
								<a href="solucoes.php">
							<img src="media/blog/24.jpg" alt="blog-thumb">
						</a>
							</div>
							<!-- /.feature-image -->
							<div class="blog-content">
								<!-- <ul class="post-meta">
									<li>By<a href="#"> Admin</a></li>
									<li><a href="#">Jun 30, 2019</a></li>
								</ul> -->

								<h3 class="entry-title">
									<a href="solucoes.php">SOLUÇÕES PARA TODOS OS NEGÓCIO</a>
								</h3>

								<!-- <p>
									Bobby car boot bubble and squeak Charles tinkety tonk old fruit vagabond are you taking the piss elizabeth the wireless.
								</p> -->

								<a href="solucoes.php" class="read-more">Ver nossa solução</a>

							</div>
							<!-- /.blog-content -->
						</article>
						<!-- /.blog-post-two -->
					</div>
					<!-- /.col-lg-4 -->

				</div>
				<!-- /.row -->

				<svg xmlns="http://www.w3.org/2000/svg" class="circle-blog" xmlns:xlink="http://www.w3.org/1999/xlink" width="800px" height="800px">
			<path fill-rule="evenodd" fill="rgb(250, 250, 255)" d="M400.000,-0.000 C620.914,-0.000 800.000,179.086 800.000,400.000 C800.000,620.914 620.914,800.000 400.000,800.000 C179.086,800.000 0.000,620.914 0.000,400.000 C0.000,179.086 179.086,-0.000 400.000,-0.000 Z" />
		</svg>
			</div>
			<!-- /.container -->
		</section>
		<!-- /#blog-grid -->

		<?php include('includes/footer.php')?>


	</div>
	<!-- /#site -->

	<!-- Dependency Scripts -->
	<?php include('includes/scripts.php')?>

</body>

</html>