<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<meta name="description" content=""  />
	<meta name="keywords" content=""  />
	<title>Home — Advice System</title>
	<?php include('includes/head.php');?>
</head>

<body id="home-version-1" class="home-version-4" data-style="default">

	<a href="#main_content" data-type="section-switch" class="return-to-top">
		<i class="fa fa-chevron-up"></i>
	</a>
	<!--=========================-->
	<!--=        Loader         =-->
	<!--=========================-->
	<?php include('includes/loader.php');?>
	<!-- /.page-loader -->

	<div id="main_content">


		<!--=========================-->
		<!--=        Navbar         =-->
		<!--=========================-->
		<?php include('includes/header.php');?>
		<!-- /.site-header -->

		<!--==========================-->
		<!--=         Banner         =-->
		<!--==========================-->
		<?php include('includes/banners.php');?>
		
		<!--===========================-->
		<!--=         Feature         =-->
		<!--===========================-->
		<section class="featured">
			<div class="container">
				<div class="section-title text-center wow pixFade" data-wow-delay="0.8s">
					<h3 class="sub-title">Advice POS</h3>
					<h2 class="title" >Tecnologia para a sua gestão.<br> Praticidade para o seu dia a dia</h2>
				</div>
				<!-- /.section-title -->

				

				<div class="row">
					<div class="col-md-3">
					<div class="pixsass-icon-box-wrapper style-one wow pixFadeLeft text-center" data-wow-delay="1.1s">
						<div class="iapp-icon-box-icon">
							<img src="media/feature/0.png" alt="">
						</div>
						<div class="pixsass-icon-box-content">
							<h6 class="pixsass-icon-box-title"><a href="#">Visão real das operações, agilizando atividades estrátegias e operacionais.</a></h6>
						</div>
					</div>
					<!-- /.pixsass-box style-one -->
				</div>
				<!-- /.col-md-3 -->
					<div class="col-md-3">
						<div class="pixsass-icon-box-wrapper style-one wow pixFadeLeft text-center" data-wow-delay="1.3s">
							<div class="iapp-icon-box-icon">
								<img src="media/feature/1.png" alt="">
							</div>
							<div class="pixsass-icon-box-content">
								<h5 class="pixsass-icon-box-title"><a href="#">Controle total das tarefas por status e sinalizadores</a></h5>
							</div>
						</div>
						<!-- /.pixsass-box style-one -->
					</div>
					<!-- /.col-md-3 -->

					

					<div class="col-md-3">
						<div class="pixsass-icon-box-wrapper style-one wow pixFadeLeft text-center" data-wow-delay="1.5s">
							<div class="iapp-icon-box-icon">
								<img src="media/feature/3.png" alt="">
							</div>
							<div class="pixsass-icon-box-content">
								<h5 class="pixsass-icon-box-title"><a href="#">Otimização dos processos para as tomadas de decisões</a></h5>
							</div>
                            
						</div>
						<!-- /.pixsass-box style-one -->
					</div>
					<!-- /.col-md-3 -->
                    
                    	<div class="col-md-3">
						<div class="pixsass-icon-box-wrapper style-one wow pixFadeLeft text-center" data-wow-delay="1.7s">
							<div class="iapp-icon-box-icon">
								<img src="media/feature/2.png" alt="">
							</div>
							<div class="pixsass-icon-box-content">
								<h5 class="pixsass-icon-box-title"><a href="#">Interface Web para acesso e gestão da empresa</a></h5>
							</div>
                            
						</div>
						<!-- /.pixsass-box style-one -->
					</div>
					<!-- /.col-md-4 -->
                    
                    
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		</section>
		<!-- /.featured -->

		<!--=================================-->
		<!--=         Editor Design         =-->
		<!--=================================-->
		<section class="editor-design">
			<div class="container">
				<div class="row">

					<div class="editure-feature-image wow pixFadeRight">
						<div class="image-one" data-parallax='{"x" : 30}'>
							<img src="media/feature/4.png" class="wow pixFadeRight" data-wow-delay="0.7s" alt="feature-image">
						</div>
						<div class="image-two">
							<div class="image-two-inner" data-parallax='{"x" : -30}'>
								<img src="media/feature/41.png" class="wow pixFadeLeft" data-wow-delay="0.5s" alt="feature-image">
							</div>
						</div>

					</div>

					<div class="col-lg-6 offset-lg-6">
						<div class="editor-content">
							<div class="section-title style-two">
								<h2 class="title wow pixFadeUp" data-wow-delay="0.9s">
									Soluções para Instituições de ensino.
								</h2>

								<p class="wow pixFadeUp" data-wow-delay="1.0s">
									Tenha a gestão da sua empresa na palma de sua mão.
								</p>
							</div>

							<div class="description wow pixFadeUp" data-wow-delay="1.1s">
								<ul class="list-items wow pixFadeUp" data-wow-delay="1.1s">
									<li>Central Pedagógica</li>
									<li>Gestão Acadêmica</li>
									<li>Planejamento Escolar</li>
									<li>Matrícula Online</li>
									<li>Captação de Alunos</li>
								</ul>

								<br><a href="#" class="pix-btn wow pixFadeUp" data-wow-delay="0.5s" style="margin-right: 15px">Saiba Mais</a> <a href="#" class="pix-btn btn-outline wow pixFadeUp" data-wow-delay="0.7s">Agendar Demonstração</a>
							</div>
						</div>
						<!-- /.editor-content -->
					</div>
					<!-- /.col-lg-6 -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
			<div class="shape-bg">
				<img src="media/background/shape_bg.png" class="wow fadeInLeft" alt="shape-bg">
			</div>
		</section>
		<!-- /.editor-design -->

		<!--===================================-->
		<!--=         Genera Informes         =-->
		<!--===================================-->
		<section class="genera-informes">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 pix-order-one">
						<div class="section-title style-two">
							<h2 class="title wow pixFadeUp" data-wow-delay="0.9s">
								Soluções para todos os <br>segmentos de negócios. 
							</h2>

							<p class="wow pixFadeUp" data-wow-delay="1.2s">
								Maior qualidade e agilidade.<br> Mais vantagens para sua empresa.
							</p>
						</div>
						<!-- /.section-title style-two -->

						<ul class="list-items wow pixFadeUp" data-wow-delay="1.6s">
							<li>Gestão de CRM</li>
							<li>Gestão Financeira</li>
							<li>Gestão de Compras</li>
                            <li>Emissão de NFE</li>
						</ul>
                        </p>

						<a href="#" class="pix-btn btn-outline wow pixFadeUp" data-wow-delay="1.3s">Saiba Mais</a>
					</div>
					<!-- /.col-lg-6 -->


					<div class="informes-feature-image">
						<div class="image-one" data-parallax='{"y" : 20}'>
							<img src="media/feature/5.png" class="wow pixFadeDown" alt="informes" data-wow-delay="0.9s">
						</div>

						<div class="image-two" data-parallax='{"y" : -20}'>
							<img src="media/feature/51.png" class=" mw-none wow pixFadeDown" data-wow-delay="1.2s" alt="informes">
						</div>

					</div>

				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->

			<div class="shape-bg">
				<img src="media/background/shape.png" class="wow fadeInRight" alt="shape-bg">
			</div>
		</section>
		<!-- /.genera-informes -->

		<!--=================================-->
		<!--=         Revolutionize         =-->
		<!--=================================-->
		<section class="revolutionize">
			<div class="bg-angle"></div>
			<div class="container">
				<div class="section-title dark-title text-center">
					<h3 class="sub-title wow pixFadeUp">Preview do Sistema</h3>
					<h2 class="title wow pixFadeUp" data-wow-delay="0.7s">
						Gestão on line do seu negócio<br> praticidade para o seu dia a dia.
					</h2>
				</div>
				<!-- /.section-title dark-title -->

				<div id="pix-tabs" class="wow pixFadeUp" data-wow-delay="1.0s">
					<ul id="pix-tabs-nav">
						<li class="active"><a href="#tab1">CRM </a></li>
						<li><a href="#tab2">Financeira </a></li>
						<li><a href="#tab3">Compras </a></li>
						<li><a href="#tab4">Acadêmica</a></li>
                        <li><a href="#tab5">Pedagógica</a></li>
					</ul>
					<!-- tabs-nav -->

					<div id="pix-tabs-content">
						<div id="tab1" class="content">
							<img src="media/revolutionize/2.jpg" alt="revolutionize">

							<div class="shape-shadow"></div>
						</div>
						<div id="tab2" class="content">
							<img src="media/revolutionize/1.jpg" alt="revolutionize">
							<div class="shape-shadow"></div>
						</div>
						<div id="tab3" class="content">
							<img src="media/revolutionize/3.jpg" alt="revolutionize">
							<div class="shape-shadow"></div>
						</div>

						<div id="tab4" class="content">
							<img src="media/revolutionize/1.jpg" alt="revolutionize">
							<div class="shape-shadow"></div>
						</div>
                        <div id="tab5" class="content">
							<img src="media/revolutionize/2.jpg" alt="revolutionize">
							<div class="shape-shadow"></div>
						</div>
					</div>
					<!-- tabs-content -->
				</div>
				<!-- Tabs -->
			</div>
			<!-- /.container -->
		</section>
		<!-- /.revolutionize -->

		<!--===========================-->
		<!--=         Service         =-->
		<!--===========================-->
		<section class="featured-two">
			<div class="container">
				<div class="section-title text-center">
					<h3 class="sub-title wow pixFadeUp">Nossos Serviços</h3>
					<h2 class="title wow pixFadeUp" data-wow-delay="0.7s">Escolha o Advice POS</h2>
				</div>
				<!-- /.section-title -->

				<div class="row">
					<div class="col-lg-4 col-md-6">
						<div class="pixsass-icon-box-wrapper style-two wow pixFadeRight" data-wow-delay="0.9s">
							<div class="iapp-icon-box-icon">
								<h1><i class="fas fa-laptop"></i></h1>
							</div>
							<div class="pixsass-icon-box-content">
								<h3 class="pixsass-icon-box-title"><a href="#">Acesso Remoto</a></h3>
								<p>
									Conectamos diretamente aos
computadores da sua empresa para verificar
on-line a situação do sistema e oferecer uma
solução rápida e precisa sobre a situação.
								</p>
							</div>
						</div>
						<!-- /.pixsass-box style-two -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->

					<div class="col-lg-4 col-md-6">
						<div class="pixsass-icon-box-wrapper style-two wow pixFadeRight" data-wow-delay="1.1s">
							<div class="iapp-icon-box-icon">
								 <h1><i class="far fa-life-ring" ></i></h1>
							</div>
							<div class="pixsass-icon-box-content">
								<h3 class="pixsass-icon-box-title"><a href="#">Suporte</a></h3>

								<p>
									A Advice System possui uma equipe de profissionais
qualificados para o pronto atendimento e
esclarecimento de dúvidas através da Central
Telefônica.
								</p>
							</div>
						</div>
						<!-- /.pixsass-box style-two -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->

					<div class="col-lg-4 col-md-6">
						<div class="pixsass-icon-box-wrapper style-two wow pixFadeRight" data-wow-delay="1.3s">
							<div class="iapp-icon-box-icon">
								 <h1><i class="far fa-comments"></i></h1>
							</div>
							<div class="pixsass-icon-box-content">
								<h3 class="pixsass-icon-box-title"><a href="#">Help Desk</a></h3>
								<p>
									Através dessa área, por meio da abertura de um
chamado, o cliente é rapidamente atendido por um
de nossos profissionais, que irá auxiliá-lo em
relação as suas dúvidas e dificuldades.
								</p>
							</div>
						</div>
						<!-- /.pixsass-box style-two -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->

					<div class="col-lg-4 col-md-6">
						<div class="pixsass-icon-box-wrapper style-two wow pixFadeRight" data-wow-delay="0.7s">
							<div class="iapp-icon-box-icon">
								 <h1><i class="far fa-compass"></i></i></h1>
							</div>
							<div class="pixsass-icon-box-content">
								<h3 class="pixsass-icon-box-title"><a href="#">Consultoria</a></h3>
								<p>
									Equipe de Consultores com amplo conhecimento no
ADVICE POS e em atividades de Gestão, que auxiliam
no tratamento de aspectos como: otimização de
processos e implementação de rotinas específicas.
								</p>
							</div>
						</div>
						<!-- /.pixsass-box style-two -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->

					<div class="col-lg-4 col-md-6">
						<div class="pixsass-icon-box-wrapper style-two wow pixFadeRight" data-wow-delay="0.9s">
							<div class="iapp-icon-box-icon">
								 <h1><i class="far fa-star"></i></h1>
							</div>
							<div class="pixsass-icon-box-content">
								<h3 class="pixsass-icon-box-title"><a href="#">Treinamentos</a></h3>
								<p>
									Os treinamentos são essenciais para garantir a
economia de tempo e melhor uso dos recursos
humanos, pois irão refletir na qualidade do
trabalho e no retorno sobre o investimento
realizado.
								</p>
							</div>
						</div>
						<!-- /.pixsass-box style-two -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->

					<div class="col-lg-4 col-md-6">
						<div class="pixsass-icon-box-wrapper style-two wow pixFadeRight" data-wow-delay="1.1s">
							<div class="iapp-icon-box-icon">
								<h1><i class="far fa-flag"></i></h1>
							</div>
							<div class="pixsass-icon-box-content">
								<h3 class="pixsass-icon-box-title"><a href="#">Implantação</a></h3>
								<p>
									
Temos como diferencial a metodologia de implantação presencial, onde
através de um cronograma detalhado, são realizadas as atividades de configurações, parametrizações e treinamento dos usuários.
								</p>
							</div>
						</div>
						<!-- /.pixsass-box style-two -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		</section>
		<!-- /.featured -->

		<!--===============================-->
		<!--=         Testimonial         =-->
		<!--===============================-->
		<?php include('includes/depoimentos.php');?>
		<!-- /.testimonial -->

		<!--===========================-->
		<!--=         Pricing         =-->
		<!--===========================-->
		<section class="pricing">
			<div class="container">
				<div class="section-title text-center">
					<h3 class="sub-title wow pixFadeUp">Nossos Planos</h3>
					<h2 class="title wow pixFadeUp" data-wow-delay="0.7s">
						Sem taxas adicionais! <br> Escolha o seu plano.
					</h2>
				</div>
				<!-- /.section-title -->
				<nav class="pricing-tab wow pixFadeUp" data-wow-delay="0.9s">
					<span class="tab-btn monthly_tab_title">
				Mensal
			</span>
					<span class="pricing-tab-switcher"></span>
					<span class="tab-btn annual_tab_title">
				Anual
			</span>
				</nav>

				<div class="row advanced-pricing-table no-gutters wow pixFadeUp" data-wow-delay="1.0s">

					<div class="col-lg-4">
						<div class="pricing-table br-left">
							<div class="pricing-header pricing-amount">
								<div class="annual_price">
									<h2 class="price">R$ 1.800,00</h2>
								</div>
								<!-- /.annual_price -->

								<div class="monthly_price">
									<h2 class="price">R$ 170,00</h2>
								</div>
								<!-- /.monthly_price -->

								<h3 class="price-title">Gestão Escolar</h3>
								<p>Suporte Completo</p>
							</div>
							<!-- /.pricing-header -->

							<ul class="price-feture">
								<li class="have">Central Pedagógica e Acadêmica </li>
								<li class="have">Controle de Indicadores</li>
								<li class="have">Planejamento Escolar</li>
                                <li class="have">Matrícula Online</li>
								<li class="have">Captação de Alunos</li>
                               
							</ul>

							<div class="action text-center">
								<a href="#" class="pix-btn btn-outline">Solicitar Orçamento</a>
							</div>
						</div>
						<!-- /.pricing-table -->
					</div>
					<!-- /.col-lg-4 -->

					<div class="col-lg-4">
						<div class="pricing-table color-two">
							<div class="pricing-header pricing-amount">
								<div class="annual_price">
									<h2 class="price">R$ 1.450,00</h2>
								</div>
								<!-- /.annual_price -->

								<div class="monthly_price">
									<h2 class="price">R$ 150,00</h2>
								</div>
								<!-- /.monthly_price -->

								<h3 class="price-title">Gestão Empresarial</h3>
								<p>Consulte nossos consultores</p>
							</div>
							<!-- /.pricing-header -->

							<ul class="price-feture">
								<li class="have">Gestão de CRM</li>
								<li class="have">Gestão Financeira</li>
								<li class="have">Gestão de Compras</li>
								<li class="have">Emissão de Nota fiscal</li>
								<li class="not">Infraestrutura</li>
							</ul>

							<div class="action text-center">
								<a href="#" class="pix-btn btn-outline">Solicitar Orçamento</a>
							</div>
						</div>
						<!-- /.pricing-table -->
					</div>
					<!-- /.col-lg-4 -->

					<div class="col-lg-4">
						<div class="pricing-table color-three">

							<div class="pricing-header pricing-amount">
								<div class="annual_price">
									<h2 class="price">R$ 1200.00</h2>
								</div>
								<!-- /.annual_price -->

								<div class="monthly_price">
									<h2 class="price">R$ 125,00</h2>
								</div>
								<!-- /.monthly_price -->

								<h3 class="price-title">Serviços Advices</h3>
								<p>Consulte nossos consultores</p>
							</div>
							<!-- /.pricing-header -->

							<ul class="price-feture">
								<li class="have">Consultoria</li>
                                <li class="have">Suporte</li>
                                <li class="have">Treinamento</li>
								<li class="have">Implantação</li>
                                <li class="have">Infraestrutura</li>
							</ul>

							<div class="action text-center">
								<a href="#" class="pix-btn btn-outline">Solicitar Orçamento</a>
							</div>
						</div>
						<!-- /.pricing-table -->
					</div>
					<!-- /.col-lg-4 -->


				</div>
				<!-- /.advanced-pricing-table -->
			</div>
			<!-- /.container -->

			<div class="faq-section">
				<div class="container">
					<div class="section-title text-center">
						<h3 class="sub-title wow pixFadeUp">Perguntas Frequentes</h3>
						<h2 class="title wow pixFadeUp" data-wow-delay="0.8s">
							Quer perguntar algo para nós?
						</h2>
					</div>
					<!-- /.section-title -->
					<div class="tabs-wrapper wow pixFadeUp" data-wow-delay="1.1s">
						<ul class="nav faq-tabs" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="design-tab" data-toggle="tab" href="#design" role="tab" aria-controls="design" aria-selected="true">Advice Pos</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="service-tab" data-toggle="tab" href="#service" role="tab" aria-controls="service" aria-selected="false">Financeiro</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="false">Gestão de Compras</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="branding-tab" data-toggle="tab" href="#branding" role="tab" aria-controls="branding" aria-selected="false">Notas Fiscais</a>
							</li>
						</ul>
						<div class="tab-content" id="myTabContent">
							<div class="tab-pane fade show active" id="design" role="tabpanel" aria-labelledby="design-tab">
								<div id="accordion" class="faq faq-two pixFade">
									<div class="card active">
										<div class="card-header" id="heading100">
											<h5 class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse001" aria-expanded="false" aria-controls="collapse001">
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse001" aria-expanded="false" aria-controls="collapse001">
											Quais as vantagens da Advice Pos?
										</button>
											</h5>
										</div>
										<div id="collapse001" class="collapse show" aria-labelledby="heading100" data-parent="#accordion" style="">
											<div class="card-body">
												<p>
													Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias sint repellendus culpa assumenda repudiandae ipsa! Ut laudantium at et placeat hic tenetur eos repellendus sunt. Earum officia et quas minus?
												</p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header" id="heading200">
											<h5 class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse100" aria-expanded="false" aria-controls="collapse100">
												<button class="btn btn-link" data-toggle="collapse" data-target="#collapse100" aria-expanded="true" aria-controls="collapse100">
											Como posso obter um acesso demonstrativo?
										</button>
											</h5>
										</div>
										<div id="collapse100" class="collapse" aria-labelledby="heading200" data-parent="#accordion" style="">
											<div class="card-body">
												<p>
													Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias sint repellendus culpa assumenda repudiandae ipsa! Ut laudantium at et placeat hic tenetur eos repellendus sunt. Earum officia et quas minus?
												</p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header" id="heading300">
											<h5 class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse200" aria-expanded="false" aria-controls="collapse200">
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse200" aria-expanded="false" aria-controls="collapse200">
											Como funciona o Treinamento, Suporte e Help Desk?
										</button>
											</h5>
										</div>
										<div id="collapse200" class="collapse" aria-labelledby="heading300" data-parent="#accordion" style="">
											<div class="card-body">
												<p>
													Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias sint repellendus culpa assumenda repudiandae ipsa! Ut laudantium at et placeat hic tenetur eos repellendus sunt. Earum officia et quas minus?
												</p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header" id="heading400">
											<h5 class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse300" aria-expanded="false" aria-controls="collapse300">
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse300" aria-expanded="false" aria-controls="collapse300">
											Como é realizado o serviço de consultoria?
										</button>
											</h5>
										</div>
										<div id="collapse300" class="collapse" aria-labelledby="heading400" data-parent="#accordion" style="">
											<div class="card-body">
												<p>
													Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias sint repellendus culpa assumenda repudiandae ipsa! Ut laudantium at et placeat hic tenetur eos repellendus sunt. Earum officia et quas minus?
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="service" role="tabpanel" aria-labelledby="service-tab">
								<div id="accordion-2" class="faq faq-two pixFade">
									<div class="card active">
										<div class="card-header" id="heading100">
											<h5 class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse001" aria-expanded="false" aria-controls="collapse001">
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse001" aria-expanded="false" aria-controls="collapse001">
											Quais as vantagens da Advice Pos?
										</button>
											</h5>
										</div>
										<div id="collapse001" class="collapse show" aria-labelledby="heading100" data-parent="#accordion" style="">
											<div class="card-body">
												<p>
													Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias sint repellendus culpa assumenda repudiandae ipsa! Ut laudantium at et placeat hic tenetur eos repellendus sunt. Earum officia et quas minus?
												</p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header" id="heading200">
											<h5 class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse100" aria-expanded="false" aria-controls="collapse100">
												<button class="btn btn-link" data-toggle="collapse" data-target="#collapse100" aria-expanded="true" aria-controls="collapse100">
											Como posso obter um acesso demonstrativo?
										</button>
											</h5>
										</div>
										<div id="collapse100" class="collapse" aria-labelledby="heading200" data-parent="#accordion" style="">
											<div class="card-body">
												<p>
													Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias sint repellendus culpa assumenda repudiandae ipsa! Ut laudantium at et placeat hic tenetur eos repellendus sunt. Earum officia et quas minus?
												</p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header" id="heading300">
											<h5 class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse200" aria-expanded="false" aria-controls="collapse200">
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse200" aria-expanded="false" aria-controls="collapse200">
											Como funciona o Treinamento, Suporte e Help Desk?
										</button>
											</h5>
										</div>
										<div id="collapse200" class="collapse" aria-labelledby="heading300" data-parent="#accordion" style="">
											<div class="card-body">
												<p>
													Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias sint repellendus culpa assumenda repudiandae ipsa! Ut laudantium at et placeat hic tenetur eos repellendus sunt. Earum officia et quas minus?
												</p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header" id="heading400">
											<h5 class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse300" aria-expanded="false" aria-controls="collapse300">
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse300" aria-expanded="false" aria-controls="collapse300">
											Como é realizado o serviço de consultoria?
										</button>
											</h5>
										</div>
										<div id="collapse300" class="collapse" aria-labelledby="heading400" data-parent="#accordion" style="">
											<div class="card-body">
												<p>
													Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias sint repellendus culpa assumenda repudiandae ipsa! Ut laudantium at et placeat hic tenetur eos repellendus sunt. Earum officia et quas minus?
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="general" role="tabpanel" aria-labelledby="general-tab">
								<div id="accordion-3" class="faq faq-two pixFade">
									<div class="card active">
										<div class="card-header" id="heading100">
											<h5 class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse001" aria-expanded="false" aria-controls="collapse001">
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse001" aria-expanded="false" aria-controls="collapse001">
											Quais as vantagens da Advice Pos?
										</button>
											</h5>
										</div>
										<div id="collapse001" class="collapse show" aria-labelledby="heading100" data-parent="#accordion" style="">
											<div class="card-body">
												<p>
													Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias sint repellendus culpa assumenda repudiandae ipsa! Ut laudantium at et placeat hic tenetur eos repellendus sunt. Earum officia et quas minus?
												</p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header" id="heading200">
											<h5 class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse100" aria-expanded="false" aria-controls="collapse100">
												<button class="btn btn-link" data-toggle="collapse" data-target="#collapse100" aria-expanded="true" aria-controls="collapse100">
											Como posso obter um acesso demonstrativo?
										</button>
											</h5>
										</div>
										<div id="collapse100" class="collapse" aria-labelledby="heading200" data-parent="#accordion" style="">
											<div class="card-body">
												<p>
													Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias sint repellendus culpa assumenda repudiandae ipsa! Ut laudantium at et placeat hic tenetur eos repellendus sunt. Earum officia et quas minus?
												</p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header" id="heading300">
											<h5 class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse200" aria-expanded="false" aria-controls="collapse200">
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse200" aria-expanded="false" aria-controls="collapse200">
											Como funciona o Treinamento, Suporte e Help Desk?
										</button>
											</h5>
										</div>
										<div id="collapse200" class="collapse" aria-labelledby="heading300" data-parent="#accordion" style="">
											<div class="card-body">
												<p>
													Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias sint repellendus culpa assumenda repudiandae ipsa! Ut laudantium at et placeat hic tenetur eos repellendus sunt. Earum officia et quas minus?
												</p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header" id="heading400">
											<h5 class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse300" aria-expanded="false" aria-controls="collapse300">
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse300" aria-expanded="false" aria-controls="collapse300">
											Como é realizado o serviço de consultoria?
										</button>
											</h5>
										</div>
										<div id="collapse300" class="collapse" aria-labelledby="heading400" data-parent="#accordion" style="">
											<div class="card-body">
												<p>
													Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias sint repellendus culpa assumenda repudiandae ipsa! Ut laudantium at et placeat hic tenetur eos repellendus sunt. Earum officia et quas minus?
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="branding" role="tabpanel" aria-labelledby="branding-tab">
								<div id="accordion-4" class="faq faq-two pixFade">
									<div class="card active">
										<div class="card-header" id="heading100">
											<h5 class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse001" aria-expanded="false" aria-controls="collapse001">
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse001" aria-expanded="false" aria-controls="collapse001">
											Quais as vantagens da Advice Pos?
										</button>
											</h5>
										</div>
										<div id="collapse001" class="collapse show" aria-labelledby="heading100" data-parent="#accordion" style="">
											<div class="card-body">
												<p>
													Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias sint repellendus culpa assumenda repudiandae ipsa! Ut laudantium at et placeat hic tenetur eos repellendus sunt. Earum officia et quas minus?
												</p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header" id="heading200">
											<h5 class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse100" aria-expanded="false" aria-controls="collapse100">
												<button class="btn btn-link" data-toggle="collapse" data-target="#collapse100" aria-expanded="true" aria-controls="collapse100">
											Como posso obter um acesso demonstrativo?
										</button>
											</h5>
										</div>
										<div id="collapse100" class="collapse" aria-labelledby="heading200" data-parent="#accordion" style="">
											<div class="card-body">
												<p>
													Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias sint repellendus culpa assumenda repudiandae ipsa! Ut laudantium at et placeat hic tenetur eos repellendus sunt. Earum officia et quas minus?
												</p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header" id="heading300">
											<h5 class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse200" aria-expanded="false" aria-controls="collapse200">
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse200" aria-expanded="false" aria-controls="collapse200">
											Como funciona o Treinamento, Suporte e Help Desk?
										</button>
											</h5>
										</div>
										<div id="collapse200" class="collapse" aria-labelledby="heading300" data-parent="#accordion" style="">
											<div class="card-body">
												<p>
													Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias sint repellendus culpa assumenda repudiandae ipsa! Ut laudantium at et placeat hic tenetur eos repellendus sunt. Earum officia et quas minus?
												</p>
											</div>
										</div>
									</div>
									<div class="card">
										<div class="card-header" id="heading400">
											<h5 class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse300" aria-expanded="false" aria-controls="collapse300">
												<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse300" aria-expanded="false" aria-controls="collapse300">
											Como é realizado o serviço de consultoria?
										</button>
											</h5>
										</div>
										<div id="collapse300" class="collapse" aria-labelledby="heading400" data-parent="#accordion" style="">
											<div class="card-body">
												<p>
													Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias sint repellendus culpa assumenda repudiandae ipsa! Ut laudantium at et placeat hic tenetur eos repellendus sunt. Earum officia et quas minus?
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.tabs-wrapper -->

					<div class="btn-container text-center mt-40 wow pixFadeUp">
						<a href="#" class="pix-btn btn-outline">Veja Mais</a>
					</div>
					<!-- /.btn-container text-center -->

				</div>
				<!-- /.container -->
			</div>
			<!-- /.faq-section -->

			<div class="scroll-circle wow pixFadeLeft">
				<img src="media/background/circle8.png" data-parallax='{"y" : 130}' alt="circle">
			</div>
		</section>
		<!-- /.pricing -->

		<!--==================================-->
		<!--=         Call To Action         =-->
		<!--==================================-->
		<?php include('includes/banner-demonstracao.php');?>
		<!-- /.call-to-action -->

		<!--=========================-->
		<!--=        Footer         =-->
		<!--=========================-->
		<?php include('includes/footer.php')?>
		<!-- /#footer -->


	</div>
	<!-- /#site -->

	<?php include('includes/scripts.php')?>

</body>

</html>