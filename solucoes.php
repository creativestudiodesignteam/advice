<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Meta Data -->
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<title>Home - Software, App, SaaS landing HTML Template</title>

	<?php include('includes/head.php');?>


</head>

<body id="home-version-1" class="home-color-two" data-style="default">

<a href="#main_content" data-type="section-switch" class="return-to-top">
		<i class="fa fa-chevron-up"></i>
	</a>

	<!--=========================-->
	<!--=        Loader         =-->
	<!--=========================-->
	<?php include('includes/loader.php');?>
	<!-- /.page-loader -->

	<div id="main_content">


		<!--=========================-->
		<!--=        Navbar         =-->
		<!--=========================-->
		<?php include('includes/header-interna.php');?>
		<!-- /.site-header -->

		<!--==========================-->
		<!--=         Banner         =-->
		<!--==========================-->
		<section class="page-banner">
			<div class="container">
				<div class="page-title-wrapper">
					<h1 class="page-title">Soluções para<br>instituição de ensino</h1>

					<ul class="bradcurmed">
						<li><a href="index.php" rel="noopener noreferrer">Home</a></li>
						<li>Soluções</li>
					</ul>
				</div>
				<!-- /.page-title-wrapper -->
			</div>
			<!-- /.container -->


			<svg class="circle" data-parallax='{"x" : -200}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
                <path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
            </svg>

			<ul class="animate-ball">
				<li class="ball"></li>
				<li class="ball"></li>
				<li class="ball"></li>
				<li class="ball"></li>
				<li class="ball"></li>
			</ul>
		</section>
		<!-- /.page-banner -->

	<div id="main_content">

        <!--===========================-->
		<!--=         Feature         =-->
		<!--===========================-->
		<section class="featured-four-ab why">
            <div class="bg-angle-2"></div>
			<div class="container">
				<div class="section-title text-center">
					<h2 class="title wow pixFadeUp text-light" data-wow-delay="0.3s">Por que escolher a Advice</h2>
				</div>
				<!-- /.section-title -->

				<div class="row justify-content-center">
					<div class="col-lg-4 col-md-6">
						<div class="pixsass-icon-box-wrapper style-four wow pixFadeLeft" data-wow-delay="0.5s">
							<div class="iapp-icon-box-icon text-left">
								<img src="media/feature/01icon-missao.png" alt="">
							</div>
							<div class="pixsass-icon-box-content text-left">
								<h3 class="pixsass-icon-box-title"><a href="#">Matrícula Online</a></h3>
								<p>
                                    Otimiza o tempo dos pais, responsáveis e da 
                                    Instituição;
                                    Possibilita a escolha de atividades extracurriculares;
								</p>

								<a href="#" class="more-btn"><i class="ei ei-arrow_right"></i></a>

								<svg class="layer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="370px" height="270px">
                                    <path fill-rule="evenodd" fill="rgb(253, 248, 248)" d="M-0.000,269.999 L-0.000,-0.001 L370.000,-0.001 C370.000,-0.001 347.889,107.879 188.862,112.181 C35.160,116.338 -0.000,269.999 -0.000,269.999 Z" />
                                </svg>
							</div>
						</div>
						<!-- /.pixsass-box style-four -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->

					<div class="col-lg-4 col-md-6">
						<div class="pixsass-icon-box-wrapper style-four wow pixFadeLeft" data-wow-delay="0.6s">
							<div class="iapp-icon-box-icon text-left">
								<img src="media/feature/02icon-controle.png" alt="">
							</div>
							<div class="pixsass-icon-box-content text-left">
								<h3 class="pixsass-icon-box-title"><a href="#">Solução - Quadro Horário</a></h3>

								<p>
									Tenha controle total das tarefas por status e sinalizadores, tendo a visão real das operações agilizando atividades estratégicas, táticas e operacionais.
								</p>

								<a href="#" class="more-btn"><i class="ei ei-arrow_right"></i></a>
							</div>

							<svg class="layer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="370px" height="270px">
						<path fill-rule="evenodd" fill="rgb(253, 248, 248)" d="M-0.000,269.999 L-0.000,-0.001 L370.000,-0.001 C370.000,-0.001 347.889,107.879 188.862,112.181 C35.160,116.338 -0.000,269.999 -0.000,269.999 Z" />
					</svg>
						</div>
						<!-- /.pixsass-box style-four -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->

					<div class="col-lg-4 col-md-6">
						<div class="pixsass-icon-box-wrapper style-four wow pixFadeLeft" data-wow-delay="0.9s">
							<div class="iapp-icon-box-icon text-left">
								<img src="media/feature/03icon-parceiros.png" alt="">
							</div>
							<div class="pixsass-icon-box-content text-left">
								<h3 class="pixsass-icon-box-title"><a href="#">Interface web</a></h3>
								<p>
									Realizamos assessoria completa com consultoria personalizada, criação e execução de projetos, implantação, treinamento e suporte total.
								</p>

								<a href="#" class="more-btn"><i class="ei ei-arrow_right"></i></a>
							</div>

							<svg class="layer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="370px" height="270px">
						<path fill-rule="evenodd" fill="rgb(253, 248, 248)" d="M-0.000,269.999 L-0.000,-0.001 L370.000,-0.001 C370.000,-0.001 347.889,107.879 188.862,112.181 C35.160,116.338 -0.000,269.999 -0.000,269.999 Z" />
					</svg>
						</div>
						<!-- /.pixsass-box style-four -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		</section>
        <!-- /.featured -->


        <!--=================================-->
		<!--=         Revolutionize         =-->
		<!--=================================-->
		<section class="revolutionize tab">
			
			<div class="container">
				<!-- /.section-title dark-title -->

				<div id="pix-tabs" class="wow pixFadeUp" data-wow-delay="1.0s">
					<ul id="pix-tabs-nav">
						<li class="active"><a href="#tab1">Academico </a></li>
						<li><a href="#tab2">Pedagógica </a></li>
						<li><a href="#tab3">Responsáveis </a></li>
						<li><a href="#tab4">Planejamento</a></li>
                        <li><a href="#tab5">Controle</a></li>
					</ul>
					<!-- tabs-nav -->

					<div id="pix-tabs-content">
						<div id="tab1" class="content">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-7 text-left align-self-center">
                                        <div class="editor-content">
                                            <div class="section-title style-two">
                                                <h5 class="title wow pixFadeUp" data-wow-delay="0.9s">
                                                Central Acadêmica – Pais, Alunos e Responsáveis
                                                </h5>
                                            </div>

                                            <div class="description wow pixFadeUp" data-wow-delay="1.1s">
                                                <ul class="list-items wow pixFadeUp" data-wow-delay="1.1s">
                                                    <li>Consulta de atividades internas e externas;</li>
                                                    <li>Consulta de Ocorrências;</li>
                                                    <li>Consulta de Avaliações;</li>
                                                    <li>Consulta de Ficha Financeira;</li>
                                                    <li>Emissão de Boletim;</li>
                                                    <li>Consulta e emissão de boleto bancário;</li>
                                                    <li>Visualização de conteúdos publicados pelo colégio;</li>
                                                </ul>

                                                <a href="#" class="pix-btn btn-outline wow pixFadeUp mt-4" data-wow-delay="0.7s" data-toggle="modal" data-target="#modalExemplo">Saiba mais</a>
                                            </div>
                                        </div>
                                        <!-- /.editor-content -->
                                    </div>

                                    <div class="col-lg-5 align-self-center">
                                        <div class="swiper-container">
                                            <div class="swiper-wrapper">
                                                <div class="swiper-slide"><img src="media/revolutionize/2.jpg" alt="revolutionize"></div>
                                                <div class="swiper-slide"><img src="media/revolutionize/2.jpg" alt="revolutionize"></div>
                                            </div>
                                            <!-- Add Arrows -->
                                            <div class="swiper-button-next"><i class="fas fa-chevron-right"></i></div>
                                            <div class="swiper-button-prev"><i class="fas fa-chevron-left"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
						</div>
						<div id="tab2" class="content">
							<img src="media/revolutionize/1.jpg" alt="revolutionize">
							<div class="shape-shadow"></div>
						</div>
						<div id="tab3" class="content">
							<img src="media/revolutionize/3.jpg" alt="revolutionize">
							<div class="shape-shadow"></div>
						</div>

						<div id="tab4" class="content">
							<img src="media/revolutionize/1.jpg" alt="revolutionize">
							<div class="shape-shadow"></div>
						</div>
                        <div id="tab5" class="content">
							<img src="media/revolutionize/2.jpg" alt="revolutionize">
							<div class="shape-shadow"></div>
						</div>
					</div>
					<!-- tabs-content -->
				</div>
				<!-- Tabs -->
			</div>
			<!-- /.container -->
		</section>
		<!-- /.revolutionize -->

        <!--===========================-->
		<!--=         Feature         =-->
		<!--===========================-->
		<section class="featured-four-ab plus">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-6">
						<div class="pixsass-icon-box-wrapper style-four wow pixFadeLeft -boxe" data-wow-delay="0.5s">
							<div class="pixsass-icon-box-content text-left">
								<h3 class="pixsass-icon-box-title"><a href="#">Soluções para todos os<br>segmentos de negócio</a></h3>
								<p>
                                Você controla e organiza os registros de cada
                                contato realizado com prospects e clientes:<br>

                                Controle dos atendimentos de captação;<br>

                                Histórico de atendimentos (por período, atendente e status);

								</p>

								<a href="#" class="more-btn"><i class="ei ei-arrow_right"></i></a>

								<svg class="layer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="370px" height="270px">
                                    <path fill-rule="evenodd" fill="rgb(253, 248, 248)" d="M-0.000,269.999 L-0.000,-0.001 L370.000,-0.001 C370.000,-0.001 347.889,107.879 188.862,112.181 C35.160,116.338 -0.000,269.999 -0.000,269.999 Z" />
                                </svg>
							</div>
						</div>
						<!-- /.pixsass-box style-four -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->

					<div class="col-lg-6">
						<div class="pixsass-icon-box-wrapper style-four wow pixFadeLeft -boxe" data-wow-delay="0.6s">
							<div class="pixsass-icon-box-content text-left">
								<h3 class="pixsass-icon-box-title"><a href="#">Maior qualidade e agilidade. Mais vantagens para sua empresa.</a></h3>

								<p>
                                A implantação de um Sistema de Gestão deve proporcionar à empresa uma ferramenta para aprimorar a qualidade das informações e garantir o maior controle e segurança dos dados. O processo de implantação é primordial, pois é a base de sustentação
                                para a utilização eficiente da ferramenta.
								</p>

								<a href="#" class="more-btn"><i class="ei ei-arrow_right"></i></a>
							</div>

							<svg class="layer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="370px" height="270px">
						<path fill-rule="evenodd" fill="rgb(253, 248, 248)" d="M-0.000,269.999 L-0.000,-0.001 L370.000,-0.001 C370.000,-0.001 347.889,107.879 188.862,112.181 C35.160,116.338 -0.000,269.999 -0.000,269.999 Z" />
					</svg>
						</div>
						<!-- /.pixsass-box style-four -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		</section>
        <!-- /.featured -->
        

		<?php include('includes/banner-demonstracao.php');?>
		<?php include('includes/footer.php')?>
	</div>
	<!-- /#site -->

    <div class="modal fade" id="modalExemplo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Título do modal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-primary">Salvar mudanças</button>
            </div>
            </div>
        </div>
    </div>

	<!-- Dependency Scripts -->
    <?php include('includes/scripts.php')?>
    <script>
        var swiper = new Swiper('.swiper-container', {
            loop: true,
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
    </script>

</body>

</html>