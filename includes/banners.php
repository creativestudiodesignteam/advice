<section class="banner banner-one">
			<div class="circle-shape" data-parallax='{"y" : 230}'><img src="media/banner/circle-shape.png" alt="circle"></div>
			<div class="container">
				
				<div class="owl-carousel owl-theme owl-slide">
				<div class="item">
						<div class="banner-content-wrap alinha-slide">
							<div class="row align-items-center">
								<div class="col-lg-5">
									<div class="banner-content">
										<h1 class="banner-title">
											Sistema<br> <span>Advice POS <br>
									</span> 
										</h1>

										<p class="description">
											A melhor plataforma para gestão da sua <br> empresa e instituições de ensino!
										</p>
										<a href="#" class="pxs-btn banner-btn">Solicitar Demonstração</a>
										
									</div>
									<!-- /.banner-content -->
								</div>
								<!-- /.col-lg-6 -->

								<div class="col-lg-7">
									<div class="promo-mockup">
										<img src="media/banner/macbook.png" alt="mpckup">
									</div>
									<!-- /.promo-mockup -->
								</div>
								<!-- /.col-lg-6 -->
							</div>
							<!-- /.row -->
						</div>
					</div>					
					<div class="item">
						<div class="banner-content-wrap alinha-slide">
							<div class="row align-items-center">

								
								<!-- /.col-lg-6 -->
								<div class="col-lg-5">
									<div class="banner-content">
										<h1 class="banner-title">
											App<br> <span>Advice POS <br>
									</span> 
										</h1>

										<p class="description">
											Tenha a gestão da sua empresa<br>na palma da sua mão.
										</p>
										<a href="#" class="pxs-btn banner-btn">Solicitar Demonstração</a>
										
									</div>
									<!-- /.banner-content -->
								</div>
								<!-- /.col-lg-6 -->
								<div class="col-lg-7">
									<div class="promo-mockup">
										<img src="media/banner/celular.png" alt="mpckup">
									</div>
									<!-- /.promo-mockup -->
								</div>
							</div>
							<!-- /.row -->
						</div>
					</div>
					<div class="item">
						<div class="banner-content-wrap alinha-slide">
							<div class="row align-items-center">
								<div class="col-lg-5">
									<div class="banner-content">
										<h1 class="banner-title">
											<b>Advice</b><br>System
										</h1>

										<p class="description">
											participa de reportagem especial<br>sobre empreendedorismo.
										</p>

										<a href="https://www.youtube.com/watch?v=3CNEW07eFRA" class="play-btn popup-video"><i class="ei ei-arrow_triangle-right"></i> Assista nosso Video</a> <br><br>
									</div>
									<!-- /.banner-content -->
								</div>
								<!-- /.col-lg-6 -->

								<div class="col-lg-7">
									<div class="promo-mockup">
										<img src="media/banner/macbook-video.png" alt="mpckup">
									</div>
									<!-- /.promo-mockup -->
								</div>
								<!-- /.col-lg-6 -->
							</div>
							<!-- /.row -->
						</div>
					</div>					
					<div class="item">
						<div class="banner-content-wrap alinha-slide">
							<div class="row align-items-center">
								<div class="col-lg-5">
									<div class="banner-content">
										<h1 class="banner-title">
											Sistema<br> <span>Advice POS <br>
									</span> 
										</h1>

										<p class="description">
											A melhor plataforma para gestão da sua <br> empresa e instituições de ensino!
										</p>
										<a href="#" class="pxs-btn banner-btn">Solicitar Demonstração</a>
										
									</div>
									<!-- /.banner-content -->
								</div>
								<!-- /.col-lg-6 -->

								<div class="col-lg-7">
									<div class="promo-mockup">
										<img src="media/banner/macbook.png" alt="mpckup">
									</div>
									<!-- /.promo-mockup -->
								</div>
								<!-- /.col-lg-6 -->
							</div>
							<!-- /.row -->
						</div>
					</div>
				</div>
				
				<!-- /.banner-content-wrap -->
			</div>
			<!-- /.container -->

			<div class="bg-shape">
				<img src="media/banner/shape-bg.png" alt="">
			</div>
		</section>
		<!-- /.banner banner-one -->