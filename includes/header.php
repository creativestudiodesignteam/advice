		<header class="site-header header_trans-fixed" data-top="992">
			<div class="container">
				<div class="heder-inner">
					<div class="toggle-menu">
						<span class="bar"></span>
						<span class="bar"></span>
						<span class="bar"></span>
					</div>
					<!-- /.toggle-menu -->

					<div class="site-mobile-logo">
						<a href="index.php" class="logo">
					<img src="assets/img/main-logo.png" alt="site logo" class="main-logo">
					<img src="assets/img/sticky-logo.png" alt="site logo" class="sticky-logo">
				</a>
					</div>

					<nav class="site-nav">
						<div class="close-menu">
							<span>Close</span>
							<i class="ei ei-icon_close"></i>
						</div>

						<div class="site-logo">
							<a href="index.php" class="logo">
						<img src="assets/img/main-logo.png" alt="site logo" class="main-logo">
						<img src="assets/img/sticky-logo.png" alt="site logo" class="sticky-logo">						
					</a>
						</div>
						<!-- /.site-logo -->

						<div class="menu-wrapper" data-top="992">
							<ul class="site-main-menu">
								<li><a href="index.php">Home</a></li>
								<li><a href="empresa.php">Empresa</a></li>
                               	<li class="menu-item-has-children">
                               		<a href="solucoes.php">Soluções</a>
                               		<ul class="sub-menu">
										<li><a href="">Gestão de Ensino</a></li>
										<li><a href="">Gestão Financeira</a></li>
										<li><a href="">Gestão CRM</a></li>
										<li><a href="">Gestão de Compras</a></li>
									</ul>
                               </li>
								<li><a href="servicos.php">Serviços</a></li>
								<li><a href="cases.php">Cases</a></li>
                                <li><a href="blog.php">News</a></li>
								<li><a href="contato.php">Contato</a></li>
							</ul>

							<div class="nav-right">
								<a href="signup.php" class="nav-btn">Teste Grátis</a>
							</div>
						</div>
						<!-- /.menu-wrapper -->

					</nav>
					<!-- /.site-nav -->
				</div>
				<!-- /.heder-inner -->
			</div>
			<!-- /.container -->
		</header>