        <section class="call-to-action">
			<div class="overlay-bg"><img src="media/background/ellipse.png" alt="bg"></div>
			<div class="container">
				<div class="action-content text-center wow pixFadeUp">
					<h2 class="title" data-wow-delay="0.9s">
						Conheça tudo o que o Advice POS<br> pode fazer por você.
					</h2>

					<p>
						Nossos consultores Advice System estão à disposição para apresentar<br>todas as funcionalidades da nossa solução.
					</p>

					<a href="#" class="pix-btn btn-light">Agendar Demonstração</a>
				</div>
				<!-- /.action-content -->
			</div>
			<!-- /.container -->

			<div class="scroll-circle">
				<img src="media/background/circle13.png" data-parallax='{"y" : -130}' alt="circle">
			</div>
			<!-- /.scroll-circle -->
		</section>