<!--===============================-->
		<!--=         Testimonial         =-->
		<!--===============================-->
		<section class="testimonials-two">
			<div class="animate-shape wow pixFadeDown">
				<img src="media/background/circle9.png" data-parallax='{"y" : 230}' alt="shape">
			</div>
			<div class="container">
				<div class="section-title color-two text-center">
					<h3 class="sub-title wow pixFadeUp">Depoimentos</h3>
					<h2 class="title wow pixFadeUp" data-wow-delay="0.3s">O que nossos clientes dizem</h2>
				</div>
				<!-- /.section-title -->

				<div id="testimonial-wrapper" class="wow pixFadeUp" data-wow-delay="0.4s">
					<div class="swiper-container" id="testimonial-two" data-speed="700" data-autoplay="5000" data-perpage="2" data-space="50" data-breakpoints='{"991": {"slidesPerView": 1}}'>
						<div class="swiper-wrapper">
							<div class="swiper-slide">
								<div class="testimonial-two">
									<div class="testi-content-inner">
										<div class="testimonial-bio">
											<div class="avatar">
												<img src="media/testimonial/isaac_newton.png" alt="testimonial">
											</div>
											<!-- /.avatar -->

											<div class="bio-info">
												<h3 class="name">Marcos Roberto da Ponte</h3>
												<span class="job">Mantenedor</span>
											</div>
										</div>
										<!-- /.testimonial -->


										<div class="testimonial-content">
											<p>Passamos a contar com uma plataforma mais moderna que facilitou muito as integrações entre os departamentos. Com pouco tempo de funcionamento o ADVICE POS já confirma resultados determinantes para a instituição com agilidade nas informações, otimização dos processos e mais qualidade no atendimento a pais e alunos.</p>
										</div>
										<!-- /.testimonial-content -->

										<a href="http://www.cosin.com.br" target="_blank"> www.cosin.com.br</a>

										<div class="quote">
											<img src="media/testimonial/quote.png" alt="quote">
										</div>
										<!-- /.quote -->
									</div>
									<!-- /.testi-content-inner -->

									<div class="shape-shadow"></div>
								</div>
								<!-- /.testimonial-two -->
							</div>

							<div class="swiper-slide">
								<div class="testimonial-two">
									<div class="testi-content-inner">
										<div class="testimonial-bio">
											<div class="avatar">
												<img src="media/testimonial/rezende_rezende2.png" alt="Colégio Rezende Rezende">
											</div>
											<!-- /.avatar -->

											<div class="bio-info">
												<h3 class="name">Ely Dall’Agnol</h3>
												<span class="job">Mantenedor</span>
											</div>
										</div>
										<!-- /.testimonial -->


										<div class="testimonial-content">
											<p> A implantação do Advice POS nos permitiu trabalhar de forma integrada. Entre as mudanças mais significativas está o acesso a informação, antes os dados	estavam concentrados em cada área do colégio dificultando o acesso, hoje com a automatização conseguimos economizar tempo e oferecer maior agilidade no atendimento de nossos clientes.</p>
										</div>
										<!-- /.testimonial-content -->
										<a href="http://www.rezenderezende.com.br" target="_blank"> www.rezenderezende.com.br</a>
                                       

										<div class="quote">
											<img src="media/testimonial/quote.png" alt="quote">
										</div>
									</div>
									<!-- /.testi-content-inner -->

									<div class="shape-shadow"></div>
								</div>
								<!-- /.testimonial-two -->
							</div>

							<div class="swiper-slide">
								<div class="testimonial-two">
									<div class="testi-content-inner">
										<div class="testimonial-bio">
											<div class="avatar">
												<img src="media/testimonial/logo_dombosco.png" alt="testimonial">
											</div>
											<!-- /.avatar -->

											<div class="bio-info">
												<h3 class="name">Adriano Castanho</h3>
												<span class="job">Diretor</span>
											</div>
										</div>
										<!-- /.testimonial -->


										<div class="testimonial-content">
											<p>Decidimos trocar nosso sistema por uma ferramenta de gestão integrada que entre outras coisas permitisse	uma maior interação entre o Dom Bosco e seus alunos. Consultamos as opções do mercado e	optamos pela parceria com a Advice, hoje temos uma ferramenta que nos garante maior controle, qualidade	e segurança</p>
										</div>
										<!-- /.testimonial-content -->
                                        <a href="http://www.cermac.com.br" target="_blank"> www.cermac.com.br</a>

										
										<div class="quote">
											<img src="media/testimonial/quote.png" alt="quote">
										</div>
									</div>
									<!-- /.testi-content-inner -->

									<div class="shape-shadow"></div>
								</div>
								<!-- /.testimonial-two -->
							</div>

						</div>
						<!-- /.swiper-wrapper -->

					</div>
					<!-- /.swiper-container -->
					<div class="shape-shadow"></div>

					<div class="slider-nav wow pixFadeUp" data-wow-delay="0.3s">
						<div id="slide-prev" class="swiper-button-prev">
							<i class="ei ei-arrow_left"></i>
						</div>
						<div id="slide-next" class=" swiper-button-next">
							<i class="ei ei-arrow_right"></i>
						</div>
					</div>
				</div>
				<!-- /#testimonial-wrapper -->


			</div>
			<!-- /.container -->
		</section>
		<!-- /.testimonial -->