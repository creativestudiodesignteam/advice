        <footer id="footer">
			<div class="container">
				<div class="footer-nner wow pixFadeUp">
					<div class="row">
						<div class="col-lg-3 col-md-6">
							<div class="widget footer-widget">
								<h3 class="widget-title">Menu</h3>

								<ul class="footer-menu">
									<li><a href="#">Empresa</a></li>
									<li><a href="#">Soluções</a></li>
									<li><a href="#">Serviços</a></li>
									<li><a href="#">Cases</a></li>
									<li><a href="#">News</a></li>
								</ul>
							</div>
							<!-- /.widget footer-widget -->
						</div>
						<!-- /.col-lg-3 col-md-6 -->

						<div class="col-lg-3 col-md-6">
							<div class="widget footer-widget">
								<h3 class="widget-title">Soluções</h3>

								<ul class="footer-menu">
									<li><a href="#">Para Instituições de Ensino</a></li>
									<li><a href="#">Para seu Negócio</a></li>
									<li><a href="#">Gestão Financeira</a></li>
									<li><a href="#">Gestão de Compras</a></li>
									<li><a href="#">Gestão de CRM</a></li>
								</ul>
							</div>
							<!-- /.widget footer-widget -->
						</div>
						<!-- /.col-lg-3 col-md-6 -->

						<div class="col-lg-3 col-md-6">
							<div class="widget footer-widget">
								<h3 class="widget-title">Experiência</h3>

								<ul class="footer-menu">
									<li><a href="#">Teste Grátis</a></li>
									<li><a href="#">Ver Apresentação</a></li>
									<li><a href="#">Agendar Visita</a></li>
                                    <li><a href="#">Orçamentos</a></li>
									<li><a href="#">Enviar Mensagem</a></li>
								</ul>
							</div>
							<!-- /.widget footer-widget -->
						</div>
						<!-- /.col-lg-3 col-md-6 -->

						<div class="col-lg-3 col-md-6">
							<div class="widget footer-widget">
								<h3 class="widget-title">Atendimento</h3>
                                
                                <!-- /.contact-info -->

							<div class="contact-info">
								
                                <div class="info phone mb-3">
									<i class="ei ei-icon_phone"></i>
									<span> (11) 3513-5075 | (11) 93513-5075</span>
								</div>
								
                                <div class="info address mb-3">
									<i class="fas fa-map-marker-alt"></i>
									<span> Rua Voluntário da Pátria, 1092 - Santana São Paulo - SP - CEP 02010-100</span>
								</div>
                                
                                

								
							</div>
							<!-- /.contact-info -->

									<ul class="footer-social-link">
									<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
									<li><a href="#"><i class="fab fa-twitter"></i></a></li>
									<li><a href="#"><i class="fab fa-youtube"></i></a></li>
									<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                    
								</ul>
							</div>
							<!-- /.widget footer-widget -->
						</div>
						<!-- /.col-lg-3 col-md-6 -->
					</div>
					<!-- /.row -->

				</div>
				<!-- /.footer-nner -->

				<div class="site-info wow pixFadeUp" data-wow-delay="0.3s">
					<div class="copyright">
						<p class=" d-flex justify-content-center">© <?php echo date('Y');?> Todos os direitos reservados&nbsp;<a href="http://www.advicesystem.com.br" target="_blank"> Advice System</a></p>
					</div>

					<ul class="site-info-menu">Desenvolvido por
						<li><a href="http://www.deepocean.com.br" target="_blank">Deep Ocean</a>.</li>
						
					</ul>
				</div>
				<!-- /.site-info -->
			</div>
			<!-- /.container -->
		</footer>

<!-- popup whatsapp -->
<div>
    <a href="https://api.whatsapp.com/send?phone=5511935135075&text=Olá,%20Que%20bom%20que%20você%20nos%20visitou.%20%20
Envie%20sua%20mensagem%20que%20logo%20entrarei%20em%20contato." 
       target="_blank" class="link_whap">
       <i class="fab fa-whatsapp" style="margin-top:16px"></i>
    </a>
</div>
<!-- fim popup whatsapp -->		

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5d5ed27c77aa790be3304361/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->