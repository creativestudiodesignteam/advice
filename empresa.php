<!DOCTYPE html>
<html lang="en">

<head>
	<!-- Meta Data -->
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta name="description" content=""  />
	<meta name="keywords" content=""  />
	<title>Empresa — Advice System</title>
    <?php include('includes/head.php');?>
</head>

<body id="home-version-1" class="home-version-1" data-style="default">

	<a href="#main_content" data-type="section-switch" class="return-to-top">
		<i class="fa fa-chevron-up"></i>
	</a>

	<!--=========================-->
	<!--=        Loader         =-->
	<!--=========================-->
	<?php include('includes/loader.php');?>
	<!-- /.page-loader -->

	<div id="main_content">


		<!--=========================-->
		<!--=        Navbar         =-->
		<!--=========================-->
		<?php include('includes/header-interna.php');?>
		<!-- /.site-header -->

		<!--==========================-->
		<!--=         Banner         =-->
		<!--==========================-->
		<section class="page-banner">
			<div class="container">
				<div class="page-title-wrapper">
					<h1 class="page-title">Empresa</h1>

					<ul class="bradcurmed">
						<li><a href="index.php" rel="noopener noreferrer">Home</a></li>
						<li>Empresa</li>
					</ul>
				</div>
				<!-- /.page-title-wrapper -->
			</div>
			<!-- /.container -->


			<svg class="circle" data-parallax='{"x" : -200}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
		<path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
	</svg>

			<ul class="animate-ball">
				<li class="ball"></li>
				<li class="ball"></li>
				<li class="ball"></li>
				<li class="ball"></li>
				<li class="ball"></li>
			</ul>
		</section>
		<!-- /.page-banner -->

		<!--=========================-->
		<!--=         About         =-->
		<!--=========================-->
		<section class="about">
			<div class="container">
				<div class="row">
					<div class="col-lg-7">
						<div class="about-content">
							<div class="section-title">
								<h3 class="sub-title wow pixFadeUp">Sobre Nós</h3>
								<h2 class="title wow pixFadeUp" data-wow-delay="0.3s">
									Advice System
								</h2><br>
								<h4 class="singiture wow pixFadeUp" data-wow-delay="0.4s">
									Tecnologia conectada à eficiência
								</h4>
							</div>
							<!-- /.section-title -->

							<p class="description wow pixFadeUp" data-wow-delay="0.4s">
								Uma empresa sólida e especializada em desenvolver
soluções tecnológicas com forte destaque no
segmento educacional. Através da sua expertise,
trouxe para clientes e mercado soluções inovadoras
e diferenciadas na área de Sistemas de Gestão,
Consultoria especializada em Infraestrutura e Sites. <br><br>
Prezando pelo alto grau de comprometimento
com cada cliente, a Advice System possui uma equipe com profissionais altamente capacitados,
para que cada solução gere maior lucratividade e resultados concretos. Através da parceria com a
Meira Fernandes Consultoria & Assessoria, uma das mais conceituadas e respeitadas empresas
de Contabilidade e Soluções, desenvolvemos soluções tecnológicas que tornam a Gestão da
sua empresa cada vez mais eficiente e voltada para o controle e resultados.							</p>

							
					</div>
						<!-- /.about-content -->
					</div>
					<!-- /.col-lg-7 -->

					<div class="col-lg-5">
						<div class="about-thumb wow pixFadeRight" data-wow-delay="0.6s">
							<img src="media/about/1.jpg" alt="about">
						</div>
						<!-- /.about-thumb -->
					</div>
					<!-- /.col-lg-5 -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		</section>
		<!-- /.about -->

		<!--===========================-->
		<!--=         Feature         =-->
		<!--===========================-->
		<section class="featured-four-ab">
			<div class="container">
				<div class="section-title text-center">
					<h3 class="sub-title wow pixFadeUp">Diferenciais</h3>
					<h2 class="title wow pixFadeUp" data-wow-delay="0.3s">Por que escolher a Advice</h2>
				</div>
				<!-- /.section-title -->

				<div class="row justify-content-center">
					<div class="col-lg-4 col-md-6">
						<div class="pixsass-icon-box-wrapper style-four wow pixFadeLeft" data-wow-delay="0.5s">
							<div class="iapp-icon-box-icon text-center">
								<img src="media/feature/01icon-missao.png" alt="">
							</div>
							<div class="pixsass-icon-box-content text-center">
								<h3 class="pixsass-icon-box-title"><a href="#">Visão e Missão</a></h3>
								<p>
									Nossa missão é proporcionar utilização eficiente do capital financeiro, humano, intelectual e tecnológico. Otimizando recursos para o lançamento da empresa.
								</p>

								<a href="#" class="more-btn"><i class="ei ei-arrow_right"></i></a>

								<svg class="layer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="370px" height="270px">
							<path fill-rule="evenodd" fill="rgb(253, 248, 248)" d="M-0.000,269.999 L-0.000,-0.001 L370.000,-0.001 C370.000,-0.001 347.889,107.879 188.862,112.181 C35.160,116.338 -0.000,269.999 -0.000,269.999 Z" />
						</svg>
							</div>
						</div>
						<!-- /.pixsass-box style-four -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->

					<div class="col-lg-4 col-md-6">
						<div class="pixsass-icon-box-wrapper style-four wow pixFadeLeft" data-wow-delay="0.6s">
							<div class="iapp-icon-box-icon text-center">
								<img src="media/feature/02icon-controle.png" alt="">
							</div>
							<div class="pixsass-icon-box-content text-center">
								<h3 class="pixsass-icon-box-title"><a href="#">Controle e Indicadores</a></h3>

								<p>
									Tenha controle total das tarefas por status e sinalizadores, tendo a visão real das operações agilizando atividades estratégicas, táticas e operacionais.
								</p>

								<a href="#" class="more-btn"><i class="ei ei-arrow_right"></i></a>
							</div>

							<svg class="layer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="370px" height="270px">
						<path fill-rule="evenodd" fill="rgb(253, 248, 248)" d="M-0.000,269.999 L-0.000,-0.001 L370.000,-0.001 C370.000,-0.001 347.889,107.879 188.862,112.181 C35.160,116.338 -0.000,269.999 -0.000,269.999 Z" />
					</svg>
						</div>
						<!-- /.pixsass-box style-four -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->

					<div class="col-lg-4 col-md-6">
						<div class="pixsass-icon-box-wrapper style-four wow pixFadeLeft" data-wow-delay="0.9s">
							<div class="iapp-icon-box-icon text-center">
								<img src="media/feature/03icon-parceiros.png" alt="">
							</div>
							<div class="pixsass-icon-box-content text-center">
								<h3 class="pixsass-icon-box-title"><a href="#">Assessoria e Consultoria</a></h3>
								<p>
									Realizamos assessoria completa com consultoria personalizada, criação e execução de projetos, implantação, treinamento e suporte total.
								</p>

								<a href="#" class="more-btn"><i class="ei ei-arrow_right"></i></a>
							</div>

							<svg class="layer" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="370px" height="270px">
						<path fill-rule="evenodd" fill="rgb(253, 248, 248)" d="M-0.000,269.999 L-0.000,-0.001 L370.000,-0.001 C370.000,-0.001 347.889,107.879 188.862,112.181 C35.160,116.338 -0.000,269.999 -0.000,269.999 Z" />
					</svg>
						</div>
						<!-- /.pixsass-box style-four -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		</section>
		<!-- /.featured -->

		<!--===========================-->
		<!--=         Count Up        =-->
		<!--===========================-->
		<section class="countup">
			<div class="bg-map" data-bg-image="media/background/map2.png">

			</div>
			<div class="container">
				<div class="section-title text-center">
					<h3 class="sub-title wow pixFadeUp">Advice System</h3>
					<h2 class="title wow pixFadeUp" data-wow-delay="0.2s">
						Indicadores
					</h2>
				</div>
				<!-- /.section-title -->

				<div class="countup-wrapper">
					<div class="row">
						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="fun-fact color-two wow pixFadeUp" data-wow-delay="0.3s">
								<div class="counter">
									<h4 class="count" data-counter="40">40 </h4>
									<span></span>
								</div>
								<!-- /.counter -->

								<p>Anos de Experiência</p>
							</div>
							<!-- /.fun-fact -->
						</div>
						<!-- /.col-lg-3 col-md-6 col-sm-6 -->

						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="fun-fact color-two wow pixFadeUp" data-wow-delay="0.5s">
								<div class="counter">
									<h4 class="count" data-counter="279">279 </h4>
									<span></span>
								</div>
								<!-- /.counter -->
								<p>clientes</p>
							</div>
							<!-- /.fun-fact -->
						</div>
						<!-- /.col-lg-3 col-md-6 col-sm-6 -->

						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="fun-fact color-two wow pixFadeUp" data-wow-delay="0.7s">
								<div class="counter">
									<h4 class="count" data-counter="70">70 </h4>
									<span>K</span>
								</div>
								<!-- /.counter -->
								<p>Usuários</p>
							</div>
							<!-- /.fun-fact -->
						</div>
						<!-- /.col-lg-3 col-md-6 col-sm-6 -->

						<div class="col-lg-3 col-md-6 col-sm-6">
							<div class="fun-fact color-two wow pixFadeUp" data-wow-delay="0.9s">
								<div class="counter">
									<h4 class="count" data-counter="153">153 </h4>
									<span>K</span>
								</div>
								<!-- /.counter -->
								<p>Documentos</p>
							</div>
							<!-- /.fun-fact -->
						</div>
						<!-- /.col-lg-3 col-md-6 col-sm-6 -->
					</div>
					<!-- /.row -->
				</div>
				<!-- /.countup-wrapper -->

				<div class="button-container text-center wow pixFadeUp" data-wow-delay="0.3s">
					<a href="#" class="pix-btn btn-outline">Testar Grátis</a>
				</div>
				<!-- /.button-container -->
			</div>
			<!-- /.container -->
		</section>
		<!-- /.countup -->

		<!--===============================-->
		<!--=         Testimonial         =-->
		<!--===============================-->
		<!--===============================-->
		<!--=         Testimonial         =-->
		<!--===============================-->
		<section class="testimonials-two">
			<div class="animate-shape wow pixFadeDown">
				<img src="media/background/circle9.png" data-parallax='{"y" : 230}' alt="shape">
			</div>
			<div class="container">
				<div class="section-title color-two text-center">
					<h3 class="sub-title wow pixFadeUp">Parceiros</h3>
					<h2 class="title wow pixFadeUp" data-wow-delay="0.3s">Empresas que agregam valor</h2>
				</div>
				<!-- /.section-title -->

				<div id="testimonial-wrapper" class="wow pixFadeUp" data-wow-delay="0.4s">
					<div class="swiper-container" id="testimonial-two" data-speed="700" data-autoplay="5000" data-perpage="2" data-space="50" data-breakpoints='{"991": {"slidesPerView": 1}}'>
						<div class="swiper-wrapper">
							<div class="swiper-slide">
								<div class="testimonial-two">
									<div class="testi-content-inner item-noticia">
										<div class="testimonial-bio">
											<div class="img-fluid">
												<img src="media/brand/logo-meira-fernades.jpg" alt="Meira Fernandes">
											</div>
											<!-- /.avatar -->
										</div>
										<!-- /.testimonial -->


										<div class="testimonial-content">
											<p>A Meira Fernandes foi a primeira empresa a especializar-se na prestação de serviços exclusivos para Instituições de Ensino (pequeno, médio e grande porte), nas áreas de Finanças, Contábil, Fiscal, Pessoal, Legal,  3º Setor e Tributário.<br><br></p>
										</div>
										<!-- /.testimonial-content -->

										<a href="https://www.meirafernandes.com.br" target="_blank"> www.meirafernandes.com.br</a>

										<div class="quote">
											<img src="media/testimonial/quote.png" alt="quote">
										</div>
										<!-- /.quote -->
									</div>
									<!-- /.testi-content-inner -->

									<div class="shape-shadow"></div>
								</div>
								<!-- /.testimonial-two -->
							</div>

							<div class="swiper-slide">
								<div class="testimonial-two">
									<div class="testi-content-inner item-noticia">
										<div class="testimonial-bio">
											<div class="img-fluid">
												<img src="media/brand/logo-ccfm.jpg" alt="CCFM ">
											</div>
											<!-- /.avatar -->
										</div>
										<!-- /.testimonial -->


										<div class="testimonial-content">
											<p> Fundado em 1986, a CCFM conta com uma equipe profissional do mais alto nível, com expertise em todas as áreas do Direito, com destaque no segmento educacional, utilizando tecnologias de ponta e oferecendo alta  qualidade em atendimentos personalizados.</p>
										</div>
										<!-- /.testimonial-content -->
										<a href="http://www.ccfmadvocacia.com.br" target="_blank"> www.ccfmadvocacia.com.br</a>
                                       

										<div class="quote">
											<img src="media/testimonial/quote.png" alt="quote">
										</div>
									</div>
									<!-- /.testi-content-inner -->

									<div class="shape-shadow"></div>
								</div>
								<!-- /.testimonial-two -->
							</div>


						</div>
						<!-- /.swiper-wrapper -->

					</div>
					<!-- /.swiper-container -->
					<div class="shape-shadow"></div>

					<div class="slider-nav wow pixFadeUp" data-wow-delay="0.3s">
						<div id="slide-prev" class="swiper-button-prev">
							<i class="ei ei-arrow_left"></i>
						</div>
						<div id="slide-next" class=" swiper-button-next">
							<i class="ei ei-arrow_right"></i>
						</div>
					</div>
				</div>
				<!-- /#testimonial-wrapper -->


			</div>
			<!-- /.container -->
		</section>
		<!-- /.testimonial -->
		<!-- /.testimonial -->

		<!--========================-->
		<!--=         Team         =-->
		<!--========================-->
		<section class="teams">
			<div class="container">
				<div class="section-title text-center">
					<h3 class="sub-title wow pixFadeUp">Conheça nossa equipe</h3>
					<h2 class="title wow pixFadeUp" data-wow-delay="0.3s">Time Advice</h2>
				</div>
				<!-- /.section-title text-center -->


				<div class="row">
					<div class="col-lg-4 col-md-6">
						<div class="team-member wow pixFadeRight" data-wow-delay="0.3s">
							<div class="member-avater">
								<img src="media/team/1.jpg" alt="avater">

								<ul class="member-social">
									<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
									<li><a href="#"><i class="fab fa-twitter"></i></a></li>
									<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
								</ul>

								<svg class="layer-one" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="370px" height="210px">
							<path fill-rule="evenodd" opacity="0.302" fill="rgb(250, 112, 112)" d="M-0.000,79.999 C-0.000,79.999 85.262,-11.383 187.324,50.502 C297.703,117.429 370.000,-0.001 370.000,-0.001 L370.000,203.999 C370.000,207.313 367.314,209.999 364.000,209.999 L6.000,209.999 C2.686,209.999 -0.000,207.313 -0.000,203.999 L-0.000,79.999 Z" />
						</svg>

								<svg class="layer-two" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="370px" height="218px">
							<path fill-rule="evenodd" opacity="0.8" fill="rgb(250, 112, 112)" d="M-0.000,27.999 C-0.000,27.999 95.694,-46.224 188.615,48.781 C278.036,140.208 370.000,57.999 370.000,57.999 L370.000,211.999 C370.000,215.313 367.314,217.999 364.000,217.999 L6.000,217.999 C2.686,217.999 -0.000,215.313 -0.000,211.999 L-0.000,27.999 Z" />
						</svg>
							</div>

							<div class="team-info">
								<h3 class="name">Cristiane Sousa</h3>
								<h4 class="job">Tecnologia</h4>
							</div>
						</div>
						<!-- /.team-member -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->

					<div class="col-lg-4 col-md-6">
						<div class="team-member wow pixFadeRight" data-wow-delay="0.5s">
							<div class="member-avater">
								<img src="media/team/2.jpg" alt="avater">

								<ul class="member-social">
									<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
									<li><a href="#"><i class="fab fa-twitter"></i></a></li>
									<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
								</ul>

								<svg class="layer-one" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="370px" height="210px">
							<path fill-rule="evenodd" opacity="0.302" fill="rgb(250, 112, 112)" d="M-0.000,79.999 C-0.000,79.999 85.262,-11.383 187.324,50.502 C297.703,117.429 370.000,-0.001 370.000,-0.001 L370.000,203.999 C370.000,207.313 367.314,209.999 364.000,209.999 L6.000,209.999 C2.686,209.999 -0.000,207.313 -0.000,203.999 L-0.000,79.999 Z" />
						</svg>

								<svg class="layer-two" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="370px" height="218px">
							<path fill-rule="evenodd" opacity="0.8" fill="rgb(250, 112, 112)" d="M-0.000,27.999 C-0.000,27.999 95.694,-46.224 188.615,48.781 C278.036,140.208 370.000,57.999 370.000,57.999 L370.000,211.999 C370.000,215.313 367.314,217.999 364.000,217.999 L6.000,217.999 C2.686,217.999 -0.000,215.313 -0.000,211.999 L-0.000,27.999 Z" />
						</svg>
							</div>

							<div class="team-info">
								<h3 class="name">Fábio de Almeida</h3>
								<h4 class="job">Marketing</h4>
							</div>
						</div>
						<!-- /.team-member -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->

					<div class="col-lg-4 col-md-6">
						<div class="team-member wow pixFadeRight" data-wow-delay="0.7s">
							<div class="member-avater">
								<img src="media/team/3.jpg" alt="avater">

								<ul class="member-social">
									<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
									<li><a href="#"><i class="fab fa-twitter"></i></a></li>
									<li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
								</ul>

								<svg class="layer-one" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="370px" height="210px">
							<path fill-rule="evenodd" opacity="0.302" fill="rgb(250, 112, 112)" d="M-0.000,79.999 C-0.000,79.999 85.262,-11.383 187.324,50.502 C297.703,117.429 370.000,-0.001 370.000,-0.001 L370.000,203.999 C370.000,207.313 367.314,209.999 364.000,209.999 L6.000,209.999 C2.686,209.999 -0.000,207.313 -0.000,203.999 L-0.000,79.999 Z" />
						</svg>

								<svg class="layer-two" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="370px" height="218px">
							<path fill-rule="evenodd" opacity="0.8" fill="rgb(250, 112, 112)" d="M-0.000,27.999 C-0.000,27.999 95.694,-46.224 188.615,48.781 C278.036,140.208 370.000,57.999 370.000,57.999 L370.000,211.999 C370.000,215.313 367.314,217.999 364.000,217.999 L6.000,217.999 C2.686,217.999 -0.000,215.313 -0.000,211.999 L-0.000,27.999 Z" />
						</svg>
							</div>

							<div class="team-info">
								<h3 class="name">Luciana Silva</h3>
								<h4 class="job">Financeiro</h4>
							</div>
						</div>
						<!-- /.team-member -->
					</div>
					<!-- /.col-lg-4 col-md-6 -->

					<div class="col-lg-12">
						<div class="button-container text-center wow pixFadeUp" data-wow-delay="0.3s">
							<a href="#" class="pix-btn">Ver Equipe</a>
						</div>
					</div>
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->

		</section>
		<!-- /.teams -->

		<!--=================================-->
		<!--=         Logo Carousel         =-->
		<!--=================================-->
		<section class="brand-logo-ab">
        	<div class="container">
				<!-- /.section-title text-center -->
			        <br><div class="section-title text-center">
					<h3 class="sub-title wow pixFadeUp">Quem já utiliza o Advice POS</h3>
					<h2 class="title wow pixFadeUp" data-wow-delay="0.3s">Nossos Clientes</h2>
				</div>
				
			<div class="container">
				<div class="swiper-container logo-carousel wow pixFadeUp" id="logo-carousel" data-perpage="6" data-breakpoints='{"1024": {"slidesPerView": 4}, "768": {"slidesPerView": 4}, "620": {"slidesPerView": 3}}'>
					<div class="swiper-wrapper">
						<div class="swiper-slide">
							<div class="brand-logo"><img src="media/brand/1.png" alt="brand"></div>
						</div>
						<!-- /.swiper-slide -->

						<div class="swiper-slide">
							<div class="brand-logo"><img src="media/brand/2.png" alt="brand"></div>
						</div>
						<!-- /.swiper-slide -->

						<div class="swiper-slide">
							<div class="brand-logo"><img src="media/brand/3.png" alt="brand"></div>
						</div>
						<!-- /.swiper-slide -->

						<div class="swiper-slide">
							<div class="brand-logo"><img src="media/brand/4.png" alt="brand"></div>
						</div>
						<!-- /.swiper-slide -->

						<div class="swiper-slide">
							<div class="brand-logo"><img src="media/brand/5.png" alt="brand"></div>
						</div>
						<!-- /.swiper-slide -->

						<div class="swiper-slide">
							<div class="brand-logo"><img src="media/brand/6.png" alt="brand"></div>
						</div>
						<!-- /.swiper-slide -->
					</div>
					<!-- /.swiper-wrapper -->
				</div>
				<!-- /#logo-carousel.swiper-container logo-carousel -->
			</div>
			<!-- /.container -->
		</section>
		<!-- /.brand-logo -->

		<!--==================================-->
		<!--=         Call To Action         =-->
		<!--==================================-->
		<?php include('includes/banner-demonstracao.php');?>
		<!-- /.call-to-action -->

		<!--=========================-->
		<!--=        Footer         =-->
		<!--=========================-->
		<?php include('includes/footer.php')?>
		<!-- /#footer -->


	</div>
	<!-- /#site -->
    <?php include('includes/scripts.php')?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js"></script>
    <script>
    $(function() {
	    $('.item-noticia').matchHeight({
                byRow: true,
                property: 'height',
                target: null,
                remove: false
            });
        });
</body>

</html>