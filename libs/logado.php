<?php
require_once '../dao/daoUsuario.php';

    if (!isset($_SESSION["EMAIL_USUARIO"]) && !isset($_SESSION["SENHA_USUARIO"])){
        header('Location: index.php');
    } else {
        $daoUsuario = new DaoUsuario();
        $count = $daoUsuario->logado($_SESSION["EMAIL_USUARIO"], $_SESSION["SENHA_USUARIO"]);
        if($count==0){
            header('Location: index.php');
        }				
}//fim else
?>
