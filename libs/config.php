<?php

if ($_SERVER['SERVER_NAME'] == '127.0.0.1' || $_SERVER['SERVER_NAME'] == '192.168.0.106'){
        
        define('URL_SITE', 'http://'.$_SERVER['SERVER_NAME'].':8000');
        define('URL_IMG', 'E:/clientes/MeiraFernandes/2019/advice/site/assets/img');
        define('URL_CAMINHO_IMG', 'http://'.$_SERVER['SERVER_NAME'].':8000/assets/img');
        define('RAIZ', $_SERVER['DOCUMENT_ROOT']); 

} else {
        define('URL_SITE', 'http://www.deepocean.com.br/advice');
        define('URL_IMG', $_SERVER['DOCUMENT_ROOT'].'/assets/img');
        define('URL_CAMINHO_IMG', 'http://www.deepocean.com.br/advice/assets/img');
        define('RAIZ', $_SERVER['DOCUMENT_ROOT']);
} 
        
require RAIZ.'/vendor/autoload.php';

use Dao\DaoConfiguracao;
use Dao\DaoSite;
use Dao\DaoSeo;
use Dao\DaoContato;
use Dao\DaoMenu;
use Dao\DaoRedesSociais;
use Dao\DaoEndereco;
use Dao\DaoLinguagem;
use Dao\DaoTraducao;
use Dao\DaoNoticia;
use Dao\DaoCategoria;
use Dao\Conexao;


session_start();           	            

include('funcoes.php');


define('DB_USER', 'deepocean17');
define('DB_PASS', 'advice1357');
define('DB_HOST', 'mysql.deepocean.com.br');
define('DB_NAME', 'deepocean17');


$daoConfiguracao = new DaoConfiguracao();
$daoSeo = new DaoSeo();
$daoContato = new DaoContato();
$daoSite = new DaoSite();
$daoMenuSite = new DaoMenu();
$daoRedeSocialSite = new DaoRedesSociais();
$daoEnderecoSite = new DaoEndereco();
$daoLinguagemSite = new DaoLinguagem();
$daoTraducaoSite = new DaoTraducao();
$daoNoticiaSite = new DaoNoticia();
$daoCategoriaSite = new DaoCategoria();


if(isset($_GET['lang'])){
        
        $lang = $daoLinguagemSite->BuscarPorPrefix($_GET['lang']);
        $_SESSION['linguagem'] = $lang->getId();
        $_SESSION['linguagem_prefix'] = $lang->getPrefix();

        /* echo "entrou". $_GET['lang'];
        exit; */ 

    }

if(!isset($_SESSION['linguagem'])){
        $_SESSION['linguagem'] = 1;
        $_SESSION['linguagem_prefix'] = 'pt';
}

$RSDESC = $daoSeo->BuscarPorCOD(1);
$rs_config = $daoConfiguracao->BuscarPorCOD(1);
$rs_site = $daoSite->BuscarTodos($_SESSION['linguagem']);
$contatos = $daoContato->TotalSemVisualizacao();
$menus = $daoMenuSite->BuscarTodosSubMenu($_SESSION['linguagem'], 's', 0);

$redes_sociais = $daoRedeSocialSite->BuscarTodos();

$linguagens = $daoLinguagemSite->BuscarTodos('n');

$endereco = $daoEnderecoSite->BuscarPorCOD(1);
$tfooter = $daoTraducaoSite->BuscarPorPagina('rodape', $_SESSION['linguagem']);

//config
define('TITULO_SITE', $rs_site[0]->getTituloSite());
define('HOME', 'index.php');
define('EMAIL_CONTATO',$rs_config->getEmail());
define('ATENDIMENTO',$rs_site[0]->getAtendimento());
define('COPYRIGHT',$rs_site[0]->getCopyright());
define('NPAGINACAO',20);
define('EMAIL_ENVIO',$rs_config->getEmailEnvio());
define('SENHA_ENVIO',$rs_config->getSenhaEnvio());
define('SMTP',$rs_config->getSmtp());
define('TELEFONE',$rs_config->getTelefone());
define('ANALYTICS',$rs_config->getAnalytics());

define('TOTAL_CONTATOS',$contatos);
        
define('META_DESCRICAO', $RSDESC->getMetaDescricao());
define('KEYWORDS', $RSDESC->getKeywords());

define('ERROR_LOGIN',"Usuário não encontrado. Verifique se os dados estão corretos.");
define('ERROR_ADDRESS',"E-mail não encontrado. Verifique se digitou o e-mail correto");
define('INVALID_ADDRESS',"Digite um email válido.");

define('CORZEBRA1',"#f7f6f6");
define('CORZEBRA2',"#EBEBEB");

date_default_timezone_set('America/Sao_Paulo');       
?>