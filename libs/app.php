<?php
$app = new \Slim\Slim();
\Slim\Slim::registerAutoloader();




$app->map('/', function () use ($app) {
    $data['lang'] = 'pt';
    initroute('home', $data);
})->via('GET', 'POST');

$app->group('/', function () use ($app) {

    $app->map(':lang', function ($lang) use ($app) {
        $data['lang'] = $lang;
        initroute('home', $data);       
    })->via('GET', 'POST');


    $app->map('home', function () use ($app) {
        $data['lang'] = 'pt';
        initroute('home', $data);
    })->via('GET', 'POST');

    $app->map('index', function () use ($app) {
        $data['lang'] = 'pt';
        initroute('home', $data);
    })->via('GET', 'POST');

    $app->map(':lang/home', function ($lang) use ($app) {
        $data['lang'] = $lang;
        initroute('home', $data);       
    })->via('GET', 'POST');

    $app->map(':lang/index', function ($lang) use ($app) {
        $data['lang'] = $lang;
        initroute('home', $data);       
    })->via('GET', 'POST');    

    $app->map(':lang/idioma/:id', function ($lang, $id) use ($app) {
        $data['lang_'] = $lang;
        $data['cod'] = $id;
        initroute('action.idioma', $data);       
    })->via('GET', 'POST');


    $rotas_empr = ['empresa','company', 'la-empresa', 'sobre', 'acerca-de-nosotros'];
    foreach($rotas_empr as $rse){

        $app->map(':lang/'.$rse, function ($lang) use ($app) {
            $data['lang'] = $lang;
            initroute('sobre', $data);
        })->via('GET', 'POST');
    }


/*     $app->map(':lang/empresa', function ($lang) use ($app) {
        $data['lang'] = $lang;
        initroute('sobre', $data);       
    })->via('GET', 'POST'); */

    $rotas_fatos = ['fatos-e-dados-financeiros','facts-and-financial-data', 'hechos-y-datos-financieros'];
    foreach($rotas_fatos as $rfat){

        $app->map(':lang/'.$rfat, function ($lang) use ($app) {
            $data['lang'] = $lang;
            initroute('fatos-e-dados', $data);
        })->via('GET', 'POST');
    }

/*     $app->map(':lang/fatos-e-dados', function ($lang) use ($app) {
        $data['lang'] = $lang;
        initroute('fatos-e-dados', $data);
    })->via('GET', 'POST'); */

    $rotas_hist = ['historia','story', 'nuestra-historia'];
    foreach($rotas_hist as $rhi){

        $app->map(':lang/'.$rhi, function ($lang) use ($app) {
            $data['lang'] = $lang;
            initroute('historia', $data);
        })->via('GET', 'POST');
    }

/*     $app->map(':lang/historia', function ($lang) use ($app) {
        $data['lang'] = $lang;
        initroute('historia', $data);
    })->via('GET', 'POST'); */

    $rotas_m = ['visao-missao-valor','vision-mission-values', 'vision-mision-y-valores'];
    foreach($rotas_m as $rm){

        $app->map(':lang/'.$rm, function ($lang) use ($app) {
            $data['lang'] = $lang;
            initroute('missao-visao-valor', $data);
        })->via('GET', 'POST');
    }

/*     $app->map(':lang/missao-visao-valor', function ($lang) use ($app) {
        $data['lang'] = $lang;
        initroute('missao-visao-valor', $data);
    })->via('GET', 'POST'); */


    $rotas_prod = ['produtos','products', 'productos'];
    foreach($rotas_prod as $rp){

        $app->map(':lang/'.$rp, function ($lang) use ($app) {
            $data['lang'] = $lang;
            initroute('produtos', $data);
        })->via('GET', 'POST');

        /* ativar quando programar*/
        $app->map(':lang/'.$rp.'/:id/:desc', function ($lang, $id, $desc) use ($app) {
            $data['lang'] = $lang;
            $data['id'] = $id;
            $data['desc'] = $desc;
            initroute('produtos', $data);
        })->via('GET', 'POST'); 
    }

/*     $app->map(':lang/produtos', function ($lang) use ($app) {
        $data['lang'] = $lang;
        initroute('produtos', $data);
    })->via('GET', 'POST'); */




    $rotas_cont = ['contato','contact', 'contactar','contacto'];
    foreach($rotas_cont as $rcont){

        $app->map(':lang/'.$rcont, function ($lang) use ($app) {
            $data['lang'] = $lang;
            initroute('contato', $data);
        })->via('GET', 'POST');
    }

/*     $app->map(':lang/contato', function ($lang) use ($app) {
        $data['lang'] = $lang;
        initroute('contato', $data);
    })->via('GET', 'POST'); */


    $rotas_onde = ['onde-estamos','where-we-are', 'donde-estamos'];
    foreach($rotas_onde as $ronde){

        $app->map(':lang/'.$ronde, function ($lang) use ($app) {
            $data['lang'] = $lang;
            initroute('onde-estamos', $data);
        })->via('GET', 'POST');
    }

/*     $app->map(':lang/onde-estamos', function ($lang) use ($app) {
        $data['lang'] = $lang;
        initroute('onde-estamos', $data);
    })->via('GET', 'POST'); */

    /* apagar quando programar */

/*     $app->map(':lang/produtos/1/cosmeticos', function ($lang) use ($app) {
        $data['lang'] = $lang;
        initroute('cosmeticos', $data);
    })->via('GET', 'POST');

    $app->map(':lang/produtos/1/nutricao-animal', function ($lang) use ($app) {
        $data['lang'] = $lang;
        initroute('nutricao-animal', $data);
    })->via('GET', 'POST');

    $app->map(':lang/produtos/1/nutricao-humana-esportiva', function ($lang) use ($app) {
        $data['lang'] = $lang;
        initroute( 'nutricao-humana-esportiva', $data);
    })->via('GET', 'POST');

    $app->map(':lang/produtos/1/agro-micronutrientes', function ($lang) use ($app) {
        $data['lang'] = $lang;
        initroute('produtos', $data);
    })->via('GET', 'POST');

    $app->map(':lang/produtos/1/poliuretanos', function ($lang) use ($app) {
        $data['lang'] = $lang;
        initroute('poliuretanos', $data);
    })->via('GET', 'POST');

    $app->map(':lang/produtos/1/quimicos-industriais', function ($lang) use ($app) {
        $data['lang'] = $lang;
        initroute('quimicos-industriais', $data);
    })->via('GET', 'POST');
 */
  });

$app->run();
?>