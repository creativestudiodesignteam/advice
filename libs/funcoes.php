<?php

    function prevXssInjection($stVariable){
        $stVariable     = htmlentities($stVariable);
        $stVariable   = strip_tags($stVariable);
        $stVariable     = str_replace("'", "''", $stVariable);
        return $stVariable;
    }

    function prevSqlInjection($stVariable){
        if(is_numeric($stVariable)){
            $stVariable         = $stVariable;
        }else{
            $stVariable         = 0;
        }

        return $stVariable;
    }
	
// Função para gerar senha automaticamente
// Senha composta por letras e numeros
// tipo = L - letra, N - Numero    
function GerarSenha($tipo="L N L L N L") {
// o explode retira os espaços presentes entre as letras (L) e números (N)        
        $tipo = explode(" ", $tipo);

// Criação de um padrão de letras e números (no meu caso, usei letras maiúsculas
// mas você pode intercalar maiusculas e minusculas, ou adaptar ao seu modo.)
        $padrao_letras = "A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|X|W|Y|Z";
		$padrao_letras .= "a|b|c|d|e|f|g|h|i|j|k|l|m|n|o|p|q|r|s|t|u|v|x|w|y|z";
        $padrao_numeros = "0|1|2|3|4|5|6|7|8|9";

// criando os arrays, que armazenarão letras e números
// o explode retire os separadores | para utilizar as letras e números
        $array_letras = explode("|", $padrao_letras);
        $array_numeros = explode("|", $padrao_numeros);

// cria a senha baseado nas informações da função (L para letras e N para números)
        $senha = "";
        for ($i=0; $i<sizeOf($tipo); $i++) {
            if ($tipo[$i] == "L") {
                $senha.= $array_letras[array_rand($array_letras,1)];
            } else {
                if ($tipo[$i] == "N") {
                    $senha.= $array_numeros[array_rand($array_numeros,1)];
                }
            }
        }
// informa qual foi a senha gerada para o usuário naquele momento
        return $senha;

    }
	
function antiInjection ($var,$q='') {
    //Verifica se o parâmetro é um array
    if (!is_array($var)) {
        //identifico o tipo da variável e trato a string
        switch (gettype($var)) {
            case 'double':
            case 'integer':
				if($var==null) {
				$var = 0;	
				}
                $return = $var;
                break;
            case 'string':
                /*Verifico quantas vírgulas tem na string.
                  Se for mais de uma trato como string normal,
                  caso contrário trato como String Numérica*/
                $temp = (substr_count($var,',')==1) ? str_replace(',','*;*',$var) : $var;
                //aqui eu verifico se existe valor para não adicionar aspas desnecessariamente    
                if (!empty($temp)) {
                    if (is_numeric(str_replace('*;*','.',$temp))) {
                        $temp = str_replace('*;*','.',$temp);
                        $return = strstr($temp,'.') ? floatval($temp) : intval($temp);
                    } elseif (get_magic_quotes_gpc()) {
                              //aqui eu verifico o parametro q para o caso de ser necessário utilizar LIKE com %
                              $return = (empty($q)) ? '\''.str_replace('*;*',',',$temp).'\'' : '\'%'.str_replace('*;*',',',$temp).'%\'';
                    } else {
                        //aqui eu verifico o parametro q para o caso de ser necessário utilizar LIKE com %
                        $return = (empty($q)) ? '\''.addslashes(str_replace('*;*',',',$temp)).'\'' : '\'%'.addslashes(str_replace('*;*',',',$temp)).'%\'';
                    }
                } else {
                  // $return = $temp;
				  return '\'\'';
                }
                break;
				case '':
				  return '\'\'';
				break;
				case empty($temp):
				  return '\'\'';
				break;
				case null:
				  return '\'\'';
				break;
            default:
                /*Abaixo eu coloquei uma msg de erro para poder tratar
                  antes de realizar a query caso seja enviado um valor
                  que nao condiz com nenhum dos tipos tratatos desta
                  função. Porém você pode usar o retorno como preferir*/
                $return = '\'\'';
        }
        //Retorna o valor tipado
        return $return;
    } else {
        //Retorna os valores tipados de um array
        return array_map('antiInjection',$var);
    }
}


function limitarTexto($texto, $limite){ // FUNCAO PARA LIMITAR TEXTO - ADD HEBER 070112

    //$texto_clean = strip_tags($texto, '<(.*?)>');
    $texto_clean = $texto;
    if(strlen($texto_clean)>$limite){
        $texto = substr($texto_clean, 0, strrpos(substr($texto_clean, 0, $limite), ' ')) . '...';
        return $texto;
    }else{
        return $texto_clean;
    }

}

/*por Heber 16/02/11*/
function consulta_nivel($nivel){
	extract($GLOBALS, EXTR_SKIP);
	$niveis ='';
	$sql = "Select $nivel from tbl_adm_permissoes";
	//print $sql;
	$sql = $SITE->Executar($sql);
	if(mysqli_num_rows($sql) > 0){
		while ($row=mysqli_fetch_array($sql)){
		if(strlen($niveis) < strlen($row[0])){
			$niveis = $row[0];
			 //print  "<br>Nivel=".$nivel." -> ".$niveis."<br>";
		}}
	}
	//print $niveis."<br>";
return $niveis;
}

/*por Heber 17/02/11*/
function consulta_nivelUsuario($nivel,$cod_usuario,$valor){
	extract($GLOBALS, EXTR_SKIP);
	$status ='';
	$sql = "SELECT tbl_adm_permissoes.$nivel FROM tbl_adm_permissoes WHERE tbl_adm_permissoes.cod_usuario='".$cod_usuario."'";
	//print $sql;
	//exit;
	$sql = $SITE->Executar($sql);
	if(mysqli_num_rows($sql) > 0){
		
		$ver=mysqli_fetch_row($sql);
		$pos = strpos($ver[0], $valor);
		$status=0;
		// Note o uso de ===.  Simples == não funcionaria como esperado
		if ($pos === false) {
			$pos2 = strpos($ver[0], ",$valor,");
			if($pos2===false){
					$pos3 = strpos($ver[0], ",$valor");
					if($pos2===false){
						$status=0;
						}else{$status=1;}
				}else{$status=1;}
				
		} else {
			$status=1;		
		}
	}//fim if
	//print $status;
return $status;

}


function Convertem($term, $tp=1) { 
    if ($tp == "1") $palavra = strtr(strtoupper($term),"àáâãäåæçèéêëìíîïðñòóôõö÷øùüúþÿ","ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÜÚÞß"); 
    elseif ($tp == "0") $palavra = strtr(strtolower($term),"ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÜÚÞß","àáâãäåæçèéêëìíîïðñòóôõö÷øùüúþÿ"); 
    return $palavra; 
} 

function remove_accent($str) 
{ 
  $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ'); 
  $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o'); 
  return str_replace($a, $b, $str); 
} 

function post_slug($str, $carac='-') 
{ 
  $str = str_replace("/",$carac,$str);
  $str = remove_accent($str);
  return trim(strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/','/^\//'), array('', $carac, ''), remove_accent($str)))); 
}

function IntToMonth($int) {
		switch($int) {
			case "01" :
			return "Janeiro";
			break;
			case "02" :
			return "Fevereiro";
			break;
			case "03" :
			return "Março";
			break;
			case "04" :
			return "Abril";
			break;
			case "05" :
			return "Maio";
			break;
			case "06" :
			return "Junho";
			break;
			case "07" :
			return "Julho";
			break;
			case "08" :
			return "Agosto";
			break;
			case "09" :
			return "Setembro";
			break;
			case "10" :
			return "Outubro";
			break;
			case "11" :
			return "Novembro";
			break;
			case "12" :
			return "Dezembro";
			break;		
		}
}//fim função


function validaCPF($cpf)
{	// Verifiva se o número digitado contém todos os digitos
    $cpf = str_pad(preg_replace('[^0-9]', '', $cpf), 11, '0', STR_PAD_LEFT);
	
	// Verifica se nenhuma das sequências abaixo foi digitada, caso seja, retorna falso
    if (strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999')
	{
	return false;
    }
	else
	{   // Calcula os números para verificar se o CPF é verdadeiro
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
 
            $d = ((10 * $d) % 11) % 10;
 
            if ($cpf{$c} != $d) {
                return false;
            }
        }
 
        return true;
    }
}

function validaCNPJ($cnpj)
{
	$cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);
	// Valida tamanho
	if (strlen($cnpj) != 14)
		return false;
	// Valida primeiro dígito verificador
	for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++)
	{
		$soma += $cnpj{$i} * $j;
		$j = ($j == 2) ? 9 : $j - 1;
	}
	$resto = $soma % 11;
	if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto))
		return false;
	// Valida segundo dígito verificador
	for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++)
	{
		$soma += $cnpj{$i} * $j;
		$j = ($j == 2) ? 9 : $j - 1;
	}
	$resto = $soma % 11;
	return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
}








function URL_amigavel($string)
{
    $string = trim($string);
    $table = array(
        'Š'=>'S', 'š'=>'s', 'Ð'=>'D', 'd'=>'d', 'Ž'=>'Z',
        'ž'=>'z', 'C'=>'C', 'c'=>'c', 'C'=>'C', 'c'=>'c',
        'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
        'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
        'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
        'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
        'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
        'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
        'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
        'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
        'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
        'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
        'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
        'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
        'ÿ'=>'y', 'R'=>'R', 'r'=>'r',
    );
    // Traduz os caracteres em $string, baseado no vetor $table
    $string = strtr($string, $table);
    // converte para minúsculo
    $string = strtolower($string);
    // remove caracteres indesejáveis (que não estão no padrão)
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    // Remove múltiplas ocorrências de hífens ou espaços
    $string = preg_replace("/[\s-]+/", " ", $string);
    // Transforma espaços e underscores em hífens
    $string = preg_replace("/[\s_]/", "-", $string);
    // retorna a string
    return $string;
}

function converte_mes($string){
    $mes = array(
        'Jan'=>'Janeiro', 
        'Feb'=>'Fevereiro', 
        'Mar'=>'Março',
        'Apr'=>'Abril',
        'May'=>'Maio',
        'Jun'=>'Junho',
        'Jul'=>'Julho',
        'Aug'=>'Agosto',
        'Sep'=>'Setembro',
        'Oct'=>'Outubro',
        'Nov'=>'Novembro',
        'Dec'=>'Dezembro',
    );
    // Traduz os caracteres em $string, baseado no vetor $mes
    $string = strtr($string, $mes);

    return $string;
}

function converte_mes_reduzido($string){
    $mes = array(
        'Jan'=>'Jan', 
        'Feb'=>'Fev', 
        'Mar'=>'Mar',
        'Apr'=>'Abr',
        'May'=>'Mai',
        'Jun'=>'Jun',
        'Jul'=>'Jul',
        'Aug'=>'Ago',
        'Sep'=>'Set',
        'Oct'=>'Out',
        'Nov'=>'Nov',
        'Dec'=>'Dez',
    );
    // Traduz os caracteres em $string, baseado no vetor $mes
    $string = strtr($string, $mes);

    return $string;
}

function trataTexto($texto){
    $quem_somos = str_replace("<span>","<p>",$texto);
    $quem_somos = str_replace("</span>","</p>",$quem_somos);

    $quem_somos = str_replace("<blockquote>","",$quem_somos);
    $quem_somos = str_replace("</blockquote>","",$quem_somos);

    $quem_somos = str_replace("<div>","",$quem_somos);
    $quem_somos = str_replace("</div>","",$quem_somos);

    $return = $quem_somos;
    return $return;
}

function idade($data){
   
    // Separa em dia, mês e ano
    list($ano, $mes, $dia) = explode('-', $data);
   
    // Descobre que dia é hoje e retorna a unix timestamp
    $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
    // Descobre a unix timestamp da data de nascimento do fulano
    $nascimento = mktime( 0, 0, 0, $mes, $dia, $ano);
   
    // Depois apenas fazemos o cálculo já citado :)
    $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);
    return $idade;
}



// from http://wezfurlong.org/blog/2006/nov/http-post-from-php-without-curl
function do_post_request($url, $data, $optional_headers = null)
{
  $params = array('http' => array(
              'method' => 'POST',
              'content' => $data
            ));
  if ($optional_headers !== null) {
    $params['http']['header'] = $optional_headers;
  }
  $ctx = stream_context_create($params);
  $fp = @fopen($url, 'rb', false, $ctx);
  if (!$fp) {
    throw new Exception("Problem with $url, $php_errormsg");
  }
  $response = @stream_get_contents($fp);
  if ($response === false) {
    throw new Exception("Problem reading data from $url, $php_errormsg");
  }
  return $response;
}


function converteIngles($a, $linguagem){
    $a_categoria = array('Trabalhos Recentes', 'Notícia da ASA', 'Notícia jurídica','Artigo', 'Eventos');
    $a_categoria_pt = array('Trabalhos Recentes' => 'Trabalhos Recentes','Notícia da ASA' => 'Notícia ASA','Notícia jurídica' => 'Notícias Jurídicas','Artigo' => 'Artigos','Eventos' => 'Eventos');
    $a_categoria_en = array('Trabalhos Recentes' => 'Recent Work','Notícia da ASA' => 'News','Notícia jurídica' => 'Legal News','Artigo' => 'Articles','Eventos' => 'Events');

    if($linguagem == 1){
        echo $a_categoria_pt[$a];
    }else{
        echo $a_categoria_en[$a]; 
    }
}


?>