<?php
include("config.php");
require_once '../dao/DaoNewsletter.php';

$daoNewsletter = new DaoNewsletter();

// Nome do Arquivo do Excel que serÃ¡ gerado
$arquivo = 'emails_news.xls';

// Criamos uma tabela HTML com o formato da planilha para excel
$tabela = '<table border="1">';
$tabela .= '<tr>';
$tabela .= '<td colspan="4" align="center">E-mails Newsletter</tr>';
$tabela .= '</tr>';
$tabela .= '<tr>';
$tabela .= '<td><b>codigo</b></td>';
$tabela .= '<td><b>nome</b></td>';
$tabela .= '<td style="width:300px"><b>e-mail</b></td>';
$tabela .= '<td><b>data de cadastro</b></td>';
$tabela .= '</tr>';

// Puxando dados do Banco de dados
$retorno = $daoNewsletter->BuscarTodos();		

foreach ($retorno as $RS){
$tabela .= '<tr>';
$tabela .= '<td>'.$RS->getId().'</td>';
$tabela .= '<td>'.$RS->getNome().'</td>';
$tabela .= '<td>'.$RS->getEmail().'</td>';
$tabela .= '<td>'.$RS->getDataCadastro().'</td>';
$tabela .= '</tr>';
}

$tabela .= '</table>';

// ForÃ§a o Download do Arquivo Gerado
header ('Cache-Control: no-cache, must-revalidate');
header ('Pragma: no-cache');
header('Content-Type: application/x-msexcel');
header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
echo $tabela;
?>