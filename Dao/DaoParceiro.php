<?php

namespace Dao;

use Dao\Classes\Parceiro;

class DaoParceiro {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoParceiro();
        }
        return self::$instance;
    }

    public function Inserir(Parceiro $parceiro) {
        try {
            $sql = "INSERT INTO tbl_parceiro (nome, texto, cod_tipo, site,imagem,ativo) 
                                     VALUES (:nome, :texto, :cod_tipo, :site, :imagem, :ativo)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(":nome", $parceiro->getNome());
            $p_sql->bindValue(":texto", $parceiro->getTexto());
            $p_sql->bindValue(":cod_tipo", $parceiro->getTipo());
            $p_sql->bindValue(":site", $parceiro->getSite());
            $p_sql->bindValue(":imagem", $parceiro->getImagem());
            $p_sql->bindValue(":ativo", $parceiro->getAtivo());
            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
            //return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Editar(Parceiro $parceiro) {
        try {
            $sql = "UPDATE tbl_parceiro SET nome = :nome, texto = :texto, cod_tipo = :cod_tipo, site = :site, ativo = :ativo, imagem = :imagem"
                                   . " WHERE id = :id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(":nome", $parceiro->getNome());
            $p_sql->bindValue(":texto", $parceiro->getTexto());
            $p_sql->bindValue(":cod_tipo", $parceiro->getTipo());
            $p_sql->bindValue(":site", $parceiro->getSite());
            $p_sql->bindValue(":imagem", $parceiro->getImagem());
            $p_sql->bindValue(":ativo", $parceiro->getAtivo());
            $p_sql->bindValue(":id", $parceiro->getId());
            
            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";            
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "UPDATE tbl_parceiro SET ativo = :ativo WHERE id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", 'd');
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_parceiro WHERE id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            return $this->populaParceiro($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodos() {
        try {
            $sql = "SELECT * FROM tbl_parceiro WHERE ativo <> :ativo";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->execute();
            $parceiros = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $parceiros[] = $this->populaParceiro($row);
            }
                return $parceiros;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarTodosPorTipo($cod_tipo) {
        try {
            $sql = "SELECT * FROM tbl_parceiro WHERE ativo = :ativo AND cod_tipo = :cod_tipo";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", "s"); 
            $p_sql->bindValue(":cod_tipo", $cod_tipo);
            $p_sql->execute();
            $parceiros = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $parceiros[] = $this->populaParceiro($row);
            }
                return $parceiros;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function Total() {
        try {
            $sql = "SELECT * FROM tbl_parceiro WHERE ativo <> :ativo";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodosPaginacao($inicio, $limit) {
        try {
            $sql = "SELECT * FROM tbl_parceiro ORDER BY parceiro LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->execute();
            $parceiros = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $parceiros[] = $this->populaParceiro($row);
            }
                return $parceiros;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage();
        }
    }
    
    public function BuscarMaisRecentes($inicio, $limit) {
        try {
            $sql = "SELECT * FROM tbl_parceiro ORDER BY id DESC LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->execute();
            $parceiro = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $parceiro[] = $this->populaParceiro($row);
            }
                return $parceiro;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage();
        }
    }
    
    private function populaParceiro($row) {
        $pojo = new Parceiro();
        $pojo->setId($row['id']);
        $pojo->setNome($row['nome']);
        $pojo->setAtivo($row['ativo']);
        $pojo->setImagem($row['imagem']);
        $pojo->setTexto($row['texto']);
        $pojo->setTipo($row['cod_tipo']);
        $pojo->setSite($row['site']);
        return $pojo;
    }

}

