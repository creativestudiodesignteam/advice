<?php

namespace Dao;

use Dao\Classes\Noticia;

class DaoNoticia {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoNoticia();
        }
        return self::$instance;
    }

    public function Inserir(Noticia $obj) {
        try {
            $sql = "INSERT INTO tbl_noticia (titulo, usuario, linguagem_id, advogado_id, texto, imagem, ativo, resumo, cadastro, ultima_alteracao, categoria_id, noticia_id, link) 
                                     VALUES (:titulo, :usuario, :linguagem_id, :advogado_id, :texto, :imagem, :ativo, :resumo, :cadastro, :ultima_alteracao, :categoria_id, :noticia_id, :link)";

            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $this->setObj($p_sql, $obj);
            $p_sql->bindValue(":usuario", $obj->getUsuario());
            
            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }

    public function Editar(Noticia $obj) {
        try {
            $sql = "UPDATE tbl_noticia SET titulo = :titulo, cadastro = :cadastro, advogado_id = :advogado_id, texto = :texto, imagem = :imagem, ativo = :ativo, resumo = :resumo, ultima_alteracao = :ultima_alteracao, categoria_id = :categoria_id, link = :link"
                                 . " WHERE noticia_id = :noticia_id AND linguagem_id = :linguagem_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "DELETE FROM tbl_noticia WHERE noticia_id = :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":id", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_noticia WHERE noticia_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarPorCODLing($cod, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_noticia WHERE noticia_id = :cod AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();     
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }

    public function BuscarPorCODProxAntLing($cod, $linguagem, $op) {
        try {
            if($op == '<'){
                $order = 'DESC';
            }else{
                $order = 'ASC';
            }
            $sql = "SELECT * FROM tbl_noticia WHERE noticia_id ".$op." :cod AND linguagem_id = :linguagem_id ORDER BY noticia_id ".$order." LIMIT 0, 1 ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();     
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }
    
    public function BuscarTodos($linguagem) {
        try {
            $sql = "SELECT * FROM tbl_noticia WHERE linguagem_id = :linguagem_id ORDER BY cadastro DESC";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarTodosAdvogado($linguagem, $advogado) {
        try {
            $sql = "SELECT * FROM tbl_noticia WHERE linguagem_id = :linguagem_id AND advogado_id = :advogado_id ORDER BY cadastro DESC";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":advogado_id", $advogado);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function Total($linguagem) {
        try {
            $sql = "SELECT * FROM tbl_noticia WHERE titulo <> '' AND cadastro <= NOW() AND ativo = 's' AND linguagem_id = :linguagem_id ORDER BY cadastro DESC";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodosTituloPaginacao($titulo, $inicio, $limit, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_noticia WHERE (titulo LIKE :titulo OR texto LIKE :titulo ) AND cadastro <= NOW() AND ativo <> 'n' AND linguagem_id = :linguagem_id ORDER BY titulo LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(':titulo', "%".$titulo."%", \PDO::PARAM_STR);
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarTodosAdvogadoPaginacao($cod, $inicio, $limit, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_noticia 
            WHERE cadastro <= NOW() 
            AND tbl_noticia.ativo = 's' 
            AND linguagem_id = :linguagem_id 
            AND tbl_noticia.advogado_id = :advogado_id 
            ORDER BY cadastro DESC, noticia_id LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT);
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->bindParam(':advogado_id', $cod, \PDO::PARAM_INT); 
            
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage();
        }
    }

    public function TotalBuscaAdvogado($cod, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_noticia 
            WHERE cadastro <= NOW() 
            AND tbl_noticia.ativo = 's' 
            AND linguagem_id = :linguagem_id 
            AND tbl_noticia.advogado_id = :advogado_id 
            ORDER BY cadastro DESC, noticia_id  ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindParam(':advogado_id', $cod, \PDO::PARAM_INT); 
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
  
    
    public function TotalBuscaTitulo($titulo, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_noticia WHERE titulo LIKE :titulo AND cadastro <= NOW() AND ativo <> 'n' AND linguagem_id = :linguagem_id ORDER BY titulo";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':titulo', "%".$titulo."%", \PDO::PARAM_STR);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.". $titulo;
        }
    }

    public function InserirLike($cod, $numero) {
        try {
            $sql = "INSERT INTO tbl_noticia_likes (noticia_id, count) 
                                           VALUES (:noticia_id, :count)";

            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $p_sql->bindValue(":count", $numero);
            $p_sql->bindValue(":noticia_id", $cod);
            
            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }

    public function EditarLike($cod, $numero) {
        try {
            $sql = "UPDATE tbl_noticia_likes SET count = :count WHERE noticia_id = :noticia_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(":count", $numero);
            $p_sql->bindValue(":noticia_id", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }

    public function TotalLike($cod) {
        try {
            $sql = "SELECT * FROM tbl_noticia_likes WHERE noticia_id = :noticia_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":noticia_id", $cod);
            $p_sql->execute();
            
            return $row = $p_sql->fetchAll();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.". $titulo;
        }
    }

    public function BuscarTodosDataPaginacao($mes, $ano, $inicio, $limit, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_noticia WHERE MONTH(cadastro) = :mes AND YEAR(cadastro) = :ano AND ativo <> 'n' AND linguagem_id = :linguagem_id ORDER BY cadastro DESC LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":mes", $mes);
            $p_sql->bindValue(":ano", $ano);
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
  
    
    public function TotalBuscaData($mes, $ano, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_noticia WHERE MONTH(cadastro) = :mes AND YEAR(cadastro) = :ano AND ativo <> 'n' AND linguagem_id = :linguagem_id ORDER BY titulo";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":mes", $mes);
            $p_sql->bindValue(":ano", $ano);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.". $titulo;
        }
    }
    
    public function BuscarTodosPaginacao($inicio, $limit, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_noticia WHERE titulo <> '' AND cadastro <= NOW() AND ativo = 's' AND linguagem_id = :linguagem_id ORDER BY cadastro DESC LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage();
        }
    }

    public function TotalBuscarTodosPaginacao($inicio, $limit, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_noticia WHERE cadastro <= NOW() AND ativo = 's' AND linguagem_id = :linguagem_id ORDER BY cadastro DESC LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    
    public function BuscarTodosDiferentesPaginacao($cod, $inicio, $limit, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_noticia WHERE noticia_id <> :noticia_id AND cadastro <= NOW() AND ativo <> 'n' AND linguagem_id = :linguagem_id ORDER BY cadastro DESC LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->bindParam(':noticia_id', $cod, \PDO::PARAM_INT); 
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage();
        }
    }

    public function BuscarTodosAreaPaginacao($cod, $inicio, $limit, $linguagem) {
        try {
            $sql = "SELECT tbl_noticia.* FROM tbl_noticia 
            INNER JOIN tbl_noticia_areas
            ON tbl_noticia_areas.noticia_id = tbl_noticia.noticia_id
            WHERE cadastro <= NOW() 
            AND ativo <> 'n' 
            AND linguagem_id = :linguagem_id
            AND tbl_noticia_areas.area_id = :area_id 
            ORDER BY tbl_noticia.cadastro DESC, tbl_noticia.noticia_id LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT);
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->bindParam(':area_id', $cod, \PDO::PARAM_INT); 
            
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage();
        }
    }

    public function TotalBuscaArea($cod, $linguagem) {
        try {
            $sql = "SELECT tbl_noticia.* FROM tbl_noticia 
            INNER JOIN tbl_noticia_areas
            ON tbl_noticia_areas.noticia_id = tbl_noticia.noticia_id
            WHERE cadastro <= NOW() 
            AND ativo <> 'n' 
            AND linguagem_id = :linguagem_id
            AND tbl_noticia_areas.area_id = :area_id 
            ORDER BY tbl_noticia.cadastro DESC, tbl_noticia.noticia_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindParam(':area_id', $cod, \PDO::PARAM_INT); 
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodosAntigosPaginacao($cod, $inicio, $limit, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_noticia WHERE noticia_id <> :noticia_id AND cadastro <= NOW() AND ativo <> 'n' AND linguagem_id = :linguagem_id ORDER BY cadastro ASC, noticia_id LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->bindParam(':noticia_id', $cod, \PDO::PARAM_INT); 
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage();
        }
    }
    
    public function BuscarTodosCategoriaPaginacao($cod, $inicio, $limit, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_noticia WHERE categoria_id = :categoria_id AND cadastro <= NOW() AND ativo = 's' AND linguagem_id = :linguagem_id ORDER BY cadastro DESC, noticia_id LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->bindParam(':categoria_id', $cod, \PDO::PARAM_INT); 
            
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage();
        }
    }
    
    public function TotalBuscaCategoria($cod, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_noticia WHERE titulo <> '' AND categoria_id = :categoria_id AND cadastro <= NOW() AND ativo = 's' AND linguagem_id = :linguagem_id ORDER BY titulo";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindParam(':categoria_id', $cod, \PDO::PARAM_INT); 
            $p_sql->bindParam(":linguagem_id", $linguagem, \PDO::PARAM_INT);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.". $titulo;
        }
    }
    
    public function MostrarData() {
        try {
            $sql = "SELECT DISTINCT DATE_FORMAT(cadastro, '%M %Y') as cadastro, DATE_FORMAT(cadastro, '%m') as mes, DATE_FORMAT(cadastro, '%Y') as ano FROM tbl_noticia GROUP BY cadastro ORDER BY tbl_noticia.cadastro DESC;";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            
            return $p_sql->fetchAll();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.". $titulo;
        }
    }

    public function CheckedAtuacao($cod, $atua){
        try {
            $sql = "SELECT area_id FROM tbl_noticia_areas WHERE area_id = :area_id AND noticia_id = :noticia_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":area_id", $atua);
            $p_sql->bindValue(":noticia_id", $cod);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function CheckedAdv($cod, $adv){
        try {
            $sql = "SELECT advogado_id FROM tbl_noticia_adv WHERE advogado_id = :advogado_id AND noticia_id = :noticia_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":advogado_id", $adv);
            $p_sql->bindValue(":noticia_id", $cod);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function DeletarAtuacao($cod) {
        try {
            $sql = "DELETE FROM tbl_noticia_areas WHERE noticia_id = :noticia_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":noticia_id", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function DeletarAdv($cod) {
        try {
            $sql = "DELETE FROM tbl_noticia_adv WHERE noticia_id = :noticia_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":noticia_id", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }    

    public function InserirAtuacao($cod, $atuacao) {
        try {
            $sql = "INSERT INTO tbl_noticia_areas (area_id, noticia_id) 
                                       VALUES (:area_id, :noticia_id)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(":area_id", $atuacao);
            $p_sql->bindValue(":noticia_id", $cod);

            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage()." <br>";
        }
    }

    public function InserirAdv($cod, $adv) {
        try {
            $sql = "INSERT INTO tbl_noticia_adv (advogado_id, noticia_id) 
                                       VALUES (:advogado_id, :noticia_id)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $p_sql->bindValue(":advogado_id", $adv);
            $p_sql->bindValue(":noticia_id", $cod);

            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage()." <br>";
        }
    }    
   

    
    public function DeletarFoto($cod,$img) {
        try {
            $sql = "UPDATE tbl_noticia SET imagem = '' WHERE noticia_id = :noticia_id ";

            
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":noticia_id", $cod);            

            return $p_sql->execute();

            @unlink(URL_CAMINHO_IMG.'/noticias/'.$img); 

        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.. ".$e->getMessage()." <br>";
        }
    }


    private function populaObj($row) {
        $pojo = new Noticia();
        $pojo->setId($row['noticia_id']);
        $pojo->setTitulo($row['titulo']);
        $pojo->setTexto($row['texto']);
        $pojo->setImagem($row['imagem']);
        $pojo->setResumo($row['resumo']);
        $pojo->setAtivo($row['ativo']);
        $pojo->setAdvogado($row['advogado_id']);
        $pojo->setCategoria($row['categoria_id']);
        $pojo->setUltima_alteracao($row['ultima_alteracao']);
        $pojo->setCadastro($row['cadastro']);
        $pojo->setUsuario($row['usuario']);
        $pojo->setLinguagem($row['linguagem_id']);
        $pojo->setLink($row['link']);
        return $pojo;
    }
    
    private function setObj($p_sql, Noticia $obj){
        $p_sql->bindValue(":titulo", $obj->getTitulo());
        $p_sql->bindValue(":texto", $obj->getTexto());
        $p_sql->bindValue(":imagem", $obj->getImagem());
        $p_sql->bindValue(":resumo", $obj->getResumo());
        $p_sql->bindValue(":ativo", $obj->getAtivo());
        $p_sql->bindValue(":ultima_alteracao", $obj->getUltima_alteracao());
        $p_sql->bindValue(":categoria_id", $obj->getCategoria());
        $p_sql->bindValue(":advogado_id", $obj->getAdvogado());
        $p_sql->bindValue(":linguagem_id", $obj->getLinguagem());
        $p_sql->bindValue(":link", $obj->getLink());
        $p_sql->bindValue(":cadastro", $obj->getCadastro());
        $p_sql->bindValue(":noticia_id", $obj->getId());
        
        return $p_sql;
        
    }
    
    public function statusTable($tabela) {
        
        $sql = "SHOW TABLE STATUS LIKE '".$tabela."'";
        $p_sql = Conexao::getInstance()->prepare($sql);
        $p_sql->execute();
        
        return $p_sql->fetch(\PDO::FETCH_ASSOC);     
    }
}

