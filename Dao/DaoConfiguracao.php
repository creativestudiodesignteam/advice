<?php

namespace Dao;

use Dao\Classes\Configuracao;

class DaoConfiguracao {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoConfiguracao();
        }
        return self::$instance;
    }

    public function Inserir(Configuracao $obj) {
        try {
            $sql = "INSERT INTO tbl_config (telefone, smtp, email, email_envio, senha_envio, analytics) 
                                      VALUES (:telefone, :smtp, :email, :email_envio, :senha_envio, :analytics)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            
            $p_sql->execute();


            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Editar(Configuracao $obj) {
        try {
            $sql = "UPDATE tbl_config SET telefone = :telefone, smtp = :smtp, email_envio = :email_envio, email = :email, "
                                   . "analytics = :analytics, senha_envio = :senha_envio"
                                   . " WHERE config_id = :config_id";
            

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            $p_sql->bindValue(":config_id", $obj->getId());

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "DELETE FROM tbl_config WHERE config_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_config WHERE config_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }
    
    
    private function populaObj($row) {
        $pojo = new Configuracao();
        $pojo->setId($row['config_id']);
        $pojo->setSmtp($row['smtp']);
        $pojo->setEmail($row['email']);
        $pojo->setEmailEnvio($row['email_envio']);
        $pojo->setAnalytics($row['analytics']);
        $pojo->setSenhaEnvio($row['senha_envio']);
        $pojo->setTelefone($row['telefone']);
        return $pojo;
    }
    
    private function setObj($p_sql, Configuracao $obj) {
        $p_sql->bindValue(":telefone", $obj->getTelefone());
        $p_sql->bindValue(":smtp", $obj->getSmtp());
        $p_sql->bindValue(":email", $obj->getEmail());
        $p_sql->bindValue(":email_envio", $obj->getEmailEnvio());
        $p_sql->bindValue(":senha_envio", $obj->getSenhaEnvio());
        $p_sql->bindValue(":analytics", $obj->getAnalytics());
        
        return $p_sql;
    }

}

