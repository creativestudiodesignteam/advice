<?php

namespace Dao;

use Dao\Classes\CategoriaProd;

class DaoCategoriaProd {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoCategoriaProd();
        }
        return self::$instance;
    }

    public function Inserir(CategoriaProd $obj) {
        try {
            $sql = "INSERT INTO categorias_prod (categoria_prod, ativo, linguagem_id, cod_categoria_prod) 
                                       VALUES (:categoria_prod, :ativo, :linguagem_id, :cod_categoria_prod)";

            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $this->setObj($p_sql, $obj);
            
            $p_sql->execute();
            
            return Conexao::getInstance()->lastInsertId();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function InserirCatXCatProd($cat, $cProd) {
        try {
            $sql = "INSERT INTO categoria_prodxcategorias (cod_categoria, cod_categoria_prod) 
                                                   VALUES (:cod_categoria, :cod_categoria_prod)";

            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $p_sql->bindValue(":cod_categoria", $cat);
            $p_sql->bindValue(":cod_categoria_prod", $cProd);
            
            $p_sql->execute();
            
            return Conexao::getInstance()->lastInsertId();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Editar(CategoriaProd $obj) {
        try {
            $sql = "UPDATE categorias_prod SET categoria_prod = :categoria_prod, ativo = :ativo"
                                   . " WHERE cod_categoria_prod = :cod_categoria_prod AND linguagem_id = :linguagem_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            
            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";            
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "UPDATE categorias_prod SET ativo = :ativo WHERE cod_categoria_prod = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", 'd');
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function DeletarCatXCatProd($cod) {
        try {
            $sql = "DELETE FROM categoria_prodxcategorias WHERE cod_categoria_prod = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM categorias_prod WHERE cod_categoria_prod = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarTodosCategoria($linguagem, $cat) {
        try {
            $sql = "SELECT categorias_prod.* 
            FROM categorias_prod
      INNER JOIN categoria_prodxcategorias 
              ON categorias_prod.cod_categoria_prod = categoria_prodxcategorias.cod_categoria_prod
           WHERE ativo = :ativo
             AND cod_categoria = :cod_categoria  
             AND linguagem_id = :linguagem_id 
        order by categoria_prod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "s");
            $p_sql->bindValue(":cod_categoria", $cat);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }
    
    public function BuscarPorCODLing($cod, $linguagem) {
        try {
            $sql = "SELECT * FROM categorias_prod WHERE cod_categoria_prod = :cod AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();     
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }
    
    public function BuscarTodos($linguagem, $ativo) {
        try {
            $sql = "SELECT * FROM categorias_prod WHERE ativo <> :ativo AND ativo <> :ativo2 AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function Total($linguagem, $ativo) {
        try {
            $sql = "SELECT * FROM categorias_prod WHERE ativo <> :ativo AND ativo <> :ativo2 AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodosPaginacao($inicio, $limit, $linguagem, $ativo) {
        try {
            $sql = "SELECT * FROM categorias_prod WHERE ativo <> :ativo AND ativo <> :ativo2 AND linguagem_id = :linguagem_id ORDER BY categoria LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage();
        }
    }

    public function TotalBuscaTitulo($categoria, $linguagem) {
        try {
            $sql = "SELECT * FROM categorias_prod WHERE categoria LIKE :categoria WHERE linguagem_id = :linguagem_id ORDER BY categoria";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(':categoria', "%".$categoria."%", \PDO::PARAM_STR);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function CheckedCategoria($cat, $catProd){
        try {
            $sql = "SELECT cod_categoria FROM categoria_prodxcategorias WHERE cod_categoria = :cod_categoria AND cod_categoria_prod = :cod_categoria_prod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod_categoria", $cat);
            $p_sql->bindValue(":cod_categoria_prod", $catProd);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    
    private function populaObj($row) {
        $pojo = new CategoriaProd();
        $pojo->setId($row['cod_categoria_prod']);
        $pojo->setTitulo($row['categoria_prod']);
        $pojo->setAtivo($row['ativo']);
        $pojo->setLinguagem($row['linguagem_id']);
        return $pojo;
    }
    
    private function setObj($p_sql, CategoriaProd $obj) {
        
        $p_sql->bindValue(":categoria_prod", $obj->getTitulo());
        $p_sql->bindValue(":ativo", $obj->getAtivo());
        $p_sql->bindValue(":linguagem_id", $obj->getLinguagem());
        $p_sql->bindValue(":cod_categoria_prod", $obj->getId());
            
        return $p_sql;     
    }
    
    public function statusTable($tabela) {
        
        $sql = "SHOW TABLE STATUS LIKE '".$tabela."'";
        $p_sql = Conexao::getInstance()->prepare($sql);
        $p_sql->execute();
        
        return $p_sql->fetch(\PDO::FETCH_ASSOC);     
    }

}

