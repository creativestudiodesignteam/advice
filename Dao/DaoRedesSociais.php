<?php

namespace Dao;

use Dao\Classes\RedesSociais;

class DaoRedesSociais {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoRedesSociais();
        }
        return self::$instance;
    }

    public function Inserir(RedesSociais $obj) {
        try {
            $sql = "INSERT INTO tbl_redes_sociais (titulo, link, icone, ativo) 
                                           VALUES (:titulo, :link, :icone, :ativo)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql,$obj);


            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Editar(RedesSociais $obj) {
        try {
            $sql = "UPDATE tbl_redes_sociais SET titulo = :titulo, link = :link, icone = :icone, ativo = :ativo"
                                   . " WHERE rs_id = :rs_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql,$obj);
            $p_sql->bindValue(":rs_id", $obj->getId());

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Deletar() {
        try {
            $sql = "DELETE FROM tbl_redes_sociais";
            $p_sql = Conexao::getInstance()->prepare($sql);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_redes_sociais WHERE rs_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodos() {
        try {
            $sql = "SELECT * FROM tbl_redes_sociais";
            $p_sql = Conexao::getInstance()->prepare($sql);
           
            $p_sql->execute();
            $obj = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $obj[] = $this->populaObj($row);
            }
                return $obj;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    
    private function populaObj($row) {
        $pojo = new RedesSociais();
        $pojo->setId($row['rs_id']);
        $pojo->setTitulo($row['titulo']);
        $pojo->setLink($row['link']);
        $pojo->setIcone($row['icone']);
        $pojo->setAtivo($row['ativo']);
        return $pojo;
    }
    
    private function setObj($p_sql, RedesSociais $obj){
        $p_sql->bindValue(":titulo", $obj->getTitulo());
        $p_sql->bindValue(":link", $obj->getLink());
        $p_sql->bindValue(":icone", $obj->getIcone());
        $p_sql->bindValue(":ativo", $obj->getAtivo());
        
        return $p_sql;
        
    }

}

