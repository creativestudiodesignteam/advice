<?php

namespace Dao;

use Dao\Classes\InstitucionalConteudo;

class DaoInstitucionalConteudo {

    public static $instance;



    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoInstitucionalConteudo();
        }
        return self::$instance;
    }

    public function Inserir(InstitucionalConteudo $obj) {
        try {
            $sql = "INSERT INTO tbl_institucional_conteudo (texto, titulo, ativo, icone, ordem, linguagem_id, id_institucional)
                                           VALUES (:texto, :titulo, :ativo, :icone, :ordem, :linguagem_id, :id_institucional)";

            $p_sql = Conexao::getInstance()->prepare($sql);



            $this->setObj($p_sql, $obj);
            $p_sql->execute();

            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }

    public function Editar(InstitucionalConteudo $obj) {
        try {
            $sql = "UPDATE tbl_institucional_conteudo SET texto = :texto, titulo = :titulo, ativo = :ativo, icone = :icone,
                                         ordem = :ordem, id_institucional: id_institucional WHERE id_conteudo = :id_conteudo AND linguagem_id = :linguagem_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            $p_sql->bindValue(":id_conteudo", $obj->getId());

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "UPDATE tbl_institucional_conteudo SET ativo = :ativo WHERE id_conteudo = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", 'q');
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_institucional_conteudo WHERE id_conteudo = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            $institucionais = [];
            while ($linha = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $institucionais[] = $this->populaObj($linha);
            }

                return $institucionais;
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCODLing($cod, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_institucional_conteudo WHERE id_conteudo = :cod AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }

    public function BuscarTodos($a, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_institucional_conteudo WHERE linguagem_id = :linguagem_id AND ativo <> :ativo AND ativo <> :ativo2";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", "q");
            $p_sql->bindValue(":ativo2", $a);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();
            $institucionais = [];
            while ($linha = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $institucionais[] = $this->populaObj($linha);
            }

                return $institucionais;

        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarTodosOrdemInst($a, $linguagem, $cod) {
        try {
            $sql = "SELECT * FROM tbl_institucional_conteudo WHERE linguagem_id = :linguagem_id AND ativo <> :ativo AND ativo <> :ativo2 AND id_institucional = :id_institucional ORDER BY ordem";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", "q");
            $p_sql->bindValue(":ativo2", $a);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":id_institucional", $cod);
            $p_sql->execute();
            $institucionais = [];
            while ($linha = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $institucionais[] = $this->populaObj($linha);
            }

                return $institucionais;

        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }    

    public function BuscarHome($linguagem) {
        try {
            $sql = "SELECT * FROM tbl_institucional_conteudo WHERE linguagem_id = :linguagem_id AND ativo = 's' AND destaque = 's'";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();
            $institucionais = [];
            while ($linha = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $institucionais[] = $this->populaObj($linha);
            }

                return $institucionais;

        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    private function populaObj($row) {
        $pojo = new InstitucionalConteudo();
        $pojo->setId($row['id_conteudo']);
        $pojo->setId_institucional($row['id_institucional']);
        $pojo->setTitulo($row['titulo']);
        $pojo->setTexto($row['texto']);
        $pojo->setAtivo($row['ativo']);
        $pojo->setIcone($row['icone']);
        $pojo->setOrdem($row['ordem']);
        $pojo->setLinguagem($row['linguagem_id']);
        return $pojo;
    }

    private function setObj($p_sql, InstitucionalConteudo $obj){
        $p_sql->bindValue(":texto", $obj->getTexto());
        $p_sql->bindValue(":titulo", $obj->getTitulo());
        $p_sql->bindValue(":ativo", $obj->getAtivo());
        $p_sql->bindValue(":icone", $obj->getIcone());
        $p_sql->bindValue(":ordem", $obj->getOrdem());
        $p_sql->bindValue(":linguagem_id", $obj->getLinguagem());
        $p_sql->bindValue(":id_conteudo", $obj->getId());
        $p_sql->bindValue(":id_institucional", $obj->getId_institucional());

        return $p_sql;

    }

    public function statusTable($tabela) {

        $sql = "SHOW TABLE STATUS LIKE '".$tabela."'";
        $p_sql = Conexao::getInstance()->prepare($sql);
        $p_sql->execute();

        return $p_sql->fetch(\PDO::FETCH_ASSOC);
    }

}
