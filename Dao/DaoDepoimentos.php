<?php

namespace Dao;

use Dao\Classes\Depoimentos;

class DaoDepoimentos {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoDepoimentos();
        }
        return self::$instance;
    }

    public function Inserir(Depoimentos $obj) {
        try {
            $sql = "INSERT INTO tbl_depoimentos (titulo, ativo, texto, data, depoimento_id, linguagem_id) 
                                     VALUES (:titulo, :ativo, :texto, :data, :depoimento_id, :linguagem_id)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);

            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage()." <br>";
        }
    }

    public function Editar(Depoimentos $obj) {
        try {
            $sql = "UPDATE tbl_depoimentos SET titulo = :titulo, ativo = :ativo, texto = :texto, data = :data "
                                 . " WHERE depoimento_id = :depoimento_id AND linguagem_id = :linguagem_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage()." <br>";
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "DELETE FROM tbl_depoimentos WHERE depoimento_id = :depoimento_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":depoimento_id", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_depoimentos WHERE depoimento_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();     
            $objs = [];       
            while ($linha = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($linha);
            }

                return $objs;
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }

    public function BuscarPorCODLing($cod, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_depoimentos WHERE depoimento_id = :cod AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();     
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }
   
    
    public function BuscarTodos($ativo, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_depoimentos WHERE ativo <> :ativo AND linguagem_id = :linguagem_id ORDER BY data DESC";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", $ativo);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();
            $obj = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $obj[] = $this->populaObj($row);
            }
                return $obj;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage();;
        }
    }
    
    public function Total($ativo, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_depoimentos WHERE ativo <> :ativo AND linguagem_id = :linguagem_id ORDER BY data DESC";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", $ativo);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodosPaginacao($inicio, $limit, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_depoimentos where ativo = 's' AND linguagem_id = :linguagem_id ORDER BY data DESC LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->execute();
            $obj = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $obj[] = $this->populaObj($row);
            }
                return $obj;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage();
        }
    }
    
    private function populaObj($row) {
        $pojo = new Depoimentos();
        
        $pojo->setId($row['depoimento_id']);
        $pojo->setTitulo($row['titulo']);
        $pojo->setAtivo($row['ativo']);
        $pojo->setTexto($row['texto']);
        $pojo->setData($row['data']);
        $pojo->setLinguagem($row['linguagem_id']);
        
        return $pojo;
    }
    
    private function setObj($p_sql, Depoimentos $obj){
        $p_sql->bindValue(":titulo", $obj->getTitulo());
        $p_sql->bindValue(":ativo", $obj->getAtivo());
        $p_sql->bindValue(":texto", $obj->getTexto());
        $p_sql->bindValue(":data", $obj->getData());
        $p_sql->bindValue(":linguagem_id", $obj->getLinguagem());
        $p_sql->bindValue(":depoimento_id", $obj->getId());
        
        return $p_sql;
        
    }
    
    public function statusTable($tabela) {
        
        $sql = "SHOW TABLE STATUS LIKE '".$tabela."'";
        $p_sql = Conexao::getInstance()->prepare($sql);
        $p_sql->execute();
        
        return $p_sql->fetch(\PDO::FETCH_ASSOC);     
    }

}

