<?php

namespace Dao;

use Dao\Classes\Newsletter;

class DaoNewsletter {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoNewsletter();
        }
        return self::$instance;
    }

    public function Inserir(Newsletter $obj) {
        try {
            $sql = "INSERT INTO tbl_newsletter (nome, email, data_cadastro, telefone, empresa) 
                                        VALUES (:nome, :email, :data_cadastro, :telefone, :empresa)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            $p_sql->bindValue(":data_cadastro", $obj->getDataCadastro());


            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Editar(Newsletter $obj) {
        try {
            $sql = "UPDATE tbl_newsletter SET nome = :nome, email = :email, empresa = :empresa, telefone = :telefone WHERE newsletter_id = :newsletter_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            $p_sql->bindValue(":newsletter_id", $obj->getId());

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage()." ".$obj->getAtivo();
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "DELETE FROM tbl_newsletter WHERE newsletter_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_newsletter WHERE newsletter_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarPorEmail($email) {
        try {
            $sql = "SELECT * FROM tbl_newsletter WHERE email = :email";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":email", $email);
            $p_sql->execute();
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodos() {
        try {
            $sql = "SELECT * FROM tbl_newsletter";
            $p_sql = Conexao::getInstance()->prepare($sql);
           
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function Total() {
        try {
            $sql = "SELECT * FROM tbl_newsletter";
            $p_sql = Conexao::getInstance()->prepare($sql);
           
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodosPaginacao($inicio, $limit) {
        try {
            $sql = "SELECT * FROM tbl_newsletter LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
           
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage();
        }
    }
    
    private function populaObj($row) {
        $pojo = new Newsletter();
        $pojo->setId($row['newsletter_id']);
        $pojo->setNome($row['nome']);
        $pojo->setEmail($row['email']);
        $pojo->setDataCadastro($row['data_cadastro']);
        $pojo->setTelefone($row['telefone']);
        $pojo->setEmpresa($row['empresa']);
        
        return $pojo;
    }
    
    private function setObj($p_sql, Newsletter $obj){
        $p_sql->bindValue(":nome", $obj->getNome());
        $p_sql->bindValue(":email", $obj->getEmail());
        $p_sql->bindValue(":empresa", $obj->getEmpresa());
        $p_sql->bindValue(":telefone", $obj->getTelefone());
        
        return $p_sql;
        
    }
}

