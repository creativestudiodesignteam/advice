<?php

namespace Dao;

use Dao\Classes\Site;

class DaoSite {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoSite();
        }
        return self::$instance;
    }

    public function Inserir(Site $obj) {
        try {
            $sql = "INSERT INTO tbl_site (titulo_site, copyright, atendimento, linguagem_id, site_id) 
                                  VALUES (:titulo_site, :copyright, :atendimento, :linguagem_id, :site_id)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);


            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Editar(Site $obj) {
        try {
            $sql = "UPDATE tbl_site SET titulo_site = :titulo_site, copyright = :copyright, atendimento = :atendimento, linguagem_id = :linguagem_id"
                                   . " WHERE site_id = :site_id AND linguagem_id = :linguagem_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "DELETE FROM tbl_site WHERE site_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_site WHERE site_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            $obj = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $obj[] = $this->populaObj($row);
            }
                return $obj;
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarPorCODLing($cod, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_site WHERE site_id = :cod AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();     
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }
    
    public function BuscarTodos($linguagem) {
        try {
            $sql = "SELECT * FROM tbl_site WHERE linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();
            $obj = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $obj[] = $this->populaObj($row);
            }
                return $obj;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    
    private function populaObj($row) {
        $pojo = new Site();
        $pojo->setId($row['site_id']);
        $pojo->setTituloSite($row['titulo_site']);
        $pojo->setCopyright($row['copyright']);
        $pojo->setAtendimento($row['atendimento']);
        $pojo->setLinguagem($row['linguagem_id']);
        return $pojo;
    }
    
    private function setObj($p_sql, Site $obj){
        $p_sql->bindValue(":titulo_site", $obj->getTituloSite());
        $p_sql->bindValue(":copyright", $obj->getCopyright());
        $p_sql->bindValue(":atendimento", $obj->getAtendimento());
        $p_sql->bindValue(":linguagem_id", $obj->getLinguagem());
        $p_sql->bindValue(":site_id", $obj->getId());
        
        return $p_sql;
    }
    

}

