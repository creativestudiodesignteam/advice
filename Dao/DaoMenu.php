<?php

namespace Dao;

use Dao\Classes\Menu;

class DaoMenu {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoMenu();
        }
        return self::$instance;
    }

    public function Inserir(Menu $obj) {
        try {
            $sql = "INSERT INTO tbl_menu (titulo, ativo, linguagem_id, menu_id, sub_menu, local, link, ordem, id_institucional) 
                                       VALUES (:titulo, :ativo, :linguagem_id, :menu_id, :sub_menu, :local, :link, :ordem, :id_institucional)";

            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $this->setObj($p_sql, $obj);
            
            $p_sql->execute();
            
            return Conexao::getInstance()->lastInsertId();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Editar(Menu $obj) {
        try {
            $sql = "UPDATE tbl_menu SET titulo = :titulo, ativo = :ativo, sub_menu = :sub_menu, local = :local, link = :link, ordem = :ordem, id_institucional = :id_institucional"
                                   . " WHERE menu_id = :menu_id AND linguagem_id = :linguagem_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            
            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";            
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "UPDATE tbl_menu SET ativo = :ativo WHERE menu_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", 'd');
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_menu WHERE menu_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarPorCODLing($cod, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_menu WHERE menu_id = :cod AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();     
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }
    
    public function BuscarTodos($linguagem, $ativo) {
        try {
            $sql = "SELECT * FROM tbl_menu WHERE ativo <> :ativo AND ativo <> :ativo2 AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Total($linguagem) {
        try {
            $sql = "SELECT * FROM tbl_menu WHERE ativo <> :ativo AND linguagem_id = :linguagem_id AND sub_menu = ''";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarTodosSubMenu($linguagem, $ativo, $menu) {
        try {
            $sql = "SELECT * FROM tbl_menu WHERE ativo = :ativo2 AND linguagem_id = :linguagem_id AND sub_menu = :sub_menu ORDER BY ordem";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->bindValue(":sub_menu", $menu);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function TotalSubMenu($linguagem, $ativo, $menu) {
        try {
            $sql = "SELECT * FROM tbl_menu WHERE ativo = :ativo2 AND linguagem_id = :linguagem_id AND sub_menu = :sub_menu";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->bindValue(":sub_menu", $menu);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodosPaginacao($inicio, $limit, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_menu WHERE linguagem_id = :linguagem_id ORDER BY menu LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage();
        }
    }

    public function BuscarMenuInstitucional($id_institucional) {
        try {
            $sql = "SELECT * FROM tbl_menu WHERE id_institucional = :id_institucional ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":id_institucional", $id_institucional);
            $p_sql->execute();     
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }

    public function EditarMenuInstitucional($id_institucional, $menu_id) {
        try {
            $sql = "UPDATE tbl_menu SET id_institucional = :id_institucional"
                                   . " WHERE menu_id = :menu_id ";

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":id_institucional", $id_institucional);
            $p_sql->bindValue(":menu_id", $menu_id);           
            
            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";            
        }
    }
        
    private function populaObj($row) {
        $pojo = new Menu();
        $pojo->setId($row['menu_id']);
        $pojo->setTitulo($row['titulo']);
        $pojo->setAtivo($row['ativo']);
        $pojo->setLinguagem($row['linguagem_id']);
        $pojo->setLink($row['link']);
        $pojo->setSubMenu($row['sub_menu']);
        $pojo->setLocal($row['local']);
        $pojo->setLink($row['link']);
        $pojo->setOrdem($row['ordem']);
        $pojo->setId_institucional($row['id_institucional']);
        $pojo->setClasse($row['class']);
        return $pojo;
    }
    
    private function setObj($p_sql, Menu $obj) {
        
        $p_sql->bindValue(":titulo", $obj->getTitulo());
        $p_sql->bindValue(":ativo", $obj->getAtivo());
        $p_sql->bindValue(":linguagem_id", $obj->getLinguagem());
        $p_sql->bindValue(":menu_id", $obj->getId());
        $p_sql->bindValue(":sub_menu", $obj->getSubMenu());
        $p_sql->bindValue(":local", $obj->getLocal());
        $p_sql->bindValue(":link", $obj->getLink());
        $p_sql->bindValue(":ordem", $obj->getOrdem());
        $p_sql->bindValue(":id_institucional", $obj->getId_institucional());
        $p_sql->bindValue(":classe", $obj->getClasse());
            
        return $p_sql;     
    }
    
    public function statusTable($tabela) {
        
        $sql = "SHOW TABLE STATUS LIKE '".$tabela."'";
        $p_sql = Conexao::getInstance()->prepare($sql);
        $p_sql->execute();
        
        return $p_sql->fetch(\PDO::FETCH_ASSOC);     
    }

}

