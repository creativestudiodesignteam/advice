<?php

namespace Dao;

use Dao\Classes\Estado;

class DaoEstado {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoEstado();
        }
        return self::$instance;
    }

    public function Inserir(Estado $obj) {
        try {
            $sql = "INSERT INTO tbl_estado (descricao, sigla, regiao_id, estado_id) 
                                       VALUES (:descricao, :sigla, :regiao_id, :estado_id)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            
            $p_sql->execute();
            
            return Conexao::getInstance()->lastInsertId();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Editar(Estado $obj) {
        try {
            $sql = "UPDATE tbl_estado SET descricao = :descricao, sigla = :sigla, regiao_id = :regiao_id"
                                   . " WHERE estado_id = :estado_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            

            
            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";            
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "UPDATE tbl_estado SET sigla = :sigla WHERE estado_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":sigla", 'd');
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_estado WHERE estado_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            $p_sql->execute();     
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
                return $objs;
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarPorRegiao($regiao) {
        try {
            $sql = "SELECT * FROM tbl_estado 
INNER JOIN tbl_endereco 
ON tbl_endereco.estado_id = tbl_estado.estado_id
INNER JOIN tbl_projeto 
ON tbl_projeto.endereco_id  = tbl_endereco.endereco_id 
WHERE regiao_id = :regiao_id AND tbl_projeto.linguagem_id =1 GROUP BY tbl_estado.estado_id ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":regiao_id", $regiao);
            $p_sql->execute();     
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }

    public function TotalPorRegiao($regiao) {
        try {
            $sql = "SELECT * FROM tbl_estado 
INNER JOIN tbl_endereco 
ON tbl_endereco.estado_id = tbl_estado.estado_id
INNER JOIN tbl_projeto 
ON tbl_projeto.endereco_id  = tbl_endereco.endereco_id 
WHERE regiao_id = :regiao_id AND tbl_projeto.linguagem_id =1 ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":regiao_id", $regiao);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodos() {
        try {
            $sql = "SELECT * FROM tbl_estado ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarRegiao() {
        try {
            $sql = "SELECT tbl_regiao.* FROM tbl_estado
            INNER JOIN tbl_endereco
            ON tbl_endereco.estado_id = tbl_estado.estado_id
            INNER JOIN tbl_regiao
            ON tbl_regiao.regiao_id = tbl_estado.regiao_id GROUP BY tbl_estado.regiao_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $row;
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function TotalRegiao() {
        try {
            $sql = "SELECT tbl_regiao.* FROM tbl_estado
            INNER JOIN tbl_endereco
            ON tbl_endereco.estado_id = tbl_estado.estado_id
            INNER JOIN tbl_regiao
            ON tbl_regiao.regiao_id = tbl_estado.regiao_id GROUP BY tbl_estado.regiao_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    private function populaObj($row) {
        $pojo = new Estado();
        $pojo->setId($row['estado_id']);
        $pojo->setDescricao($row['descricao']);
        $pojo->setSigla($row['sigla']);
        $pojo->setRegiao($row['regiao_id']);
        return $pojo;
    }
    
    private function setObj($p_sql, Estado $obj) {
        
        $p_sql->bindValue(":descricao", $obj->getDescricao());
        $p_sql->bindValue(":sigla", $obj->getSigla());
        $p_sql->bindValue(":regiao_id", $obj->getRegiao());
        $p_sql->bindValue(":estado_id", $obj->getId());            
        return $p_sql;     
    }
    
    public function statusTable($tabela) {
        
        $sql = "SHOW TABLE STATUS LIKE '".$tabela."'";
        $p_sql = Conexao::getInstance()->prepare($sql);
        $p_sql->execute();
        
        return $p_sql->fetch(\PDO::FETCH_ASSOC);     
    }

}

