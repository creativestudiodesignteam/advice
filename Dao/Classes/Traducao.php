<?php

namespace Dao\Classes;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Traducao
 *
 * @author Deep Ocean
 */
class Traducao {
    private $Id = null;
    private $texto1 = null;
    private $texto2 = null;
    private $texto3 = null;
    private $texto4 = null;
    private $texto5 = null;
    private $texto6 = null;
    private $texto7 = null;
    private $texto8 = null;
    private $pagina = null;
    private $linguagem = null;
    
    function getId() {
        return $this->Id;
    }

    function getTexto1() {
        return $this->texto1;
    }

    function getTexto2() {
        return $this->texto2;
    }

    function getTexto3() {
        return $this->texto3;
    }

    function getTexto4() {
        return $this->texto4;
    }

    function getTexto5() {
        return $this->texto5;
    }

    function getTexto6() {
        return $this->texto6;
    }
    function getTexto7() {
        return $this->texto7;
    }
    function getTexto8() {
        return $this->texto8;
    }

    function getPagina() {
        return $this->pagina;
    }

    function getLinguagem() {
        return $this->linguagem;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setTexto1($texto1) {
        $this->texto1 = $texto1;
    }

    function setTexto2($texto2) {
        $this->texto2 = $texto2;
    }

    function setTexto3($texto3) {
        $this->texto3 = $texto3;
    }

    function setTexto4($texto4) {
        $this->texto4 = $texto4;
    }

    function setTexto5($texto5) {
        $this->texto5 = $texto5;
    }

    function setTexto6($texto6) {
        $this->texto6 = $texto6;
    }
    function setTexto7($texto7) {
        $this->texto7 = $texto7;
    }
    function setTexto8($texto8) {
        $this->texto8 = $texto8;
    }

    function setPagina($pagina) {
        $this->pagina = $pagina;
    }

    function setLinguagem($linguagem) {
        $this->linguagem = $linguagem;
    }


}
