<?php

namespace Dao\Classes;

class Banner {
    private $Id = null;
    private $link = null;
    private $ativo = null; 
    private $imagem = null; 
    private $ordem = null;
    private $texto = null;
    private $texto2 = null;
    private $linguagem = null;
    private $titulo = null;

    
    function getId() {
        return $this->Id;
    }

    function getLink() {
        return $this->link;
    }

    function getAtivo() {
        return $this->ativo;
    }

    function getImagem() {
        return $this->imagem;
    }

    function getOrdem() {
        return $this->ordem;
    }

    function getTexto() {
        return $this->texto;
    }

    function getTexto2() {
        return $this->texto2;
    }

    function getLinguagem() {
        return $this->linguagem;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setLink($link) {
        $this->link = $link;
    }

    function setAtivo($ativo) {
        $this->ativo = $ativo;
    }

    function setImagem($imagem) {
        $this->imagem = $imagem;
    }

    function setOrdem($ordem) {
        $this->ordem = $ordem;
    }

    function setTexto($texto) {
        $this->texto = $texto;
    }

    function setTexto2($texto2) {
        $this->texto2 = $texto2;
    }

    function setLinguagem($linguagem) {
        $this->linguagem = $linguagem;
    }
    
    function getTitulo() {
        return $this->titulo;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }



    
}
