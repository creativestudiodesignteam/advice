<?php

namespace Dao\Classes;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Site
 *
 * @author Deep Ocean
 */
class Site {
    private $id = null;
    private $titulo_site = null;
    private $copyright = null;
    private $atendimento = null;
    private $linguagem = null;
    
    function getId() {
        return $this->id;
    }

    function getTituloSite() {
        return $this->titulo_site;
    }

    function getCopyright() {
        return $this->copyright;
    }

    function getAtendimento() {
        return $this->atendimento;
    }

    function getLinguagem() {
        return $this->linguagem;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTituloSite($titulo_site) {
        $this->titulo_site = $titulo_site;
    }

    function setCopyright($copyright) {
        $this->copyright = $copyright;
    }

    function setAtendimento($atendimento) {
        $this->atendimento = $atendimento;
    }

    function setLinguagem($linguagem) {
        $this->linguagem = $linguagem;
    }


    
}
