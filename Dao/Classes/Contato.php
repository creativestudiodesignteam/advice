<?php

namespace Dao\Classes;

class Contato {
    private $Id = null;
    private $nome = null;
    private $telefone = null;
    private $assunto = null; 
    private $mensagem = null; 
    private $email = null;
    private $data_contato = null;
    private $celular = null;
    private $view = null;
    private $tipo = null;
    private $empresa = null;

    function getId() {
        return $this->Id;
    }

    function getNome() {
        return $this->nome;
    }

    function getTelefone() {
        return $this->telefone;
    }

    function getAssunto() {
        return $this->assunto;
    }

    function getMensagem() {
        return $this->mensagem;
    }

    function getEmail() {
        return $this->email;
    }

    function getDataCadastro() {
        return $this->data_contato;
    }

    function getCelular() {
        return $this->celular;
    }

    function getView() {
        return $this->view;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setTelefone($telefone) {
        $this->telefone = $telefone;
    }

    function setAssunto($assunto) {
        $this->assunto = $assunto;
    }

    function setMensagem($mensagem) {
        $this->mensagem = $mensagem;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setDataCadastro($data_contato) {
        $this->data_contato = $data_contato;
    }

    function setCelular($celular) {
        $this->celular = $celular;
    }

    function setView($view) {
        $this->view = $view;
    }


    

    /**
     * Get the value of tipo
     */ 
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set the value of tipo
     *
     * @return  self
     */ 
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get the value of empresa
     */ 
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * Set the value of empresa
     *
     * @return  self
     */ 
    public function setEmpresa($empresa)
    {
        $this->empresa = $empresa;

        return $this;
    }
}