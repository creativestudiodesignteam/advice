<?php

namespace Dao\Classes;

class Estado {
    private $Id = null;
    private $sigla = null;
    private $descricao = null;
    private $regiao = null;

    function getId() {
        return $this->Id;
    }

    function getDescricao() {
        return $this->descricao;
    }

    function getSigla() {
        return $this->sigla;
    }

    function getRegiao() {
        return $this->regiao;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    function setSigla($sigla) {
        $this->sigla = $sigla;
    }

    function setRegiao($regiao) {
        $this->regiao = $regiao;
    }

}