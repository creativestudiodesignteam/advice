<?php

namespace Dao\Classes;

class DocumentoTipo{
    private $Id = null;
    private $titulo = null;
    private $ativo = null;
    private $sCodigo = null;

    /**
     * Get the value of Id
     */ 
    public function getId()
    {
        return $this->Id;
    }

    /**
     * Set the value of Id
     *
     * @return  self
     */ 
    public function setId($Id)
    {
        $this->Id = $Id;

        return $this;
    }

    /**
     * Get the value of titulo
     */ 
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set the value of titulo
     *
     * @return  self
     */ 
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get the value of ativo
     */ 
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set the value of ativo
     *
     * @return  self
     */ 
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get the value of sCodigo
     */ 
    public function getSCodigo()
    {
        return $this->sCodigo;
    }

    /**
     * Set the value of sCodigo
     *
     * @return  self
     */ 
    public function setSCodigo($sCodigo)
    {
        $this->sCodigo = $sCodigo;

        return $this;
    }
}

