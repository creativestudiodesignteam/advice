<?php

namespace Dao\Classes;

class Menu {
    private $titulo = null;
    private $Id = null;
    private $ativo = null;
    private $linguagem = null;
    private $link = null;
    private $sub_menu = null;
    private $local = null;
    private $ordem = null;
    private $id_institucional = null;
    private $classe = null;

    function getTitulo() {
        return $this->titulo;
    }

    function getId() {
        return $this->Id;
    }

    function getAtivo() {
        return $this->ativo;
    }

    function getLinguagem() {
        return $this->linguagem;
    }

    function getSubMenu() {
        return $this->sub_menu;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setAtivo($ativo) {
        $this->ativo = $ativo;
    }

    function setLinguagem($linguagem) {
        $this->linguagem = $linguagem;
    }

    function setSubMenu($sub_menu) {
        $this->sub_menu = $sub_menu;
    }
    
    function getLink() {
        return $this->link;
    }

    function setLink($link) {
        $this->link = $link;
    }





    /**
     * Get the value of local
     */ 
    public function getLocal()
    {
        return $this->local;
    }

    /**
     * Set the value of local
     *
     * @return  self
     */ 
    public function setLocal($local)
    {
        $this->local = $local;

        return $this;
    }

    /**
     * Get the value of ordem
     */ 
    public function getOrdem()
    {
        return $this->ordem;
    }

    /**
     * Set the value of ordem
     *
     * @return  self
     */ 
    public function setOrdem($ordem)
    {
        $this->ordem = $ordem;

        return $this;
    }

    /**
     * Get the value of id_institucional
     */ 
    public function getId_institucional()
    {
        return $this->id_institucional;
    }

    /**
     * Set the value of id_institucional
     *
     * @return  self
     */ 
    public function setId_institucional($id_institucional)
    {
        $this->id_institucional = $id_institucional;

        return $this;
    }

    /**
     * Get the value of classe
     */ 
    public function getClasse()
    {
        return $this->classe;
    }

    /**
     * Set the value of classe
     *
     * @return  self
     */ 
    public function setClasse($classe)
    {
        $this->classe = $classe;

        return $this;
    }
}
