<?php

namespace Dao\Classes;

class Idioma{
     private $titulo = null;
     private $Id = null;
     private $ativo = null;
     private $linguagem = null;
     
     function getTitulo() {
         return $this->titulo;
     }

     function getId() {
         return $this->Id;
     }

     function getAtivo() {
         return $this->ativo;
     }

     function getLinguagem() {
         return $this->linguagem;
     }

     function setTitulo($titulo) {
         $this->titulo = $titulo;
     }

     function setId($Id) {
         $this->Id = $Id;
     }

     function setAtivo($ativo) {
         $this->ativo = $ativo;
     }

     function setLinguagem($linguagem) {
         $this->linguagem = $linguagem;
     }



}

