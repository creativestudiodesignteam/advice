<?php

namespace Dao\Classes;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Image
 *
 * @author Deep Ocean
 */
class Image {
    private $Id = null; 
    private $imagem = null; 
    private $data = null;
    private $fk = null;
    private $tipo = null;
    
    function getId() {
        return $this->Id;
    }

    function getImagem() {
        return $this->imagem;
    }

    function getData() {
        return $this->data;
    }

    function getFk() {
        return $this->fk;
    }

    function getTipo() {
        return $this->tipo;
    }

    function setId($Id) {
        $this->Id = $Id;
    }

    function setImagem($imagem) {
        $this->imagem = $imagem;
    }

    function setData($data) {
        $this->data = $data;
    }

    function setFk($fk) {
        $this->fk = $fk;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }


}
