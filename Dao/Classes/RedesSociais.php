<?php

namespace Dao\Classes;

class RedesSociais {
    private $id = null;
    private $titulo = null;
    private $link = null;
    private $ativo = null; 
    private $icone = null; 

    function getId() {
        return $this->id;
    }

    function getTitulo() {
        return $this->titulo;
    }

    function getLink() {
        return $this->link;
    }

    function getAtivo() {
        return $this->ativo;
    }

    function getIcone() {
        return $this->icone;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    function setLink($link) {
        $this->link = $link;
    }

    function setAtivo($ativo) {
        $this->ativo = $ativo;
    }

    function setIcone($icone) {
        $this->icone = $icone;
    }


}
