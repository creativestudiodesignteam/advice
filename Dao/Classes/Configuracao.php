<?php

namespace Dao\Classes;

 class Configuracao{

     private $Id = null;
     private $email = null;
     private $smtp = null; 
     private $email_envio = null;
     private $senha_envio = null; 
     private $analytics = null;
     private $telefone = null;

     function getId() {
         return $this->Id;
     }

     function getEmail() {
         return $this->email;
     }

     function getSmtp() {
         return $this->smtp;
     }

     function getEmailEnvio() {
         return $this->email_envio;
     }

     function getSenhaEnvio() {
         return $this->senha_envio;
     }

     function getAnalytics() {
         return $this->analytics;
     }

     function getTelefone() {
         return $this->telefone;
     }

     function setId($Id) {
         $this->Id = $Id;
     }

     function setEmail($email) {
         $this->email = $email;
     }

     function setSmtp($smtp) {
         $this->smtp = $smtp;
     }

     function setEmailEnvio($email_envio) {
         $this->email_envio = $email_envio;
     }

     function setSenhaEnvio($senha_envio) {
         $this->senha_envio = $senha_envio;
     }

     function setAnalytics($analytics) {
         $this->analytics = $analytics;
     }

     function setTelefone($telefone) {
         $this->telefone = $telefone;
     }





}