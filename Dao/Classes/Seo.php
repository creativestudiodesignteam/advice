<?php

namespace Dao\Classes;

 class Seo{

    private $seo_id = null;
    private $keywords = null;
    private $meta_descricao = null;

    
    function getId() {
        return $this->seo_id;
    }

    function getKeywords() {
        return $this->keywords;
    }

    function getMetaDescricao() {
        return $this->meta_descricao;
    }

    function setId($seo_id) {
        $this->seo_id = $seo_id;
    }

    function setKeywords($keywords) {
        $this->keywords = $keywords;
    }

    function setMetaDescricao($meta_descricao) {
        $this->meta_descricao = $meta_descricao;
    }



}