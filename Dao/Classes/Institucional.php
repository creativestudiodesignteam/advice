<?php

namespace Dao\Classes;

 class Institucional{

     private $id = null;
     private $titulo = null;
     private $sub_titulo = null;
     private $ativo = null;
     private $imagem = null;
     private $texto = null;
     private $icone = null;
     private $ordem = null;
     private $linguagem = null;
     private $destaque = null;
     private $link = null;
     private $link_logo = null;

     function getId() {
         return $this->id;
     }

     function getTitulo() {
         return $this->titulo;
     }

     function getSub_titulo() {
         return $this->sub_titulo;
     }

     function getAtivo() {
         return $this->ativo;
     }

     function getImagem() {
         return $this->imagem;
     }

     function getTexto() {
         return $this->texto;
     }

     function getIcone() {
         return $this->icone;
     }

     function getOrdem() {
         return $this->ordem;
     }

     function getLinguagem() {
         return $this->linguagem;
     }

     function setId($id) {
         $this->id = $id;
     }

     function setTitulo($titulo) {
         $this->titulo = $titulo;
     }

     function setSub_titulo($sub_titulo) {
         $this->sub_titulo = $sub_titulo;
     }

     function setAtivo($ativo) {
         $this->ativo = $ativo;
     }

     function setImagem($imagem) {
         $this->imagem = $imagem;
     }

     function setTexto($texto) {
         $this->texto = $texto;
     }

     function setIcone($icone) {
         $this->icone = $icone;
     }

     function setOrdem($ordem) {
         $this->ordem = $ordem;
     }

     function setLinguagem($linguagem) {
         $this->linguagem = $linguagem;
     }

     function getDestaque() {
         return $this->destaque;
     }

     function setDestaque($destaque) {
         $this->destaque = $destaque;
     }




     /**
      * Get the value of link
      */
     public function getLink()
     {
          return $this->link;
     }

     /**
      * Set the value of link
      *
      * @return  self
      */
     public function setLink($link)
     {
          $this->link = $link;

          return $this;
     }

    /**
     * Get the value of Sub Titulo
     *
     * @return mixed
     */
    public function getSubTitulo()
    {
        return $this->sub_titulo;
    }

    /**
     * Set the value of Sub Titulo
     *
     * @param mixed sub_titulo
     *
     * @return self
     */
    public function setSubTitulo($sub_titulo)
    {
        $this->sub_titulo = $sub_titulo;

        return $this;
    }

    /**
     * Get the value of Link Logo
     *
     * @return mixed
     */
    public function getLinkLogo()
    {
        return $this->link_logo;
    }

    /**
     * Set the value of Link Logo
     *
     * @param mixed link_logo
     *
     * @return self
     */
    public function setLinkLogo($link_logo)
    {
        $this->link_logo = $link_logo;

        return $this;
    }

}
