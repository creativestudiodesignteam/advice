<?php

namespace Dao\Classes;

class UsuarioPaginas{

     private $id = null;
     private $pagina = null;
     private $funcao = null;
     private $nivel = null;
     private $ordem = null;   
     private $visivel = null;   
     private $icone = null;

     function getId() {
         return $this->id;
     }

     function getPagina() {
         return $this->pagina;
     }

     function getFuncao() {
         return $this->funcao;
     }

     function getNivel() {
         return $this->nivel;
     }

     function getOrdem() {
         return $this->ordem;
     }

     function getVisivel() {
         return $this->visivel;
     }

     function getIcone() {
         return $this->icone;
     }

     function setId($id) {
         $this->id = $id;
     }

     function setPagina($pagina) {
         $this->pagina = $pagina;
     }

     function setFuncao($funcao) {
         $this->funcao = $funcao;
     }

     function setNivel($nivel) {
         $this->nivel = $nivel;
     }

     function setOrdem($ordem) {
         $this->ordem = $ordem;
     }

     function setVisivel($visivel) {
         $this->visivel = $visivel;
     }

     function setIcone($icone) {
         $this->icone = $icone;
     }





}
