<?php

namespace Dao\Classes;

class CategoriaSub{
    private $Id = null;
    private $linguagem = null;
    private $cod_categoria = null;
    private $titulo = null;
    private $ordem = null;
    private $ativo = null;
    private $css = null;
    private $color = null;
    private $imagem = null;
    private $img2 = null;
    private $doc = null;


    /**
     * Get the value of Id
     */ 
    public function getId()
    {
        return $this->Id;
    }

    /**
     * Set the value of Id
     *
     * @return  self
     */ 
    public function setId($Id)
    {
        $this->Id = $Id;

        return $this;
    }

    /**
     * Get the value of linguagem
     */ 
    public function getLinguagem()
    {
        return $this->linguagem;
    }

    /**
     * Set the value of linguagem
     *
     * @return  self
     */ 
    public function setLinguagem($linguagem)
    {
        $this->linguagem = $linguagem;

        return $this;
    }

    /**
     * Get the value of cod_categoria
     */ 
    public function getCod_categoria()
    {
        return $this->cod_categoria;
    }

    /**
     * Set the value of cod_categoria
     *
     * @return  self
     */ 
    public function setCod_categoria($cod_categoria)
    {
        $this->cod_categoria = $cod_categoria;

        return $this;
    }

    /**
     * Get the value of titulo
     */ 
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set the value of titulo
     *
     * @return  self
     */ 
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get the value of ordem
     */ 
    public function getOrdem()
    {
        return $this->ordem;
    }

    /**
     * Set the value of ordem
     *
     * @return  self
     */ 
    public function setOrdem($ordem)
    {
        $this->ordem = $ordem;

        return $this;
    }

    /**
     * Get the value of ativo
     */ 
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set the value of ativo
     *
     * @return  self
     */ 
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get the value of css
     */ 
    public function getCss()
    {
        return $this->css;
    }

    /**
     * Set the value of css
     *
     * @return  self
     */ 
    public function setCss($css)
    {
        $this->css = $css;

        return $this;
    }

    /**
     * Get the value of color
     */ 
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set the value of color
     *
     * @return  self
     */ 
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get the value of imagem
     */ 
    public function getImagem()
    {
        return $this->imagem;
    }

    /**
     * Set the value of imagem
     *
     * @return  self
     */ 
    public function setImagem($imagem)
    {
        $this->imagem = $imagem;

        return $this;
    }

    /**
     * Get the value of img2
     */ 
    public function getImg2()
    {
        return $this->img2;
    }

    /**
     * Set the value of img2
     *
     * @return  self
     */ 
    public function setImg2($img2)
    {
        $this->img2 = $img2;

        return $this;
    }

    /**
     * Get the value of doc
     */ 
    public function getDoc()
    {
        return $this->doc;
    }

    /**
     * Set the value of doc
     *
     * @return  self
     */ 
    public function setDoc($doc)
    {
        $this->doc = $doc;

        return $this;
    }
}

