<?php

namespace Dao\Classes;

class Noticia{		

    private $id;
    private $titulo;
    private $texto;
    private $resumo;
    private $usuario;
    private $cadastro;
    private $ultima_alteracao;
    private $ativo;
    private $imagem;
    private $categoria;
    private $advogado;
    private $linguagem;
    private $link;
    
    function __construct() {
       $this->cadastro = date('Y-m-d');
    }

    
    function getId() {
        return $this->id;
    }

    function getTitulo() {
        return $this->titulo;
    }

    function getTexto() {
        return $this->texto;
    }

    function getResumo() {
        return $this->resumo;
    }

    function getUsuario() {
        return $this->usuario;
    }

    function getCadastro() {
        return $this->cadastro;
    }

    function getUltima_alteracao() {
        return $this->ultima_alteracao;
    }

    function getAtivo() {
        return $this->ativo;
    }

    function getImagem() {
        return $this->imagem;
    }

    function getCategoria() {
        return $this->categoria;
    }

    function getLinguagem() {
        return $this->linguagem;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    function setTexto($texto) {
        $this->texto = $texto;
    }

    function setResumo($resumo) {
        $this->resumo = $resumo;
    }

    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }

    function setCadastro($cadastro) {
        $this->cadastro = $cadastro;
    }

    function setUltima_alteracao($ultima_alteracao) {
        $this->ultima_alteracao = $ultima_alteracao;
    }

    function setAtivo($ativo) {
        $this->ativo = $ativo;
    }

    function setImagem($imagem) {
        $this->imagem = $imagem;
    }

    function setCategoria($categoria) {
        $this->categoria = $categoria;
    }

    function setLinguagem($linguagem) {
        $this->linguagem = $linguagem;
    }






    /**
     * Get the value of advogado
     */ 
    public function getAdvogado()
    {
        return $this->advogado;
    }

    /**
     * Set the value of advogado
     *
     * @return  self
     */ 
    public function setAdvogado($advogado)
    {
        $this->advogado = $advogado;

        return $this;
    }

    /**
     * Get the value of link
     */ 
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set the value of link
     *
     * @return  self
     */ 
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }
}

