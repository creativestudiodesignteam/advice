<?php

namespace Dao\Classes;

 class Endereco{

     private $Id = null;
     private $endereco = null;
     private $numero = null;
     private $cep = null;
     private $bairro = null; 
     private $cidade = null;
     private $estado = null; 
     private $telefone = null;
     private $celular = null;
     private $complemento = null; 
     private $email = null;
     private $unidade = null;
     private $latitude = null;
     private $longitude = null;
     private $imagem = null;

     function getId() {
         return $this->Id;
     }

     function getEndereco() {
         return $this->endereco;
     }

     function getNumero() {
         return $this->numero;
     }

     function getCep() {
         return $this->cep;
     }

     function getBairro() {
         return $this->bairro;
     }

     function getCidade() {
         return $this->cidade;
     }

     function getEstado() {
         return $this->estado;
     }

     function getTelefone() {
         return $this->telefone;
     }

     function getCelular() {
         return $this->celular;
     }

     function getComplemento() {
         return $this->complemento;
     }

     function getEmail() {
         return $this->email;
     }

     function getUnidade() {
         return $this->unidade;
     }

     function getLatitude() {
         return $this->latitude;
     }

     function getLongitude() {
         return $this->longitude;
     }

     function setId($Id) {
         $this->Id = $Id;
     }

     function setEndereco($endereco) {
         $this->endereco = $endereco;
     }

     function setNumero($numero) {
         $this->numero = $numero;
     }

     function setCep($cep) {
         $this->cep = $cep;
     }

     function setBairro($bairro) {
         $this->bairro = $bairro;
     }

     function setCidade($cidade) {
         $this->cidade = $cidade;
     }

     function setEstado($estado) {
         $this->estado = $estado;
     }

     function setTelefone($telefone) {
         $this->telefone = $telefone;
     }

     function setCelular($celular) {
         $this->celular = $celular;
     }

     function setComplemento($complemento) {
         $this->complemento = $complemento;
     }

     function setEmail($email) {
         $this->email = $email;
     }

     function setUnidade($unidade) {
         $this->unidade = $unidade;
     }

     function setLatitude($latitude) {
         $this->latitude = $latitude;
     }

     function setLongitude($longitude) {
         $this->longitude = $longitude;
     }







     /**
      * Get the value of imagem
      */ 
     public function getImagem()
     {
          return $this->imagem;
     }

     /**
      * Set the value of imagem
      *
      * @return  self
      */ 
     public function setImagem($imagem)
     {
          $this->imagem = $imagem;

          return $this;
     }
}