<?php

namespace Dao\Classes;

class Categoria{
    private $Id = null;
    private $titulo = null;
    private $email = null;
    private $css = null;
    private $color = null;
    private $ativo = null;
    private $imagem = null;
    private $linguagem = null;
    private $background = null;
    private $img_adicional_um = null;
    private $img_adicional_dois = null;
    private $img_adicional_tres = null;
    private $titulo_img_1 = null;
    private $titulo_img_2 = null;
    private $titulo_img_3 = null;
    private $titulo_img_4 = null;
    private $url_amigavel = null;


    /**
     * Get the value of Id
     */ 
    public function getId()
    {
        return $this->Id;
    }

    /**
     * Set the value of Id
     *
     * @return  self
     */ 
    public function setId($Id)
    {
        $this->Id = $Id;

        return $this;
    }

    /**
     * Get the value of titulo
     */ 
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set the value of titulo
     *
     * @return  self
     */ 
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get the value of ativo
     */ 
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set the value of ativo
     *
     * @return  self
     */ 
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get the value of imagem
     */ 
    public function getImagem()
    {
        return $this->imagem;
    }

    /**
     * Set the value of imagem
     *
     * @return  self
     */ 
    public function setImagem($imagem)
    {
        $this->imagem = $imagem;

        return $this;
    }

    /**
     * Get the value of linguagem
     */ 
    public function getLinguagem()
    {
        return $this->linguagem;
    }

    /**
     * Set the value of linguagem
     *
     * @return  self
     */ 
    public function setLinguagem($linguagem)
    {
        $this->linguagem = $linguagem;

        return $this;
    }

    /**
     * Get the value of css
     */ 
    public function getCss()
    {
        return $this->css;
    }

    /**
     * Set the value of css
     *
     * @return  self
     */ 
    public function setCss($css)
    {
        $this->css = $css;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of color
     */ 
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set the value of color
     *
     * @return  self
     */ 
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get the value of img_adicional_um
     */ 
    public function getImg_adicional_um()
    {
        return $this->img_adicional_um;
    }

    /**
     * Set the value of img_adicional_um
     *
     * @return  self
     */ 
    public function setImg_adicional_um($img_adicional_um)
    {
        $this->img_adicional_um = $img_adicional_um;

        return $this;
    }

    /**
     * Get the value of img_adicional_dois
     */ 
    public function getImg_adicional_dois()
    {
        return $this->img_adicional_dois;
    }

    /**
     * Set the value of img_adicional_dois
     *
     * @return  self
     */ 
    public function setImg_adicional_dois($img_adicional_dois)
    {
        $this->img_adicional_dois = $img_adicional_dois;

        return $this;
    }

    /**
     * Get the value of background
     */ 
    public function getBackground()
    {
        return $this->background;
    }

    /**
     * Set the value of background
     *
     * @return  self
     */ 
    public function setBackground($background)
    {
        $this->background = $background;

        return $this;
    }

    /**
     * Get the value of titulo_img_1
     */ 
    public function getTitulo_img_1()
    {
        return $this->titulo_img_1;
    }

    /**
     * Set the value of titulo_img_1
     *
     * @return  self
     */ 
    public function setTitulo_img_1($titulo_img_1)
    {
        $this->titulo_img_1 = $titulo_img_1;

        return $this;
    }

    /**
     * Get the value of titulo_img_2
     */ 
    public function getTitulo_img_2()
    {
        return $this->titulo_img_2;
    }

    /**
     * Set the value of titulo_img_2
     *
     * @return  self
     */ 
    public function setTitulo_img_2($titulo_img_2)
    {
        $this->titulo_img_2 = $titulo_img_2;

        return $this;
    }

    /**
     * Get the value of titulo_img_3
     */ 
    public function getTitulo_img_3()
    {
        return $this->titulo_img_3;
    }

    /**
     * Set the value of titulo_img_3
     *
     * @return  self
     */ 
    public function setTitulo_img_3($titulo_img_3)
    {
        $this->titulo_img_3 = $titulo_img_3;

        return $this;
    }

    /**
     * Get the value of img_adicional_tres
     */ 
    public function getImg_adicional_tres()
    {
        return $this->img_adicional_tres;
    }

    /**
     * Set the value of img_adicional_tres
     *
     * @return  self
     */ 
    public function setImg_adicional_tres($img_adicional_tres)
    {
        $this->img_adicional_tres = $img_adicional_tres;

        return $this;
    }

    /**
     * Get the value of titulo_img_4
     */ 
    public function getTitulo_img_4()
    {
        return $this->titulo_img_4;
    }

    /**
     * Set the value of titulo_img_4
     *
     * @return  self
     */ 
    public function setTitulo_img_4($titulo_img_4)
    {
        $this->titulo_img_4 = $titulo_img_4;

        return $this;
    }

    /**
     * Get the value of url_amigavel
     */ 
    public function getUrl_amigavel()
    {
        return $this->url_amigavel;
    }

    /**
     * Set the value of url_amigavel
     *
     * @return  self
     */ 
    public function setUrl_amigavel($url_amigavel)
    {
        $this->url_amigavel = $url_amigavel;

        return $this;
    }
}

