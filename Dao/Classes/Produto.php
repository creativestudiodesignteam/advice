<?php

namespace Dao\Classes;

class Produto {
    private $Id;
    private $cod_iqa;
    private $titulo;
    private $ativo;
    private $atualizados;
    private $linguagem;
    private $farmacopeias;
    private $categoria_id;

    /**
     * Get the value of Id
     */ 
    public function getId()
    {
        return $this->Id;
    }

    /**
     * Set the value of Id
     *
     * @return  self
     */ 
    public function setId($Id)
    {
        $this->Id = $Id;

        return $this;
    }

    /**
     * Get the value of cod_iqa
     */ 
    public function getCod_iqa()
    {
        return $this->cod_iqa;
    }

    /**
     * Set the value of cod_iqa
     *
     * @return  self
     */ 
    public function setCod_iqa($cod_iqa)
    {
        $this->cod_iqa = $cod_iqa;

        return $this;
    }

    /**
     * Get the value of titulo
     */ 
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set the value of titulo
     *
     * @return  self
     */ 
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get the value of ativo
     */ 
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set the value of ativo
     *
     * @return  self
     */ 
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get the value of atualizados
     */ 
    public function getAtualizados()
    {
        return $this->atualizados;
    }

    /**
     * Set the value of atualizados
     *
     * @return  self
     */ 
    public function setAtualizados($atualizados)
    {
        $this->atualizados = $atualizados;

        return $this;
    }

    /**
     * Get the value of linguagem
     */ 
    public function getLinguagem()
    {
        return $this->linguagem;
    }

    /**
     * Set the value of linguagem
     *
     * @return  self
     */ 
    public function setLinguagem($linguagem)
    {
        $this->linguagem = $linguagem;

        return $this;
    }

    /**
     * Get the value of Farmacopeias
     */
    public function getFarmacopeias()
    {
        return $this->farmacopeias;
    }

    /**
     * Set the value of Farmacopeias
     *
     * @return  self
     */
    public function setFarmacopeias( $farmacopeias)
    {
        $this->farmacopeias = $farmacopeias;

        return $this;
    }

    /**
     * Get the value of categoria_id
     */ 
    public function getCategoria_id()
    {
        return $this->categoria_id;
    }

    /**
     * Set the value of categoria_id
     *
     * @return  self
     */ 
    public function setCategoria_id($categoria_id)
    {
        $this->categoria_id = $categoria_id;

        return $this;
    }
}