<?php

namespace Dao\Classes;

class Parceiro {
    private $id = null;
    private $nome = null;
    private $texto = null;
    private $cod_tipo = null;
    private $site = null;
    private $imagem = null;
    private $ativo = null;
    
    function getId() {
        return $this->id;
    }

    function getNome() {
        return $this->nome;
    }

    function getTexto() {
        return $this->texto;
    }

    function getTipo() {
        return $this->cod_tipo;
    }

    function getSite() {
        return $this->site;
    }

    function getImagem() {
        return $this->imagem;
    }

    function getAtivo() {
        return $this->ativo;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNome($nome) {
        $this->nome = $nome;
    }

    function setTexto($texto) {
        $this->texto = $texto;
    }

    function setTipo($cod_tipo) {
        $this->cod_tipo = $cod_tipo;
    }

    function setSite($site) {
        $this->site = $site;
    }

    function setImagem($imagem) {
        $this->imagem = $imagem;
    }

    function setAtivo($ativo) {
        $this->ativo = $ativo;
    }



}
