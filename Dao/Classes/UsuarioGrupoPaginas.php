<?php

namespace Dao\Classes;

class UsuarioGrupoPaginas{

    private $pagina = null;
    private $grupo = null;
    private $id = null;
     
    function getPagina() {
        return $this->pagina;
    }

    function getGrupo() {
        return $this->grupo;
    }

    function setPagina($pagina) {
        $this->pagina = $pagina;
    }

    function setGrupo($grupo) {
        $this->grupo = $grupo;
    }

    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
    }


}

