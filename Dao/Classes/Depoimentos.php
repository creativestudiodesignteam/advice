<?php

namespace Dao\Classes;

class Depoimentos {
    private $id = null;
    private $titulo = null;
    private $ativo = null; 
    private $texto = null;
    private $data = null;
    private $linguagem = null;
    
    
    function getId() {
        return $this->id;
    }

    function getTitulo() {
        return $this->titulo;
    }

    function getAtivo() {
        return $this->ativo;
    }

    function getTexto() {
        return $this->texto;
    }

    function getData() {
        return $this->data;
    }

    function getLinguagem() {
        return $this->linguagem;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    function setAtivo($ativo) {
        $this->ativo = $ativo;
    }

    function setTexto($texto) {
        $this->texto = $texto;
    }

    function setData($data) {
        $this->data = $data;
    }

    function setLinguagem($linguagem) {
        $this->linguagem = $linguagem;
    }

}
