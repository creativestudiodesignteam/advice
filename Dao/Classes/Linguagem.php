<?php

namespace Dao\Classes;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Linguagem
 *
 * @author Deep Ocean
 */
class Linguagem {
    private $id = null;
    private $titulo = null;
    private $bandeira = null;
    private $ativo = null;
    private $prefix = null;
    
    function getId() {
        return $this->id;
    }

    function getTitulo() {
        return $this->titulo;
    }

    function getBandeira() {
        return $this->bandeira;
    }

    function getAtivo() {
        return $this->ativo;
    }
    function getPrefix() {
        return $this->prefix;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setTitulo($titulo) {
        $this->titulo = $titulo;
    }

    function setBandeira($bandeira) {
        $this->bandeira = $bandeira;
    }

    function setAtivo($ativo) {
        $this->ativo = $ativo;
    }

    function setPrefix($prefix) {
        $this->prefix = $prefix;
    }


}
