<?php

namespace Dao\Classes;

class Documento{
    private $Id = null;
    private $documento = null;
    private $dir = null;
    private $tipo = null;
    private $cod_iqa = null;
    private $ativo = null;
    private $tipoTitulo = null;


    /**
     * Get the value of Id
     */ 
    public function getId()
    {
        return $this->Id;
    }

    /**
     * Set the value of Id
     *
     * @return  self
     */ 
    public function setId($Id)
    {
        $this->Id = $Id;

        return $this;
    }

    /**
     * Get the value of documento
     */ 
    public function getDocumento()
    {
        return $this->documento;
    }

    /**
     * Set the value of documento
     *
     * @return  self
     */ 
    public function setDocumento($documento)
    {
        $this->documento = $documento;

        return $this;
    }

    /**
     * Get the value of dir
     */ 
    public function getDir()
    {
        return $this->dir;
    }

    /**
     * Set the value of dir
     *
     * @return  self
     */ 
    public function setDir($dir)
    {
        $this->dir = $dir;

        return $this;
    }

    /**
     * Get the value of tipo
     */ 
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set the value of tipo
     *
     * @return  self
     */ 
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get the value of cod_iqa
     */ 
    public function getCod_iqa()
    {
        return $this->cod_iqa;
    }

    /**
     * Set the value of cod_iqa
     *
     * @return  self
     */ 
    public function setCod_iqa($cod_iqa)
    {
        $this->cod_iqa = $cod_iqa;

        return $this;
    }

    /**
     * Get the value of ativo
     */ 
    public function getAtivo()
    {
        return $this->ativo;
    }

    /**
     * Set the value of ativo
     *
     * @return  self
     */ 
    public function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        return $this;
    }

    /**
     * Get the value of tipoTitulo
     */ 
    public function getTipoTitulo()
    {
        return $this->tipoTitulo;
    }

    /**
     * Set the value of tipoTitulo
     *
     * @return  self
     */ 
    public function setTipoTitulo($tipoTitulo)
    {
        $this->tipoTitulo = $tipoTitulo;

        return $this;
    }
}