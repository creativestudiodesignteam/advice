<?php

namespace Dao\Classes;

class Usuario{

     private $id = null;
     private $grupo = null;
     private $nome = null;
     private $email = null;  
     private $senha = null;    
     private $ativo = null;   
     private $data_cadastro = null;

     function getId() {
         return $this->id;
     }

     function getGrupo() {
         return $this->grupo;
     }

     function getNome() {
         return $this->nome;
     }

     function getEmail() {
         return $this->email;
     }

     function getSenha() {
         return $this->senha;
     }

     function getAtivo() {
         return $this->ativo;
     }

     function getDataCadastro() {
         return $this->data_cadastro;
     }

     function setId($id) {
         $this->id = $id;
     }

     function setGrupo($grupo) {
         $this->grupo = $grupo;
     }

     function setNome($nome) {
         $this->nome = $nome;
     }

     function setEmail($email) {
         $this->email = $email;
     }

     function setSenha($senha) {
         $this->senha = $senha;
     }

     function setAtivo($ativo) {
         $this->ativo = $ativo;
     }

     function setDataCadastro($data_cadastro) {
         $this->data_cadastro = $data_cadastro;
     }



}
