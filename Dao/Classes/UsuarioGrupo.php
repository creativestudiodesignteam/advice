<?php

namespace Dao\Classes;

class UsuarioGrupo{
     private $titulo = null;
     private $id = null;
     private $ativo = null;
     
    public function getId(){
        return $this->id;
    }
    public function getGrupo(){
        return $this->titulo;
    }
    public function getAtivo(){
        return $this->ativo;
    }
    
    public function setGrupo($titulo){
        $this->titulo = $titulo;
    }
    public function setId($id){
        $this->id = $id;
    }
    public function setAtivo($ativo){
        $this->ativo = $ativo;
    }
}

