<?php

namespace Dao\Classes;

 class InstitucionalConteudo{

     private $id = null;
     private $linguagem = null;
     private $titulo = null;
     private $texto = null;
     private $id_institucional = null;
     private $icone = null;
     private $ativo = null;
     private $ordem = null;



     /**
      * Get the value of id
      */ 
     public function getId()
     {
          return $this->id;
     }

     /**
      * Set the value of id
      *
      * @return  self
      */ 
     public function setId($id)
     {
          $this->id = $id;

          return $this;
     }

     /**
      * Get the value of linguagem
      */ 
     public function getLinguagem()
     {
          return $this->linguagem;
     }

     /**
      * Set the value of linguagem
      *
      * @return  self
      */ 
     public function setLinguagem($linguagem)
     {
          $this->linguagem = $linguagem;

          return $this;
     }

     /**
      * Get the value of titulo
      */ 
     public function getTitulo()
     {
          return $this->titulo;
     }

     /**
      * Set the value of titulo
      *
      * @return  self
      */ 
     public function setTitulo($titulo)
     {
          $this->titulo = $titulo;

          return $this;
     }

     /**
      * Get the value of texto
      */ 
     public function getTexto()
     {
          return $this->texto;
     }

     /**
      * Set the value of texto
      *
      * @return  self
      */ 
     public function setTexto($texto)
     {
          $this->texto = $texto;

          return $this;
     }

     /**
      * Get the value of id_institucional
      */ 
     public function getId_institucional()
     {
          return $this->id_institucional;
     }

     /**
      * Set the value of id_institucional
      *
      * @return  self
      */ 
     public function setId_institucional($id_institucional)
     {
          $this->id_institucional = $id_institucional;

          return $this;
     }

     /**
      * Get the value of icone
      */ 
     public function getIcone()
     {
          return $this->icone;
     }

     /**
      * Set the value of icone
      *
      * @return  self
      */ 
     public function setIcone($icone)
     {
          $this->icone = $icone;

          return $this;
     }

     /**
      * Get the value of ativo
      */ 
     public function getAtivo()
     {
          return $this->ativo;
     }

     /**
      * Set the value of ativo
      *
      * @return  self
      */ 
     public function setAtivo($ativo)
     {
          $this->ativo = $ativo;

          return $this;
     }

     /**
      * Get the value of ordem
      */ 
     public function getOrdem()
     {
          return $this->ordem;
     }

     /**
      * Set the value of ordem
      *
      * @return  self
      */ 
     public function setOrdem($ordem)
     {
          $this->ordem = $ordem;

          return $this;
     }
}
