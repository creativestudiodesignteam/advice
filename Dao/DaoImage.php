<?php

namespace Dao;

use Dao\Classes\Image;

class DaoImage {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoImage();
        }
        return self::$instance;
    }

    public function Inserir(Image $obj) {
        try {
            $sql = "INSERT INTO tbl_image (imagem, data, fk_id, imagem_id, tipo) 
                                       VALUES (:imagem, :data, :fk_id, :imagem_id, :tipo)";

            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $this->setObj($p_sql, $obj);
            
            $p_sql->execute();
            
            return Conexao::getInstance()->lastInsertId();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }

    public function Editar(Image $obj) {
        try {
            $sql = "UPDATE tbl_image SET imagem = :imagem, data = :data, tipo = :tipo"
                                   . " WHERE imagem_id = :imagem_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            
            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";            
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "DELETE FROM tbl_image WHERE imagem = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_image WHERE imagem_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            $p_sql->execute();
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarPorCODFk($fk, $tipo) {
        try {
            $sql = "SELECT * FROM tbl_image WHERE fk_id = :fk_id AND tipo = :tipo";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":fk_id", $fk);
            $p_sql->bindValue(":tipo", $tipo);
            $p_sql->execute();     
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }
    
    public function TotalFk($fk, $tipo) {
        try {
            $sql = "SELECT * FROM tbl_image WHERE fk_id = :fk_id AND tipo = :tipo";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":fk_id", $fk);
            $p_sql->bindValue(":tipo", $tipo);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodos() {
        try {
            $sql = "SELECT * FROM tbl_image ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function Total() {
        try {
            $sql = "SELECT * FROM tbl_image";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    private function populaObj($row) {
        $pojo = new Image();
        $pojo->setId($row['imagem_id']);
        $pojo->setImagem($row['imagem']);
        $pojo->setData($row['data']);
        $pojo->setFk($row['fk_id']);
        $pojo->setTipo($row['tipo']);
        return $pojo;
    }
    
    private function setObj($p_sql, Image $obj) {
        
        $p_sql->bindValue(":imagem", $obj->getImagem());
        $p_sql->bindValue(":data", $obj->getData());
        $p_sql->bindValue(":fk_id", $obj->getFk());
        $p_sql->bindValue(":imagem_id", $obj->getId());
        $p_sql->bindValue(":tipo", $obj->getTipo());    
        return $p_sql;     
    }
    
    public function statusTable($tabela) {
        
        $sql = "SHOW TABLE STATUS LIKE '".$tabela."'";
        $p_sql = Conexao::getInstance()->prepare($sql);
        $p_sql->execute();
        
        return $p_sql->fetch(\PDO::FETCH_ASSOC);     
    }

}

