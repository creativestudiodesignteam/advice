<?php

namespace Dao;

use Dao\Classes\Banner;

class DaoBanner {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoBanner();
        }
        return self::$instance;
    }

    public function Inserir(Banner $obj) {
        try {
            $sql = "INSERT INTO tbl_banners (link, imagem, ativo, ordem, texto, texto2, linguagem_id, banner_id, titulo) 
                                     VALUES (:link, :imagem, :ativo, :ordem, :texto, :texto2, :linguagem_id, :banner_id, :titulo)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            
            $p_sql->execute();
            
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Editar(Banner $obj) {
        try {
            $sql = "UPDATE tbl_banners SET texto = :texto, texto2 = :texto2, link = :link, imagem = :imagem, ativo = :ativo, ordem = :ordem, titulo = :titulo "
                                 . " WHERE banner_id = :banner_id AND linguagem_id = :linguagem_id";

            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $this->setObj($p_sql, $obj);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "DELETE FROM tbl_banners WHERE banner_id = :banner_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":banner_id", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_banners WHERE banner_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            $obj = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $obj[] = $this->populaObj($row);
            }
                return $obj;
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarPorCODLing($cod, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_banners WHERE banner_id = :cod AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();     
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }
    
    public function BuscarTodos($linguagem, $ativo) {
        try {
            $sql = "SELECT * FROM tbl_banners WHERE ativo <> :ativo AND linguagem_id = :linguagem_id ORDER BY ordem";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", $ativo);
            $p_sql->execute();
            $obj = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $obj[] = $this->populaObj($row);
            }
                return $obj;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    
    private function populaObj($row) {
        $pojo = new Banner();
        $pojo->setId($row['banner_id']);
        $pojo->setTexto($row['texto']);
        $pojo->setTexto2($row['texto2']);
        $pojo->setLink($row['link']);
        $pojo->setImagem($row['imagem']);
        $pojo->setOrdem($row['ordem']);
        $pojo->setAtivo($row['ativo']);
        $pojo->setLinguagem($row['linguagem_id']);
        $pojo->setTitulo($row['titulo']);
        
        return $pojo;
    }
    
    private function setObj($p_sql, Banner $obj) {
        $p_sql->bindValue(":texto", $obj->getTexto());
        $p_sql->bindValue(":texto2", $obj->getTexto2());
        $p_sql->bindValue(":link", $obj->getLink());
        $p_sql->bindValue(":imagem", $obj->getImagem());
        $p_sql->bindValue(":ordem", $obj->getOrdem());
        $p_sql->bindValue(":ativo", $obj->getAtivo());
        $p_sql->bindValue(":linguagem_id", $obj->getLinguagem());
        $p_sql->bindValue(":banner_id", $obj->getId());
        $p_sql->bindValue(":titulo", $obj->getTitulo());
        
        return $p_sql;
    }
    
    public function statusTable($tabela) {
        
        $sql = "SHOW TABLE STATUS LIKE '".$tabela."'";
        $p_sql = Conexao::getInstance()->prepare($sql);
        $p_sql->execute();
        
        return $p_sql->fetch(\PDO::FETCH_ASSOC);     
    }
}

