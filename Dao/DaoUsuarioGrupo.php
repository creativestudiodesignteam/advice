<?php

namespace Dao;

use Dao\Classes\UsuarioGrupo;

class DaoUsuarioGrupo {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoUsuarioGrupo();
        }
        return self::$instance;
    }

    public function Inserir(UsuarioGrupo $obj) {
        try {
            $sql = "INSERT INTO tbl_usuarios_grupo (grupo, ativo) 
                                            VALUES (:grupo, :ativo)";

            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $this->setObj($p_sql, $obj);
            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
            //return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Editar(UsuarioGrupo $obj) {
        try {
            $sql = "UPDATE tbl_usuarios_grupo SET grupo = :grupo, ativo = :ativo"
                                   . " WHERE grupo_id = :grupo_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            $p_sql->bindValue(":grupo_id", $obj->getId());
            
            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "UPDATE tbl_usuarios_grupo SET ativo = :ativo WHERE grupo_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", 'd');
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_usuarios_grupo WHERE grupo_id = :cod AND ativo <> 'd'";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodos() {
        try {
            $sql = "SELECT * FROM tbl_usuarios_grupo WHERE ativo <> :ativo";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->execute();
            $obj = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $obj[] = $this->populaObj($row);
            }
                return $obj;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function Total() {
        try {
            $sql = "SELECT * FROM tbl_usuarios_grupo WHERE ativo <> :ativo";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodosPaginacao($inicio, $limit) {
        try {
            $sql = "SELECT * FROM tbl_usuarios_grupo WHERE ativo <> :ativo LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->execute();
            $obj = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $obj[] = $this->populaObj($row);
            }
                return $obj;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage();
        }
    }
    
    private function populaObj($row) {
        $pojo = new UsuarioGrupo();
        $pojo->setId($row['grupo_id']);
        $pojo->setGrupo($row['grupo']);
        $pojo->setAtivo($row['ativo']);
        return $pojo;
    }
    
    private function setObj($p_sql, UsuarioGrupo $obj){
        $p_sql->bindValue(":grupo", $obj->getGrupo());
        $p_sql->bindValue(":ativo", $obj->getAtivo());
        
        return $p_sql;
        
    }

}

