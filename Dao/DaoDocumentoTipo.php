<?php

namespace Dao;

use Dao\Classes\DocumentoTipo;

class DaoDocumentoTipo {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoDocumentoTipo();
        }
        return self::$instance;
    }

    public function Inserir(DocumentoTipo $obj) {
        try {
            $sql = "INSERT INTO documentos_tipo (tipo, ativo, sCodigo) 
                                         VALUES (:tipo, :ativo, :sCodigo)";

            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $this->setObj($p_sql, $obj);
            
            $p_sql->execute();
            
            return Conexao::getInstance()->lastInsertId();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Editar(DocumentoTipo $obj) {
        try {
            $sql = "UPDATE documentos_tipo SET tipo = :tipo, ativo = :ativo, sCodigo = :sCodigo"
                                   . " WHERE cod_documentos_tipo = :cod_documentos_tipo";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            $p_sql->bindValue(":cod_documentos_tipo", $obj->getId());
            
            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";            
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "UPDATE documentos_tipo SET ativo = :ativo WHERE cod_documentos_tipo = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", 'd');
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM documentos_tipo WHERE cod_documentos_tipo = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();     
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodos($ativo) {
        try {
            $sql = "SELECT * FROM documentos_tipo WHERE ativo <> :ativo AND ativo <> :ativo2";
            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function Total($ativo) {
        try {
            $sql = "SELECT * FROM documentos_tipo WHERE ativo <> :ativo AND ativo <> :ativo2";
            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    
    private function populaObj($row) {
        $pojo = new DocumentoTipo();
        $pojo->setId($row['cod_documentos_tipo']);
        $pojo->setTitulo($row['tipo']);
        $pojo->setSCodigo($row['sCodigo']);
        $pojo->setAtivo($row['ativo']);
        return $pojo;
    }
    
    private function setObj($p_sql, DocumentoTipo $obj) {
        
        $p_sql->bindValue(":tipo", $obj->getTitulo());
        $p_sql->bindValue(":sCodigo", $obj->getSCodigo());
        $p_sql->bindValue(":ativo", $obj->getAtivo());
            
        return $p_sql;     
    }
    
    public function statusTable($tabela) {
        
        $sql = "SHOW TABLE STATUS LIKE '".$tabela."'";
        $p_sql = Conexao::getInstance()->prepare($sql);
        $p_sql->execute();
        
        return $p_sql->fetch(\PDO::FETCH_ASSOC);     
    }

}

