<?php

namespace Dao;

use Dao\Classes\UsuarioGrupoPaginas;

class DaoUsuarioGrupoPaginas {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoUsuarioGrupoPaginas();
        }
        return self::$instance;
    }

    public function Inserir(UsuarioGrupoPaginas $obj) {
        try {
            $sql = "INSERT INTO tbl_usuarios_grupoxpagina (grupo_id, pagina_id) 
                                                   VALUES (:grupo_id, :pagina_id)";

            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $this->setObj($p_sql, $obj);


            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function Deletar($cod) {
        try {
            $sql = "DELETE FROM tbl_usuarios_grupoxpagina WHERE grupo_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_usuarios_grupoxpagina WHERE grupo_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            return $this->populaUsuarioGrupoPaginas($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodosGrupo($cod) {
        try {
            $sql = "SELECT * FROM tbl_usuarios_grupoxpagina WHERE grupo_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            $usuariosGruposPaginas = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $usuariosGruposPaginas[] = $this->populaUsuarioGrupoPaginas($row);
            }
                return $usuariosGruposPaginas;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function GrupoxPag($codGrup, $codPag){
        try {
            $sql = "SELECT grupoxpagina_id FROM tbl_usuarios_grupoxpagina WHERE grupo_id = :grupo_id AND pagina_id = :pagina_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":grupo_id", $codGrup);
            $p_sql->bindValue(":pagina_id", $codPag);
            $p_sql->execute(); 
            
            if($p_sql->rowCount()>0){
                    $checked = 'checked="checked"';
            }else{
                    $checked =  '';
            }
                
            return $checked;
		
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }	
    }
    
    private function populaUsuarioGrupoPaginas($row) {
        $pojo = new UsuarioGrupoPaginas();
        $pojo->setId($row['grupoxpagina_id']);
        $pojo->setGrupo($row['grupo_id']);
        $pojo->setPagina($row['pagina_id']);
        return $pojo;
    }
    
    private function setObj($p_sql, UsuarioGrupoPaginas $obj){
        $p_sql->bindValue(":grupo_id", $obj->getGrupo());
        $p_sql->bindValue(":pagina_id", $obj->getPagina());
        
        return $p_sql;
    }
}