<?php

namespace Dao;

use Dao\Classes\Aplicacao;

class DaoAplicacao {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoAplicacao();
        }
        return self::$instance;
    }

    public function Inserir(Aplicacao $obj) {
        try {
            $sql = "INSERT INTO aplicacao (aplicacao, ativo, linguagem_id, cod_aplicacao, cod_categoria) 
                                       VALUES (:aplicacao, :ativo, :linguagem_id, :cod_aplicacao, :cod_categoria)";

            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $this->setObj($p_sql, $obj);
            
            $p_sql->execute();
            
            return Conexao::getInstance()->lastInsertId();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Editar(Aplicacao $obj) {
        try {
            $sql = "UPDATE aplicacao SET aplicacao = :aplicacao, ativo = :ativo, cod_categoria = :cod_categoria"
                                   . " WHERE cod_aplicacao = :cod_aplicacao AND linguagem_id = :linguagem_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            
            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";            
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "UPDATE aplicacao SET ativo = :ativo WHERE cod_aplicacao = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", 'd');
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }


    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM aplicacao WHERE cod_aplicacao = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarPorCODLing($cod, $linguagem) {
        try {
            $sql = "SELECT * FROM aplicacao WHERE cod_aplicacao = :cod AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();     
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }
    
    public function BuscarTodos($linguagem, $ativo) {
        try {
            $sql = "SELECT * FROM aplicacao WHERE ativo <> :ativo AND ativo <> :ativo2 AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarTodosCategoria($linguagem, $cat) {
        try {
            $sql = "SELECT * FROM aplicacao WHERE ativo = :ativo AND cod_categoria = :cod_categoria AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "s");
            $p_sql->bindValue(":cod_categoria", $cat);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarCategoriaXAplicacao($linguagem, $cod_produto) {
        try {
            $sql = "SELECT aplicacao.*, categoria_prod FROM aplicacaoxproduto
            INNER JOIN aplicacao 
            ON aplicacao.cod_aplicacao = aplicacaoxproduto.cod_aplicacao
            WHERE cod_produto = :cod_produto AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":cod_produto", $cod_produto);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarCategoriaXAplicacaoProduto($linguagem, $cod_produto, $cod_categoria) {
        try {
            $sql = "SELECT produtos.descricao, aplicacao.aplicacao, categorias_prod.categoria_prod 

            FROM produtos
            
            LEFT OUTER JOIN aplicacaoxproduto 
            
            ON produtos.cod_produto = aplicacaoxproduto.cod_produto 
            
            LEFT OUTER JOIN aplicacao
            
            ON aplicacaoxproduto.cod_aplicacao = aplicacao.cod_aplicacao 
            
            LEFT OUTER JOIN categorias_prod 
            
            ON categorias_prod.cod_categoria_prod = aplicacaoxproduto.categoria_prod    
            
            WHERE produtos.cod_produto= :cod_produto AND cod_categoria= :cod_categoria AND aplicacao.linguagem_id = :linguagem_id AND produtos.linguagem_id = :linguagem_id ORDER BY aplicacao.aplicacao ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":cod_produto", $cod_produto);
            $p_sql->bindValue(":cod_categoria", $cod_categoria);
            $p_sql->execute();
            
            return $row = $p_sql->fetchAll(\PDO::FETCH_ASSOC);
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function Total($linguagem, $ativo) {
        try {
            $sql = "SELECT * FROM aplicacao WHERE ativo <> :ativo AND ativo <> :ativo2 AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodosPaginacao($inicio, $limit, $linguagem, $ativo) {
        try {
            $sql = "SELECT * FROM aplicacao WHERE ativo <> :ativo AND ativo <> :ativo2 AND linguagem_id = :linguagem_id ORDER BY categoria LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage();
        }
    }

    public function TotalBuscaTitulo($categoria, $linguagem) {
        try {
            $sql = "SELECT * FROM aplicacao WHERE categoria LIKE :categoria WHERE linguagem_id = :linguagem_id ORDER BY categoria";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(':categoria', "%".$categoria."%", \PDO::PARAM_STR);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    
    private function populaObj($row) {
        $pojo = new Aplicacao();
        $pojo->setId($row['cod_aplicacao']);
        $pojo->setTitulo($row['aplicacao']);
        $pojo->setAtivo($row['ativo']);
        $pojo->setCategoria($row['cod_categoria']);
        $pojo->setLinguagem($row['linguagem_id']);
        if(isset($row['categoria_prod'])){
            $pojo->setCategoria_prod($row['categoria_prod']); 
        }
        return $pojo;
    }
    
    private function setObj($p_sql, Aplicacao $obj) {
        
        $p_sql->bindValue(":aplicacao", $obj->getTitulo());
        $p_sql->bindValue(":ativo", $obj->getAtivo());
        $p_sql->bindValue(":cod_categoria", $obj->getCategoria());
        $p_sql->bindValue(":linguagem_id", $obj->getLinguagem());
        $p_sql->bindValue(":cod_aplicacao", $obj->getId());
            
        return $p_sql;     
    }
    
    public function statusTable($tabela) {
        
        $sql = "SHOW TABLE STATUS LIKE '".$tabela."'";
        $p_sql = Conexao::getInstance()->prepare($sql);
        $p_sql->execute();
        
        return $p_sql->fetch(\PDO::FETCH_ASSOC);     
    }

}

