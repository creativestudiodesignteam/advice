<?php

namespace Dao;

use Dao\Classes\UsuarioPaginas;

class DaoUsuarioPaginas {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoUsuarioPaginas();
        }
        return self::$instance;
    }


    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_usuarios_paginas WHERE pagina_id = :cod AND visivel = :ativo ORDER BY ordem";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->bindValue(":ativo", 's');
            $p_sql->execute();
            return $this->populaUsuarioPagina($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodos($nivel) {
        try {
            $sql = "SELECT * FROM tbl_usuarios_paginas WHERE visivel = :ativo AND nivel = :nivel ORDER BY ordem";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":nivel", $nivel);
            $p_sql->bindValue(":ativo", "s");
            $p_sql->execute();
            $usuarioPaginas = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $usuarioPaginas[] = $this->populaUsuarioPagina($row);
            }
                return $usuarioPaginas;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    private function populaUsuarioPagina($row) {
        $pojo = new UsuarioPaginas();
        $pojo->setId($row['pagina_id']);
        $pojo->setPagina($row['pagina']);
        $pojo->setFuncao($row['funcao']);
        $pojo->setNivel($row['nivel']);
        $pojo->setOrdem($row['ordem']);
        $pojo->setVisivel($row['visivel']);
        $pojo->setIcone($row['icone']);
        return $pojo;
    }

}

