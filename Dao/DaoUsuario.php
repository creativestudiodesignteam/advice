<?php

namespace Dao;

use Dao\Classes\Usuario;

class DaoUsuario {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoUsuario();
        }
        return self::$instance;
    }

    public function Inserir(Usuario $obj) {
        try {
            $sql = "INSERT INTO tbl_usuarios (nome,  senha, email, grupo_id, data_cadastro, ativo) 
                                      VALUES (:nome,  :senha, :email, :grupo_id, :data_cadastro, :ativo)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            $p_sql->bindValue(":data_cadastro", $obj->getDataCadastro());


            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Editar(Usuario $obj) {
        try {
            $sql = "UPDATE tbl_usuarios SET nome = :nome, senha = :senha, email = :email, grupo_id = :grupo_id, ativo = :ativo WHERE usuario_id = :usuario_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            $p_sql->bindValue(":usuario_id", $obj->getId());

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage()." ".$obj->getAtivo();
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "UPDATE tbl_usuarios SET ativo = :ativo WHERE usuario_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", 'd');
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_usuarios WHERE usuario_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarPorEmail($email, $cod) {
        try {
            $sql = "SELECT * FROM tbl_usuarios WHERE email = :email AND usuario_id <> :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":email", $email);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodos() {
        try {
            $sql = "SELECT * FROM tbl_usuarios WHERE ativo <> :ativo";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function Total() {
        try {
            $sql = "SELECT * FROM tbl_usuarios WHERE ativo <> :ativo";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodosPaginacao($inicio, $limit) {
        try {
            $sql = "SELECT * FROM tbl_usuarios WHERE ativo <> :ativo LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage();
        }
    }
    
    public function BuscarGrupoPaginacao($inicio, $limit, $grupo) {
        try {
            $sql = "SELECT * FROM tbl_usuarios WHERE ativo <> :ativo AND grupo_id = :grupo_id LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->bindParam(':grupo_id', $grupo, \PDO::PARAM_INT);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage();
        }
    }
    
    public function BuscarGrupo($grupo) {
        try {
            $sql = "SELECT * FROM tbl_usuarios WHERE ativo <> :ativo AND grupo_id = :grupo_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindParam(':grupo_id', $grupo, \PDO::PARAM_INT);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function TotalGrupo($grupo) {
        try {
            $sql = "SELECT * FROM tbl_usuarios WHERE ativo <> :ativo AND grupo_id = :grupo_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindParam(':grupo_id', $grupo, \PDO::PARAM_INT);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    private function populaObj($row) {
        $pojo = new Usuario();
        $pojo->setId($row['usuario_id']);
        $pojo->setNome($row['nome']);
        $pojo->setSenha($row['senha']);
        $pojo->setEmail($row['email']);
        $pojo->setGrupo($row['grupo_id']);
        $pojo->setDataCadastro($row['data_cadastro']);
        $pojo->setAtivo($row['ativo']);
        return $pojo;
    }
    
    private function setObj($p_sql, Usuario $obj){
        $p_sql->bindValue(":usuario_id", $obj->getId());
        $p_sql->bindValue(":nome", $obj->getNome());
        $p_sql->bindValue(":senha", $obj->getSenha());
        $p_sql->bindValue(":email", $obj->getEmail());
        $p_sql->bindValue(":grupo_id", $obj->getGrupo());
        $p_sql->bindValue(":ativo", $obj->getAtivo());
        
        return $p_sql;
        
    }

    
    public function logar($email, $senha) {
            try {
                $sql = "SELECT * FROM tbl_usuarios WHERE email = :email AND senha = :senha";
                $p_sql = Conexao::getInstance()->prepare($sql);
                $p_sql->bindValue(":email", $email);
                $p_sql->bindValue(":senha", $senha);
                $p_sql->execute();
                return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
            } catch (Exception $e) {
                print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
            }
    }    


    public function logado($email, $senha) {
            try {
                $sql = "SELECT * FROM tbl_usuarios WHERE email = :email AND senha = :senha";
                $p_sql = Conexao::getInstance()->prepare($sql);
                $p_sql->bindValue(":email", $email);
                $p_sql->bindValue(":senha", $senha);
                $p_sql->execute();
                return $p_sql->rowCount();
            } catch (Exception $e) {
                print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
            }
    }

    public function recupera($email) {
            try {
                $sql = "SELECT * FROM tbl_usuarios WHERE email = :email";
                $p_sql = Conexao::getInstance()->prepare($sql);
                $p_sql->bindValue(":email", $email);
                $p_sql->execute();
                return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
            } catch (Exception $e) {
                print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
            }
    }

    public function envia_senha(Usuario $obj, $email){
        $to       = $obj->getEmail();
        $subject  = 'Advice System - Recuperar Senha!';
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

        $headers .= 'From: Advice System <'.$email.'>';
        
        $corpoEmail = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
        <html xmlns=\"http://www.w3.org/1999/xhtml\">
        <head>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
        </head>

        <body>
            <table width=\"700\" border=\"0\">
                <tr>
                    <td><img src='".URL_SITE."/img/site-logo.png' height=\"100px\"></td>
                </tr>
                <tr>
                    <td><br><br>Ol&aacute;,<strong> ".$obj->getNome()."</strong><br><br> 
                <br>
                <br>
                Confira seus dados de acesso:<br><br>
                <b>Nome:</b> <em><font class=\"tamanho13\">".$obj->getNome().".</font></em><br>
                <b>E-mail:</b> <em><font class=\"tamanho13\">".$obj->getEmail()."</font></em><br>
                <b>Senha: </b> <em><font class=\"tamanho13\">".$obj->getSenha().".</font></em><br>
                    </td>
                </tr>
                <tr>
                <td></td>
                </tr>
            </table>

        </body>
        </html>";
    }
}

