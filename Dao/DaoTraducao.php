<?php

namespace Dao;

use Dao\Classes\Traducao;

class DaoTraducao {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoTraducao();
        }
        return self::$instance;
    }

    public function Inserir(Traducao $obj) {
        try {
            $sql = "INSERT INTO tbl_site_traducao (texto1, texto2, texto3, texto4, texto5, texto6, texto7, texto8, pagina, linguagem_id, traducao_id) 
                                           VALUES (:texto1, :texto2, :texto3, :texto4, :texto5, :texto6,:texto7,:texto8, :pagina, :linguagem_id, :traducao_id)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);


            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Editar(Traducao $obj) {
        try {
            $sql = "UPDATE tbl_site_traducao SET texto1 = :texto1, texto2 = :texto2, texto3 = :texto3, texto4 = :texto4, texto5 = :texto5, texto6 = :texto6, texto7 = :texto7, texto8 = :texto8, pagina = :pagina"
                                   . " WHERE traducao_id = :traducao_id AND linguagem_id = :linguagem_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "DELETE FROM tbl_site_traducao WHERE traducao_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_site_traducao WHERE traducao_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            $obj = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $obj[] = $this->populaObj($row);
            }
                return $obj;
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarPorPagina($pag, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_site_traducao WHERE pagina = :pagina AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":pagina", $pag);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarPorCODLing($cod, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_site_traducao WHERE traducao_id = :cod AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();     
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }
    
    public function BuscarTodos($linguagem) {
        try {
            $sql = "SELECT * FROM tbl_site_traducao WHERE linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();
            $obj = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $obj[] = $this->populaObj($row);
            }
                return $obj;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    
    private function populaObj($row) {
        $pojo = new Traducao();
        $pojo->setId($row['traducao_id']);
        $pojo->setTexto1($row['texto1']);
        $pojo->setTexto2($row['texto2']);
        $pojo->setTexto3($row['texto3']);
        $pojo->setTexto4($row['texto4']);
        $pojo->setTexto5($row['texto5']);
        $pojo->setTexto6($row['texto6']);
        $pojo->setTexto7($row['texto7']);
        $pojo->setTexto8($row['texto8']);
        $pojo->setPagina($row['pagina']);
        $pojo->setLinguagem($row['linguagem_id']);
        return $pojo;
    }
    
    private function setObj($p_sql, Traducao $obj){
        $p_sql->bindValue(":texto1", $obj->getTexto1());
        $p_sql->bindValue(":texto2", $obj->getTexto2());
        $p_sql->bindValue(":texto3", $obj->getTexto3());
        $p_sql->bindValue(":texto4", $obj->getTexto4());
        $p_sql->bindValue(":texto5", $obj->getTexto5());
        $p_sql->bindValue(":texto6", $obj->getTexto6());
        $p_sql->bindValue(":texto7", $obj->getTexto7());
        $p_sql->bindValue(":texto8", $obj->getTexto8());
        $p_sql->bindValue(":pagina", $obj->getPagina());
        $p_sql->bindValue(":linguagem_id", $obj->getLinguagem());
        $p_sql->bindValue(":traducao_id", $obj->getId());
        
        return $p_sql;
    }
    

}

