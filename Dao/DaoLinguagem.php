<?php
namespace Dao;

use Dao\Classes\Linguagem;

class DaoLinguagem {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoLinguagem();
        }
        return self::$instance;
    }

    public function Inserir(Linguagem $obj) {
        try {
            $sql = "INSERT INTO tbl_linguagem (linguagem, bandeira,ativo, prefix) 
                                       VALUES (:linguagem, :bandeira, :ativo, :prefix)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            
            
            $this->setObj($p_sql, $obj);
            $p_sql->execute();
            
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }

    public function Editar(Linguagem $obj) {
        try {
            $sql = "UPDATE tbl_linguagem SET linguagem = :linguagem, bandeira = :bandeira, ativo = :ativo, prefix = :prefix
                                          WHERE linguagem_id = :linguagem_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            $p_sql->bindValue(":linguagem_id", $obj->getId());

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "UPDATE tbl_linguagem SET ativo = :ativo WHERE linguagem_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", 'd');
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_linguagem WHERE linguagem_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorPrefix($cod)
    {
        try {
            $sql = "SELECT * FROM tbl_linguagem WHERE prefix = :cod AND ativo='s'";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
   
    public function BuscarTodos($a) {
        try {
            $sql = "SELECT * FROM tbl_linguagem WHERE ativo <> :ativo AND ativo <> :ativo2";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $a);
            $p_sql->execute();
            $institucionais = [];       
            while ($linha = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $institucionais[] = $this->populaObj($linha);
            }

                return $institucionais;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    private function populaObj($row) {
        $pojo = new Linguagem();
        $pojo->setId($row['linguagem_id']);
        $pojo->setBandeira($row['bandeira']);
        $pojo->setTitulo($row['linguagem']);
        $pojo->setPrefix($row['prefix']);
        $pojo->setAtivo($row['ativo']);
        return $pojo;
    }

    private function setObj($p_sql, Linguagem $obj){
        $p_sql->bindValue(":bandeira", $obj->getBandeira());
        $p_sql->bindValue(":linguagem", $obj->getTitulo());
        $p_sql->bindValue(":ativo", $obj->getAtivo());
        $p_sql->bindValue(":prefix", $obj->getPrefix());
        
        return $p_sql;
        
    }    
    
}