<?php

namespace Dao;

use Dao\Classes\Contato;

class DaoContato {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoContato();
        }
        return self::$instance;
    }

    public function Inserir(Contato $obj) {
        try {
            $sql = "INSERT INTO tbl_contato_site (nome, telefone, assunto, mensagem, email, data_contato, celular,  view, tipo, empresa) 
                                          VALUES (:nome, :telefone, :assunto, :mensagem, :email, :data_contato, :celular, 0, :tipo, :empresa)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            
            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ";
        }
    }

    public function Editar(Contato $obj) {
        try {
            $sql = "UPDATE tbl_contato_site SET nome = :nome, telefone = :telefone, assunto = :assunto, mensagem = :mensagem, email = :email,"
                                             . "celular = :celular, view = :view, tipo = :tipo, empresa = :empresa"
                                      . " WHERE contato_id = :contato_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            $p_sql->bindValue(":view", $obj->getView());
            $p_sql->bindValue(":contato_id", $obj->getId());

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "DELETE FROM tbl_contato_site WHERE contato_id = :contato_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":contato_id", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function Visualizar($cod, $v) {
        try {
            $sql = "UPDATE tbl_contato_site SET view = :view WHERE contato_id = :contato_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":contato_id", $cod);
            $p_sql->bindValue(":view", $v);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_contato_site WHERE contato_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodos() {
        try {
            $sql = "SELECT * FROM tbl_contato_site ORDER BY data_contato DESC, contato_id DESC";
            $p_sql = Conexao::getInstance()->prepare($sql);
           
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodosSemVisualizacao() {
        try {
            $sql = "SELECT * FROM tbl_contato_site WHERE view = 0 ORDER BY data_contato DESC, contato_id DESC";
            $p_sql = Conexao::getInstance()->prepare($sql);
           
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function TotalSemVisualizacao() {
        try {
            $sql = "SELECT * FROM tbl_contato_site WHERE view = 0 ORDER BY data_contato DESC, contato_id DESC";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function Total() {
        try {
            $sql = "SELECT * FROM tbl_contato_site ORDER BY data_contato DESC, contato_id DESC";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodosPaginacao($inicio, $limit) {
        try {
            $sql = "SELECT * FROM tbl_contato_site ORDER BY data_contato DESC, contato_id DESC LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage();
        }
    }
    
    
    private function populaObj($row) {
        $pojo = new Contato();
        $pojo->setId($row['contato_id']);
        $pojo->setNome($row['nome']);
        $pojo->setTelefone($row['telefone']);
        $pojo->setAssunto($row['assunto']);
        $pojo->setEmail($row['email']);
        $pojo->setMensagem($row['mensagem']);
        $pojo->setDataCadastro($row['data_contato']);
        $pojo->setCelular($row['celular']);
        $pojo->setView($row['view']);
        $pojo->setTipo($row['tipo']);
        $pojo->setEmpresa($row['empresa']);
        
        return $pojo;
    }
    
    private function setObj($p_sql, Contato $obj){
        $p_sql->bindValue(":nome", $obj->getNome());
        $p_sql->bindValue(":telefone", $obj->getTelefone());
        $p_sql->bindValue(":assunto", $obj->getAssunto());
        $p_sql->bindValue(":email", $obj->getEmail());
        $p_sql->bindValue(":mensagem", $obj->getMensagem());
        $p_sql->bindValue(":data_contato", $obj->getDataCadastro());
        $p_sql->bindValue(":celular", $obj->getCelular());
        $p_sql->bindValue(":tipo", $obj->getTipo());
        $p_sql->bindValue(":empresa", $obj->getEmpresa());
        
        return $p_sql;
        
    }

}

