<?php

namespace Dao;

use Dao\Classes\Endereco;

class DaoEndereco {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoEndereco();
        }
        return self::$instance;
    }

    public function Inserir(Endereco $obj) {
        try {
            $sql = "INSERT INTO tbl_endereco (endereco, unidade, numero, cep, bairro, cidade, estado, telefone, celular, complemento, email, latitude, longitude, imagem) 
                                      VALUES (:endereco, :unidade, :numero, :cep, :bairro, :cidade, :estado, :telefone, :celular, :complemento, :email, :latitude, :longitude, :imagem)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);


            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Editar(Endereco $obj) {
        try {
            $sql = "UPDATE tbl_endereco SET endereco = :endereco, unidade = :unidade, numero = :numero, cep = :cep, bairro = :bairro, cidade = :cidade, imagem = :imagem, "
                                   . "estado = :estado, telefone = :telefone, celular = :celular, complemento = :complemento, email = :email, latitude = :latitude, longitude = :longitude"
                                   . " WHERE endereco_id = :endereco_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            $p_sql->bindValue(":endereco_id", $obj->getId());

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage();
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "DELETE FROM tbl_endereco WHERE endereco_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_endereco WHERE endereco_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }
    
    public function BuscarTodos() {
        try {
            $sql = "SELECT * FROM tbl_endereco ORDER BY unidade ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    
    private function populaObj($row) {
        $pojo = new Endereco();
        $pojo->setId($row['endereco_id']);
        $pojo->setEndereco($row['endereco']);
        $pojo->setUnidade($row['unidade']);
        $pojo->setNumero($row['numero']);
        $pojo->setCep($row['cep']);
        $pojo->setBairro($row['bairro']);
        $pojo->setCidade($row['cidade']);
        $pojo->setEstado($row['estado']);
        $pojo->setTelefone($row['telefone']);
        $pojo->setCelular($row['celular']);
        $pojo->setComplemento($row['complemento']);
        $pojo->setEmail($row['email']);
        $pojo->setLatitude($row['latitude']);
        $pojo->setLongitude($row['longitude']);
        $pojo->setImagem($row['imagem']);
        return $pojo;
    }
    
    private function setObj($p_sql, Endereco $obj){
        $p_sql->bindValue(":endereco", $obj->getEndereco());
        $p_sql->bindValue(":unidade", $obj->getUnidade());
        $p_sql->bindValue(":numero", $obj->getNumero());
        $p_sql->bindValue(":cep", $obj->getCep());
        $p_sql->bindValue(":bairro", $obj->getBairro());
        $p_sql->bindValue(":cidade", $obj->getCidade());
        $p_sql->bindValue(":estado", $obj->getEstado());
        $p_sql->bindValue(":telefone", $obj->getTelefone());
        $p_sql->bindValue(":celular", $obj->getCelular());
        $p_sql->bindValue(":complemento", $obj->getComplemento());
        $p_sql->bindValue(":email", $obj->getEmail());
        $p_sql->bindValue(":latitude", $obj->getLatitude());
        $p_sql->bindValue(":longitude", $obj->getLongitude());
        $p_sql->bindValue(":imagem", $obj->getImagem());
        
        return $p_sql;
        
    }

}

