<?php

namespace Dao;

use Dao\Classes\Documento;

class DaoDocumento {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoDocumento();
        }
        return self::$instance;
    }

    public function Inserir(Documento $obj) {
        try {
            $sql = "INSERT INTO documentos (documento, dir, cod_documentos_tipo, cod_iqa, ativo) 
                                         VALUES (:documento, :dir, :cod_documentos_tipo, :cod_iqa, :ativo)";

            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $this->setObj($p_sql, $obj);
            
            $p_sql->execute();
            
            return Conexao::getInstance()->lastInsertId();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Editar(Documento $obj) {
        try {
            $sql = "UPDATE documentos SET documento = :documento, dir = :dir, cod_documentos_tipo = :cod_documentos_tipo, cod_iqa = :cod_iqa, ativo = :ativo"
                                   . " WHERE cod_documento = :cod_documento";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            $p_sql->bindValue(":cod_documento", $obj->getId());
            
            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";            
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "UPDATE documentos SET ativo = :ativo WHERE cod_iqa = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", 'd');
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM documentos WHERE cod_documento = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();     
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorProduto($cod) {
        try {
            $sql = "SELECT documentos.*, documentos_tipo.tipo FROM documentos
            INNER JOIN documentos_tipo 
                    ON documentos_tipo.cod_documentos_tipo = documentos.cod_documentos_tipo
                 WHERE documentos.cod_iqa = :cod AND documentos.ativo = 's'";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();     
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodos($ativo) {
        try {
            $sql = "SELECT * FROM documentos WHERE ativo <> :ativo AND ativo <> :ativo2";
            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function Total($ativo) {
        try {
            $sql = "SELECT * FROM documentos WHERE ativo <> :ativo AND ativo <> :ativo2";
            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    
    
    private function populaObj($row) {
        $pojo = new Documento();
        $pojo->setId($row['cod_documento']);
        $pojo->setDocumento($row['documento']);
        $pojo->setDir($row['dir']);
        $pojo->setTipo($row['cod_documentos_tipo']);
        $pojo->setCod_iqa($row['cod_iqa']);
        $pojo->setAtivo($row['ativo']);

        if(isset($row['tipo'])){
            $pojo->setTipoTitulo($row['tipo']);
        }
        return $pojo;
    }
    
    private function setObj($p_sql, Documento $obj) {
        
        $p_sql->bindValue(":documento", $obj->getDocumento());
        $p_sql->bindValue(":cod_iqa", $obj->getCod_iqa());
        $p_sql->bindValue(":dir", $obj->getDir());
        $p_sql->bindValue(":cod_documentos_tipo", $obj->getTipo());
        $p_sql->bindValue(":ativo", $obj->getAtivo());
            
        return $p_sql;     
    }
    
    public function statusTable($tabela) {
        
        $sql = "SHOW TABLE STATUS LIKE '".$tabela."'";
        $p_sql = Conexao::getInstance()->prepare($sql);
        $p_sql->execute();
        
        return $p_sql->fetch(\PDO::FETCH_ASSOC);     
    }

}

