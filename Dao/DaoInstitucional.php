<?php

namespace Dao;

use Dao\Classes\Institucional;

class DaoInstitucional {

    public static $instance;



    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoInstitucional();
        }
        return self::$instance;
    }

    public function Inserir(Institucional $obj) {
        try {
            $sql = "INSERT INTO tbl_institucional (texto, imagem, titulo, sub_titulo,  ativo, icone, ordem, linguagem_id, institucional_id, destaque, link, link_logo)
                                           VALUES (:texto, :imagem, :titulo, :sub_titulo,  :ativo, :icone, :ordem, :linguagem_id, :institucional_id, :destaque, :link, :link_logo)";

            $p_sql = Conexao::getInstance()->prepare($sql);



            $this->setObj($p_sql, $obj);
            $p_sql->execute();

            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }

    public function Editar(Institucional $obj) {
        try {
            $sql = "UPDATE tbl_institucional SET texto = :texto, imagem = :imagem, titulo = :titulo, sub_titulo = :sub_titulo, imagem = :imagem, ativo = :ativo, icone = :icone,
                                         link = :link, ordem = :ordem, destaque = :destaque, link_logo = :link_logo WHERE institucional_id = :institucional_id AND linguagem_id = :linguagem_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            $p_sql->bindValue(":institucional_id", $obj->getId());

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "UPDATE tbl_institucional SET ativo = :ativo WHERE institucional_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", 'q');
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_institucional WHERE institucional_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            $institucionais = [];
            while ($linha = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $institucionais[] = $this->populaObj($linha);
            }

                return $institucionais;
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCODLing($cod, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_institucional WHERE institucional_id = :cod AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }

    public function BuscarTodos($a, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_institucional WHERE linguagem_id = :linguagem_id AND ativo <> :ativo AND ativo <> :ativo2";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", "q");
            $p_sql->bindValue(":ativo2", $a);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();
            $institucionais = [];
            while ($linha = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $institucionais[] = $this->populaObj($linha);
            }

                return $institucionais;

        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarHome($linguagem) {
        try {
            $sql = "SELECT * FROM tbl_institucional WHERE linguagem_id = :linguagem_id AND ativo = 's' AND destaque = 's'";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();
            $institucionais = [];
            while ($linha = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $institucionais[] = $this->populaObj($linha);
            }

                return $institucionais;

        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    private function populaObj($row) {
        $pojo = new Institucional();
        $pojo->setId($row['institucional_id']);
        $pojo->setTitulo($row['titulo']);
        $pojo->setTexto($row['texto']);
        $pojo->setImagem($row['imagem']);
        $pojo->setAtivo($row['ativo']);
        $pojo->setIcone($row['icone']);
        $pojo->setOrdem($row['ordem']);
        $pojo->setDestaque($row['destaque']);
        $pojo->setSub_titulo($row['sub_titulo']);
        $pojo->setLinguagem($row['linguagem_id']);
        $pojo->setLink($row['link']);
        $pojo->setLinkLogo($row['link_logo']);
        return $pojo;
    }

    private function setObj($p_sql, Institucional $obj){
        $p_sql->bindValue(":texto", $obj->getTexto());
        $p_sql->bindValue(":titulo", $obj->getTitulo());
        $p_sql->bindValue(":imagem", $obj->getImagem());
        $p_sql->bindValue(":ativo", $obj->getAtivo());
        $p_sql->bindValue(":icone", $obj->getIcone());
        $p_sql->bindValue(":ordem", $obj->getOrdem());
        $p_sql->bindValue(":sub_titulo", $obj->getSub_titulo());
        $p_sql->bindValue(":destaque", $obj->getDestaque());
        $p_sql->bindValue(":linguagem_id", $obj->getLinguagem());
        $p_sql->bindValue(":link", $obj->getLink());
        $p_sql->bindValue(":link_logo", $obj->getLinkLogo());
        $p_sql->bindValue(":institucional_id", $obj->getId());

        return $p_sql;

    }

    public function statusTable($tabela) {

        $sql = "SHOW TABLE STATUS LIKE '".$tabela."'";
        $p_sql = Conexao::getInstance()->prepare($sql);
        $p_sql->execute();

        return $p_sql->fetch(\PDO::FETCH_ASSOC);
    }

}
