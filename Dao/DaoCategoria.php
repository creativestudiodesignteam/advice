<?php

namespace Dao;

use Dao\Classes\Categoria;

class DaoCategoria {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoCategoria();
        }
        return self::$instance;
    }

    public function Inserir(Categoria $obj) {
        try {
            $sql = "INSERT INTO categorias (categoria, ativo, linguagem_id, cod_categoria, email, img_modulo, img_adicional_um, img_adicional_dois, img_adicional_tres, background, titulo_img_1, titulo_img_2, titulo_img_3, titulo_img_4) 
                                       VALUES (:categoria, :ativo, :linguagem_id, :cod_categoria, :email, :img_modulo, :img_adicional_um, :img_adicional_dois, :img_adicional_tres, :background, :titulo_img_1, :titulo_img_2, :titulo_img_3, :titulo_img_4)";

            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $this->setObj($p_sql, $obj);
            
            $p_sql->execute();
            
            return Conexao::getInstance()->lastInsertId();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Editar(Categoria $obj) {
        try {
            $sql = "UPDATE categorias SET categoria = :categoria, ativo = :ativo, email = :email, img_modulo = :img_modulo, img_adicional_um = :img_adicional_um, img_adicional_dois = :img_adicional_dois, img_adicional_tres = :img_adicional_tres, 
            background = :background, titulo_img_1 = :titulo_img_1, titulo_img_2 = :titulo_img_2, titulo_img_3 = :titulo_img_3, titulo_img_4 = :titulo_img_4 "
                                   . " WHERE cod_categoria = :cod_categoria AND linguagem_id = :linguagem_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            
            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";            
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "UPDATE categorias SET ativo = :ativo WHERE cod_categoria = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", 'd');
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM categorias WHERE cod_categoria = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarPorCODLing($cod, $linguagem) {
        try {
            $sql = "SELECT * FROM categorias WHERE cod_categoria = :cod AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();     
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }
    
    public function BuscarTodos($linguagem, $ativo) {
        try {
            $sql = "SELECT * FROM categorias WHERE ativo <> :ativo AND ativo <> :ativo2 AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarTodosSidebar($linguagem, $ativo) {
        try {
            //$sql = "SELECT * FROM categorias WHERE ativo <> :ativo AND ativo <> :ativo2 AND linguagem_id = :linguagem_id";
            $sql = "SELECT categorias.cod_categoria, categorias.url_amigavel, categorias.categoria, categorias_sub.cod_categoria_sub as cod_sub, categorias_sub.subcategoria
            FROM categorias INNER JOIN categorias_sub ON categorias_sub.cod_categoria = categorias.cod_categoria 
            WHERE categorias.ativo <> :ativo AND categorias.ativo <> :ativo2 AND categorias.linguagem_id = :linguagem_id GROUP BY categorias.cod_categoria";
            
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $row;
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }    
   
    
    public function Total($linguagem, $ativo) {
        try {
            $sql = "SELECT * FROM categorias WHERE ativo <> :ativo AND ativo <> :ativo2 AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodosPaginacao($inicio, $limit, $linguagem, $ativo) {
        try {
            $sql = "SELECT * FROM categorias WHERE ativo <> :ativo AND ativo <> :ativo2 AND linguagem_id = :linguagem_id ORDER BY categoria LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage();
        }
    }

    public function TotalBuscaTitulo($categoria, $linguagem) {
        try {
            $sql = "SELECT * FROM categorias WHERE categoria LIKE :categoria WHERE linguagem_id = :linguagem_id ORDER BY categoria";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(':categoria', "%".$categoria."%", \PDO::PARAM_STR);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    
    private function populaObj($row) {
        $pojo = new Categoria();
        $pojo->setId($row['cod_categoria']);
        $pojo->setTitulo($row['categoria']);
        $pojo->setAtivo($row['ativo']);
        $pojo->setColor($row['color']);
        $pojo->setCss($row['css']);
        $pojo->setLinguagem($row['linguagem_id']);
        $pojo->setImagem($row['img_modulo']);
        $pojo->setEmail($row['email']);
        $pojo->setBackground($row['background']);
        $pojo->setImg_adicional_um($row['img_adicional_um']);
        $pojo->setImg_adicional_dois($row['img_adicional_dois']);
        $pojo->setImg_adicional_tres($row['img_adicional_tres']);
        $pojo->setTitulo_img_1($row['titulo_img_1']);
        $pojo->setTitulo_img_2($row['titulo_img_2']);
        $pojo->setTitulo_img_3($row['titulo_img_3']);
        $pojo->setTitulo_img_4($row['titulo_img_4']);
        $pojo->setUrl_amigavel($row['url_amigavel']);
        return $pojo;
    }
    
    private function setObj($p_sql, Categoria $obj) {
        
        $p_sql->bindValue(":categoria", $obj->getTitulo());
        $p_sql->bindValue(":ativo", $obj->getAtivo());
        $p_sql->bindValue(":linguagem_id", $obj->getLinguagem());
        $p_sql->bindValue(":email", $obj->getEmail());
        $p_sql->bindValue(":cod_categoria", $obj->getId());
        $p_sql->bindValue(":img_modulo", $obj->getImagem());
        $p_sql->bindValue(":background", $obj->getBackground());
        $p_sql->bindValue(":img_adicional_um", $obj->getImg_adicional_um());
        $p_sql->bindValue(":img_adicional_dois", $obj->getImg_adicional_dois());
        $p_sql->bindValue(":img_adicional_tres", $obj->getImg_adicional_tres());
        $p_sql->bindValue(":titulo_img_1", $obj->getTitulo_img_1());
        $p_sql->bindValue(":titulo_img_2", $obj->getTitulo_img_2());
        $p_sql->bindValue(":titulo_img_3", $obj->getTitulo_img_3());
        $p_sql->bindValue(":titulo_img_4", $obj->getTitulo_img_4());
            
        return $p_sql;     
    }
    
    public function statusTable($tabela) {
        
        $sql = "SHOW TABLE STATUS LIKE '".$tabela."'";
        $p_sql = Conexao::getInstance()->prepare($sql);
        $p_sql->execute();
        
        return $p_sql->fetch(\PDO::FETCH_ASSOC);     
    }

}

