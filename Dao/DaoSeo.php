<?php

namespace Dao;

use Dao\Classes\Seo;

class DaoSeo {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoSeo();
        }
        return self::$instance;
    }

    public function Inserir(Seo $obj) {
        try {
            $sql = "INSERT INTO tbl_seo (keywords, meta_descricao) 
                                      VALUES (:keywords, :meta_descricao)";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);


            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Editar(Seo $obj) {
        try {
            $sql = "UPDATE tbl_seo SET keywords = :keywords, meta_descricao = :meta_descricao"
                                   . " WHERE seo_id = :seo_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            $p_sql->bindValue(":seo_id", $obj->getId());

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "DELETE FROM tbl_seo WHERE seo_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_seo WHERE seo_id = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            return $this->populaSeo($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    
    private function populaSeo($row) {
        $pojo = new Seo();
        $pojo->setId($row['seo_id']);
        $pojo->setKeywords($row['keywords']);
        $pojo->setMetaDescricao($row['meta_descricao']);
        return $pojo;
    }
    
    private function setObj($p_sql, Seo $obj){
        $p_sql->bindValue(":keywords", $obj->getKeywords());
        $p_sql->bindValue(":meta_descricao", $obj->getMetaDescricao());
        
        return $p_sql;
    }
    

}

