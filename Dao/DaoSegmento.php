<?php

namespace Dao;

use Dao\Classes\Segmento;

class DaoSegmento {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoSegmento();
        }
        return self::$instance;
    }

    public function Inserir(Segmento $obj) {
        try {
            $sql = "INSERT INTO tbl_segmento (segmento, ativo, linguagem_id, background, cod_segmento, cod_categoria) 
                                       VALUES (:segmento, :ativo, :linguagem_id, :background, :cod_segmento, :cod_categoria)";

            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $this->setObj($p_sql, $obj);
            
            $p_sql->execute();
            
            return Conexao::getInstance()->lastInsertId();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Editar(Segmento $obj) {
        try {
            $sql = "UPDATE tbl_segmento SET segmento = :segmento, ativo = :ativo, background = :background, cod_categoria = :cod_categoria"
                                   . " WHERE cod_segmento = :cod_segmento AND linguagem_id = :linguagem_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            
            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";            
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "UPDATE tbl_segmento SET ativo = :ativo WHERE cod_segmento = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", 'd');
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }


    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_segmento WHERE cod_segmento = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarPorCODLing($cod, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_segmento WHERE cod_segmento = :cod AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();     
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }
    
    public function BuscarTodos($linguagem, $ativo) {
        try {
            $sql = "SELECT * FROM tbl_segmento WHERE ativo <> :ativo AND ativo <> :ativo2 AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarTodosCategoria($linguagem, $cat) {
        try {
            $sql = "SELECT * FROM tbl_segmento 
            WHERE ativo = :ativo 
            AND cod_categoria = :cod_categoria 
            AND linguagem_id = :linguagem_id
            ORDER BY segmento";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "s");
            $p_sql->bindValue(":cod_categoria", $cat);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarTodosCategoriaPaginacao($linguagem, $cat, $inicio, $limit) {
        try {
            $sql = "SELECT * FROM tbl_segmento 
            WHERE ativo = :ativo 
            AND cod_categoria = :cod_categoria 
            AND linguagem_id = :linguagem_id
            ORDER BY segmento
            LIMIT :inicio, :limit
            ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "s");
            $p_sql->bindValue(":cod_categoria", $cat);
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarCategoriaXSegmento($linguagem, $cod_produto) {
        try {
            $sql = "SELECT tbl_segmento.* FROM tbl_segmento
            INNER JOIN segmentoxproduto ON segmentoxproduto.cod_segmento = tbl_segmento.cod_segmento
            WHERE segmentoxproduto.cod_produto = :cod_produto AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":cod_produto", $cod_produto);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function Total($linguagem, $ativo) {
        try {
            $sql = "SELECT * FROM tbl_segmento WHERE ativo <> :ativo AND ativo <> :ativo2 AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodosPaginacao($inicio, $limit, $linguagem, $ativo) {
        try {
            $sql = "SELECT * FROM tbl_segmento WHERE ativo <> :ativo AND ativo <> :ativo2 AND linguagem_id = :linguagem_id ORDER BY categoria LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage();
        }
    }

    public function TotalBuscaTitulo($categoria, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_segmento WHERE categoria LIKE :categoria WHERE linguagem_id = :linguagem_id ORDER BY categoria";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(':categoria', "%".$categoria."%", \PDO::PARAM_STR);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    
    private function populaObj($row) {
        $pojo = new Segmento();
        $pojo->setId($row['cod_segmento']);
        $pojo->setTitulo($row['segmento']);
        $pojo->setAtivo($row['ativo']);
        $pojo->setCategoria($row['cod_categoria']);
        $pojo->setImagem($row['background']);
        $pojo->setLinguagem($row['linguagem_id']);
        return $pojo;
    }
    
    private function setObj($p_sql, Segmento $obj) {
        
        $p_sql->bindValue(":segmento", $obj->getTitulo());
        $p_sql->bindValue(":ativo", $obj->getAtivo());
        $p_sql->bindValue(":cod_categoria", $obj->getCategoria());
        $p_sql->bindValue(":background", $obj->getImagem());
        $p_sql->bindValue(":linguagem_id", $obj->getLinguagem());
        $p_sql->bindValue(":cod_segmento", $obj->getId());
            
        return $p_sql;     
    }
    
    public function statusTable($tabela) {
        
        $sql = "SHOW TABLE STATUS LIKE '".$tabela."'";
        $p_sql = Conexao::getInstance()->prepare($sql);
        $p_sql->execute();
        
        return $p_sql->fetch(\PDO::FETCH_ASSOC);     
    }

}

