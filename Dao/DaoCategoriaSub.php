<?php

namespace Dao;

use Dao\Classes\CategoriaSub;

class DaoCategoriaSub {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoCategoriaSub();
        }
        return self::$instance;
    }

    public function Inserir(CategoriaSub $obj) {
        try {
            $sql = "INSERT INTO categorias_sub (subcategoria, ativo, linguagem_id, cod_categoria_sub, email, img_modulo, img_adicional_um, img_adicional_dois, img_adicional_tres, background, titulo_img_1, titulo_img_2, titulo_img_3, titulo_img_4) 
                                       VALUES (:subcategoria, :ativo, :linguagem_id, :cod_categoria_sub, :email, :img_modulo, :img_adicional_um, :img_adicional_dois, :img_adicional_tres, :background, :titulo_img_1, :titulo_img_2, :titulo_img_3, :titulo_img_4)";

            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $this->setObj($p_sql, $obj);
            
            $p_sql->execute();
            
            return Conexao::getInstance()->lastInsertId();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function Editar(CategoriaSub $obj) {
        try {
            $sql = "UPDATE categorias_sub SET subcategoria = :subcategoria, ativo = :ativo, email = :email, img_modulo = :img_modulo, img_adicional_um = :img_adicional_um, img_adicional_dois = :img_adicional_dois, img_adicional_tres = :img_adicional_tres, 
            background = :background, titulo_img_1 = :titulo_img_1, titulo_img_2 = :titulo_img_2, titulo_img_3 = :titulo_img_3, titulo_img_4 = :titulo_img_4 "
                                   . " WHERE cod_categoria_sub = :cod_categoria_sub AND linguagem_id = :linguagem_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            
            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";            
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "UPDATE categorias_sub SET ativo = :ativo WHERE cod_categoria_sub = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", 'd');
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM categorias_sub WHERE cod_categoria_sub = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarPorCODLing($cod, $linguagem) {
        try {
            $sql = "SELECT * FROM categorias_sub WHERE cod_categoria_sub = :cod AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();     
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }

    public function BuscarPorCODCatLing($cod, $linguagem) {
        try {
            $sql = "SELECT * FROM categorias_sub WHERE cod_categoria = :cod AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();     
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }    
    
    public function BuscarTodos($linguagem, $ativo) {
        try {
            $sql = "SELECT * FROM categorias_sub WHERE ativo <> :ativo AND ativo <> :ativo2 AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarTodosCat($linguagem, $ativo, $cat) {
        try {
            $sql = "SELECT * FROM categorias_sub WHERE ativo <> :ativo AND ativo <> :ativo2 AND linguagem_id = :linguagem_id AND cod_categoria = :cod_categoria";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->bindValue(":cod_categoria", $cat);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarPrimeiroCat($linguagem, $ativo, $cod) {
        try {
            $sql = "SELECT cod_categoria_sub FROM categorias_sub WHERE ativo <> :ativo AND ativo <> :ativo2 AND linguagem_id = :linguagem_id AND cod_categoria = :cod_categoria LIMIT 1";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->bindValue(":cod_categoria", $cod);
            $p_sql->execute();
            $row = $p_sql->fetch();
            return $row;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }     
    
    public function Total($linguagem, $ativo) {
        try {
            $sql = "SELECT * FROM categorias_sub WHERE ativo <> :ativo AND ativo <> :ativo2 AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodosPaginacao($inicio, $limit, $linguagem, $ativo) {
        try {
            $sql = "SELECT * FROM categorias_sub WHERE ativo <> :ativo AND ativo <> :ativo2 AND linguagem_id = :linguagem_id ORDER BY subcategoria LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage();
        }
    }

    public function TotalBuscaTitulo($subcategoria, $linguagem) {
        try {
            $sql = "SELECT * FROM categorias_sub WHERE subcategoria LIKE :subcategoria WHERE linguagem_id = :linguagem_id ORDER BY subcategoria";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(':subcategoria', "%".$subcategoria."%", \PDO::PARAM_STR);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    
    private function populaObj($row) {
        $pojo = new CategoriaSub();
        $pojo->setId($row['cod_categoria_sub']);
        $pojo->setLinguagem($row['linguagem_id']);
        $pojo->setCod_categoria($row['cod_categoria']);
        $pojo->setTitulo($row['subcategoria']);
        $pojo->setOrdem($row['ordem']);
        $pojo->setAtivo($row['ativo']);        
        $pojo->setCss($row['css']);
        $pojo->setColor($row['color']);
        $pojo->setImagem($row['img']);
        $pojo->setImg2($row['img_2']);
        $pojo->setDoc($row['doc']);
        return $pojo;
    }
    
    private function setObj($p_sql, CategoriaSub $obj) {
        
        $p_sql->bindValue(":cod_categoria_sub", $obj->getId());
        $p_sql->bindValue(":linguagem_id", $obj->getLinguagem());
        $p_sql->bindValue(":cod_categoria", $obj->getCod_categoria());
        $p_sql->bindValue(":subcategoria", $obj->getTitulo());
        $p_sql->bindValue(":ordem", $obj->getOrdem());
        $p_sql->bindValue(":ativo", $obj->getAtivo());
        $p_sql->bindValue(":css", $obj->getCss());
        $p_sql->bindValue(":color", $obj->getColor());
        $p_sql->bindValue(":img", $obj->getImagem());
        $p_sql->bindValue(":img_2", $obj->getImg2());
        $p_sql->bindValue(":doc", $obj->getDoc());
            
        return $p_sql;     
    }
    
    public function statusTable($tabela) {
        
        $sql = "SHOW TABLE STATUS LIKE '".$tabela."'";
        $p_sql = Conexao::getInstance()->prepare($sql);
        $p_sql->execute();
        
        return $p_sql->fetch(\PDO::FETCH_ASSOC);     
    }

}

