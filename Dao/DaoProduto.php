<?php

namespace Dao;

use Dao\Classes\Produto;

class DaoProduto {

    public static $instance;

    

    public static function getInstance() {
        if (!isset(self::$instance)){
            self::$instance = new DaoProduto();
        }
        return self::$instance;
    }

    public function Inserir(Produto $obj) {
        try {
            $sql = "INSERT INTO tbl_produtos (descricao, ativo, linguagem_id, cod_produto, farmacopeias) 
                                       VALUES (:descricao, :ativo, :linguagem_id, :cod_produto, :farmacopeias)";

            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $this->setObj($p_sql, $obj);
            
            $p_sql->execute();
            
            return Conexao::getInstance()->lastInsertId();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function InserirProdutoSegmento($cod, $segmento) {
        try {
            $sql = "INSERT INTO segmentoxproduto (cod_produto, cod_segmento)
            VALUES (:cod_produto, :cod_segmento);";

            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $p_sql->bindValue(":cod_produto", $cod);
            $p_sql->bindValue(":cod_segmento", $segmento);
            $p_sql->execute();
            
            return Conexao::getInstance()->lastInsertId();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function InserirProdutoCategoria_prod($cod, $subcategoria, $categoria) {
        try {
            $sql = "INSERT INTO categoria_prodxproduto (cod_produto, cod_categoria_prod, cod_categoria)
            VALUES (:cod_produto, :cod_categoria_prod, :cod_categoria);";

            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $p_sql->bindValue(":cod_produto", $cod);
            $p_sql->bindValue(":cod_categoria_prod", $subcategoria);
            $p_sql->bindValue(":cod_categoria", $categoria);
            $p_sql->execute();
            
            return Conexao::getInstance()->lastInsertId();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function InserirProdutoAplicacao($cod, $aplicacao, $subcategoria) {
        try {
            $sql = "INSERT INTO aplicacaoxproduto (cod_produto, cod_aplicacao, categoria_prod)
            VALUES (:cod_produto, :cod_aplicacao, :cod_categoria_prod);";

            $p_sql = Conexao::getInstance()->prepare($sql);
            
            $p_sql->bindValue(":cod_produto", $cod);
            $p_sql->bindValue(":cod_aplicacao", $aplicacao);
            $p_sql->bindValue(":cod_categoria_prod", $subcategoria);
            $p_sql->execute();
            
            return Conexao::getInstance()->lastInsertId();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }

    public function Editar(Produto $obj) {
        try {
            $sql = "UPDATE tbl_produtos SET descricao = :descricao, ativo = :ativo, farmacopeias = :farmacopeias "
                                   . " WHERE cod_produto = :cod_produto AND linguagem_id = :linguagem_id";

            $p_sql = Conexao::getInstance()->prepare($sql);

            $this->setObj($p_sql, $obj);
            
            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";            
        }
    }

    public function Deletar($cod) {
        try {
            $sql = "UPDATE tbl_produtos SET ativo = :ativo WHERE cod_produto = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":ativo", 'd');
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function DeletarProdutoSegmento($cod) {
        try {
            $sql = "DELETE FROM segmentoxproduto WHERE cod_produto = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }

    public function DeletarProdutoCategoria_prod($cod) {
        try {
            $sql = "DELETE FROM categoria_prodxproduto WHERE cod_produto = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }

    public function DeletarProdutoAplicacao($cod) {
        try {
            $sql = "DELETE FROM aplicacaoxproduto WHERE cod_produto = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);

            return $p_sql->execute();
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }

    public function BuscarPorCOD($cod) {
        try {
            $sql = "SELECT * FROM tbl_produtos WHERE cod_produto = :cod";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarPorCODLing($cod, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_produtos WHERE cod_produto = :cod AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":cod", $cod);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->execute();     
            return $this->populaObj($p_sql->fetch(\PDO::FETCH_ASSOC));
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde. ".$e->getMessage();
        }
    }
    
    public function BuscarTodos($linguagem, $ativo) {
        try {
            $sql = "SELECT * FROM tbl_produtos WHERE ativo <> :ativo AND ativo <> :ativo2 AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarTodosTituloPaginacao($linguagem, $titulo) {
        try {
            $sql = "SELECT * FROM tbl_produtos WHERE descricao LIKE :descricao AND ativo = 's' AND linguagem_id = :linguagem_id ORDER BY descricao ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(':descricao', "%".$titulo."%", \PDO::PARAM_STR);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarTodosSegmento($linguagem, $segmento) {
        try {
            $sql = "SELECT tbl_produtos.* FROM tbl_produtos 
            INNER JOIN segmentoxproduto
            ON segmentoxproduto.cod_produto = tbl_produtos.cod_produto 
            WHERE ativo = :ativo AND cod_segmento = :cod_segmento AND linguagem_id = :linguagem_id ORDER BY tbl_produtos.descricao";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "s");
            $p_sql->bindValue(":cod_segmento", $segmento);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }

    public function BuscarTodosSegmentoPaginacao($linguagem, $segmento, $inicio, $limit) {
        try {
            $sql = "SELECT tbl_produtos.* FROM tbl_produtos 
            INNER JOIN segmentoxproduto
            ON segmentoxproduto.cod_produto = tbl_produtos.cod_produto 
            WHERE ativo = :ativo AND cod_segmento = :cod_segmento AND linguagem_id = :linguagem_id 
            LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "s");
            $p_sql->bindValue(":cod_segmento", $segmento);
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function Total($linguagem, $ativo) {
        try {
            $sql = "SELECT * FROM tbl_produtos WHERE ativo <> :ativo AND ativo <> :ativo2 AND linguagem_id = :linguagem_id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    public function BuscarTodosPaginacao($inicio, $limit, $linguagem, $ativo) {
        try {
            $sql = "SELECT * FROM tbl_produtos WHERE ativo <> :ativo AND ativo <> :ativo2 AND linguagem_id = :linguagem_id ORDER BY produto LIMIT :inicio, :limit";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(":ativo", "d");
            $p_sql->bindValue(":ativo2", $ativo);
            $p_sql->bindParam(':inicio', $inicio, \PDO::PARAM_INT); 
            $p_sql->bindParam(':limit', $limit, \PDO::PARAM_INT);
            $p_sql->execute();
            $objs = [];       
            while ($row = $p_sql->fetch(\PDO::FETCH_ASSOC)){
                $objs[] = $this->populaObj($row);
            }
                return $objs;
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.".$e->getMessage();
        }
    }

    public function TotalBuscaTitulo($produto, $linguagem) {
        try {
            $sql = "SELECT * FROM tbl_produtos WHERE produto LIKE :produto WHERE linguagem_id = :linguagem_id ORDER BY produto";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(":linguagem_id", $linguagem);
            $p_sql->bindValue(':produto', "%".$produto."%", \PDO::PARAM_STR);
            $p_sql->execute();
            
            return $p_sql->rowCount();
            
        } catch (Exception $e) {
            print "error-Ocorreu um erro ao tentar executar esta ação, tente novamente mais tarde.";
        }
    }
    
    
    private function populaObj($row) {
        $pojo = new Produto();
        $pojo->setId($row['cod_produto']);
        $pojo->setTitulo($row['descricao']);
        $pojo->setAtivo($row['ativo']);
        $pojo->setLinguagem($row['linguagem_id']);
        $pojo->setFarmacopeias($row['farmacopeias']);
        return $pojo;
    }
    
    private function setObj($p_sql, Produto $obj) {
        
        $p_sql->bindValue(":descricao", $obj->getTitulo());
        $p_sql->bindValue(":ativo", $obj->getAtivo());
        $p_sql->bindValue(":linguagem_id", $obj->getLinguagem());
        $p_sql->bindValue(":farmacopeias", $obj->getFarmacopeias());
        $p_sql->bindValue(":cod_produto", $obj->getId());
            
        return $p_sql;     
    }
    
    public function statusTable($tabela) {
        
        $sql = "SHOW TABLE STATUS LIKE '".$tabela."'";
        $p_sql = Conexao::getInstance()->prepare($sql);
        $p_sql->execute();
        
        return $p_sql->fetch(\PDO::FETCH_ASSOC);     
    }

}

