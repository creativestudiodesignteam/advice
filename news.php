<!DOutYPE html>
<html lang="en">

<head>
	<!-- Meta Data -->
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta name="description" content=""  />
	<meta name="keywords" content=""  />
	<title>Notícias — Advice System</title>
    <?php include('includes/head.php');?>
</head>

<body id="home-version-1" class="home-version-1" data-style="default">

	<a href="#main_content" data-type="section-switch" class="return-to-top">
		<i class="fa fa-chevron-up"></i>
	</a>

	<!--=========================-->
	<!--=        Loader         =-->
	<!--=========================-->
	<?php include('includes/loader.php');?>
	<!-- /.page-loader -->

	<div id="main_content">


		<!--=========================-->
		<!--=        Navbar         =-->
		<!--=========================-->
		<?php include('includes/header-interna.php');?>
		<!-- /.site-header -->

		<!--==========================-->
		<!--=         Banner         =-->
		<!--==========================-->
		<section class="page-banner">
			<div class="container">
				<div class="page-title-wrapper">
					<h1 class="page-title">Notícias</h1>

					<ul class="bradcurmed">
						<li><a href="index.php" rel="noopener noreferrer">Home</a></li>
						<li>Notícias</li>
					</ul>
				</div>
				<!-- /.page-title-wrapper -->
			</div>
			<!-- /.container -->


			<svg class="circle" data-parallax='{"x" : -200}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
		<path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
	</svg>

			<ul class="animate-ball">
				<li class="ball"></li>
				<li class="ball"></li>
				<li class="ball"></li>
				<li class="ball"></li>
				<li class="ball"></li>
			</ul>
		</section>
		<!-- /.page-banner -->

		<!--========================-->
		<!--=         Blog         =-->
		<!--========================-->
		<div class="blog-post-archive">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<div class="post-wrapper">
							<article class="post">
								<div class="feature-image">
									<a href="blog-signle.html">
                                    <img src="media/news/tecnologia-advice-system.jpg" alt="">
                                </a>
								</div>
								<div class="blog-content">
									<ul class="post-meta">
										<li><a href="#">Out 16, 2019</a></li>
										<li><a href="#">4 Comentários</a></li>
									</ul>

									<h3 class="entry-title"><a href="#">Advice POS solução para facilitar a gestão acadêmica e pedagógica.</a></h3>

									<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
									Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
									Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
									Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
									</p>

									<a href="#" class="read-more">Leia Mais <i class="ei ei-arrow_right"></i></a>

									<div class="author">
										<img src="media/news/author-advice.jpg" alt="author">
										<a href="#" class="author-link">Advice System</a>
									</div>

								</div>
								<!-- /.post-content -->
							</article>
							<!-- /.post -->

							<article class="post video-post">
								<div class="feature-image">
									<a href="blog-signle.html">
                                    <img src="media/news/reportagem-advice-system.jpg" alt="">
                                </a>

									<a href="https://www.youtube.com/watch?v=3CNEW07eFRA" class="video-btn popup-video">
                                    <i class="ei ei-arrow_triangle-right"></i>
                                </a>
								</div>
								<div class="blog-content">
									<ul class="post-meta">
										<li><a href="#">Out 16, 2019</a></li>
										<li><a href="#">4 Comentários</a></li>
									</ul>

									<h3 class="entry-title"><a href="#">Advice System, participa de reportagem sobre empreendedorismo.</a></h3>

									<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
									Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
									Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
									Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
									</p>

									<a href="#" class="read-more">Leia Mais <i class="ei ei-arrow_right"></i></a>

									<div class="author">
										<img src="media/news/author-advice.jpg" alt="author">
										<a href="#" class="author-link">Advice System</a>
									</div>

								</div>
								<!-- /.post-content -->
							</article>
							<!-- /.post -->

							<article class="post quote-post">

								<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
									Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
									Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
									Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
									</p>

								<span class="quote-author">Luiz Antonio</span>

								<span class="quote"><img src="media/blog/quote.png" alt=""></span>

							</article>
							<!-- /.post -->

							<article class="post">
								<div class="feature-image">
									<a href="blog-signle.html">
                                    <img src="media/news/tecnologia-na-escola.jpg" alt="">
                                </a>
								</div>
								<div class="blog-content">
									<ul class="post-meta">
										<li><a href="#">Out 16, 2019</a></li>
										<li><a href="#">4 Comentários</a></li>
									</ul>

									<h3 class="entry-title"><a href="#">Porque os alunos precisam de tecnologia nas escolas.</a></h3>

									<p>
									Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
									Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
									Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
									Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
									</p>

									<a href="#" class="read-more">Leia Mais <i class="ei ei-arrow_right"></i></a>

									<div class="author">
										<img src="media/news/author-advice.jpg" alt="author">
										<a href="#" class="author-link">Advice System</a>
									</div>

								</div>
								<!-- /.post-content -->
							</article>
							<!-- /.post -->


							<article class="post link-post">
								<div class="blog-content">
									<p>
										<a href="#">The little rotter spiffing good time lemon squeezy smashing excuse my french old.!</a>
									</p>

								</div>
								<!-- /.post-content -->
							</article>
							<!-- /.post -->


							<article class="post">
								<div class="feature-image">
									<a href="blog-signle.html">
                                    <img src="media/news/educacao-no-brasil.jpg" alt="">
                                </a>
								</div>
								<div class="blog-content">
									<ul class="post-meta">
										<li><a href="#">Out 16, 2019</a></li>
										<li><a href="#">4 Comentários</a></li>
									</ul>

									<h3 class="entry-title"><a href="#">Participação dos pais na vida escolhar melhora desempenho dos filhos.</a></h3>

									<p>
										The little rotter spiffing good time lemon squeezy smashing excuse my French old, cheesed off give us a bell happy days brown bread, blow off Harry barney bobby. Cup of char gormless hors.!
									</p>

									<a href="#" class="read-more">Leia Mais <i class="ei ei-arrow_right"></i></a>

									<div class="author">
										<img src="media/news/author-advice.jpg" alt="author">
										<a href="#" class="author-link">Advice System</a>
									</div>

								</div>
								<!-- /.post-content -->
							</article>
							<!-- /.post -->


							<ul class="post-navigation">
								<li class="active">1</li>
								<li><a href="#">2</a></li>
								<li class="next"><a href="#"><i class="ei ei-arrow_carrot-right"></i></a></li>
							</ul>

						</div>
						<!-- /.post-wrapper -->
					</div>
					<!-- /.col-lg-8 -->

					<div class="col-lg-4">
						<div class="sidebar">
							<div id="search" class="widget widget_search">
								<form role="search" class="search-form-widget">
									<label>
			<input type="search" class="search-field" placeholder="Buscar...">
		</label>
									<button type="submit" class="search-submit">
			<i class="ei ei-icon_search"></i>
		</button>
								</form>
							</div>
							<div id="categories" class="widget widget_categories">
								<h2 class="widget-title">Categorias</h2>
								<ul>
									<li>
										<a href="#">Advice System <span class="count">(23)</span></a>
									</li>
									<li>
										<a href="#">No Brasil  <span class="count">(17)</span></a>
									</li>
									<li>
										<a href="#">No Mundo  <span class="count">(12)</span></a>
									</li>
									<li>
										<a href="#">Feiras e Negócios <span class="count">(10)</span></a>
									</li>
									<li>
										<a href="#">Tecnologia  <span class="count">(4)</span></a>
									</li>
									<li>
										<a href="#">Educação   <span class="count">(9)</span></a>
									</li>

									<li>
										<a href="#">Administração  <span class="count">(8)</span></a>
									</li>

								</ul>
							</div>
							<div id="gp-posts-widget-2" class="widget gp-posts-widget">
								<h2 class="widget-title">Últimas Notícias</h2>
								<div class="gp-posts-widget-wrapper">
									<div class="post-item">
										<div class="post-widget-thumbnail">
											<a href="#">
					<img src="media/news/news1.jpg" alt="thumb">
				</a>
										</div>
										<div class="post-widget-info">
											<h5 class="post-widget-title">
												<a href="#" title="This Is Test Post">Tecnologia e controle na palma de sua mão.</a>
											</h5>
											<span class="post-date">Out 04, 2019</span>

										</div>
									</div>

									<div class="post-item">
										<div class="post-widget-thumbnail">
											<a href="#">
					<img src="media/news/news2.jpg" alt="thumb">
				</a>
										</div>
										<div class="post-widget-info">
											<h5 class="post-widget-title">
												<a href="#" title="This Is Test Post">Segurança e sigilo de dados online. </a>
											</h5>
											<span class="post-date">Out 01, 2019</span>
										</div>
									</div>

									<div class="post-item">
										<div class="post-widget-thumbnail">
											<a href="#">
					<img src="media/news/news3.jpg" alt="thumb">
				</a>
										</div>
										<div class="post-widget-info">
											<h5 class="post-widget-title">
												<a href="#" title="This Is Test Post">Valorização do professor em algumas cidades, impactam alunos. </a>
											</h5>
											<span class="post-date">Set 27, 2019</span>

										</div>
									</div>
									<div class="post-item">
										<div class="post-widget-thumbnail">
											<a href="#">
					<img src="media/news/news4.jpg" alt="thumb">
				</a>
										</div>
										<div class="post-widget-info">
											<h5 class="post-widget-title">
												<a href="#" title="This Is Test Post">Otimização de processos e maximização de resultados. </a>
											</h5>
											<span class="post-date">Set 24 , 2019</span>

										</div>
									</div>
									<div class="post-item">
										<div class="post-widget-thumbnail">
											<a href="#">
					<img src="media/news/news5.jpg" alt="thumb">
				</a>
										</div>
										<div class="post-widget-info">
											<h5 class="post-widget-title">
												<a href="#" title="This Is Test Post">Capacitação de equipe geram resultados a curto prazo.</a>
											</h5>
											<span class="post-date">Set 24 , 2019</span>

										</div>
									</div>
								
								</div>
							</div>
							<div id="widget_recent_Comentários" class="widget widget_recent_Comentários">
								<h2 class="widget-title">Comentários Recentes</h2>
								<div class="recent-Comentários">
									<div class="comment-list">
										<div class="icon">
											<i class="ei ei-icon_chat_alt"></i>
										</div>

										<div class="comment-content">
											<h3>Bailey Wonger <span>on</span></h3>
											<p>
												<a href="#">
						My lady mush hanky panky young delinquent.!
					</a>
											</p>
										</div>
									</div>
									<!-- /.comment-list -->

									<div class="comment-list">
										<div class="icon">
											<i class="ei ei-icon_chat_alt"></i>
										</div>

										<div class="comment-content">
											<h3>Advice System <span>on</span></h3>
											<p>
												<a href="#">
						Why I say old chap that is <br>spiffing daft, blow.!
					</a>
											</p>
										</div>
									</div>
									<!-- /.comment-list -->

									<div class="comment-list">
										<div class="icon">
											<i class="ei ei-icon_chat_alt"></i>
										</div>

										<div class="comment-content">
											<h3>Justin Case <span>on</span></h3>
											<p>
												<a href="#">
						Tomfoolery hanky panky<br> geeza I my good.!
					</a>
											</p>
										</div>
									</div>
									<!-- /.comment-list -->

								</div>
								<!-- /.recent-Comentários -->
							</div>
							<aside id="tags" class="widget widget_tag">
								<h3 class="widget-title">Tags Populares</h3>
								<div class="tagcloud">
									<a href="#">Sistemas</a>
									<a href="#">Educação</a>
									<a href="#">Administração</a>
									<a href="#">No Brasil</a>
									<a href="#">Financeiro</a>
									<a href="#">Marketing</a>
									<a href="#">Clientes</a>
									<a href="#">Suporte</a>
								</div>
							</aside>
							<!-- /.widget -->
						</div>
						<!-- /.sidebar -->
					</div>
					<!-- /.col-lg-4 -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		</div>
		<!-- /.blog-post-archive -->


		<!--=========================-->
		<!--=        Footer         =-->
		<!--=========================-->
		<?php include('includes/footer.php')?>
		<!-- /#footer -->


	</div>
	<!-- /#site -->
    <?php include('includes/scripts.php')?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js"></script>
    <script>
    $(function() {
	    $('.item-noticia').matchHeight({
                byRow: true,
                property: 'height',
                target: null,
                remove: false
            });
        });
</body>

</html>