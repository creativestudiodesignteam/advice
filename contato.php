<!DOCTYPE html>
<html lang="pt-br">

<head>
	<!-- Meta Data -->
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta name="description" content=""  />
	<meta name="keywords" content=""  />
	<title>Contato — Advice System</title>
    <?php include('includes/head.php');?>
</head>

<body id="home-version-1" class="home-version-1" data-style="default">

	<a href="#main_content" data-type="section-switch" class="return-to-top">
		<i class="fa fa-chevron-up"></i>
	</a>

	<!--=========================-->
	<!--=        Loader         =-->
	<!--=========================-->
	<?php include('includes/loader.php');?>
	<!-- /.page-loader -->

	<div id="main_content">


		<!--=========================-->
		<!--=        Navbar         =-->
		<!--=========================-->
		<?php include('includes/header-interna.php');?>
		<!-- /.site-header -->

		<!--==========================-->
		<!--=         Banner         =-->
		<!--==========================-->
		<section class="page-banner-contact">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<div class="page-title-wrapper">
							<div class="page-title-inner">
								<h1 class="page-title">Envie sua mensagem</h1>

								<p>
									Seu contato é muito importante para nós.<br> Retornaremos muito em breve.
								</p>
							</div>
							<!-- /.page-title-inner -->
						</div>
						<!-- /.page-title-wrapper -->
					</div>
					<!-- /.col-lg-8 -->

					<div class="col-lg-4">
						<div class="animate-element-contact">
							<img src="media/animated/001.png" alt="" class="wow pixFadeDown" data-wow-duration="1s">
							<img src="media/animated/002.png" alt="" class="wow pixFadeUp" data-wow-duration="2s">
							<img src="media/animated/003.png" alt="" class="wow pixFadeLeft" data-wow-delay="0.3s" data-wow-duration="2s">
							<img src="media/animated/004.png" alt="man" class="wow pixFadeUp" data-wow-duration="2s">
						</div>
						<!-- /.animate-element-contact -->
					</div>
					<!-- /.col-lg-4 -->
				</div>
				<!-- /.row -->

			</div>
			<!-- /.container -->

			<svg class="circle" data-parallax='{"y" : 250}' xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="950px" height="950px">
		<path fill-rule="evenodd" stroke="rgb(250, 112, 112)" stroke-width="100px" stroke-linecap="butt" stroke-linejoin="miter" opacity="0.051" fill="none" d="M450.000,50.000 C670.914,50.000 850.000,229.086 850.000,450.000 C850.000,670.914 670.914,850.000 450.000,850.000 C229.086,850.000 50.000,670.914 50.000,450.000 C50.000,229.086 229.086,50.000 450.000,50.000 Z" />
	</svg>

			<ul class="animate-ball">
				<li class="ball"></li>
				<li class="ball"></li>
				<li class="ball"></li>
				<li class="ball"></li>
				<li class="ball"></li>
			</ul>
		</section>
		<!-- /.page-banner -->

		<!--===========================-->
		<!--=         Contact         =-->
		<!--===========================-->
		<section class="contactus">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="contact-infos">
							<div class="contact-info">
								<h3 class="title">Atendimento</h3>
								<p class="description">
									Segunda à quinta das 08hs às 18h <br>
									Sexta das 08h às 17
								</p>
							</div>
							<!-- /.contact-info -->

							<div class="contact-info">
								<h3 class="title">Fale Conosco</h3>
                                <div class="info phone">
									<i class="ei ei-icon_phone"></i>
									<span>(11) 3513-5075 | (11) 93513-5075</span>
								</div><br>
								<div class="info e-mail">
									<i class="ei ei-icon_mail_alt"></i>
									<span>comercial@advicesystem.com.br</span>
								</div><br>
                                <div class="info address">
									<i class="fas fa-map-marker-alt"></i>
									<span>Rua Voluntário da Pátria, 1092 - Santana<br> São Paulo - SP - CEP 02010-100</span>
								</div>
							</div>
							<!-- /.contact-info -->
						</div>
						<!-- /.contact-infos -->
					</div>
					<!-- /.col-md-4 -->
					<div class="col-md-8">
						<div class="contact-froms">
							<form action="php/mailer.php" class="contact-form" data-pixsaas="contact-froms">
								<div class="row">
									<div class="col-md-6">
										<input type="text" name="name" placeholder="Nome" required>
									</div>

									<div class="col-md-6">
										<input type="text" name="email" placeholder="Email" required>
									</div>
								</div>
                                <div class="row">
									<div class="col-md-6">
										<input type="text" name="empresa" placeholder="Empresa" required>
									</div>

									<div class="col-md-6">
										<input type="text" name="Phone" placeholder="Telefone" required>
									</div>
								</div>
								<textarea name="content" placeholder="Seu Comentário" required></textarea>

								<button type="submit" class="pix-btn submit-btn">Enviar Mensagem</button>

								<div class="form-result alert">
									<div class="content"></div>
								</div>
							</form>
							<!-- /.faq-form -->
						</div>
						<!-- /.faq-froms -->
					</div>
					<!-- /.col-md-8 -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->
		</section>
		<!-- /.contactus -->

		<!--========================-->
		<!--=         Map         =-->
		<!--========================-->
		<section id="google-maps">
			<div class="google-map">
				<div class="gmap3-area" data-lat="-23.5111692" data-lng="-46.639732" data-mrkr="assets/img/map-marker.png">

				</div>
			</div>
			<!-- /.google-map -->
		</section>
		<!-- /#google-maps -->

		<!--=========================-->
		<!--=        Footer         =-->
		<!--=========================-->
		<?php include('includes/footer.php')?>
		<!-- /#footer -->


	</div>
	<!-- /#site -->
    <?php include('includes/scripts.php')?>

</body>

</html>